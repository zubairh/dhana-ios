//
//  Constants.swift


import UIKit
import Foundation

/**********************  Size Constants  ************************/

let DEVICE_VERSION = UIDevice .current.systemVersion
let SCREEN_WIDTH:Float = Float(UIScreen.main.bounds.width)
let SCREEN_HEIGHT:Float = Float(UIScreen.main.bounds.height)
let appDelegate = UIApplication.shared.delegate as! AppDelegate
let appSceneDelegate = UIApplication.shared.connectedScenes.first!.delegate as! SceneDelegate

let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && SCREEN_HEIGHT < 568.0
let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && SCREEN_HEIGHT == 568.0
let IS_IPHONE_6_7        = UIDevice.current.userInterfaceIdiom == .phone && SCREEN_HEIGHT == 667.0
let IS_IPHONE_6P_7P      = UIDevice.current.userInterfaceIdiom == .phone && SCREEN_HEIGHT == 736.0
let IS_IPHONE_X          = UIDevice.current.userInterfaceIdiom == .phone && SCREEN_HEIGHT == 812.0
let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && SCREEN_HEIGHT == 1024.0
let IS_IPAD_PRO          = UIDevice.current.userInterfaceIdiom == .pad && SCREEN_HEIGHT == 1366.0
let IS_IPAD_PRO_LARGE    = UIDevice.current.userInterfaceIdiom == .pad && SCREEN_HEIGHT == 1112.0
let IS_IPAD_AIR          = UIDevice.current.userInterfaceIdiom == .pad && SCREEN_HEIGHT == 768.0

let IS_SANDBOX = false

/**********************  Constant  ************************/

let SUCESS_CODE:Int! = 1
let DEFAULT_ERROR = "Something went wrong."
let NETWORK_ERROR = "Network connection lost."
let TIMEOUT_ERROR = "Request timed out."
let INVALID_RESPONSE_ERROR = "Response format is invalid."
let SERVER_ERROR = "Something went wrong, please contact support team."
let ACTIVE_ERROR = "Sorry your account is in-activate,Please contact with administrator.Thanks"

/********************** Other Constant  ************************/

let TERMSCONDITION = "Terms & Conditions"
let PRIVACY = "Privacy Policy"
let ABOUTUS = "About Us"
let APP_TOKEN = "55df8d2135734779be1b1f35db09acfd3868cc409f5bd9a2ffd8804d135ead43547448b4e644d4c96954dc519c82d0db1288e9814b4b57a785488ade27d59a63O6PKl3yr8WbkX2zFnj7XKVzaF4b5hgEvCxbOde49ipc--2"
let USER_TOKEN = "userToken"
var ISENGLISH = "en"
let BACK_AR = "back_ar"
let SIDE_ICON_AR = "side icon_ar"

/**********************  URLS  ************************/


struct APPURL {
    static var BASE_URL = "https://designedkw.com/dhana/app/"
    static var ABOUT_US = "https://designedkw.com/dhana/about-us"
    static var PRIVACY_POLICY = "https://designedkw.com/dhana/privacy-policy"
    static var TERMS_CONDITION = "https://designedkw.com/dhana/terms-conditions"
}

struct COLORS {
    
    static let SEARCH_HEADER_COLOR = UIColor(red: 244.0/255.0, green: 244.0/255.0, blue: 244.0/255.0, alpha: 1.0)
    static let APPROVED_COLOR = UIColor(red: 37.0/255.0, green: 154.0/255.0, blue: 53.0/255.0, alpha: 1.0)
    static let UNSELECT_CATEGORY_COLOR = UIColor(red: 212.0/255.0, green: 208.0/255.0, blue: 228.0/255.0, alpha: 1.0)
    static let SELECT_BORDER_COLOR = UIColor(red: 235.0/255.0, green: 235.0/255.0, blue: 235.0/255.0, alpha: 1.0)
    static let TABBAR_COLOR = UIColor(red: 76.0/255.0, green: 39.0/255.0, blue: 129.0/255.0, alpha: 1.0)
    static let APP_THEME_COLOR = UIColor(red: 130.0/255.0, green: 105.0/255.0, blue: 187.0/255.0, alpha: 1.0)
    static let APP_BOTTOM_COLOR = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    static let APP_RED_COLOR = UIColor(red: 248.0/255.0, green: 63.0/255.0, blue: 74.0/255.0, alpha: 1.0)
    static let ADD_ACTIVITY_COLOR = UIColor(red: 237.0/255.0, green: 237.0/255.0, blue: 237.0/255.0, alpha: 1.0)
    static let COLOR_PROGRESS_COLOR = UIColor(red: 180.0/255.0, green: 157.0/255.0, blue: 221.0/255.0, alpha: 1.0)
    static let BUTTON_SELECT_COLOR = UIColor(red: 67.0/255.0, green: 44.0/255.0, blue: 109.0/255.0, alpha: 1.0)
}
/*-------------------------------------------------------------------------------*/


/********************** APIS NAME Constant  ************************/
struct API {
    static let LOGIN = "login"
    static let SIGN_UP = "registration"
    static let FORGOT = "forgot_password"
    static let ADD_GROWTH = "add_growth"
    static let GET_GROWTH = "get_growths"
    static let GET_GUIDE_CAT = "get_article_categories"
    static let GET_ARTICLES = "get_articles"
    static let GET_ARTICLES_DETAIL = "get_article_detail"
    static let GET_BABY_LIST = "get_babies_list"
    static let VERIFY_OTP = "verifyOtp"
    static let GET_ALERGY = "getAllergies"
    static let GET_Relations = "getRelationships"
    static let GET_Medical = "getMedicalConditions"
    static let UPDATE_PROFILE = "update_user_dp"
    static let UPDATE_PROFILE_USER = "user_update"
    static let RESEND_OTP = "resend_otp"
    static let GET_VACCINE = "getVaccinations"
    static let ADD_NEW_BABY = "add_new_baby"
    static let LOGOUT = "logout"
    static let GET_BABY_DETAIL = "get_baby"
    static let GET_GROWTH_CHARTS = "get_growth_charts"
    static let GET_MILESTONE = "get_milestones"
    static let SAVE_ACHIEVE_MILESTONE = "save_achieve_milestone"
    static let DELETE_ACCOUNT = "delete_user_account"
    static let GET_USER_SETTING = "get_user_setting"
    static let UPDATE_USER_SETTING = "update_user_setting"
    static let GET_CONTACT_DETAIL = "get_contact_detail"
    static let CHANGE_PASSWORD = "change_password"
    static let CONTACT_ADD_DETAIL = "send_contact_message"
    static let EDIT_BABY = "edit_baby"
    static let ADD_FOOD = "add_new_favourite_food"
    static let ADD_MEDICAL = "add_baby_medical_condition"
    static let ADD_ALERGY = "add_baby_allergy"
    static let ADD_TOYS = "add_new_toy"
    static let ADD_DOCUMENT = "add_new_document"
    static let ADD_VACCINE = "add_new_favourite_food"
    static let ADD_CARE_GIVER = "add_care_giver"
    static let GET_NOTIFICATIONS = "get_notifications"
    static let CLEAR_NOTIFICATION = "clear_notifications"
    static let DELETE_BABY = "delete_baby"
    static let LIKE_ARTICLE = "article_like"
    static let ADD_COMMENT = "add_article_comment"
    static let ADD_NOTES = "add_edit_note"
    static let ADD_SLEEP = "add_edit_sleep_log"
    static let ADD_APPOINTMENT = "add_edit_appointment"
    static let ADD_DIAPER = "add_edit_diaper_log"
    static let ADD_PUMPING = "add_edit_pumping_log"
    static let ADD_TEMPRATURE = "add_edit_temperature_log"
    static let ADD_MDEICATIONS = "add_edit_medication"
    static let GET_MEDICATION_LIST = "get_medications"
    static let ADD_FEEDRING = "add_edit_feeding_log"
    static let GET_ACTIVITY = "get_activities"
    static let ADD_ACTIVITY = "add_baby_activity"
    static let GET_TEETH = "get_teething_logs"
    static let ADD_TEETH = "add_teeth_log"
    static let GET_BABY_VACCINE = "get_baby_vaccinations"
    static let ADD_BABY_VACCINE = "add_baby_vaccination"
    static let GET_DIARY = "get_diary"
    static let GET_ANALYSING_CATEGORIES = "get_analysis_categories"
    static let GET_MEMORIES_LIST = "memories_list"
    static let GET_MEMORIES_ALBUM_LIST = "memory_album_list"
    static let ADD_EDIT_MEMORY = "add_edit_memory"
    static let ADD_EDIT_ALBUM = "add_edit_memory_album"
    static let DELETE_MEMORY = "delete_memory"
    static let GET_ANALYSIS = "get_analysis"
    static let GET_MESSAGES = "chat_messages"
    static let SEND_MESSAGES = "send_message"
    static let GET_SUBSCRIPTION_LIST = "subscriptions_list"
    static let GET_SUBSCRIPTION = "get_subscription_plans"
    static let BUY_SUBSCRIPTION = "buy_subscription"
    
    static let DELETE_GROWTH = "delete_growth"
    static let DELETE_MEDICAL = "delete_medication"
    static let DELETE_NOTES = "delete_note"
    static let DELETE_ACTIVITY = "delete_baby_activity"
    static let DELETE_VACCINATION = "delete_baby_vaccination"
    static let DELETE_APPOINTMENT = "delete_appointment"
    static let DELETE_SLEEP = "delete_sleep_log"
    static let DELETE_DIAPER = "delete_diaper_log"
    static let DELETE_PUMPING = "delete_pumping_log"
    static let DELETE_TEMPRATURE = "delete_temperature"
    static let DELETE_FEEDING = "delete_feeding_log"
    static let DELETE_TEETHING = "delete_teeth_log"
    static let DELETE_DIARY = "delete_diary"
    static let UPDATE_DIARY = "update_diary"
    
    static let EDIT_GROWTH = "edit_growth"
    static let EDIT_ACTIVITY = "edit_baby_activity"
    static let EDIT_VACCINATION = "edit_baby_vaccination"
   
//    static let EDIT_MEDICAL = "delete_medication"
//    static let EDIT_NOTES = "delete_note"
//    static let EDIT_ACTIVITY = "delete_baby_activity"
//    static let EDIT_VACCINATION = "delete_baby_vaccination"
//    static let EDIT_APPOINTMENT = "delete_appointment"
//    static let EDIT_SLEEP = "delete_sleep_log"
//    static let EDIT_DIAPER = "delete_diaper_log"
//    static let EDIT_PUMPING = "delete_pumping_log"
//    static let EDIT_TEMPRATURE = "delete_temperature"
//    static let EDIT_FEEDING = "delete_feeding_log"
//    static let EDIT_TEETHING = "delete_teeth_log"
    
}

/**********************  UserDefaults  ************************/

let USER_DEFAUTS : UserDefaults = UserDefaults.standard
let APP_DELEGATE : AppDelegate = UIApplication.shared.delegate as! AppDelegate
let USER_LOGIN = "isLogin"
let COUNTRY_CODE = "965"
let babyId = "babyId"
let LANGUAGE = "isArabic"


/**********************  Notifications  ************************/

let NOTIFICATION_GETGOOGLE_INFO = "getGoogleInfo"
let GETTING_PURPOSAL = "proposal_received"

/**********************  set height width error message  ************************/

let NETWORK_ERROR_MESSAGE = "There seems to be a problem with your internet connection."

class Constants: NSObject {
    
    class func getScreenHeight()->Float {
        
        return SCREEN_HEIGHT
    }
    
    class func getScreenWidth()->Float {
        
        return SCREEN_WIDTH
    }
    
    class func getNetworkErrorMessage()->String {
        
        return NETWORK_ERROR_MESSAGE
    }
}

enum AlertType : Int {
    case success = 1, warning, error, nothing, completed
}

enum DIARYTYPE : String {
    case medication = "medication"
    case vaccination = "vaccination"
    case appointment = "appointment"
    case temperature = "temperature"
    case teething = "teething"
    case note = "note"
    case pumping = "pumping"
    case growth = "growth"
    case sleep = "sleep"
    case diaper = "diaper"
    case feeding = "feeding"
    case activity = "activity"
}

enum GROUPTYPE : String {
    case professional = "Professional"
    case social = "Social"
}
