//
//  Activity.swift
//  Tifli
//
//  Created by zubair on 11/11/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

class Activity: NSObject {
    
    var ID: String = ""
    var activityId:String = ""
    var media:String = ""
    var mediaDuration: String = ""
    var mediaType: String = ""
    var titles: String = ""
    var titleAr: String = ""
    var isPlay = false
    var dailyRoutineIsActive = 0
    var setReminder = 0
    var arrActivity = [BabyType]()
    
    override init() {
        super.init()
    }
    
    init(withDictionary dictionary: [String: JSON]) {
        super.init()
        
        if let ID = dictionary["id"]?.stringValue {
            self.ID = ID
        }
        if let activityId = dictionary["activityId"]?.stringValue {
            self.activityId = activityId
        }
        if let media = dictionary["media"]?.stringValue {
            self.media = media
        }
        if let mediaDuration = dictionary["mediaDuration"]?.stringValue {
            self.mediaDuration = mediaDuration
        }
        if let mediaType = dictionary["mediaType"]?.stringValue {
            self.mediaType = mediaType
        }
        if let title = dictionary["title"]?.stringValue {
            self.titles = title
        }
        if let titleAr = dictionary["titleAr"]?.stringValue {
            self.titleAr = titleAr
        }
        if let dailyRoutineIsActive = dictionary["dailyRoutineIsActive"]?.intValue {
            self.dailyRoutineIsActive = dailyRoutineIsActive
        }
        if let setReminder = dictionary["setReminder"]?.intValue {
            self.setReminder = setReminder
        }
        if let value = dictionary["all_activities"]?.arrayValue {
            for item in value {
                let obj = BabyType.init(withDictionary: item.dictionaryValue)
                self.arrActivity.append(obj)
            }
        }
    }
}
