//
//  Feeding.swift
//  Tifli
//
//  Created by zubair on 10/11/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

class Feeding: NSObject {
    
    var ID: String = ""
    var type:String = ""
    var leftBreastDuration:String = ""
    var rightBreastDuration: String = ""
    var bottleType:String = ""
    var amount:String = ""
    var ingredients: String = ""
    var isPlay = false
    var logDate: String = ""
    var breastFeeding: String = ""
    var breastMilk: String = ""
    var breastMilkAmount: String = ""
    var formulaMilk: String = ""
    var formulaMilkAmount: String = ""
    var solidFood: String = ""
    var dailyRoutineIsActive = 0
    var setReminder = 0
    
    override init() {
        super.init()
    }
    
    init(withDictionary dictionary: [String: JSON]) {
        super.init()
        
        if let ID = dictionary["id"]?.stringValue {
            self.ID = ID
        }
        if let type = dictionary["type"]?.stringValue {
            self.type = type
        }
        if let leftBreastDuration = dictionary["leftBreastDuration"]?.stringValue {
            self.leftBreastDuration = leftBreastDuration
        }
        if let rightBreastDuration = dictionary["rightBreastDuration"]?.stringValue {
            self.rightBreastDuration = rightBreastDuration
        }
        if let dailyRoutineIsActive = dictionary["dailyRoutineIsActive"]?.intValue {
            self.dailyRoutineIsActive = dailyRoutineIsActive
        }
        if let setReminder = dictionary["setReminder"]?.intValue {
            self.setReminder = setReminder
        }
        if let bottleType = dictionary["bottleType"]?.stringValue {
            self.bottleType = bottleType
        }
        if let amount = dictionary["amount"]?.stringValue {
            self.amount = amount
        }
        if let ingredients = dictionary["ingredients"]?.stringValue {
            self.ingredients = ingredients
        }
        
        if let logDate = dictionary["food_log_date"]?.stringValue {
            self.logDate = logDate
        }
        if let breastFeeding = dictionary["breast_feeding"]?.intValue {
            self.breastFeeding = String(breastFeeding)
        }
        if let breastMilk = dictionary["breast_milk"]?.intValue {
            self.breastMilk = String(breastMilk)
        }
        if let breastMilkAmount = dictionary["breast_milk_amount"]?.stringValue {
            self.breastMilkAmount = breastMilkAmount
        }
        if let formulaMilk = dictionary["formula_milk"]?.intValue {
            self.formulaMilk = String(formulaMilk)
        }
        if let formulaMilkAmount = dictionary["formula_milk_amount"]?.stringValue {
            self.formulaMilkAmount = formulaMilkAmount
        }
        if let solidFood = dictionary["solid_food"]?.intValue {
            self.solidFood = String(solidFood)
        }
        if formulaMilkAmount.count == 0 {
            formulaMilkAmount = "0"
        }
    }
}
