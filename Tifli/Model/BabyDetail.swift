//
//  BabyDetail.swift
//  Tifli
//
//  Created by zubair on 01/09/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

class BabyDetail: NSObject {
    
    var babyID: String = ""
    var babyName = ""
    var babyImage: String = ""
    var babyDob: String = ""
    var age:String = ""
    var gender:String = ""
    var pregnancyWeek:String = ""
    var babyBornType:String = ""
    var babyDeliveryType:String = ""
    var arrMedical = [BabyType]()
    var arrAlergy = [BabyType]()
    var arrVaccine = [BabyType]()
    var arrToys = [BabyType]()
    var arrFood = [BabyType]()
    var arrDocuments = [BabyType]()
    var arrCaregiver = [BabyType]()
    
    override init() {
        super.init()
    }
    
    init(withDictionary dictionarys: [String: JSON]) {
        super.init()
        
        let dictionary: Dictionary<String, JSON> = dictionarys["baby"]!.dictionaryValue
        if let babyID = dictionary["babyId"]?.stringValue {
            self.babyID = babyID
        }
        if let babyName = dictionary["babyName"]?.stringValue {
            self.babyName = babyName
        }
        if let babyImage = dictionary["babyImage"]?.stringValue {
            self.babyImage = babyImage
        }
        if let babyDob = dictionary["babyDob"]?.stringValue {
            self.babyDob = babyDob
        }
        if let age = dictionary["age"]?.stringValue {
            self.age = age
        }
        if let gender = dictionary["babyGender"]?.stringValue {
            self.gender = gender
        }
        if let pregnancyWeek = dictionary["babyPregnancyWeeks"]?.stringValue {
            self.pregnancyWeek = pregnancyWeek
        }
        if let babyBornType = dictionary["babyBornType"]?.stringValue {
            self.babyBornType = babyBornType
        }
        if let babyDeliveryType = dictionary["babyDeliveryType"]?.stringValue {
            self.babyDeliveryType = babyDeliveryType
        }
        
        if let value = dictionarys["baby_allergies"]?.arrayValue {
            for item in value {
                let obj = BabyType.init(withDictionary: item.dictionaryValue)
                self.arrAlergy.append(obj)
            }
        }
        if let value = dictionarys["baby_medical_conditions"]?.arrayValue {
            for item in value {
                let obj = BabyType.init(withDictionary: item.dictionaryValue)
                self.arrMedical.append(obj)
            }
        }
        if let value = dictionarys["baby_vaccinations"]?.arrayValue {
            for item in value {
                let obj = BabyType.init(withDictionary: item.dictionaryValue)
                self.arrVaccine.append(obj)
            }
        }
        if let value = dictionarys["baby_fav_food"]?.arrayValue {
            for item in value {
                let obj = BabyType.init(withDictionary: item.dictionaryValue)
                self.arrFood.append(obj)
            }
        }
        if let value = dictionarys["baby_favourite_toys"]?.arrayValue {
            for item in value {
                let obj = BabyType.init(withDictionary: item.dictionaryValue)
                self.arrToys.append(obj)
            }
        }
        if let value = dictionarys["baby_documents"]?.arrayValue {
            for item in value {
                let obj = BabyType.init(withDictionary: item.dictionaryValue)
                self.arrDocuments.append(obj)
            }
        }
        if let value = dictionarys["care_givers"]?.arrayValue {
            for item in value {
                let obj = BabyType.init(withDictionary: item.dictionaryValue)
                self.arrCaregiver.append(obj)
            }
        }
    }
}
