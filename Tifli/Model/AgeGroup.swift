//
//  AgeGroup.swift
//  Tifli
//
//  Created by zubair on 07/09/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

class AgeGroup: NSObject {
    
    var groupId: String = ""
    var startMonth = ""
    var endMonth: String = ""
    
    override init() {
        super.init()
    }
    
    init(withDictionary dictionary: [String: JSON]) {
        super.init()
        
        if let groupId = dictionary["ageGroupId"]?.stringValue {
            self.groupId = groupId
        }
        if let startMonth = dictionary["ageGroupStartMonth"]?.stringValue {
            self.startMonth = startMonth
        }
        if let endMonth = dictionary["ageGroupEndMonth"]?.stringValue {
            self.endMonth = endMonth
        }
    }
}
