//
//  Growth.swift
//  Tifli
//
//  Created by zubair on 16/08/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

class Growth: NSObject {
    
    var growthID: String = ""
    var babyId = ""
    var chartDate: String = ""
    var weight: String = ""
    var height:String = ""
    var circle:String = ""
    var notes: String = ""
    var media = ""
    var mediaType = ""
    var mediaDuration = ""
    var userFullName = ""
    var userProfileImage = ""
    var isPlay = false
    
    override init() {
        super.init()
    }
    
    init(withDictionary dictionary: [String: JSON]) {
        super.init()
        
        if let growthID = dictionary["id"]?.stringValue {
            self.growthID = growthID
        }
        if let babyId = dictionary["growthChartBabyId"]?.stringValue {
            self.babyId = babyId
        }
//        if let chartDate = dictionary["growthChartDate"]?.stringValue {
//            self.chartDate = chartDate
//        }
        if let weight = dictionary["weight"]?.stringValue {
            self.weight = weight
        }
        if let height = dictionary["height"]?.stringValue {
            self.height = height
        }
        if let circle = dictionary["headCircle"]?.stringValue {
            self.circle = circle
        }
        if let notes = dictionary["growthChartNotes"]?.stringValue {
            self.notes = notes
        }
        if let growthChartMedia = dictionary["media"]?.stringValue {
            self.media = growthChartMedia
        }
        if let growthChartMediaType = dictionary["mediaType"]?.stringValue {
            self.mediaType = growthChartMediaType
        }
        if let mediaDuration = dictionary["mediaDuration"]?.stringValue {
            self.mediaDuration = mediaDuration
        }
//        if let userFullName = dictionary["userFullName"]?.stringValue {
//            self.userFullName = userFullName
//        }
//        if let userProfileImage = dictionary["userProfileImage"]?.stringValue {
//            self.userProfileImage = userProfileImage
//        }
    }
}
