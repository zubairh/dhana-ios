//
//  Temperature.swift
//  Tifli
//
//  Created by zubair on 10/11/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

class Temperature: NSObject {
    
    var ID: String = ""
    var temperature:String = ""
    var media:String = ""
    var mediaType:String = ""
    var mediaDuration: String = ""
    var grapshDate = ""
    var grapshValue = ""
    var isPlay = false
    
    override init() {
        super.init()
    }
    
    init(withDictionary dictionary: [String: JSON]) {
        super.init()
        
        if let ID = dictionary["id"]?.stringValue {
            self.ID = ID
        }
        if let temperature = dictionary["temperature"]?.stringValue {
            self.temperature = temperature
        }
        if let media = dictionary["media"]?.stringValue {
            self.media = media
        }
        if let mediaType = dictionary["mediaType"]?.stringValue {
            self.mediaType = mediaType
        }
        if let mediaDuration = dictionary["mediaDuration"]?.stringValue {
            self.mediaDuration = mediaDuration
        }
        if let grapshDate = dictionary["graph_date"]?.stringValue {
            self.grapshDate = grapshDate
        }
        if let grapshValue = dictionary["graph_value"]?.stringValue {
            self.grapshValue = grapshValue
        }
    }
}
