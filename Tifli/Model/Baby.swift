//
//  Baby.swift
//  Tifli
//
//  Created by zubair on 23/08/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

class Baby: NSObject {
    
    var babyID: String = ""
    var babyName = ""
    var babyImage: String = ""
    var babyDob: String = ""
    var age:String = ""
    var gender:String = ""
    var pregnancyWeek:String = ""
    var babyBornType:String = ""
    var babyDeliveryType:String = ""
    var babyRelationship:String = ""
    
    override init() {
        super.init()
    }
    
    init(withDictionary dictionary: [String: JSON]) {
        super.init()
        
        if let babyID = dictionary["babyId"]?.stringValue {
            self.babyID = babyID
        }
        if let babyName = dictionary["babyName"]?.stringValue {
            self.babyName = babyName
        }
        if let babyImage = dictionary["babyImage"]?.stringValue {
            self.babyImage = babyImage
        }
        if let babyDob = dictionary["babyDob"]?.stringValue {
            self.babyDob = babyDob
        }
        if let age = dictionary["age"]?.stringValue {
            self.age = age
        }
        if let gender = dictionary["babyGender"]?.stringValue {
            self.gender = gender
        }
        if let pregnancyWeek = dictionary["babyPregnancyWeeks"]?.stringValue {
            self.pregnancyWeek = pregnancyWeek
        }
        if let babyBornType = dictionary["babyBornType"]?.stringValue {
            self.babyBornType = babyBornType
        }
        if let babyDeliveryType = dictionary["babyDeliveryType"]?.stringValue {
            self.babyDeliveryType = babyDeliveryType
        }
        if let babyRelationship = dictionary["babyRelationTitle"]?.stringValue {
            self.babyRelationship = babyRelationship
        }
    }
}
