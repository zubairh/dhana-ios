//
//  Teething.swift
//  Tifli
//
//  Created by zubair on 10/11/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

class Teething: NSObject {
    
    var categorys: String = ""
    var code = ""
    var name: String = ""
    var ID: String = ""
    var image:String = ""
    var teethingId:String = ""
    var media:String = ""
    var mediaType:String = ""
    var mediaDuration: String = ""
    var arrTooth = [Teeth]()
    var isPlay = false
    
    override init() {
        super.init()
    }
    
    init(withDictionary dictionary: [String: JSON]) {
        super.init()
        
        if let teethingId = dictionary["teethingId"]?.stringValue {
            self.teethingId = teethingId
        }
        if let code = dictionary["code"]?.stringValue {
            self.code = code
        }
        if let name = dictionary["name"]?.stringValue {
            self.name = name
        }
        if let ID = dictionary["id"]?.stringValue {
            self.ID = ID
        }
        if let categorys = dictionary["category"]?.stringValue {
            self.categorys = categorys
        }
        if let image = dictionary["image"]?.stringValue {
            self.image = image
        }
        if let media = dictionary["media"]?.stringValue {
            self.media = media
        }
        if let mediaType = dictionary["mediaType"]?.stringValue {
            self.mediaType = mediaType
        }
        if let mediaDuration = dictionary["mediaDuration"]?.stringValue {
            self.mediaDuration = mediaDuration
        }
        if let value = dictionary["tooth"]?.arrayValue {
            for item in value {
                let obj = Teeth.init(withDictionary: item.dictionaryValue)
                self.arrTooth.append(obj)
            }
        }
    }
}
