//
//  Vaccine.swift
//  Tifli
//
//  Created by zubair on 04/11/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

class Vaccine: NSObject {
    
    var ID: String = ""
    var titles = ""
    var titleAr = ""
    var detail = ""
    var dates = ""
    var isTaken = false
    
    override init() {
        super.init()
    }
    
    init(withDictionary dictionary: [String: JSON]) {
        super.init()
        
        if let ID = dictionary["vaccinationId"]?.stringValue {
            self.ID = ID
        }
        if let titles = dictionary["title"]?.stringValue {
            self.titles = titles
        }
        if let titleAr = dictionary["title_ar"]?.stringValue {
            self.titleAr = titleAr
        }
        if let date = dictionary["date"]?.stringValue {
            self.dates = date
        }
        if let detail = dictionary["detail"]?.stringValue {
            self.detail = detail
        }
        if let isTaken = dictionary["isTaken"]?.boolValue {
            self.isTaken = isTaken
        }
    }
}
