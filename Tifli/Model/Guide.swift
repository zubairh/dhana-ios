//
//  Guide.swift
//  Tifli
//
//  Created by zubair on 16/08/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

class Guide: NSObject {
    
    var articleID: String = ""
    var articleCatID = ""
    var titleEng: String = ""
    var titleAr: String = ""
    var detailEng: String = ""
    var detailAr: String = ""
    var image: String = ""
    var autherName: String = ""
    var autherImage: String = ""
    var totalComment = 0
    var totalLike = 0
    
    override init() {
        super.init()
    }
    
    init(withDictionary dictionary: [String: JSON]) {
        super.init()
        
        if let articleID = dictionary["articleId"]?.stringValue {
            self.articleID = articleID
        }
        if let articleCatID = dictionary["articleCategoryId"]?.stringValue {
            self.articleCatID = articleCatID
        }
        if let titleEng = dictionary["articleTitleEn"]?.stringValue {
            self.titleEng = titleEng
        }
        if let titleAr = dictionary["articleTitleAr"]?.stringValue {
            self.titleAr = titleAr
        }
        if let detailEng = dictionary["articleDescriptionEn"]?.stringValue {
            self.detailEng = detailEng
        }
        if let detailAr = dictionary["articleDescriptionAr"]?.stringValue {
            self.detailAr = detailAr
        }
        if let image = dictionary["articleImage"]?.stringValue {
            self.image = image
        }
        if let autherName = dictionary["articleAuthorNameEn"]?.stringValue {
            self.autherName = autherName
        }
        if let autherImage = dictionary["articleAuthorImage"]?.stringValue {
            self.autherImage = autherImage
        }
        if let totalComment = dictionary["likes"]?.intValue {
            self.totalComment = totalComment
        }
        if let totalLike = dictionary["comments"]?.intValue {
            self.totalLike = totalLike
        }
    }
}
