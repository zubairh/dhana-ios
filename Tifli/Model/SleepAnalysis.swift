//
//  SleepAnalysis.swift
//  Tifli
//
//  Created by zubair on 08/12/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

class SleepAnalysis: NSObject {
    
    var sleepHours: String = ""
    var sleepCondition:String = ""
    var sleepPerDay:String = ""
    var sleepPerDayCondition: String = ""
    var weekDayTimeSleep:String = ""
    var weekNightTimeSleep: String = ""
    var arrRoutine = [SleepRoutine]()
    
    override init() {
        super.init()
    }
    
    init(withDictionary dictionary: [String: JSON]) {
        super.init()
        
        if let sleepHours = dictionary["average_sleep_hours"]?.intValue {
            self.sleepHours = String(sleepHours)
        }
        if let sleepCondition = dictionary["average_sleep_condition"]?.stringValue {
            self.sleepCondition = sleepCondition
        }
        if let sleepPerDay = dictionary["average_sleep_per_day"]?.intValue {
            self.sleepPerDay = String(sleepPerDay)
        }
        if let sleepPerDayCondition = dictionary["average_sleep_per_day_condition"]?.stringValue {
            self.sleepPerDayCondition = sleepPerDayCondition
        }
        if let weekDayTimeSleep = dictionary["this_week_day_time_sleeps"]?.intValue {
            self.weekDayTimeSleep = String(weekDayTimeSleep)
        }
        if let weekNightTimeSleep = dictionary["this_week_night_time_sleeps"]?.intValue {
            self.weekNightTimeSleep = String(weekNightTimeSleep)
        }
        if let value = dictionary["sleep_routine"]?.arrayValue {
            for item in value {
                let obj = SleepRoutine.init(withDictionary: item.dictionaryValue)
                self.arrRoutine.append(obj)
            }
        }
    }
}
