//
//  Diaper.swift
//  Tifli
//
//  Created by zubair on 10/11/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

class Diaper: NSObject {
    
    var ID: String = ""
    var type:String = ""
    var form:String = ""
    var size: String = ""
    var color:String = ""
    var media:String = ""
    var mediaType: String = ""
    var mediaDuration: String = ""
    var isPlay = false
    
    var logDate: String = ""
    var clean: String = ""
    var pee: String = ""
    var liquidPoo: String = ""
    var softPoo: String = ""
    var solidPoo: String = ""
    var mixed: String = ""
    var dailyRoutineIsActive = 0
    var setReminder = 0
    
    override init() {
        super.init()
    }
    
    init(withDictionary dictionary: [String: JSON]) {
        super.init()
        
        if let ID = dictionary["id"]?.stringValue {
            self.ID = ID
        }
        if let type = dictionary["type"]?.stringValue {
            self.type = type
        }
        if let form = dictionary["form"]?.stringValue {
            self.form = form
        }
        if let size = dictionary["size"]?.stringValue {
            self.size = size
        }
        if let color = dictionary["color"]?.stringValue {
            self.color = color
        }
        if let media = dictionary["media"]?.stringValue {
            self.media = media
        }
        if let mediaType = dictionary["mediaType"]?.stringValue {
            self.mediaType = mediaType
        }
        if let mediaDuration = dictionary["mediaDuration"]?.stringValue {
            self.mediaDuration = mediaDuration
        }
        if let date = dictionary["date"]?.stringValue {
            self.logDate = date
        }
        if let clean = dictionary["clean"]?.intValue {
            self.clean = String(clean)
        }
        if let pee = dictionary["pee"]?.intValue {
            self.pee = String(pee)
        }
        if let liquid_poo = dictionary["liquid_poo"]?.intValue {
            self.liquidPoo = String(liquid_poo)
        }
        if let soft_poo = dictionary["soft_poo"]?.intValue {
            self.softPoo = String(soft_poo)
        }
        if let solid_poo = dictionary["solid_poo"]?.intValue {
            self.solidPoo = String(solid_poo)
        }
        if let mixed = dictionary["mixed"]?.intValue {
            self.mixed = String(mixed)
        }
        if let dailyRoutineIsActive = dictionary["dailyRoutineIsActive"]?.intValue {
            self.dailyRoutineIsActive = dailyRoutineIsActive
        }
        if let setReminder = dictionary["setReminder"]?.intValue {
            self.setReminder = setReminder
        }
    }
}
