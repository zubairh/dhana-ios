//
//  Diary.swift
//  Tifli
//
//  Created by zubair on 10/11/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

class Diary: NSObject {
    
    var ID: String = ""
    var date = ""
    var notes: String = ""
    var createdDate: String = ""
    var userName:String = ""
    var userImage:String = ""
    var type: String = ""
    var diaryId: String = ""
    var newDifferDate = ""
    var isActive = 0
    var objMedication = Medication()
    var objVaccination = Vaccination()
    var objAppointment = Appointment()
    var objTemprature = Temperature()
    var objTeething = Teething()
    var objNote = Note()
    var objPumping = Pumping()
    var objSleep = Sleep()
    var objDiaper = Diaper()
    var objFeeding = Feeding()
    var objGrowth = Growth()
    var objActivity = Activity()
    
    override init() {
        super.init()
    }
    
    init(withDictionary dictionary: [String: JSON]) {
        super.init()
        
        if let ID = dictionary["id"]?.stringValue {
            self.ID = ID
        }
        if let date = dictionary["date"]?.stringValue {
            self.date = date
        }
        if let notes = dictionary["notes"]?.stringValue {
            self.notes = notes
        }
        if let createdDate = dictionary["createdAt"]?.stringValue {
            self.createdDate = createdDate
        }
        if let userName = dictionary["userName"]?.stringValue {
            self.userName = userName
        }
        if let userImage = dictionary["userImage"]?.stringValue {
            self.userImage = userImage
        }
        if let type = dictionary["type"]?.stringValue {
            self.type = type
        }
        if let diaryId = dictionary["diaryId"]?.stringValue {
            self.diaryId = diaryId
        }
        if let isActive = dictionary["isActive"]?.intValue {
            self.isActive = isActive
        }
        let now = Date()
        let nowTime = now.getFormattedDate(format: "hh:mm:ss")
        let fromTime = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: self.date)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm:ss"

        let date1 = formatter.date(from: nowTime)!
        let date2 = formatter.date(from: fromTime)!
        let elapsedTime = date2.timeIntervalSince(date1)
        let hours = floor(elapsedTime / 60 / 60)
        let minutes = floor((elapsedTime - (hours * 60 * 60)) / 60)

        print("\(Int(hours)) hr and \(Int(minutes)) min")
        if hours<1 || minutes<1 {
            self.newDifferDate = "\(Int(hours)) hr & \(Int(minutes)) min late"
        } else {
            self.newDifferDate = "\(Int(hours)) hr & \(Int(minutes)) min left"
        }
        
        if let dic: Dictionary<String, JSON> = dictionary["medication"]?.dictionaryValue {
            self.objMedication = Medication.init(withDictionary: dic)
        }
        if let dic: Dictionary<String, JSON> = dictionary["vaccination"]?.dictionaryValue {
            self.objVaccination = Vaccination.init(withDictionary: dic)
        }
        if let dic: Dictionary<String, JSON> = dictionary["appointment"]?.dictionaryValue {
            self.objAppointment = Appointment.init(withDictionary: dic)
        }
        if let dic: Dictionary<String, JSON> = dictionary["temperature"]?.dictionaryValue {
            self.objTemprature = Temperature.init(withDictionary: dic)
        }
        if let dic: Dictionary<String, JSON> = dictionary["teething"]?.dictionaryValue {
            self.objTeething = Teething.init(withDictionary: dic)
        }
        if let dic: Dictionary<String, JSON> = dictionary["note"]?.dictionaryValue {
            self.objNote = Note.init(withDictionary: dic)
        }
        if let dic: Dictionary<String, JSON> = dictionary["pumping"]?.dictionaryValue {
            self.objPumping = Pumping.init(withDictionary: dic)
        }
        if let dic: Dictionary<String, JSON> = dictionary["growth"]?.dictionaryValue {
            self.objGrowth = Growth.init(withDictionary: dic)
        }
        if let dic: Dictionary<String, JSON> = dictionary["sleep"]?.dictionaryValue {
            self.objSleep = Sleep.init(withDictionary: dic)
        }
        if let dic: Dictionary<String, JSON> = dictionary["diaper"]?.dictionaryValue {
            self.objDiaper = Diaper.init(withDictionary: dic)
        }
        if let dic: Dictionary<String, JSON> = dictionary["feeding"]?.dictionaryValue {
            self.objFeeding = Feeding.init(withDictionary: dic)
        }
        if let dic: Dictionary<String, JSON> = dictionary["activity"]?.dictionaryValue {
            self.objActivity = Activity.init(withDictionary: dic)
        }
    }
    
}

