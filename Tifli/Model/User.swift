//
//  User.swift
//  Tifli
//
//  Created by zubair on 15/08/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

class User: NSObject {
    
    var userID: String = ""
    var userImage = ""
    var email: String = ""
    var phoneNumber: String = ""
    var dob:String = ""
    var gender:String = ""
    var username: String = ""
    var isEmailVerified: String = ""
    var isPhoneVerified: String = ""
    var preference: String = ""
    
    override init() {
        super.init()
    }
    
    init(withDictionary dictionary: [String: JSON]) {
        super.init()
        
        if let userID = dictionary["userId"]?.stringValue {
            self.userID = userID
        } else {
            if let userID = dictionary["id"]?.stringValue {
                self.userID = userID
            }
        }
        if let username = dictionary["userFullName"]?.stringValue {
            self.username = username
        }
        if let email = dictionary["userEmail"]?.stringValue {
            self.email = email
        }
        if let phoneNumber = dictionary["userPhone"]?.stringValue {
            self.phoneNumber = phoneNumber
        }
        if let dob = dictionary["userDob"]?.stringValue {
            self.dob = dob
        }
        if let gender = dictionary["userGender"]?.stringValue {
            self.gender = gender
        }
        if let userImage = dictionary["userProfileImage"]?.stringValue {
            self.userImage = userImage
        }
        if let isEmailVerified = dictionary["isEmailVerified"]?.stringValue {
            self.isEmailVerified = isEmailVerified
        }
        if let isPhoneVerified = dictionary["isPhoneVerified"]?.stringValue {
            self.isPhoneVerified = isPhoneVerified
        }
        if let preference = dictionary["preference"]?.stringValue {
            self.preference = preference
        }
    }
}
