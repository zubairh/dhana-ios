//
//  Sleep.swift
//  Tifli
//
//  Created by zubair on 10/11/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

class Sleep: NSObject {
    
    var ID: String = ""
    var startTime:String = ""
    var endTime:String = ""
    var duration: String = ""
    var sleepTimeDifference = 0
    var dailyRoutineIsActive = 0
    var setReminder = 0
    var isPlay = false
    
    override init() {
        super.init()
    }
    
    init(withDictionary dictionary: [String: JSON]) {
        super.init()
        
        if let ID = dictionary["id"]?.stringValue {
            self.ID = ID
        }
        if let startTime = dictionary["startTime"]?.stringValue {
            self.startTime = startTime
        }
        if let endTime = dictionary["endTime"]?.stringValue {
            self.endTime = endTime
        }
        if let duration = dictionary["duration"]?.stringValue {
            self.duration = duration
        }
        if let sleepTimeDifference = dictionary["sleepTimeDifference"]?.intValue {
            self.sleepTimeDifference = sleepTimeDifference
        }
        if let dailyRoutineIsActive = dictionary["dailyRoutineIsActive"]?.intValue {
            self.dailyRoutineIsActive = dailyRoutineIsActive
        }
        if let setReminder = dictionary["setReminder"]?.intValue {
            self.setReminder = setReminder
        }
    }
}
