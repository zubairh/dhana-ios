//
//  Messages.swift
//  Tifli
//
//  Created by zubair on 23/12/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

class Messages: NSObject {
    
    var ID: String = ""
    var chatID = ""
    var senderID: String = ""
    var message: String = ""
    var media:String = ""
    var mediaFileType:String = ""
    var createdAt:String = ""
    var image = UIImage()
    var data = Data()
    var objUser = User()
    
    override init() {
        super.init()
    }
    
    init(withDictionary dictionary: [String: JSON]) {
        super.init()
        
        if let ID = dictionary["id"]?.stringValue {
            self.ID = ID
        }
        if let chatID = dictionary["chat_id"]?.stringValue {
            self.chatID = chatID
        }
        if let senderID = dictionary["send_by"]?.stringValue {
            self.senderID = senderID
        }
        if let message = dictionary["message"]?.stringValue {
            self.message = message
        }
        if let media = dictionary["media_file"]?.stringValue {
            self.media = media
        }
        if let mediaFileType = dictionary["media_file_type"]?.stringValue {
            self.mediaFileType = mediaFileType
        }
        if let createdAt = dictionary["created_at"]?.stringValue {
            self.createdAt = createdAt
        }
        if let dict: Dictionary<String, JSON> = dictionary["user"]?.dictionaryValue {
            self.objUser = User.init(withDictionary: dict)
        }
    }
}
