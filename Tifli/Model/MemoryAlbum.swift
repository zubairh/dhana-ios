//
//  MemoryAlbum.swift
//  Tifli
//
//  Created by zubair on 29/11/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

class MemoryAlbum: NSObject {
    
    var albumId: String = ""
    var memoryCount:String = ""
    var albumTitle:String = ""
    var arrMemory = [Memory]()
    
    override init() {
        super.init()
    }
    
    init(withDictionary dictionary: [String: JSON]) {
        super.init()
        
        if let albumId = dictionary["memoryAlbumId"]?.stringValue {
            self.albumId = albumId
        }
        if let memoryCount = dictionary["memories_count"]?.intValue {
            self.memoryCount = String(memoryCount)
        }
        if let albumTitle = dictionary["memoryAlbumTitle"]?.stringValue {
            self.albumTitle = albumTitle
        }
        self.arrMemory = [Memory]()
        if let value = dictionary["memories"]?.arrayValue {
            for item in value {
                let obj = Memory.init(withDictionary: item.dictionaryValue)
                self.arrMemory.append(obj)
            }
        }
    }
}
