//
//  Comment.swift
//  Tifli
//
//  Created by zubair on 22/08/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

class Comment: NSObject {
    
    var commentID: String = ""
    var articleID = ""
    var userID: String = ""
    var comment: String = ""
    var createdDate: String = ""
    var userName: String = ""
    var userImage: String = ""
    
    override init() {
        super.init()
    }
    
    init(withDictionary dictionary: [String: JSON]) {
        super.init()
        
        if let commentID = dictionary["articleCommentId"]?.stringValue {
            self.commentID = commentID
        }
        if let articleID = dictionary["articleCommentArticleId"]?.stringValue {
            self.articleID = articleID
        }
        if let userID = dictionary["articleCommentUserId"]?.stringValue {
            self.userID = userID
        }
        if let comment = dictionary["articleCommentText"]?.stringValue {
            self.comment = comment
        }
        if let createdDate = dictionary["articleCommentCreatedAt"]?.stringValue {
            self.createdDate = createdDate
        }
        if let userName = dictionary["userFullName"]?.stringValue {
            self.userName = userName
        }
        if let userImage = dictionary["userProfileImage"]?.stringValue {
            self.userImage = userImage
        }
    }
}
