//
//  MedicationAnalysis.swift
//  Tifli
//
//  Created by zubair on 08/12/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

class MedicationAnalysis: NSObject {
    
    var medicationDate: String = ""
    var medicationName:String = ""
    var amount:String = ""
    var amountType: String = ""
    var babyMedicationName:String = ""
    
    override init() {
        super.init()
    }
    
    init(withDictionary dictionary: [String: JSON]) {
        super.init()
        
        if let medicationDate = dictionary["medication_date"]?.stringValue {
            self.medicationDate = medicationDate
        }
        if let medicationName = dictionary["medicationName"]?.stringValue {
            self.medicationName = medicationName
        }
        if let amount = dictionary["babyMedicationAmount"]?.stringValue {
            self.amount = amount
        }
        if let amountType = dictionary["babyMedicationAmountType"]?.stringValue {
            self.amountType = amountType
        }
        if let babyMedicationName = dictionary["babyMedicationName"]?.stringValue {
            self.babyMedicationName = babyMedicationName
        }
    }
}
