//
//  Notifications.swift
//  Tifli
//
//  Created by zubair on 28/09/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

class Notifications: NSObject {
    
    var ID: String = ""
    var userId = ""
    var babyId = ""
    var babyTitle = ""
    var detail = ""
    var type = ""
    var createdAt = ""
    
    override init() {
        super.init()
    }
    
    init(withDictionary dictionary: [String: JSON]) {
        super.init()
        
        if let ID = dictionary["id"]?.stringValue {
            self.ID = ID
        }
        if let userId = dictionary["userId"]?.stringValue {
            self.userId = userId
        }
        if let babyId = dictionary["babyId"]?.stringValue {
            self.babyId = babyId
        }
        if let babyTitle = dictionary["title"]?.stringValue {
            self.babyTitle = babyTitle
        }
        if let detail = dictionary["description"]?.stringValue {
            self.detail = detail
        }
        if let type = dictionary["type"]?.stringValue {
            self.type = type
        }
        if let createdAt = dictionary["createdAt"]?.stringValue {
            self.createdAt = createdAt
        }
    }
}
