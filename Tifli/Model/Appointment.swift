//
//  Appointment.swift
//  Tifli
//
//  Created by zubair on 10/11/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

class Appointment: NSObject {
    
    var ID: String = ""
    var clinic:String = ""
    var doctorName: String = ""
    var isPlay = false
    var oneDayBeforeReminder = 0
    var oneHourBeforeReminder = 0
    
    override init() {
        super.init()
    }
    
    init(withDictionary dictionary: [String: JSON]) {
        super.init()
        
        if let ID = dictionary["id"]?.stringValue {
            self.ID = ID
        }
        if let clinic = dictionary["clinic"]?.stringValue {
            self.clinic = clinic
        }
        if let doctorName = dictionary["doctorName"]?.stringValue {
            self.doctorName = doctorName
        }
        if let oneDayBeforeReminder = dictionary["oneDayBeforeReminder"]?.intValue {
            self.oneDayBeforeReminder = oneDayBeforeReminder
        }
        if let oneHourBeforeReminder = dictionary["oneHourBeforeReminder"]?.intValue {
            self.oneHourBeforeReminder = oneHourBeforeReminder
        }
    }
}
