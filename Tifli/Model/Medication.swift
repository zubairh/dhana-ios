//
//  Medication.swift
//  Tifli
//
//  Created by zubair on 10/11/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

class Medication: NSObject {
    
    var medicationId: String = ""
    var titles = ""
    var titleAr: String = ""
    var ID: String = ""
    var medicationName:String = ""
    var amount:String = ""
    var amountType: String = ""
    var media:String = ""
    var mediaType:String = ""
    var mediaDuration: String = ""
    var isPlay = false
    var dailyRoutineIsActive = 0
    var setReminder = 0
    var arrMedication = [BabyType]()
    
    override init() {
        super.init()
    }
    
    init(withDictionary dictionary: [String: JSON]) {
        super.init()
        
        if let medicationId = dictionary["medicationId"]?.stringValue {
            self.medicationId = medicationId
        }
        if let titles = dictionary["title"]?.stringValue {
            self.titles = titles
        }
        if let titleAr = dictionary["titleAr"]?.stringValue {
            self.titleAr = titleAr
        }
        if let ID = dictionary["id"]?.stringValue {
            self.ID = ID
        }
        if let medicationName = dictionary["medicationName"]?.stringValue {
            self.medicationName = medicationName
        }
        if let amount = dictionary["amount"]?.stringValue {
            self.amount = amount
        }
        if let amountType = dictionary["amountType"]?.stringValue {
            self.amountType = amountType
        }
        if let media = dictionary["media"]?.stringValue {
            self.media = media
        }
        if let mediaType = dictionary["mediaType"]?.stringValue {
            self.mediaType = mediaType
        }
        if let mediaDuration = dictionary["mediaDuration"]?.stringValue {
            self.mediaDuration = mediaDuration
        }
        if let dailyRoutineIsActive = dictionary["dailyRoutineIsActive"]?.intValue {
            self.dailyRoutineIsActive = dailyRoutineIsActive
        }
        if let setReminder = dictionary["setReminder"]?.intValue {
            self.setReminder = setReminder
        }
        if let value = dictionary["all_medications"]?.arrayValue {
            for item in value {
                let obj = BabyType.init(withDictionary: item.dictionaryValue)
                self.arrMedication.append(obj)
            }
        }
    }
}
