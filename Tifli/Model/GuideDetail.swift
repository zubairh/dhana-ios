//
//  GuideDetail.swift
//  Tifli
//
//  Created by zubair on 22/08/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

class GuideDetail: NSObject {
    
    var articleID: String = ""
    var articleCatID = ""
    var titleEng: String = ""
    var titleAr: String = ""
    var detailEng: String = ""
    var detailAr: String = ""
    var image: String = ""
    var autherName: String = ""
    var autherImage: String = ""
    var articleShareUrl: String = ""
    var arrLikes = [String]()
    var arrCommment = [Comment]()
    var totalComment = 0
    var totalLike = 0
    var isLiked = 0
    
    override init() {
        super.init()
    }
    
    init(withDictionary dictionarys: [String: JSON]) {
        super.init()
        
        if let dictionary: Dictionary<String, JSON> = dictionarys["data"]?.dictionaryValue {
            if let articleID = dictionary["articleId"]?.stringValue {
                self.articleID = articleID
            }
            if let articleCatID = dictionary["articleCategoryId"]?.stringValue {
                self.articleCatID = articleCatID
            }
            if let titleEng = dictionary["articleTitleEn"]?.stringValue {
                self.titleEng = titleEng
            }
            if let titleAr = dictionary["articleTitleAr"]?.stringValue {
                self.titleAr = titleAr
            }
            if let articleShareUrl = dictionary["articleShareUrl"]?.stringValue {
                self.articleShareUrl = articleShareUrl
            }
            if let detailEng = dictionary["articleDescriptionEn"]?.stringValue {
                self.detailEng = detailEng
            }
            if let detailAr = dictionary["articleDescriptionAr"]?.stringValue {
                self.detailAr = detailAr
            }
            if let image = dictionary["articleImage"]?.stringValue {
                self.image = image
            }
            if let autherName = dictionary["articleAuthorNameEn"]?.stringValue {
                self.autherName = autherName
            }
            if let autherImage = dictionary["articleAuthorImage"]?.stringValue {
                self.autherImage = autherImage
            }
            if let totalComment = dictionary["likes"]?.intValue {
                self.totalComment = totalComment
            }
            if let totalLike = dictionary["comments"]?.intValue {
                self.totalLike = totalLike
            }
        }
        if let isLiked = dictionarys["isLiked"]?.intValue {
            self.isLiked = isLiked
        }
        if let totalLike = dictionarys["likesCount"]?.intValue {
            self.totalLike = totalLike
        }
        if let totalComment = dictionarys["commentsCount"]?.intValue {
            self.totalComment = totalComment
        }
        if let value = dictionarys["likes"]?.arrayValue {
            for item in value {
                self.arrLikes.append(item.stringValue)
            }
        }
        if let value = dictionarys["comments"]?.arrayValue {
            for item in value {
                let obj = Comment.init(withDictionary: item.dictionaryValue)
                self.arrCommment.append(obj)
            }
        }
    }
}
