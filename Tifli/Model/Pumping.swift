//
//  Pumping.swift
//  Tifli
//
//  Created by zubair on 10/11/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

class Pumping: NSObject {
    
    var ID: String = ""
    var leftBreastDuration:String = ""
    var rightBreastDuration:String = ""
    var amount: String = ""
    var pumpingDate = ""
    var rightBreast = ""
    var leftBrest = ""
    var totalAmount = ""
    var pumpingLogNotes = ""
    var isPlay = false
    
    override init() {
        super.init()
    }
    
    init(withDictionary dictionary: [String: JSON]) {
        super.init()
        
        if let ID = dictionary["id"]?.stringValue {
            self.ID = ID
        }
        if let leftBreastDuration = dictionary["leftBreastAmount"]?.stringValue {
            self.leftBreastDuration = leftBreastDuration
        }
        if let rightBreastDuration = dictionary["rightBreastAmount"]?.stringValue {
            self.rightBreastDuration = rightBreastDuration
        }
        if let amount = dictionary["amount"]?.stringValue {
            self.amount = amount
        }
        if let pumpingDate = dictionary["pumping_date"]?.stringValue {
            self.pumpingDate = pumpingDate
        }
        if let rightBreast = dictionary["right_breast"]?.stringValue {
            self.rightBreast = rightBreast
        }
        if let leftBrest = dictionary["left_breast"]?.stringValue {
            self.leftBrest = leftBrest
        }
        if let totalAmount = dictionary["total_amount"]?.stringValue {
            self.totalAmount = totalAmount
        }
        if let pumpingLogNotes = dictionary["pumpingLogNotes"]?.stringValue {
            self.pumpingLogNotes = pumpingLogNotes
        }
    }
}
