//
//  Memory.swift
//  Tifli
//
//  Created by zubair on 29/11/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

class Memory: NSObject {
    
    var ID: String = ""
    var fileDuration:String = ""
    var memoryTitle:String = ""
    var albumTitle:String = ""
    var albumId: String = ""
    var memoryDetail:String = ""
    var babyId:String = ""
    var updatedAt: String = ""
    var memoryDate: String = ""
    var memoryFile:String = ""
    var createdAt:String = ""
    var fileType: String = ""
    var memoryFileThumbnail: String = ""
    var isSelect = false
    
    override init() {
        super.init()
    }
    
    init(withDictionary dictionary: [String: JSON]) {
        super.init()
        
        if let ID = dictionary["memoryId"]?.stringValue {
            self.ID = ID
        }
        if let fileDuration = dictionary["type"]?.stringValue {
            self.fileDuration = fileDuration
        }
        if let albumTitle = dictionary["memoryTitle"]?.stringValue {
            self.albumTitle = albumTitle
        }
        if let memoryTitle = dictionary["memoryTitle"]?.stringValue {
            self.memoryTitle = memoryTitle
        }
        if let albumId = dictionary["memoryAlbumId"]?.stringValue {
            self.albumId = albumId
        }
        if let memoryDetail = dictionary["memoryDetail"]?.stringValue {
            self.memoryDetail = memoryDetail
        }
        if let babyId = dictionary["memoryBabyId"]?.stringValue {
            self.babyId = babyId
        }
        if let updatedAt = dictionary["memoryModifiedAt"]?.stringValue {
            self.updatedAt = updatedAt
        }
        if let memoryDate = dictionary["memoryDate"]?.stringValue {
            self.memoryDate = memoryDate
        }
        if let memoryFile = dictionary["memoryFile"]?.stringValue {
            self.memoryFile = memoryFile
        }
        if let createdAt = dictionary["memoryCreatedAt"]?.stringValue {
            self.createdAt = createdAt
        }
        if let fileType = dictionary["memoryFileType"]?.stringValue {
            self.fileType = fileType
        }
        if let memoryFileThumbnail = dictionary["memoryFileThumbnail"]?.stringValue {
            self.memoryFileThumbnail = memoryFileThumbnail
        }
    }
}
