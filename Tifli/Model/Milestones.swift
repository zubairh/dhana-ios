//
//  Milestones.swift
//  Tifli
//
//  Created by zubair on 07/09/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

class Milestones: NSObject {
    
    var ID: String = ""
    var ageGroupId = ""
    var categoryId: String = ""
    var titleEn: String = ""
    var descriptionEn: String = ""
    var questionEn: String = ""
    var questionAr: String = ""
    var video: String = ""
    var redFlag: String = ""
    var babyId: String = ""
    var babyDate: String = ""
    var babyNotes: String = ""
    var babyMedia: String = ""
    var babyMediaType: String = ""
    var babyAnswer: String = ""
    var isAchieved: Bool = false
    var totalAchieve: Float = 0
    var total: Float = 0
    var isAnswerExapnd = false
    var imageMedia = UIImage()
    var audioData = Data()
    var isAudio = false
    var answer: String = ""
    var milestoneThumbnail = ""
    var milestoneVideo = ""
    var color = ""
    var isPlay = false
    
    var arrAgeGroup = [AgeGroup]()
    var arrCategory = [Categories]()
    var arrProgress = [Categories]()
    var arrMilestone = [Milestones]()
    var objSelectedCategory = Categories()
    var objSelectedAgeGroup = AgeGroup()
    var objPreviousAgeGroup = AgeGroup()
    var objNextAgeGroup = AgeGroup()
    
    override init() {
        super.init()
    }
    
    init(withDictionary dictionary: [String: JSON]) {
        super.init()
        
        if let dict: Dictionary<String, JSON> = dictionary["selectedCategory"]?.dictionaryValue {
            self.objSelectedCategory = Categories.init(withDictionary: dict)
        }
        if let dict: Dictionary<String, JSON> = dictionary["selectedAgeGroup"]?.dictionaryValue {
            self.objSelectedAgeGroup = AgeGroup.init(withDictionary: dict)
        }
        if let dict: Dictionary<String, JSON> = dictionary["nextAgeGroup"]?.dictionaryValue {
            self.objNextAgeGroup = AgeGroup.init(withDictionary: dict)
        }
        if let dict: Dictionary<String, JSON> = dictionary["previousAgeGroup"]?.dictionaryValue {
            self.objPreviousAgeGroup = AgeGroup.init(withDictionary: dict)
        }
        if let dict: Dictionary<String, JSON> = dictionary["overallCountStats"]?.dictionaryValue {
            if let totalAchieve = dict["achieved"]?.intValue {
                self.totalAchieve = Float(totalAchieve)
            }
            if let total = dict["total"]?.intValue {
                self.total = Float(total)
            }
        } else {
            if let total = dictionary["total_milestones"]?.intValue {
                self.total = Float(total)
            }
            if let totalAchieve = dictionary["achieved_milestones"]?.intValue {
                self.totalAchieve = Float(totalAchieve)
            }
        }
        if let ID = dictionary["milestoneId"]?.stringValue {
            self.ID = ID
        }
        if let ageGroupId = dictionary["milestoneAgeGroupId"]?.stringValue {
            self.ageGroupId = ageGroupId
        }
        if let categoryId = dictionary["milestoneCategoryId"]?.stringValue {
            self.categoryId = categoryId
        }
        if let titleEn = dictionary["milestoneTitleEn"]?.stringValue {
            self.titleEn = titleEn
        }
        if let descriptionEn = dictionary["milestoneDescriptionEn"]?.stringValue {
            self.descriptionEn = descriptionEn
        }
        if let questionEn = dictionary["milestoneQuestionEn"]?.stringValue {
            self.questionEn = questionEn
        }
        if let questionAr = dictionary["milestoneQuestionAr"]?.stringValue {
            self.questionAr = questionAr
        }
        if let video = dictionary["milestoneVideo"]?.stringValue {
            self.video = video
        }
        if let redFlag = dictionary["milestoneRedFlagEn"]?.stringValue {
            self.redFlag = redFlag
        }
        if let babyId = dictionary["babyMilestoneId"]?.stringValue {
            self.babyId = babyId
        }
        if let babyDate = dictionary["babyMilestoneDate"]?.stringValue {
            self.babyDate = babyDate
        }
        if let babyNotes = dictionary["babyMilestoneNotes"]?.stringValue {
            self.babyNotes = babyNotes
        }
        if let babyMedia = dictionary["babyMilestoneMedia"]?.stringValue {
            self.babyMedia = babyMedia
        }
        if let babyMediaType = dictionary["babyMilestoneMediaType"]?.stringValue {
            self.babyMediaType = babyMediaType
        }
        if let babyAnswer = dictionary["babyMilestoneAnswer"]?.stringValue {
            self.babyAnswer = babyAnswer
        }
        if let milestoneThumbnail = dictionary["milestoneThumbnail"]?.stringValue {
            self.milestoneThumbnail = milestoneThumbnail
        }
        if let milestoneVideo = dictionary["milestoneVideo"]?.stringValue {
            self.milestoneVideo = milestoneVideo
        }
        if let isAchieved = dictionary["isAchieved"]?.boolValue {
            self.isAchieved = isAchieved
        }
        if let color = dictionary["milestoneCategoryColor"]?.stringValue {
            self.color = color
        }
        
        if let value = dictionary["ageGroups"]?.arrayValue {
            for item in value {
                let obj = AgeGroup.init(withDictionary: item.dictionaryValue)
                self.arrAgeGroup.append(obj)
            }
        }
        if let value = dictionary["categories"]?.arrayValue {
            for item in value {
                let obj = Categories.init(withDictionary: item.dictionaryValue)
                self.arrCategory.append(obj)
            }
        } else {
            if let value = dictionary["achieved_milestones_list"]?.arrayValue {
                for item in value {
                    let obj = Categories.init(withDictionary: item.dictionaryValue)
                    self.arrCategory.append(obj)
                }
            }
        }
        if let value = dictionary["progress"]?.arrayValue {
            for item in value {
                let obj = Categories.init(withDictionary: item.dictionaryValue)
                self.arrProgress.append(obj)
            }
        }
        if let value = dictionary["milestones"]?.arrayValue {
            for item in value {
                let obj = Milestones.init(withDictionary: item.dictionaryValue)
                self.arrMilestone.append(obj)
            }
        } else {
            if let value = dictionary["categories_milestones"]?.arrayValue {
                for item in value {
                    let obj = Milestones.init(withDictionary: item.dictionaryValue)
                    self.arrMilestone.append(obj)
                }
            }
        }
        isAnswerExapnd = true
        self.answer = ""
    }
}
