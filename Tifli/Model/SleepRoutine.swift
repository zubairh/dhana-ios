//
//  SleepRoutine.swift
//  Tifli
//
//  Created by zubair on 08/12/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

class SleepRoutine: NSObject {
    
    var sleepDate: String = ""
    var dayTimeSleepTime:Int = 0
    var nightTimeSleepTime:Int = 0
    var total: Int = 0
    
    override init() {
        super.init()
    }
    
    init(withDictionary dictionary: [String: JSON]) {
        super.init()
        
        if let sleepDate = dictionary["sleep_date"]?.stringValue {
            self.sleepDate = sleepDate
        }
        if let dayTimeSleepTime = dictionary["day_time_sleep_seconds"]?.intValue {
            self.dayTimeSleepTime = dayTimeSleepTime
            total = dayTimeSleepTime
            self.dayTimeSleepTime = self.dayTimeSleepTime/3600
        }
        if let nightTimeSleepTime = dictionary["night_time_sleep_seconds"]?.intValue {
            self.nightTimeSleepTime = nightTimeSleepTime
            total = total+nightTimeSleepTime
            self.nightTimeSleepTime = self.nightTimeSleepTime/3600
        }
        total = total/3600
    }
}
