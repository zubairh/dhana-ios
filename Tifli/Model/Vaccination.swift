//
//  Vaccination.swift
//  Tifli
//
//  Created by zubair on 10/11/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

class Vaccination: NSObject {
    
    var vaccinationId: String = ""
    var titles = ""
    var titleAr: String = ""
    var ID: String = ""
    var detail:String = ""
    var clinic:String = ""
    var doctorName: String = ""
    var oneDayBeforeReminder = 0
    var oneHourBeforeReminder = 0
    var isPlay = false
    
    override init() {
        super.init()
    }
    
    init(withDictionary dictionary: [String: JSON]) {
        super.init()
        
        if let vaccinationId = dictionary["vaccinationId"]?.stringValue {
            self.vaccinationId = vaccinationId
        }
        if let titles = dictionary["title"]?.stringValue {
            self.titles = titles
        }
        if let titleAr = dictionary["titleAr"]?.stringValue {
            self.titleAr = titleAr
        }
        if let ID = dictionary["id"]?.stringValue {
            self.ID = ID
        }
        if let detail = dictionary["detail"]?.stringValue {
            self.detail = detail
        }
        if let clinic = dictionary["clinic"]?.stringValue {
            self.clinic = clinic
        }
        if let doctorName = dictionary["doctorName"]?.stringValue {
            self.doctorName = doctorName
        }
        if let oneDayBeforeReminder = dictionary["oneDayBeforeReminder"]?.intValue {
            self.oneDayBeforeReminder = oneDayBeforeReminder
        }
        if let oneHourBeforeReminder = dictionary["oneHourBeforeReminder"]?.intValue {
            self.oneHourBeforeReminder = oneHourBeforeReminder
        }
    }
}
