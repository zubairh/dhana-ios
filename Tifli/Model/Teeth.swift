//
//  Teeth.swift
//  Tifli
//
//  Created by zubair on 04/11/2021.
//  Copyright © 2021 Flattechs. All rights reserved.

import UIKit
import SwiftyJSON

class Teeth: NSObject {
    
    var ID: String = ""
    var code = ""
    var categories = ""
    var name = ""
    var image = ""
    var teethingId = ""
    var isHighlight = false
    
    override init() {
        super.init()
    }
    
    init(withDictionary dictionary: [String: JSON]) {
        super.init()
        
        if let ID = dictionary["id"]?.stringValue {
            self.ID = ID
        }
        if let code = dictionary["code"]?.stringValue {
            self.code = code
        }
        if let categories = dictionary["category"]?.stringValue {
            self.categories = categories
        }
        if let name = dictionary["name"]?.stringValue {
            self.name = name
        }
        if let image = dictionary["image"]?.stringValue {
            self.image = image
        }
        if let teethingId = dictionary["teethingId"]?.stringValue {
            self.teethingId = teethingId
        }
        if let isHighlight = dictionary["isHighlight"]?.boolValue {
            self.isHighlight = isHighlight
        }
    }
}
