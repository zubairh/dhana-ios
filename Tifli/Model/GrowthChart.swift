//
//  GrowthChart.swift
//  Tifli
//
//  Created by zubair on 05/09/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

class GrowthChart: NSObject {
    
    var day: String = ""
    var month = ""
    var lower: String = ""
    var actual: String = ""
    var higher:String = ""
    var totalLength:String = ""
    var totalWeight:String = ""
    var totalCircle:String = ""
    
    override init() {
        super.init()
    }
    
    init(withDictionary dictionary: [String: JSON]) {
        super.init()
        
        if let day = dictionary["day"]?.stringValue {
            self.day = day
        }
        if let month = dictionary["month"]?.stringValue {
            self.month = month
        }
        if let lower = dictionary["lower"]?.stringValue {
            self.lower = lower
        }
        if let actual = dictionary["actual"]?.stringValue {
            self.actual = actual
        }
        if let higher = dictionary["higher"]?.stringValue {
            self.higher = higher
        }
    }
}
