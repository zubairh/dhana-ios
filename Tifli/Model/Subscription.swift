//
//  Subscription.swift
//  Tifli
//
//  Created by zubair on 28/12/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

class Subscription: NSObject {
    
    var planId: String = ""
    var planDuration:String = ""
    var planPrice:String = ""
    var currentPlan: Int = 0
    
    override init() {
        super.init()
    }
    
    init(withDictionary dictionary: [String: JSON]) {
        super.init()
        
        if let planId = dictionary["planId"]?.stringValue {
            self.planId = planId
        }
        if let planDuration = dictionary["planDuration"]?.stringValue {
            self.planDuration = planDuration
        }
        if let planPrice = dictionary["planPrice"]?.stringValue {
            self.planPrice = planPrice
        }
        if let currentPlan = dictionary["currentPlan"]?.intValue {
            self.currentPlan = currentPlan
        }
    }
}
