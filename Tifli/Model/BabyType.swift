//
//  BabyType.swift
//  Tifli
//
//  Created by zubair on 25/08/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

class BabyType: NSObject {
    
    var ID: String = ""
    var typeTitle = ""
    var typeTitleAr: String = ""
    var notes: String = ""
    var phone: String = ""
    var email: String = ""
    var file: String = ""
    var isAdded = false
    
    override init() {
        super.init()
    }
    
    init(withDictionary dictionary: [String: JSON]) {
        super.init()
        
        if let ID = dictionary["id"]?.stringValue {
            self.ID = ID
        } else if let ID = dictionary["activityId"]?.stringValue {
            self.ID = ID
        } else if let ID = dictionary["medicationId"]?.stringValue {
            self.ID = ID
        }
        if let typeTitle = dictionary["title"]?.stringValue {
            self.typeTitle = typeTitle
        } else {
            if let typeTitle = dictionary["type"]?.stringValue {
                self.typeTitle = typeTitle
            } else {
                if let typeTitle = dictionary["name"]?.stringValue {
                    self.typeTitle = typeTitle
                } else {
                    if let typeTitle = dictionary["medicationName"]?.stringValue {
                        self.typeTitle = typeTitle
                    } else {
                        if let typeTitle = dictionary["activityTitle"]?.stringValue {
                            self.typeTitle = typeTitle
                        }
                    }
                }
            }
        }
        if let typeTitleAr = dictionary["title_ar"]?.stringValue {
            self.typeTitleAr = typeTitleAr
        } else {
            if let typeTitleAr = dictionary["type_ar"]?.stringValue {
                self.typeTitleAr = typeTitleAr
            }
        }
        if let notes = dictionary["notes"]?.stringValue {
            self.notes = notes
        }
        if let email = dictionary["email"]?.stringValue {
            self.email = email
        }
        if let phone = dictionary["phone"]?.stringValue {
            self.phone = phone
        }
        if let file = dictionary["file"]?.stringValue {
            self.file = file
        } else {
            if let file = dictionary["image"]?.stringValue {
                self.file = file
            }
        }
        self.isAdded = false
    }
}
