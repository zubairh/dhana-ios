//
//  Categories.swift
//  Tifli
//
//  Created by zubair on 16/08/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

class Categories: NSObject {
    
    var catID: String = ""
    var titleEn = ""
    var titleAr: String = ""
    var color: String = ""
    var babyMilestoneModifiedAt = ""
    var achieveCount = 0
    var totalCount = 0
    
    override init() {
        super.init()
    }
    
    init(withDictionary dictionary: [String: JSON]) {
        super.init()
        
        if let catID = dictionary["articleCategoryId"]?.stringValue {
            self.catID = catID
        } else if let catID = dictionary["milestoneCategoryId"]?.stringValue {
            self.catID = catID
        }
        if let titleEn = dictionary["articleCategoryTitleEn"]?.stringValue {
            self.titleEn = titleEn
        } else if let titleEn = dictionary["milestoneCategoryTitleEn"]?.stringValue {
            self.titleEn = titleEn
        } else if let titleEn = dictionary["title"]?.stringValue {
            self.titleEn = titleEn
        }
        if let titleAr = dictionary["articleCategoryTitleAr"]?.stringValue {
            self.titleAr = titleAr
        } else if let titleAr = dictionary["milestoneCategoryTitleAr"]?.stringValue {
            self.titleAr = titleAr
        }
        if let titleEn = dictionary["milestoneTitleEn"]?.stringValue {
            self.titleEn = titleEn
        }
        if let color = dictionary["milestoneCategoryColor"]?.stringValue {
            self.color = color
        }
        if let babyMilestoneModifiedAt = dictionary["babyMilestoneModifiedAt"]?.stringValue {
            self.babyMilestoneModifiedAt = babyMilestoneModifiedAt
        }
        if let achieveCount = dictionary["achievedCount"]?.intValue {
            self.achieveCount = achieveCount
        } else {
            if let achieveCount = dictionary["achieved_milestones"]?.intValue {
                self.achieveCount = achieveCount
            }
        }
        if let totalCount = dictionary["totalCount"]?.intValue {
            self.totalCount = totalCount
        } else {
            if let totalCount = dictionary["total_milestones"]?.intValue {
                self.totalCount = totalCount
            }
        }
    }
}
