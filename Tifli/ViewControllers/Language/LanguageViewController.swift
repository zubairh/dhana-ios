//
//  LanguageViewController.swift
//  Tifli
//
//  Created by zubair on 11/02/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import Localize_Swift
import IQKeyboardManagerSwift

class LanguageViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MMARK:- Functions
    func openWalkthrough() {
        let viewController = WalkthroughViewController(nibName:String(describing: WalkthroughViewController.self), bundle:nil)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    //MARK:- Button Actions
    @IBAction func arabicTapped(_ sender: Any) {
        Localize.setCurrentLanguage("ar")
        ISENGLISH = "ar"
        appSceneDelegate.language = "2"
        appSceneDelegate.isArabic = true
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = "Done".localized()
        UserDefaults.standard.set(true, forKey: LANGUAGE)
        if #available(iOS 9.0, *) {
            UIView.appearance().semanticContentAttribute = UISemanticContentAttribute.forceRightToLeft
        }
        appSceneDelegate.loadWalkthrough()
    }
    
    @IBAction func englishTapped(_ sender: Any) {
        Localize.setCurrentLanguage("en")
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = "Done".localized()
        ISENGLISH = "en"
        appSceneDelegate.language = "1"
        appSceneDelegate.isArabic = false
        UserDefaults.standard.set(false, forKey: LANGUAGE)
        
        if #available(iOS 9.0, *) {
            UIView.appearance().semanticContentAttribute = UISemanticContentAttribute.forceLeftToRight
        }
        appSceneDelegate.loadWalkthrough()
    }
    
}
