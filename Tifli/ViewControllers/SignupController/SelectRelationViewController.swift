//
//  SelectRelationViewController.swift
//  Tifli
//
//  Created by zubair on 12/07/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON
import RimhTypingLetters

class SelectRelationViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tfOther: UITextField!
    @IBOutlet weak var layoutHeightTable: NSLayoutConstraint!
    @IBOutlet weak var lablProgress: UILabel!
    @IBOutlet weak var imgBuble: UIImageView!
    @IBOutlet weak var lablHdr: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var btnNext: GradientButton!
    
    var arrData = [BabyType]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if appSceneDelegate.isArabic {
            tfOther.textAlignment = .right
        }
        
        btnNext.setTitle(self.btnNext.titleLabel?.text?.localized(), for: .normal)
        btnSkip.setTitle(self.btnSkip.titleLabel?.text?.localized(), for: .normal)
        btnBack.setTitle(self.btnBack.titleLabel?.text?.localized(), for: .normal)
        
        tfOther.delegate = self
        lablProgress.text = "10/16"
        imgBuble.alpha = 0
        self.perform(#selector(showLetterAnimation), with: nil, afterDelay: 1.0)
        
        tableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
        
        self.getData()
    }
    
    @objc func showLetterAnimation() {
        UIView.animate(withDuration: 1.0, animations: {() -> Void in
            self.imgBuble.alpha = 1.0
            self.lablHdr.typeText("What's your relationship to the baby?".localized()) {
                        }
        })
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        layoutHeightTable.constant = tableView.contentSize.height
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if tfOther.text?.count ?? 0 > 0 {
            for obj in self.arrData {
                obj.isAdded = false
            }
            tableView.reloadData()
        }
        return true
    }
    
    //MARK:- Apis Functions
    func getData() {
        self.view.endEditing(true)
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN]
        APIRequestUtil.getRelationList(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.arrData.removeAll()
                    
                    let valueArray: [JSON] = dict["relationships"]!.arrayValue
                    for dict in valueArray {
                        let dictValue: Dictionary<String, JSON> = dict.dictionaryValue
                        let obj = BabyType.init(withDictionary: dictValue)
                        self.arrData.append(obj)
                    }
                    self.tableView.reloadData()
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    //MARK:- Button Actions
    @IBAction func skipTapped(_ sender: Any) {
        let viewController = AddDOBViewController(nibName:String(describing: AddDOBViewController.self), bundle:nil)
        viewController.isFromBaby = true
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func nextTapped(_ sender: Any) {
        AppManager.shared.arrBabyRelation.removeAllObjects()
        for obj in self.arrData {
            if obj.isAdded {
                AppManager.shared.arrBabyRelation.add(obj.ID)
            }
        }
        if tfOther.text?.count ?? 0 > 0 {
            AppManager.shared.arrBabyRelation.removeAllObjects()
            AppManager.shared.arrBabyRelation.add("0")
            AppManager.shared.otherRelationValue = tfOther.text!
        }
        if AppManager.shared.arrBabyRelation.count == 0 {
            AppUtility.showInfoMessage(message: "Please select relationship".localized())
            return
        }
        let viewController = AddDOBViewController(nibName:String(describing: AddDOBViewController.self), bundle:nil)
        viewController.isFromBaby = true
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func skipAllTapped(_ sender: Any) {
        
    }
    
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func selectTypeTapped(_ sender:UIButton) {
        if self.arrData[sender.tag].isAdded {
            self.arrData[sender.tag].isAdded = false
            for obj in self.arrData {
                obj.isAdded = false
            }
        } else {
            for obj in self.arrData {
                obj.isAdded = false
            }
            self.arrData[sender.tag].isAdded = true
        }
        tableView.reloadData()
    }
    
}

// MARK: - Tableview Delegate-
extension SelectRelationViewController : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        tableView.register(UINib(nibName: "RelationCell", bundle: nil), forCellReuseIdentifier: "RelationCell")
        var cell : RelationCell! = tableView.dequeueReusableCell(withIdentifier: "RelationCell") as? RelationCell
        
        if (cell == nil) {
            cell = RelationCell.init(style: UITableViewCell.CellStyle.default,
                                          reuseIdentifier:"RelationCell");
        }
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        let obj = self.arrData[indexPath.row]
        
        cell.btnRelation.tag = indexPath.row
        cell.btnRelation.setTitle(obj.typeTitle, for: .normal)
        cell.btnRelation.addTarget(self, action: #selector(selectTypeTapped(_:)), for: .touchUpInside)
        
        if !obj.isAdded {
            cell.btnRelation.endColor = COLORS.TABBAR_COLOR
            cell.btnRelation.startColor = COLORS.APP_THEME_COLOR
        } else {
            cell.btnRelation.endColor = COLORS.BUTTON_SELECT_COLOR
            cell.btnRelation.startColor = COLORS.TABBAR_COLOR
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    }
}

