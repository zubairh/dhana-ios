//
//  AddBabyNameViewController.swift
//  Tifli
//
//  Created by zubair on 12/07/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import RimhTypingLetters

class AddBabyNameViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var lablTitle: UILabel!
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var lablProgress: UILabel!
    @IBOutlet weak var imgBuble: UIImageView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var btnNext: GradientButton!
    
    var isFromBaby = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if appSceneDelegate.isArabic {
            tfName.textAlignment = .right
        }
        
        btnNext.setTitle(self.btnNext.titleLabel?.text?.localized(), for: .normal)
        btnSkip.setTitle(self.btnSkip.titleLabel?.text?.localized(), for: .normal)
        btnBack.setTitle(self.btnBack.titleLabel?.text?.localized(), for: .normal)
        
        lablProgress.text = "7/16"
        imgBuble.alpha = 0
        self.perform(#selector(showLetterAnimation), with: nil, afterDelay: 1.0)
        
        if isFromBaby {
            lablProgress.text = "12/16"
            tfName.keyboardType = .numberPad
            tfName.delegate = self
        }
    }
    
    @objc func showLetterAnimation() {
        UIView.animate(withDuration: 1.0, animations: {() -> Void in
            self.imgBuble.alpha = 1.0
            if self.isFromBaby {
                self.lablTitle.typeText("Weeks of pregnancy?".localized()) {
                        }
            } else {
                self.lablTitle.typeText("What's the baby name?".localized()) {
                        }
            }
        })
    }
    
    //MARK: Textfiled Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == tfName
        {
            if self.isFromBaby {
                
                if (tfName.text?.count)! >= 2 && range.length == 0
                {
                    return false
                }
                else
                {
                    //self .perform(#selector(checkTF), with: nil, afterDelay: 0.1)
                    return true
                }
            }
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == tfName {
            if textField.text?.count ?? 0 > 0 {
                if self.isFromBaby {
                    let tt = Int(tfName.text ?? "")
                    if tt ?? 0 < 24 || tt ?? 0 > 42 {
                        tfName.text = ""
                        AppUtility.showInfoMessage(message: "Please enter weeks value between 24 to 42".localized())
                    }
                }
            }
        }
    }
    
    //MARK: - Functions
    @objc func checkTF() {
        if tfName.text?.count == 2 {
            self.view.endEditing(true)
        }
    }
    
    //MARK:- Button Actions
    @IBAction func skipTapped(_ sender: Any) {
        if isFromBaby {
            let viewController = CheckMedicalViewController(nibName:String(describing: CheckMedicalViewController.self), bundle:nil)
            self.navigationController?.pushViewController(viewController, animated: true)
        } else {
            if tfName.text?.count == 0 {
                AppUtility.showInfoMessage(message: "please enter baby name".localized())
                return
            }
            AppManager.shared.babyName = tfName.text!
            let viewController = ChooseBabyViewController(nibName:String(describing: ChooseBabyViewController.self), bundle:nil)
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    @IBAction func nextTapped(_ sender: Any) {
        if tfName.text?.count == 0 {
            if isFromBaby {
                AppUtility.showInfoMessage(message: "please enter weeks of pregnancy".localized())
            } else {
                AppUtility.showInfoMessage(message: "please enter baby name".localized())
            }
            return
        }
        if self.isFromBaby {
            let tt = Int(tfName.text ?? "")
            if tt ?? 0 < 24 || tt ?? 0 > 42 {
                tfName.text = ""
                AppUtility.showInfoMessage(message: "Please enter weeks value between 24 to 42".localized())
                return
            }
        }
        if isFromBaby {
            AppManager.shared.babyWeekPregnancy = tfName.text!
            let viewController = CheckMedicalViewController(nibName:String(describing: CheckMedicalViewController.self), bundle:nil)
            self.navigationController?.pushViewController(viewController, animated: true)
        } else {
            AppManager.shared.babyName = tfName.text!
            let viewController = ChooseBabyViewController(nibName:String(describing: ChooseBabyViewController.self), bundle:nil)
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    @IBAction func skipAllTapped(_ sender: Any) {
        
    }
    
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}
