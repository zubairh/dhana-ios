//
//  AddBabyCountViewController.swift
//  Tifli
//
//  Created by zubair on 11/07/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON
import RimhTypingLetters

class AddBabyCountViewController: UIViewController {

    @IBOutlet weak var lablCount: UILabel!
    @IBOutlet weak var lablProgress: UILabel!
    @IBOutlet weak var imgBuble: UIImageView!
    @IBOutlet weak var lablHdr: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var btnNext: GradientButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lablProgress.text = "6/16"
        
        btnNext.setTitle(self.btnNext.titleLabel?.text?.localized(), for: .normal)
        btnBack.setTitle(self.btnBack.titleLabel?.text?.localized(), for: .normal)
        btnSkip.setTitle(self.btnSkip.titleLabel?.text?.localized(), for: .normal)
        
        imgBuble.alpha = 0
        self.perform(#selector(showLetterAnimation), with: nil, afterDelay: 1.0)
    }
    
    @objc func showLetterAnimation() {
        UIView.animate(withDuration: 1.0, animations: {() -> Void in
            self.imgBuble.alpha = 1.0
            self.lablHdr.typeText("How many kids do you have?".localized()) {
                    }
        })
    }

    //MARK:- Apis Functions
    func updateProfile() {
        self.view.endEditing(true)
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "kidsCount":lablCount.text!, "userToken":AppUtility.getUserToken(), "preference":AppUtility.getUserLoggedIn().preference]
        APIRequestUtil.updateProfile(parameters: paramDict) { (result, error) in
            if(result != nil) {
                
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    USER_DEFAUTS.set(result ?? "", forKey: USER_LOGIN)
                    USER_DEFAUTS.synchronize()
                    
                    let viewController = AddBabyNameViewController(nibName:String(describing: AddBabyNameViewController.self), bundle:nil)
                    self.navigationController?.pushViewController(viewController, animated: true)
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    //MARK:- Button Actions
    @IBAction func skipTapped(_ sender: Any) {
        let viewController = AddBabyNameViewController(nibName:String(describing: AddBabyNameViewController.self), bundle:nil)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func nextTapped(_ sender: Any) {
        self.updateProfile()
    }
    
    @IBAction func skipAllTapped(_ sender: Any) {
        
    }
    
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func minusTapped(_ sender: Any) {
        var total = Int(lablCount.text!)
        if total! > 1 {
            total = total!-1
        }
        self.lablCount.text = String(total!)
        self.lablCount.popIn()
    }
    
    @IBAction func plusTapped(_ sender: Any) {
        var total = Int(lablCount.text!)
        if total! < 100 {
            total = total!+1
        }
        self.lablCount.text = String(total!)
        self.lablCount.popIn()
    }
    
}
