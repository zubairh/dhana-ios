//
//  MobileLoginViewController.swift
//  Tifli
//
//  Created by zubair on 11/07/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON
import SKCountryPicker
import RimhTypingLetters

class MobileLoginViewController: UIViewController {

    @IBOutlet weak var tfNumbers: UITextField!
    @IBOutlet weak var lablCode: UILabel!
    @IBOutlet weak var lablProgress: UILabel!
    @IBOutlet weak var imgBuble: UIImageView!
    @IBOutlet weak var lablHdr: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnSendOtp: GradientButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lablProgress.text = "2/16"
        
        if appSceneDelegate.isArabic {
         
            tfNumbers.textAlignment = .right
        }
        
        btnSendOtp.setTitle(self.btnSendOtp.titleLabel?.text?.localized(), for: .normal)
        btnBack.setTitle(self.btnBack.titleLabel?.text?.localized(), for: .normal)
        
        guard let country = CountryManager.shared.currentCountry else {
            return
        }
        
        imgBuble.alpha = 0
        self.perform(#selector(showLetterAnimation), with: nil, afterDelay: 1.0)
        
        lablCode.text = country.dialingCode
    }
    
    @objc func showLetterAnimation() {
        UIView.animate(withDuration: 1.0, animations: {() -> Void in
            self.imgBuble.alpha = 1.0
            self.lablHdr.typeText("What's your mobile number?".localized()) {
                }
        })
    }
    
    func CheckFields() -> Bool {
       
        if (tfNumbers.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            AppUtility.showInfoMessage(message: "Please enter valid phone".localized())
            let test = false
            return test
        }
        
        let test = true
        return test
    }

    //MARK:- Apis Functions
    func signUp(paramDict:[String: Any]) {
        
        self.view.endEditing(true)
        APIRequestUtil.SignUp(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    USER_DEFAUTS.set(result ?? "", forKey: USER_LOGIN)
                    USER_DEFAUTS.synchronize()
                    
                    let viewController = OTPViewController(nibName:String(describing: OTPViewController.self), bundle:nil)
                    viewController.prefrence = "phone"
                    viewController.isFromOther = true
                    self.navigationController?.pushViewController(viewController, animated: true)
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    //MARK:- Button Actions
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func sendOTPTapped(_ sender: Any) {
        if CheckFields() {
            let code = lablCode.text!
            let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userPhone":String(code.dropFirst())+tfNumbers.text!, "preference":"phone"]
            self.signUp(paramDict: paramDict)
        }
    }
    
    @IBAction func codeTapped(_ sender: Any) {
        presentCountryPickerScene(withSelectionControlEnabled: true)
    }
    
}

private extension MobileLoginViewController {
    
    func presentCountryPickerScene(withSelectionControlEnabled selectionControlEnabled: Bool = true) {
        switch selectionControlEnabled {
        case true:
            // Present country picker with `Section Control` enabled
            let countryController = CountryPickerWithSectionViewController.presentController(on: self) { [weak self] (country: Country) in
                
                guard let self = self else { return }
                
                self.lablCode.text = country.dialingCode
            }
            
            countryController.flagStyle = .circular
            countryController.favoriteCountriesLocaleIdentifiers = ["IN", "US"]
        case false:
            // Present country picker without `Section Control` enabled
            let countryController = CountryPickerController.presentController(on: self) { [weak self] (country: Country) in
                
                guard let self = self else { return }
                
                self.lablCode.text = country.dialingCode
            }
            
            countryController.flagStyle = .corner
            countryController.favoriteCountriesLocaleIdentifiers = ["IN", "US"]
        }
    }
}
