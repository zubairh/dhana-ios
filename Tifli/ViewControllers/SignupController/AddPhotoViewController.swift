//
//  AddPhotoViewController.swift
//  Tifli
//
//  Created by zubair on 11/07/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON
import RimhTypingLetters

class AddPhotoViewController: UIViewController {

    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lablTitle: UILabel!
    @IBOutlet weak var lablStep: UILabel!
    @IBOutlet weak var progress: UIProgressView!
    @IBOutlet weak var lablProgress: UILabel!
    @IBOutlet weak var tfUsername: UITextField!
    @IBOutlet weak var viewUserName: UIView!
    @IBOutlet weak var imgBuble: UIImageView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var btnNext: GradientButton!
    
    var isFromBaby = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imgProfile.layer.cornerRadius = imgProfile.frame.width/2
        imgProfile.clipsToBounds = true
        
        lablProgress.text = "4/16"
        
        imgBuble.alpha = 0
        self.perform(#selector(showLetterAnimation), with: nil, afterDelay: 1.0)
        
        if isFromBaby {
            viewUserName.isHidden = true
            lablProgress.text = "9/16"
            progress.setProgress(0.7, animated: true)
        }
        
        if appSceneDelegate.isArabic {
            tfUsername.textAlignment = .right
        }
        
        btnBack.setTitle(self.btnBack.titleLabel?.text?.localized(), for: .normal)
        btnSkip.setTitle(self.btnSkip.titleLabel?.text?.localized(), for: .normal)
        btnNext.setTitle(self.btnNext.titleLabel?.text?.localized(), for: .normal)
    }
    
    @objc func showLetterAnimation() {
        UIView.animate(withDuration: 1.0, animations: {() -> Void in
            self.imgBuble.alpha = 1.0
            if self.isFromBaby {
                self.lablTitle.typeText("Upload a profile picture of your baby".localized()) {
                }
            } else {
                self.lablTitle.typeText("You're registered now! upload a profile picture now :)".localized()) {
                }
            }
        })
    }

    //MARK:- Apis Functions
    func uploadPhoto() {
        self.view.endEditing(true)
        
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "userId":AppUtility.getUserLoggedIn().userID, "preference":AppUtility.getUserLoggedIn().preference, "userName":tfUsername.text!]
        let data = imgProfile.image?.jpegData(compressionQuality: 0.5)
        APIRequestUtil.uploadPhoto(parameters: paramDict, data: data ?? Data(), extensionString: "image/jpeg", fileNameKey: "user_profile_image") { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    USER_DEFAUTS.set(result ?? "", forKey: USER_LOGIN)
                    USER_DEFAUTS.synchronize()
                    
                    let viewController = AddDOBViewController(nibName:String(describing: AddDOBViewController.self), bundle:nil)
                    self.navigationController?.pushViewController(viewController, animated: true)
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    //MARK:- Button Action
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func skipTapped(_ sender: Any) {
        
    }
    
    @IBAction func skipThisTapped(_ sender: Any) {
        if self.isFromBaby {
            let viewController = SelectRelationViewController(nibName:String(describing: SelectRelationViewController.self), bundle:nil)
            //AppManager.shared.babyImage = self.imgProfile.image
            self.navigationController?.pushViewController(viewController, animated: true)
        } else {
            let viewController = AddDOBViewController(nibName:String(describing: AddDOBViewController.self), bundle:nil)
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    @IBAction func nextTapped(_ sender: Any) {
        if imgProfile.image?.size.width == 0 || imgProfile.image == nil {
            AppUtility.showInfoMessage(message: "please set your profile photo first".localized())
            return
        }
        if self.isFromBaby {
            let viewController = SelectRelationViewController(nibName:String(describing: SelectRelationViewController.self), bundle:nil)
            AppManager.shared.babyImage = self.imgProfile.image
            self.navigationController?.pushViewController(viewController, animated: true)
        } else {
            self.uploadPhoto()
        }
    }
    
    @IBAction func uploadTapped(_ sender: Any) {
        ImagePickerManager().pickImage(self) { image in
            self.imgProfile.image = image
        }
    }
}
