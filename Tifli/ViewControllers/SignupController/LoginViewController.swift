//
//  LoginViewController.swift
//  Dhana
//
//  Created by zubair on 17/03/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON
import GoogleSignIn
import AuthenticationServices
import FBSDKLoginKit
import SKCountryPicker

class LoginViewController: UIViewController {

    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var lablCode: UILabel!
    @IBOutlet weak var tfNumbers: UITextField!
    @IBOutlet weak var viewLogin: UIView!
    @IBOutlet weak var viewNumber: UIView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var viewApple: UIView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lablMobileNumbr: UILabel!
    @IBOutlet weak var lablEmail: UILabel!
    @IBOutlet weak var lablPassword: UILabel!
    @IBOutlet weak var btnOtp: GradientButton!
    @IBOutlet weak var btnForgot: UIButton!
    @IBOutlet weak var btnLLogin: GradientButton!
    @IBOutlet weak var lablGmail: UILabel!
    @IBOutlet weak var lablFb: UILabel!
    @IBOutlet weak var btnDontAccount: UIButton!
    
    var email = ""
    var fbID = ""
    var googleID = ""
    var name = ""
    var socialType = "0"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewNumber.isHidden = true
        
        let titleTextAttributesNormal = [NSAttributedString.Key.foregroundColor: UIColor.lightGray]
        let titleTextAttributesSelected = [NSAttributedString.Key.foregroundColor: UIColor.white]
        segmentControl.setTitleTextAttributes(titleTextAttributesNormal, for: .normal)
        segmentControl.setTitleTextAttributes(titleTextAttributesSelected, for: .selected)
        
        segmentControl.setTitleColor(.lightGray, state: .normal)
        segmentControl.setTitleColor(.white, state: .selected)
        segmentControl.setTitleFont(UIFont(name: "OpenSans-SemiBold", size: 13)!)
        
        self.segmentControl.layer.cornerRadius = 20.0
        self.segmentControl.layer.masksToBounds = true
        
        segmentControl.setTitle("Email".localized(), forSegmentAt: 0)
        segmentControl.setTitle("Mobile".localized(), forSegmentAt: 1)
        
        if appSceneDelegate.isArabic {
            btnBack.setImage(UIImage.init(named: BACK_AR), for: .normal)
            tfEmail.textAlignment = .right
            tfNumbers.textAlignment = .right
            tfPassword.textAlignment = .right
        }
        lablMobileNumbr.text = lablMobileNumbr.text?.localized()
        lablEmail.text = lablEmail.text?.localized()
        lablPassword.text = lablPassword.text?.localized()
        lablGmail.text = lablGmail.text?.localized()
        lablFb.text = lablFb.text?.localized()
        
        btnLLogin.setTitle(self.btnLLogin.titleLabel?.text?.localized(), for: .normal)
        btnOtp.setTitle(self.btnOtp.titleLabel?.text?.localized(), for: .normal)
        btnDontAccount.setTitle(self.btnDontAccount.titleLabel?.text?.localized(), for: .normal)
        btnLLogin.setTitle(self.btnLLogin.titleLabel?.text?.localized(), for: .normal)
        
        guard let country = CountryManager.shared.currentCountry else {
            return
        }
        
        lablCode.text = country.dialingCode
        
        appDelegate.delay(0.5, closure: {
//            self.checkStatusOfAppleSignIn()
//            self.setupAppleSignInButton()
        })
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name(NOTIFICATION_GETGOOGLE_INFO), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    func CheckFields() -> Bool {
        if self.segmentControl.selectedSegmentIndex == 0 {
            if (tfEmail.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
                
                AppUtility.showInfoMessage(message: "Please enter valid Email".localized())
                let test = false
                return test
            }
            else if !(AppUtility.isValidEmail(testStr:tfEmail.text!)) {
                AppUtility.showInfoMessage(message: "Please enter valid Email".localized())
                let test = false
                return test
            }
            else if (tfPassword.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
                AppUtility.showInfoMessage(message: "Please enter password".localized())
                let test = false
                return test
            }
        } else {
            if (tfNumbers.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
                
                AppUtility.showInfoMessage(message: "Please enter valid phone".localized())
                let test = false
                return test
            }
        }
        
        let test = true
        return test
    }
    
    //MARK:- apple setup Functions
    func checkStatusOfAppleSignIn() {
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        appleIDProvider.getCredentialState(forUserID: self.fbID) { (credentialState, error) in
            
            switch credentialState {
            case .authorized:
                self.setupUserInfoAndOpenView()
                break
            default:
                break
            }
        }
    }
    
    func setupAppleSignInButton() {
        let objASAuthorizationAppleIDButton = ASAuthorizationAppleIDButton(type: .signIn, style: .white)
        objASAuthorizationAppleIDButton.translatesAutoresizingMaskIntoConstraints = true
        objASAuthorizationAppleIDButton.frame = viewApple.bounds
        objASAuthorizationAppleIDButton.addTarget(self, action: #selector(actionHandleAppleSignin), for: .touchUpInside)
        self.viewApple.addSubview(objASAuthorizationAppleIDButton)
    }
    
    @objc func actionHandleAppleSignin() {
       
        self.socialType = "4"
        
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }
    
    //MARK:- Apis Functions
    func login(paramDict:[String: Any]) {
        
        self.view.endEditing(true)
        APIRequestUtil.Login(parameters: paramDict) { (result, error) in
            if(result != nil) {
                
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    if self.segmentControl.selectedSegmentIndex == 0 {
                        AppUtility.saveUserToken(token: dict["userToken"]!.stringValue)
                    }
                    
                    USER_DEFAUTS.set(result ?? "", forKey: USER_LOGIN)
                    USER_DEFAUTS.synchronize()
                    
                    if self.segmentControl.selectedSegmentIndex == 0 {
                        if AppUtility.getUserLoggedIn().isEmailVerified == "1" || AppUtility.getUserLoggedIn().isPhoneVerified == "1" {
                            if AppManager.shared.planId == "0" {
                                appSceneDelegate.loadTab()
                            } else {
                                let viewController = ConfirmSubscriptionVC(nibName:String(describing: ConfirmSubscriptionVC.self), bundle:nil)
                                self.navigationController?.pushViewController(viewController, animated: true)
                            }
                        } else {
                            let viewController = OTPViewController(nibName:String(describing: OTPViewController.self), bundle:nil)
                            viewController.prefrence = "email"
                            self.navigationController?.pushViewController(viewController, animated: true)
                        }
                    } else {
                        let viewController = OTPViewController(nibName:String(describing: OTPViewController.self), bundle:nil)
                        viewController.prefrence = "phone"
                        self.navigationController?.pushViewController(viewController, animated: true)
                    }
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    //MARK:- Apis Functions
    func signUp(paramDict:[String: Any]) {
        
        self.view.endEditing(true)
        APIRequestUtil.SignUp(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    USER_DEFAUTS.set(result ?? "", forKey: USER_LOGIN)
                    USER_DEFAUTS.synchronize()
                    
                    if self.socialType != "0" {
                        if AppUtility.getUserLoggedIn().isEmailVerified == "1" || AppUtility.getUserLoggedIn().isPhoneVerified == "1" {
                            AppUtility.saveUserToken(token: dict["userToken"]!.stringValue)
                            if AppUtility.getUserLoggedIn().userImage.count > 0 && AppUtility.getUserLoggedIn().dob.count > 0 {
                                appSceneDelegate.loadTab()
                            } else {
                                let viewController = AddPhotoViewController(nibName:String(describing: AddPhotoViewController.self), bundle:nil)
                                self.navigationController?.pushViewController(viewController, animated: true)
                            }
                        } else {
                            let viewController = OTPViewController(nibName:String(describing: OTPViewController.self), bundle:nil)
                            viewController.prefrence = "email"
                            viewController.isFromOther = true
                            self.navigationController?.pushViewController(viewController, animated: true)
                        }
                    } else {
                        let viewController = OTPViewController(nibName:String(describing: OTPViewController.self), bundle:nil)
                        if self.segmentControl.selectedSegmentIndex == 0 {
                            viewController.prefrence = "email"
                        } else {
                            viewController.prefrence = "phone"
                        }
                        viewController.isFromOther = true
                        self.navigationController?.pushViewController(viewController, animated: true)
                    }
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    //MARK:- Fb Apis Functions
    func getFBUserData() {
        if((AccessToken.current) != nil) {
        GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
          if (error == nil) {

            let info = result as! [String : AnyObject]
            self.email = info["email"] as? String ?? ""
            self.name = info["name"] as? String ?? ""
            self.fbID = info["id"] as? String ?? ""
            self.socialType = "3"
            
            let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "email":self.email, "password":self.fbID, "preference":"email", "social_type":self.socialType]
            self.signUp(paramDict: paramDict)
          }
        })
      }
    }
    
    func loginManagerDidComplete(_ result: LoginResult) {
      switch result {
      case .cancelled:
        print("cancel")
      case .failed(let error):
        print(error)
      case .success(let _, _, _):
        self.getFBUserData()
      }
    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        let objUser = notification.object as! GIDGoogleUser
        self.email = objUser.profile.email
        self.name = objUser.profile.name
        self.fbID = objUser.userID
        self.socialType = "1"
        
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "email":email, "password":fbID, "preference":"email", "social_type":socialType, "device_type":"APPLE", "device_token":appDelegate.deviceToken]
        self.signUp(paramDict: paramDict)
        
    }
    
    //MARK:- Button Actions
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func forgotPasswordTapped(_ sender: Any) {
        let viewController = ForgotVC(nibName:String(describing: ForgotVC.self), bundle:nil)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func signInTapped(_ sender: Any) {
        if CheckFields() {
            let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "email":tfEmail.text!, "password":tfPassword.text!, "preference":"email", "device_type":"APPLE", "device_token":appDelegate.deviceToken]
            self.login(paramDict: paramDict)
        }
    }
    
    @IBAction func showPasswordTapped(_ sender: UIButton) {
        if sender.tag == 0 {
            sender.tag = 1
            tfPassword.isSecureTextEntry = false
        } else {
            sender.tag = 0
            tfPassword.isSecureTextEntry = true
        }
    }
    
    @IBAction func sendOtpTapped(_ sender: Any) {
        if CheckFields() {
            let code = lablCode.text!
            let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userPhone":String(code.dropFirst())+tfNumbers.text!, "preference":"phone", "device_type":"APPLE", "device_token":appDelegate.deviceToken]
            self.login(paramDict: paramDict)
        }
    }
    
    @IBAction func signupTapped(_ sender: Any) {
        let viewController = RegisterViewController(nibName:String(describing: RegisterViewController.self), bundle:nil)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func segmentTapped(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            self.viewLogin.isHidden = false
            self.viewNumber.isHidden = true
        } else {
            self.viewLogin.isHidden = true
            self.viewNumber.isHidden = false
        }
    }
    
    @IBAction func googleTapped(_ sender: Any) {
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func fbTapped(_ sender: Any) {
        let loginManager = LoginManager()
        loginManager.logIn(permissions: [.publicProfile, .email], viewController: self) { result in
          self.loginManagerDidComplete(result)
        }
    }
    
    @IBAction func instagramTapped(_ sender: Any) {
       
    }
    
    @IBAction func codeTapped(_ sender: Any) {
        presentCountryPickerScene(withSelectionControlEnabled: true)
    }
}


extension LoginViewController : ASAuthorizationControllerDelegate
{
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization)
    {
        switch authorization.credential {
            
        case let credentials as ASAuthorizationAppleIDCredential:
            DispatchQueue.main.async {
                
                if "\(credentials.user)" != "" {

                    self.fbID = credentials.user
                }
                if credentials.email != nil {

                    self.email = credentials.email!
                }
                if credentials.fullName!.givenName != nil {

                    self.name = credentials.fullName!.givenName!
                }
                self.setupUserInfoAndOpenView()
            }
            
        case let credentials as ASPasswordCredential:
            DispatchQueue.main.async {
                self.setupUserInfoAndOpenView()
            }
            
        default :
            let alert: UIAlertController = UIAlertController(title: "Apple Sign In", message: "Something went wrong with your Apple Sign In!", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok".localized(), style: .default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            break
        }
    }
    
    func setupUserInfoAndOpenView() {
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "email":email, "password":fbID, "preference":"email", "social_type":socialType, "device_type":"APPLE", "device_token":appDelegate.deviceToken]
        self.signUp(paramDict: paramDict)
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        
        let alert: UIAlertController = UIAlertController(title: "Error".localized(), message: "\(error.localizedDescription)", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok".localized(), style: .default, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
}

extension LoginViewController : ASAuthorizationControllerPresentationContextProviding
{
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor
    {
        return view.window!
    }
}

/// MARK: - Private Methods
private extension LoginViewController {
    
    /// Dynamically presents country picker scene with an option of including `Selection Control`.
    ///
    /// By default, invoking this function without defining `selectionControlEnabled` parameter. Its set to `True`
    /// unless otherwise and the `Selection Control` will be included into the list view.
    ///
    /// - Parameter selectionControlEnabled: Section Control State. By default its set to `True` unless otherwise.
    
    func presentCountryPickerScene(withSelectionControlEnabled selectionControlEnabled: Bool = true) {
        switch selectionControlEnabled {
        case true:
            // Present country picker with `Section Control` enabled
            let countryController = CountryPickerWithSectionViewController.presentController(on: self) { [weak self] (country: Country) in
                
                guard let self = self else { return }
                
                self.lablCode.text = country.dialingCode
            }
            
            countryController.flagStyle = .circular
            countryController.favoriteCountriesLocaleIdentifiers = ["IN", "US"]
        case false:
            // Present country picker without `Section Control` enabled
            let countryController = CountryPickerController.presentController(on: self) { [weak self] (country: Country) in
                
                guard let self = self else { return }
                
                self.lablCode.text = country.dialingCode
            }
            
            countryController.flagStyle = .corner
            countryController.favoriteCountriesLocaleIdentifiers = ["IN", "US"]
        }
    }
}
