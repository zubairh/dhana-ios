//
//  AddBabyViewController.swift
//  Dhana
//
//  Created by zubair on 17/03/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import RimhTypingLetters

class AddBabyViewController: UIViewController {
    
    @IBOutlet weak var lablProgress: UILabel!
    @IBOutlet weak var imgBuble: UIImageView!
    @IBOutlet weak var lablHdr: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnEmail: NSLayoutConstraint!
    @IBOutlet weak var btnMobile: GradientButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnBack.setTitle(self.btnBack.titleLabel?.text?.localized(), for: .normal)
        btnMobile.setTitle(self.btnMobile.titleLabel?.text?.localized(), for: .normal)
        
        lablProgress.text = "1/16"
        imgBuble.alpha = 0
        self.perform(#selector(showLetterAnimation), with: nil, afterDelay: 1.0)
    }
    
    @objc func showLetterAnimation()
    {
        UIView.animate(withDuration: 1.0, animations: {() -> Void in
            self.imgBuble.alpha = 1.0
            
            self.lablHdr.typeText("Hello! Choose your prefrence to register".localized()) {
            }
        })
    }
    
    //MARK:- Button Actions
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func emailTapped(_ sender: Any) {
        let viewController = RegisterEmailVC(nibName:String(describing: RegisterEmailVC.self), bundle:nil)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func phoneTapped(_ sender: Any) {
        let viewController = MobileLoginViewController(nibName:String(describing: MobileLoginViewController.self), bundle:nil)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}
