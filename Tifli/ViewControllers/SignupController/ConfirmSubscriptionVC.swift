//
//  ConfirmSubscriptionVC.swift
//  Tifli
//
//  Created by zubair on 28/12/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

class ConfirmSubscriptionVC: UIViewController {

    @IBOutlet weak var collectionSubscribe: UICollectionView!
    @IBOutlet weak var lablSubcriptionTitle: UILabel!
    @IBOutlet weak var btnCheckout: GradientButton!
    @IBOutlet weak var btnSkip: UIButton!
    
    var arrSubscription = [Subscription]()
    var selectedIndex = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.getSubscriptionList()
       
        lablSubcriptionTitle.text = lablSubcriptionTitle.text?.localized()
       
        btnCheckout.setTitle(self.btnCheckout.titleLabel?.text?.localized(), for: .normal)
        btnSkip.setTitle(self.btnSkip.titleLabel?.text?.localized(), for: .normal)
    }
    
    //MARK:- Apis Function
    func getSubscriptionList() {
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN]
        APIRequestUtil.getSubscription(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.arrSubscription.removeAll()
                    
                    //let dictionary: Dictionary<String, JSON> = dict["data"]!.dictionaryValue
                    let valueArray: [JSON] = dict["data"]!.arrayValue
                    var i = 0
                    for dict in valueArray {
                        let dictValue: Dictionary<String, JSON> = dict.dictionaryValue
                        let obj = Subscription.init(withDictionary: dictValue)
                        i = i+1
                        if obj.planId == AppManager.shared.planId {
                            self.selectedIndex = i
                        }
                        self.arrSubscription.append(obj)
                    }
                    self.collectionSubscribe.reloadData()
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    func buySubscriptionList() {
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "planId":AppManager.shared.planId]
        APIRequestUtil.buySubscription(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    AppManager.shared.planId = "0"
                    AppUtility.showSuccessMessage(message: message ?? "subscription buy successfully".localized())
                    appSceneDelegate.loadTab()
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    //MARK:- Button Actions
    @IBAction func checkoutTapped(_ sender: Any) {
        self.buySubscriptionList()
    }
    
    @IBAction func skipTapped(_ sender: Any) {
        appSceneDelegate.loadTab()
    }
    
}

extension ConfirmSubscriptionVC: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrSubscription.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return  CGSize(width: collectionSubscribe.bounds.width/3, height: collectionSubscribe.bounds.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        collectionView.register(UINib(nibName: String(describing: PackageCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: PackageCell.self))
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: PackageCell.self), for: indexPath as IndexPath) as! PackageCell
        
        let obj = self.arrSubscription[indexPath.row]
        
        cell.lablDuration.text = obj.planDuration
        cell.lablPrice.text = obj.planPrice
        cell.lablPerMonth.text = "per".localized()+" "+obj.planDuration
        
        if self.selectedIndex == indexPath.row {
            cell.viewOuter.isHidden = false
            cell.viewInner.backgroundColor = #colorLiteral(red: 0.9843137255, green: 0.2901960784, blue: 0.2941176471, alpha: 1)
            cell.lablDuration.textColor = .white
            cell.lablPrice.textColor = .white
            cell.lablPerMonth.textColor = .white
        } else {
            cell.viewOuter.isHidden = true
            cell.viewInner.backgroundColor = #colorLiteral(red: 0.831372549, green: 0.8156862745, blue: 0.8941176471, alpha: 1)
            cell.lablDuration.textColor = .black
            cell.lablPrice.textColor = .black
            cell.lablPerMonth.textColor = .black
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        collectionSubscribe.reloadData()
        AppManager.shared.planId = self.arrSubscription[indexPath.row].planId
    }
    
}
