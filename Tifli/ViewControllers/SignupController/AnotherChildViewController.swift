//
//  AnotherChildViewController.swift
//  Tifli
//
//  Created by zubair on 12/07/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON
import RimhTypingLetters

class AnotherChildViewController: UIViewController {
    
    @IBOutlet weak var lablProgress: UILabel!
    @IBOutlet weak var imgBuble: UIImageView!
    @IBOutlet weak var lablHdr: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnYes: GradientButton!
    @IBOutlet weak var btnAnother: GradientButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imgBuble.alpha = 0
        self.perform(#selector(showLetterAnimation), with: nil, afterDelay: 1.0)
        
        lablProgress.text = "16/16"
        
        btnYes.setTitle(self.btnYes.titleLabel?.text?.localized(), for: .normal)
        btnAnother.setTitle(self.btnAnother.titleLabel?.text?.localized(), for: .normal)
        btnBack.setTitle(self.btnBack.titleLabel?.text?.localized(), for: .normal)
    }
    
    @objc func showLetterAnimation() {
        UIView.animate(withDuration: 1.0, animations: {() -> Void in
            self.imgBuble.alpha = 1.0
            self.lablHdr.typeText("Do you want to add another child?".localized()) {
                            }
        })
    }

    //MARK:- Apis Functions
    func addBaby(jsonString:String, isFromAnotherBaby:Bool) {
        
        self.view.endEditing(true)
        
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "param":jsonString, "preference":AppUtility.getUserLoggedIn().preference]
        let data = AppManager.shared.babyImage?.jpegData(compressionQuality: 0.5)
        APIRequestUtil.addNewBaby(parameters: paramDict, data: data ?? Data(), extensionString: "image/jpeg", fileNameKey: "babyImage") { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    AppManager.shared.arrBabyMedical.removeAllObjects()
                    AppManager.shared.arrBabyAllergy.removeAllObjects()
                    AppManager.shared.arrBabyVaccine.removeAllObjects()
                    AppManager.shared.arrBabyRelation.removeAllObjects()
                    
                    AppManager.shared.babyName = ""
                    AppManager.shared.babyGender = ""
                    AppManager.shared.babyImage = UIImage()
                    AppManager.shared.babyDOB = ""
                    AppManager.shared.babyWeekPregnancy = ""
                    AppManager.shared.otherRelationValue = ""
                    
                    if isFromAnotherBaby {
                        let viewController = AddBabyNameViewController(nibName:String(describing: AddBabyNameViewController.self), bundle:nil)
//                        viewController.isFromBaby = true
                        self.navigationController?.pushViewController(viewController, animated: true)
                    } else {
                        if AppManager.shared.planId == "0" {
                            appSceneDelegate.loadTab()
                        } else {
                            let viewController = ConfirmSubscriptionVC(nibName:String(describing: ConfirmSubscriptionVC.self), bundle:nil)
                            self.navigationController?.pushViewController(viewController, animated: true)
                        }
                    }
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    //MARK:- Button Actions
    @IBAction func nextTapped(_ sender: Any) {
        
        let dicBaby = NSMutableDictionary()
        dicBaby.setValue(AppManager.shared.babyName, forKey: "babyNameEn")
        dicBaby.setValue(AppManager.shared.babyGender, forKey: "gender")
        dicBaby.setValue(AppManager.shared.babyDOB, forKey: "dob")
        dicBaby.setValue(AppManager.shared.babyDOB, forKey: "dueDate")
        dicBaby.setValue(AppManager.shared.arrBabyRelation.componentsJoined(by: ","), forKey: "relationId")
        dicBaby.setValue(AppManager.shared.babyWeekPregnancy, forKey: "babyPregnancyWeeks")
        dicBaby.setValue("born", forKey: "bornType")
        dicBaby.setValue("fullterm", forKey: "babyDeliveryType")
        dicBaby.setValue("yes", forKey: "firstLabour")
        dicBaby.setValue(AppUtility.getUserLoggedIn().userID, forKey: "userId")
        dicBaby.setValue(AppManager.shared.arrBabyMedical, forKey: "medicalConditionId")
        dicBaby.setValue(AppManager.shared.arrBabyVaccine, forKey: "vaccinationId")
        dicBaby.setValue(AppManager.shared.arrBabyAllergy, forKey: "allergyId")
        
        let rv = AppManager.shared.arrBabyRelation.componentsJoined(by: ",")
        if rv == "0" {
            dicBaby.setValue(AppManager.shared.otherRelationValue, forKey: "babyRelationTitle")
        }
        
        let jsonData = try! JSONSerialization.data(withJSONObject: dicBaby, options: JSONSerialization.WritingOptions.prettyPrinted)
        let jsonImage = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)!
        
        print(jsonImage)
        
        self.addBaby(jsonString: jsonImage as String, isFromAnotherBaby: false)
    }
    
    @IBAction func skipAllTapped(_ sender: Any) {
        appSceneDelegate.loadTab()
    }
    
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func yesTapped(_ sender: Any) {
        
        let dicBaby = NSMutableDictionary()
        dicBaby.setValue(AppManager.shared.babyName, forKey: "babyNameEn")
        dicBaby.setValue(AppManager.shared.babyGender, forKey: "gender")
        dicBaby.setValue(AppManager.shared.babyDOB, forKey: "dob")
        dicBaby.setValue(AppManager.shared.babyDOB, forKey: "dueDate")
        dicBaby.setValue(AppManager.shared.arrBabyRelation.componentsJoined(by: ","), forKey: "relationId")
        dicBaby.setValue(AppManager.shared.babyWeekPregnancy, forKey: "babyPregnancyWeeks")
        dicBaby.setValue("born", forKey: "bornType")
        dicBaby.setValue("fullterm", forKey: "babyDeliveryType")
        dicBaby.setValue("yes", forKey: "firstLabour")
        dicBaby.setValue(AppManager.shared.arrBabyMedical, forKey: "medicalConditionId")
        dicBaby.setValue(AppManager.shared.arrBabyVaccine, forKey: "vaccinationId")
        dicBaby.setValue(AppManager.shared.arrBabyAllergy, forKey: "allergyId")
        
        let rv = AppManager.shared.arrBabyRelation.componentsJoined(by: ",")
        if rv == "0" {
            dicBaby.setValue(AppManager.shared.otherRelationValue, forKey: "babyRelationTitle")
        }
        
        let jsonData = try! JSONSerialization.data(withJSONObject: dicBaby, options: JSONSerialization.WritingOptions.prettyPrinted)
        let jsonImage = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)!
        
        print(jsonImage)
        
        self.addBaby(jsonString: jsonImage as String, isFromAnotherBaby: true)
    }
}
