//
//  ForgotVC.swift
//  Tifli
//
//  Created by zubair on 10/08/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

class ForgotVC: UIViewController {

    @IBOutlet weak var lablEmail: UILabel!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var viewLogin: UIView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnSubmit: GradientButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if appSceneDelegate.isArabic {
            btnBack.setImage(UIImage.init(named: BACK_AR), for: .normal)
            tfEmail.textAlignment = .right
        }
        lablEmail.text = lablEmail.text?.localized()
        btnSubmit.setTitle(self.btnSubmit.titleLabel?.text?.localized(), for: .normal)
    }
    
    func CheckFields() -> Bool {
        if (tfEmail.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            
            AppUtility.showInfoMessage(message: "Please enter valid Email".localized())
            let test = false
            return test
        }
        else if !(AppUtility.isValidEmail(testStr:tfEmail.text!)) {
            AppUtility.showInfoMessage(message: "Please enter valid Email".localized())
            let test = false
            return test
        }
        
        let test = true
        return test
    }
    
    //MARK:- Apis Functions
    func forgotPassword() {
        
        self.view.endEditing(true)
        let paramDict: [String: Any] = ["appToken":APP_TOKEN, "email":tfEmail.text!]
        
        APIRequestUtil.forgotPassword(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    let alert = UIAlertController(title: "Information!".localized(), message: message, preferredStyle: UIAlertController.Style.alert)
                    
                    alert.addAction(UIAlertAction(title: "OK".localized(), style: .default, handler: { (action: UIAlertAction!) in
                        self.navigationController?.popViewController(animated: true)
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }

    //MARK:- Button Actions
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitTapped(_ sender: Any) {
        if CheckFields() {
            self.forgotPassword()
        }
    }
}
