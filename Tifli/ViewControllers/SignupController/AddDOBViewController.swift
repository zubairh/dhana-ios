//
//  AddDOBViewController.swift
//  Tifli
//
//  Created by zubair on 11/07/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import FSCalendar
import SwiftyJSON
import ActionSheetPicker_3_0
import RimhTypingLetters

class AddDOBViewController: UIViewController, FSCalendarDataSource, FSCalendarDelegate, FSCalendarDelegateAppearance {
    
    @IBOutlet weak var calendarView: FSCalendar!
    @IBOutlet weak var lablTitle: UILabel!
    @IBOutlet weak var progress: UIProgressView!
    @IBOutlet weak var lablProgress: UILabel!
    @IBOutlet weak var imgBuble: UIImageView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var btnNext: GradientButton!
    
    var isFromBaby = false
    var selectedDOB = ""
    var arrString = [String]()
    var dateFormatter2: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lablProgress.text = "5/16"
        
        imgBuble.alpha = 0
        self.perform(#selector(showLetterAnimation), with: nil, afterDelay: 1.0)
        
        if isFromBaby {
            lablProgress.text = "11/16"
            progress.setProgress(0.7, animated: true)
        }
        
        selectedDOB = AppUtility.convertDateToString(date: NSDate())
        
        self.calendarView.dataSource = self
        self.calendarView.delegate = self
        
        btnNext.setTitle(self.btnNext.titleLabel?.text?.localized(), for: .normal)
        btnBack.setTitle(self.btnBack.titleLabel?.text?.localized(), for: .normal)
        btnSkip.setTitle(self.btnSkip.titleLabel?.text?.localized(), for: .normal)
    }
    
    @objc func showLetterAnimation() {
        UIView.animate(withDuration: 1.0, animations: {() -> Void in
            self.imgBuble.alpha = 1.0
            if self.isFromBaby {
                self.lablTitle.typeText("When was your baby born?".localized()) {
                    }
            } else {
                self.lablTitle.typeText("When were you born?".localized()) {
                    }
            }
        })
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date) {
        selectedDOB = AppUtility.convertDateToString(date: date as NSDate)
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        selectedDOB = AppUtility.convertDateToString(date: date as NSDate)
    }
    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        let key = self.dateFormatter2.string(from: date)
        if self.arrString.contains(key) {
            return 1
        }
        return 0
    }
    
    //MARK:- Apis Functions
    func updateProfile() {
        self.view.endEditing(true)
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "dob":selectedDOB, "userToken":AppUtility.getUserToken(), "preference":AppUtility.getUserLoggedIn().preference]
        APIRequestUtil.updateProfile(parameters: paramDict) { (result, error) in
            if(result != nil) {
                
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    USER_DEFAUTS.set(result ?? "", forKey: USER_LOGIN)
                    USER_DEFAUTS.synchronize()
                    
                    let viewController = AddBabyCountViewController(nibName:String(describing: AddBabyCountViewController.self), bundle:nil)
                    self.navigationController?.pushViewController(viewController, animated: true)
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    //MARK:- Button Actions
    @IBAction func skipTapped(_ sender: Any) {
        if self.isFromBaby {
            AppManager.shared.babyDOB = selectedDOB
            let viewController = AddBabyNameViewController(nibName:String(describing: AddBabyNameViewController.self), bundle:nil)
            viewController.isFromBaby = true
            self.navigationController?.pushViewController(viewController, animated: true)
        } else {
            let viewController = AddBabyCountViewController(nibName:String(describing: AddBabyCountViewController.self), bundle:nil)
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    @IBAction func nextTapped(_ sender: Any) {
        if self.isFromBaby {
            AppManager.shared.babyDOB = selectedDOB
            let viewController = AddBabyNameViewController(nibName:String(describing: AddBabyNameViewController.self), bundle:nil)
            viewController.isFromBaby = true
            self.navigationController?.pushViewController(viewController, animated: true)
        } else {
            self.updateProfile()
        }
    }
    
    @IBAction func skipAllTapped(_ sender: Any) {
        
    }
    
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func dateTapped(_ sender: UIButton) {
        let datePicker = ActionSheetDatePicker(title: title, datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let date:NSDate = value as! NSDate
            
            self.arrString.removeAll()
            self.selectedDOB = AppUtility.convertDateToString(date: date)
            self.arrString.append(self.selectedDOB)
            self.calendarView.select(date as Date, scrollToDate: true)
            self.calendarView.reloadData()
            
            return
            
        }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview!.superview)
        datePicker?.maximumDate = Date()
        datePicker?.show()
    }
    
}
