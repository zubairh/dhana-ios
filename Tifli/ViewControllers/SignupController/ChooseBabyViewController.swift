//
//  ChooseBabyViewController.swift
//  Tifli
//
//  Created by zubair on 12/07/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import RimhTypingLetters

class ChooseBabyViewController: UIViewController {

    @IBOutlet weak var btnBabyGirl: UIButton!
    @IBOutlet weak var lablProgress: UILabel!
    @IBOutlet weak var imgBuble: UIImageView!
    @IBOutlet weak var lablHdr: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var btnNext: GradientButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnNext.setTitle(self.btnNext.titleLabel?.text?.localized(), for: .normal)
        btnBack.setTitle(self.btnBack.titleLabel?.text?.localized(), for: .normal)
        btnSkip.setTitle(self.btnSkip.titleLabel?.text?.localized(), for: .normal)
        
        AppManager.shared.babyGender = "boy"
        imgBuble.alpha = 0
        self.perform(#selector(showLetterAnimation), with: nil, afterDelay: 1.0)
        
        lablProgress.text = "8/16"
    }
    
    @objc func showLetterAnimation() {
        UIView.animate(withDuration: 1.0, animations: {() -> Void in
            self.imgBuble.alpha = 1.0
            self.lablHdr.typeText("Girl or Boy?".localized()) {
                        }
        })
    }

    //MARK:- Button Actions
    @IBAction func skipTapped(_ sender: Any) {
        let viewController = AddPhotoViewController(nibName:String(describing: AddPhotoViewController.self), bundle:nil)
        viewController.isFromBaby = true
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func nextTapped(_ sender: Any) {
        let viewController = AddPhotoViewController(nibName:String(describing: AddPhotoViewController.self), bundle:nil)
        viewController.isFromBaby = true
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func skipAllTapped(_ sender: Any) {
        
    }
    
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func chooseBabyTapped(_ sender: UIButton) {
        if sender.tag == 0 {
            sender.tag = 1
            sender.isSelected = true
            AppManager.shared.babyGender = "girl"
        } else {
            sender.tag = 0
            sender.isSelected = false
            AppManager.shared.babyGender = "boy"
        }
    }
}
