//
//  OTPViewController.swift
//  Dhana
//
//  Created by zubair on 07/07/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SimpleAnimation
import SwiftyJSON

class OTPViewController: UIViewController, UITextFieldDelegate  {
    
    @IBOutlet var tfCode1: UITextField!
    @IBOutlet weak var btnResend: UIButton!
    @IBOutlet weak var lblResendCodeTimer: UILabel!
    @IBOutlet weak var viewText: UIView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lablProgress: UILabel!
    @IBOutlet weak var progress: UIProgressView!
    @IBOutlet weak var llablTitle: UILabel!
    @IBOutlet weak var imgBuble: UIImageView!
    @IBOutlet weak var lablHdr: UILabel!
    @IBOutlet weak var lablCheckInbox: UILabel!
    
    var timer = Timer()
    var seconds = 180
    var isFromOther = false
    var prefrence = ""
    var otpReqNo = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        lablCheckInbox.text = lablCheckInbox.text?.localized()
        btnBack.setTitle(self.btnBack.titleLabel?.text?.localized(), for: .normal)
        btnResend.setTitle(self.btnResend.titleLabel?.text?.localized(), for: .normal)
        
        
        tfCode1.becomeFirstResponder()
        
        tfCode1.textContentType = .oneTimeCode
        
        btnResend.isEnabled = false
        lblResendCodeTimer.popIn()
        seconds = 180
        runTimer()
        
        lablProgress.text = "3/16"
        
        imgBuble.alpha = 0
        self.perform(#selector(showLetterAnimation), with: nil, afterDelay: 1.0)
        
        if self.isFromOther {
            self.viewText.isHidden = false
            self.btnBack.isHidden = true
            self.lablProgress.isHidden = true
            self.progress.isHidden = true
        }
    }
    
    @objc func showLetterAnimation() {
        UIView.animate(withDuration: 1.0, animations: {() -> Void in
            self.imgBuble.alpha = 1.0
            if self.isFromOther {
                if self.prefrence == "email" {
                    self.llablTitle.typeText("An OTP code should be sent Check your email and type the number here".localized()) {
                    }
                } else {
                    self.llablTitle.typeText("An OTP code should be sent Check your SMS and type the number here".localized()) {
                    }
                }
            } else {
                self.llablTitle.typeText("We've sent a verification code to verify your email".localized()) {
                }
            }
        })
    }
    
    // MARK: - TextField Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == tfCode1 {
            if (tfCode1.text?.count)! >= 5 && range.length == 0 {
                return false
            } else {
                self.perform(#selector(checkTF), with: nil, afterDelay: 0.1)
                return true
            }
        }
        return true
    }
    
    
    //MARK: - Functions
        
    @objc func checkTF() {
        if tfCode1.text?.count == 5 {
            self.view.endEditing(true)
            if otpReqNo == 0{
            self.verifyOtp()
            }
        }
    }
    
    @objc func updateTimer() {
        seconds -= 1     //This will decrement(count down)the seconds.
        lblResendCodeTimer.text = "\(seconds)" //This will update the label.
        lblResendCodeTimer.popIn()
        if(seconds == 0) {
            timer.invalidate()
            self.lblResendCodeTimer.text = "0"
            self.btnResend.isEnabled = true
        }
    }
    
    func runTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(updateTimer)), userInfo: nil, repeats: true)
    }
    
    //MARK:- Apis functions
    func verifyOtp() {
        
        
        self.view.endEditing(true)
        var code = self.tfCode1.text!
        code = code.trimmingCharacters(in: .whitespaces)
        otpReqNo = 1
        var paramDict: [String: Any]?
        if prefrence == "email" {
            paramDict = ["appToken":APP_TOKEN, "userId":AppUtility.getUserLoggedIn().userID, "otp":code, "preference":prefrence, "email":AppUtility.getUserLoggedIn().email]
        } else {
            paramDict = ["appToken":APP_TOKEN, "userId":AppUtility.getUserLoggedIn().userID, "otp":code, "preference":prefrence, "userPhone":AppUtility.getUserLoggedIn().phoneNumber]
        }
        APIRequestUtil.verifyOtp(parameters: paramDict!) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                    let dict = swiftyJsonVar.dictionaryValue
                    let status = dict["status"]?.intValue
                    let message = dict["message"]?.string
                    
                    if status == SUCESS_CODE {
                        
                        AppUtility.saveUserToken(token: dict["userToken"]!.stringValue)
                        USER_DEFAUTS.set(result ?? "", forKey: USER_LOGIN)
                        USER_DEFAUTS.synchronize()
                        
                        if self.isFromOther {
                            let viewController = AddPhotoViewController(nibName:String(describing: AddPhotoViewController.self), bundle:nil)
                            self.navigationController?.pushViewController(viewController, animated: true)
                        } else {
                            if AppManager.shared.planId == "0" {
                                appSceneDelegate.loadTab()
                            } else {
                                let viewController = ConfirmSubscriptionVC(nibName:String(describing: ConfirmSubscriptionVC.self), bundle:nil)
                                self.navigationController?.pushViewController(viewController, animated: true)
                            }
                        }
                        
                    }
                    else {
                        AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                        return
                    }
               
            }
        }
    }
    
    func resendOtp() {
        otpReqNo = 0
        self.view.endEditing(true)
        var paramDict: [String: Any]?
        if prefrence == "email" {
            paramDict = ["appToken":APP_TOKEN, "email":AppUtility.getUserLoggedIn().email, "preference":prefrence]
        } else {
            paramDict = ["appToken":APP_TOKEN, "userPhone":AppUtility.getUserLoggedIn().phoneNumber, "preference":prefrence]
        }
        APIRequestUtil.resentOtp(parameters: paramDict!) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    AppUtility.showSuccessMessage(message: message!)
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    //MARK:- Button Actions
    @IBAction func resendTapped(_ sender: Any) {
        btnResend.isEnabled = false
        seconds = 180
        runTimer()
        
        self.resendOtp()
    }
    
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
