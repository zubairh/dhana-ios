//
//  CheckMedicalViewController.swift
//  Tifli
//
//  Created by zubair on 12/07/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON
import RimhTypingLetters

class CheckMedicalViewController: UIViewController {

    @IBOutlet weak var lablTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tfSearch: UITextField!
    @IBOutlet weak var layoutHeightTable: NSLayoutConstraint!
    @IBOutlet weak var lablProgress: UILabel!
    @IBOutlet weak var imgBuble: UIImageView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var btnNext: GradientButton!
    
    var isFromAlergy = false
    var isFromVaccine = false
    var arrData = [BabyType]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
        
        imgBuble.alpha = 0
        self.perform(#selector(showLetterAnimation), with: nil, afterDelay: 1.0)
        
        if appSceneDelegate.isArabic {
            tfSearch.textAlignment = .right
        }
        
        btnNext.setTitle(self.btnNext.titleLabel?.text?.localized(), for: .normal)
        btnSkip.setTitle(self.btnSkip.titleLabel?.text?.localized(), for: .normal)
        btnBack.setTitle(self.btnBack.titleLabel?.text?.localized(), for: .normal)
        
//        else if isFromVaccine {
//            lablProgress.text = "15/16"
//            self.lablTitle.text = "What vaccines has your baby taken?"
//            self.getVaccine()
//        }
        if isFromAlergy {
            lablProgress.text = "14/16"
            self.getAlergy()
        } else {
            lablProgress.text = "13/16"
            self.getMedical()
        }
    }
    
    @objc func showLetterAnimation() {
        UIView.animate(withDuration: 1.0, animations: {() -> Void in
            self.imgBuble.alpha = 1.0
            if self.isFromAlergy {
                self.lablTitle.typeText("Any allergies?".localized()) {
                            }
            } else {
                self.lablTitle.typeText("Any chronic medical conditions?".localized()) {
                            }
            }
        })
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        layoutHeightTable.constant = tableView.contentSize.height
    }
    
    //MARK:- Apis Functions
    func getMedical() {
        self.view.endEditing(true)
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN]
        APIRequestUtil.getMedicalList(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.arrData.removeAll()
                    
                    let valueArray: [JSON] = dict["medicalConditions"]!.arrayValue
                    for dict in valueArray {
                        let dictValue: Dictionary<String, JSON> = dict.dictionaryValue
                        let obj = BabyType.init(withDictionary: dictValue)
                        self.arrData.append(obj)
                    }
                    self.tableView.reloadData()
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    func getAlergy() {
        self.view.endEditing(true)
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN]
        APIRequestUtil.getAlergyList(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.arrData.removeAll()
                    
                    let valueArray: [JSON] = dict["allergies"]!.arrayValue
                    for dict in valueArray {
                        let dictValue: Dictionary<String, JSON> = dict.dictionaryValue
                        let obj = BabyType.init(withDictionary: dictValue)
                        self.arrData.append(obj)
                    }
                    self.tableView.reloadData()
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    func getVaccine() {
        self.view.endEditing(true)
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN]
        APIRequestUtil.getVaccineList(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.arrData.removeAll()
                    
                    let valueArray: [JSON] = dict["vaccinations"]!.arrayValue
                    for dict in valueArray {
                        let dictValue: Dictionary<String, JSON> = dict.dictionaryValue
                        let obj = BabyType.init(withDictionary: dictValue)
                        self.arrData.append(obj)
                    }
                    self.tableView.reloadData()
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    //MARK:- Button Actions
    @IBAction func skipTapped(_ sender: Any) {
        
        if !isFromAlergy && !isFromVaccine {
            let viewController = CheckMedicalViewController(nibName:String(describing: CheckMedicalViewController.self), bundle:nil)
            viewController.isFromAlergy = true
            self.navigationController?.pushViewController(viewController, animated: true)
            
        } else if isFromAlergy {
            
            let viewController = AnotherChildViewController(nibName:String(describing: AnotherChildViewController.self), bundle:nil)
            self.navigationController?.pushViewController(viewController, animated: true)
            
        } else if isFromVaccine {
            
            let viewController = AnotherChildViewController(nibName:String(describing: AnotherChildViewController.self), bundle:nil)
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    @IBAction func nextTapped(_ sender: Any) {
        
        if !isFromAlergy && !isFromVaccine {
            AppManager.shared.arrBabyMedical.removeAllObjects()
            for obj in self.arrData {
                if obj.isAdded {
                    AppManager.shared.arrBabyMedical.add(obj.ID)
                }
            }
            if AppManager.shared.arrBabyMedical.count == 0 {
                AppUtility.showInfoMessage(message: "Please select medical".localized())
                return
            }
            let viewController = CheckMedicalViewController(nibName:String(describing: CheckMedicalViewController.self), bundle:nil)
            viewController.isFromAlergy = true
            self.navigationController?.pushViewController(viewController, animated: true)
            
        } else if isFromAlergy {
            AppManager.shared.arrBabyAllergy.removeAllObjects()
            for obj in self.arrData {
                if obj.isAdded {
                    AppManager.shared.arrBabyAllergy.add(obj.ID)
                }
            }
            if AppManager.shared.arrBabyAllergy.count == 0 {
                AppUtility.showInfoMessage(message: "Please select allergy".localized())
                return
            }
//            let viewController = CheckMedicalViewController(nibName:String(describing: CheckMedicalViewController.self), bundle:nil)
//            viewController.isFromVaccine = true
//            self.navigationController?.pushViewController(viewController, animated: true)
            let viewController = AnotherChildViewController(nibName:String(describing: AnotherChildViewController.self), bundle:nil)
            self.navigationController?.pushViewController(viewController, animated: true)
            
        } else if isFromVaccine {
            AppManager.shared.arrBabyVaccine.removeAllObjects()
            for obj in self.arrData {
                if obj.isAdded {
                    AppManager.shared.arrBabyVaccine.add(obj.ID)
                }
            }
            if AppManager.shared.arrBabyVaccine.count == 0 {
                AppUtility.showInfoMessage(message: "Please select vaccine".localized())
                return
            }
            
            let viewController = AnotherChildViewController(nibName:String(describing: AnotherChildViewController.self), bundle:nil)
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    @IBAction func skipAllTapped(_ sender: Any) {
        
    }
    
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func selectTypeTapped(_ sender:UIButton) {
        if self.arrData[sender.tag].isAdded {
            self.arrData[sender.tag].isAdded = false
        } else {
            self.arrData[sender.tag].isAdded = true
        }
        tableView.reloadData()
    }
}

// MARK: - Tableview Delegate-
extension CheckMedicalViewController : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        tableView.register(UINib(nibName: "AlergyCell", bundle: nil), forCellReuseIdentifier: "AlergyCell")
        var cell : AlergyCell! = tableView.dequeueReusableCell(withIdentifier: "AlergyCell") as? AlergyCell
        
        if (cell == nil) {
            cell = AlergyCell.init(style: UITableViewCell.CellStyle.default,
                                          reuseIdentifier:"AlergyCell");
        }
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        let obj = self.arrData[indexPath.row]
        cell.lablTitle.text = obj.typeTitle
        
        cell.btnAdd.tag = indexPath.row
        cell.btnAdd.addTarget(self, action: #selector(selectTypeTapped(_:)), for: .touchUpInside)
        
        if !obj.isAdded {
            cell.btnAdd.setTitle("Add".localized(), for: .normal)
            cell.btnAdd.endColor = COLORS.TABBAR_COLOR
            cell.btnAdd.startColor = COLORS.APP_THEME_COLOR
            cell.btnAdd.layer.borderWidth = 0.0
            cell.btnAdd.borderColor = .clear
            cell.btnAdd.setTitleColor(.white, for: .normal)
        } else {
            cell.btnAdd.setTitle("Remove".localized(), for: .normal)
            cell.btnAdd.endColor = COLORS.UNSELECT_CATEGORY_COLOR
            cell.btnAdd.startColor = COLORS.UNSELECT_CATEGORY_COLOR
            cell.btnAdd.layer.borderWidth = 0.5
            cell.btnAdd.borderColor = .lightGray
            cell.btnAdd.setTitleColor(.lightGray, for: .normal)
        }
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    }
}
