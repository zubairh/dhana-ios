//
//  RegisterEmailVC.swift
//  Tifli
//
//  Created by zubair on 11/08/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON
import RimhTypingLetters

class RegisterEmailVC: UIViewController {
    
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfConfirmPassword: UITextField!
    @IBOutlet weak var imgBuble: UIImageView!
    @IBOutlet weak var lablHdr: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lablEmail: UILabel!
    @IBOutlet weak var lablPassword: UILabel!
    @IBOutlet weak var lablConfirmPassword: UILabel!
    @IBOutlet weak var btnRegister: GradientButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imgBuble.alpha = 0
        self.perform(#selector(showLetterAnimation), with: nil, afterDelay: 1.0)
        
        if appSceneDelegate.isArabic {
         
            tfEmail.textAlignment = .right
            tfPassword.textAlignment = .right
            tfConfirmPassword.textAlignment = .right
        }
        lablEmail.text = lablEmail.text?.localized()
        lablPassword.text = lablPassword.text?.localized()
        lablConfirmPassword.text = lablConfirmPassword
            .text?.localized()
        
        btnRegister.setTitle(self.btnRegister.titleLabel?.text?.localized(), for: .normal)
        btnBack.setTitle(self.btnBack.titleLabel?.text?.localized(), for: .normal)
    }
    
    @objc func showLetterAnimation()
    {
        UIView.animate(withDuration: 1.0, animations: {() -> Void in
            self.imgBuble.alpha = 1.0
            
            self.lablHdr.typeText("What's your email address?".localized()) {
            }
        })
    }
   
    func CheckFields() -> Bool {
        if (tfEmail.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            
            AppUtility.showInfoMessage(message: "Please enter valid Email".localized())
            let test = false
            return test
        }
        else if !(AppUtility.isValidEmail(testStr:tfEmail.text!)) {
            AppUtility.showInfoMessage(message: "Please enter valid Email".localized())
            let test = false
            return test
        }
        else if (tfPassword.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            AppUtility.showInfoMessage(message: "Please enter password".localized())
            let test = false
            return test
        }
        else if (tfConfirmPassword.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            AppUtility.showInfoMessage(message: "Please enter confirm password".localized())
            let test = false
            return test
        }
        else if (tfPassword.text != tfConfirmPassword.text) {
            AppUtility.showInfoMessage(message: "password and confirmm password does't match".localized())
            let test = false
            return test
        }
        
        let test = true
        return test
    }
    
    //MARK:- Apis Functions
    func signUp(paramDict:[String: Any]) {
        
        self.view.endEditing(true)
        APIRequestUtil.SignUp(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    USER_DEFAUTS.set(result ?? "", forKey: USER_LOGIN)
                    USER_DEFAUTS.synchronize()
                    
                    let viewController = OTPViewController(nibName:String(describing: OTPViewController.self), bundle:nil)
                    viewController.isFromOther = true
                    viewController.prefrence = "email"
                    self.navigationController?.pushViewController(viewController, animated: true)
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }

    //MARK:- Button Action
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func registerTapped(_ sender: Any) {
        if CheckFields() {
            let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "email":tfEmail.text!, "password":tfPassword.text!, "preference":"email", "device_type":"APPLE", "device_token":appDelegate.deviceToken]
            self.signUp(paramDict: paramDict)
        }
    }
    
    @IBAction func passwordSeenTapped(_ sender: UIButton) {
        if sender.tag == 0 {
            sender.tag = 1
            tfPassword.isSecureTextEntry = false
        } else {
            sender.tag = 0
            tfPassword.isSecureTextEntry = true
        }
    }
    
    @IBAction func confirmSeenTapped(_ sender: UIButton) {
        if sender.tag == 0 {
            sender.tag = 1
            tfConfirmPassword.isSecureTextEntry = false
        } else {
            sender.tag = 0
            tfConfirmPassword.isSecureTextEntry = true
        }
    }
    
}
