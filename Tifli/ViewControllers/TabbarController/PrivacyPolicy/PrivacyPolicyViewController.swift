//
//  PrivacyPolicyViewController.swift
//  Part-Time
//
//  Created by zubair on 24/04/2019.
//  Copyright © 2019 Zubair Habib. All rights reserved.
//

import UIKit
import WebKit

class PrivacyPolicyViewController: UIViewController, WKNavigationDelegate, WKUIDelegate {
    
    @IBOutlet weak var lablTitle: UILabel!
    @IBOutlet weak var webKit: WKWebView!
 
    var type = ""
    var webUrl = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lablTitle.text = type
        
        webKit.navigationDelegate = self
        webKit.uiDelegate = self
        let url = URL(string: webUrl)!
        webKit.load(URLRequest(url: url))
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    //MARK:- Buttons Action
    @IBAction func backTapped(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- webview delegate
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        AppUtility.showProgress()
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        AppUtility.hideProgress()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        AppUtility.hideProgress()
    }
    
}
