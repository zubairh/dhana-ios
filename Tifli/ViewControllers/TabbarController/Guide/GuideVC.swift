//
//  GuideVC.swift
//  Dhana
//
//  Created by zubair on 22/06/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

class GuideVC: UIViewController {

    @IBOutlet weak var collectionCategory: UICollectionView!
    @IBOutlet weak var tableGuide: UITableView!
    @IBOutlet weak var imgBaby: UIImageView!
    @IBOutlet weak var lablBabyName: UILabel!
    @IBOutlet weak var lablYear: UILabel!
    @IBOutlet weak var tableBaby: UITableView!
    
    var currentIndex = -1
    var arrCategory = [Categories]()
    var arrGuide = [Guide]()
    let autherBtnAttribute: [NSAttributedString.Key: Any] = [
        .font: UIFont(name:"Open-Sans-Regular",size:13) as Any,
        .foregroundColor: COLORS.TABBAR_COLOR,
          .underlineStyle: NSUnderlineStyle.single.rawValue
      ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.getCategories()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        appSceneDelegate.navController = self.navigationController!
        
        for obj in AppManager.shared.arrBabies {
            if obj.babyID == AppUtility.getBabyId() {
                AppUtility.loadImage(imageView: imgBaby, urlString: obj.babyImage, placeHolderImageString: "bb")
                lablBabyName.text = obj.babyName
                lablYear.text = obj.age
            }
        }
        self.tableBaby.reloadData()
        
        self.getArticlesList(categoryID: "")
    }
    
    //MARK:- Apis Functions
    func getCategories() {
        self.view.endEditing(true)
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken()]
        APIRequestUtil.getGuideCategory(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.arrCategory.removeAll()
                    
                    let valueArray: [JSON] = dict["data"]!.arrayValue
                    for dict in valueArray {
                        let dictValue: Dictionary<String, JSON> = dict.dictionaryValue
                        let obj = Categories.init(withDictionary: dictValue)
                        self.arrCategory.append(obj)
                    }
                    self.collectionCategory.reloadData()
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    func getArticlesList(categoryID:String) {
        self.view.endEditing(true)
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "categoryId":categoryID]
        APIRequestUtil.getGuideList(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.arrGuide.removeAll()
                    
                    let valueArray: [JSON] = dict["data"]!.arrayValue
                    for dict in valueArray {
                        let dictValue: Dictionary<String, JSON> = dict.dictionaryValue
                        let obj = Guide.init(withDictionary: dictValue)
                        self.arrGuide.append(obj)
                    }
                    self.tableGuide.reloadData()
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    //MARK:- Button Actions
    @objc func categorySelectTapped(_ sender:UIButton) {
        currentIndex = sender.tag
        collectionCategory.reloadData()
        self.getArticlesList(categoryID: self.arrCategory[sender.tag].catID)
    }
    
    @IBAction func sideMenuTapped(_ sender: Any) {
        self.sideMenuViewController.presentRightMenuViewController()
    }
    
    @IBAction func notifyTapped(_ sender: Any) {
        let viewController = NotificationVC(nibName:String(describing: NotificationVC.self), bundle:nil)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func dropDownBabyTapped(_ sender: UIButton) {
        if sender.tag == 0 {
            sender.tag = 1
            tableBaby.isHidden = false
        } else {
            sender.tag = 0
            tableBaby.isHidden = true
        }
    }
}

// MARK: - Tableview Delegate-
extension GuideVC : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableBaby {
            return AppManager.shared.arrBabies.count
        }
        return arrGuide.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        if tableView == tableBaby {
                
            tableView.register(UINib(nibName: "BabyCell", bundle: nil), forCellReuseIdentifier: "BabyCell")
            var cell : BabyCell! = tableView.dequeueReusableCell(withIdentifier: "BabyCell") as? BabyCell
            
            if (cell == nil) {
                cell = BabyCell.init(style: UITableViewCell.CellStyle.default,
                                              reuseIdentifier:"BabyCell");
            }
            
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            
            let objG = AppManager.shared.arrBabies[indexPath.row]
            cell.lablBabyName.text = objG.babyName
            cell.lablYear.text = objG.age
            AppUtility.loadImage(imageView: cell.imgBaby, urlString: objG.babyImage, placeHolderImageString: "bb")
            
            return cell
            
        } else {
            
            tableView.register(UINib(nibName: "GuideCell", bundle: nil), forCellReuseIdentifier: "GuideCell")
            var cell : GuideCell! = tableView.dequeueReusableCell(withIdentifier: "GuideCell") as? GuideCell
            
            if (cell == nil) {
                cell = GuideCell.init(style: UITableViewCell.CellStyle.default,
                                              reuseIdentifier:"GuideCell");
            }
            
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            
            let obj = self.arrGuide[indexPath.row]
            cell.lablTitle.text = obj.titleEng.htmlToString
            cell.lablDetail.text = obj.detailEng.htmlToString
            cell.lablTotalLike.text = String(obj.totalLike)+" people found it helpful".localized()
            AppUtility.loadImage(imageView: cell.imgGuide, urlString: obj.image, placeHolderImageString: "bb")
            AppUtility.loadImage(imageView: cell.imgAuther, urlString: obj.autherImage, placeHolderImageString: "bb")
            cell.btnAutherName.setTitle(obj.autherName, for: .normal)
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tableBaby {
            return 50
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == tableBaby {
            
            self.tableBaby.isHidden = true
            AppUtility.saveBabyId(babyID: AppManager.shared.arrBabies[indexPath.row].babyID)
            
            self.getArticlesList(categoryID: self.arrCategory[currentIndex].catID)
            
            AppUtility.loadImage(imageView: self.imgBaby, urlString: AppManager.shared.arrBabies[indexPath.row].babyImage, placeHolderImageString: "bb")
            self.lablBabyName.text = AppManager.shared.arrBabies[indexPath.row].babyName
            self.lablYear.text = AppManager.shared.arrBabies[indexPath.row].age
            
        } else {
            
            let viewController = GuideDetailVC(nibName:String(describing: GuideDetailVC.self), bundle:nil)
            viewController.artticleID = self.arrGuide[indexPath.row].articleID
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0
        cell.layer.transform = CATransform3DMakeScale(0.5, 0.5, 0.5)
        UIView.animate(withDuration: 0.5) {
            cell.alpha = 1
            cell.layer.transform = CATransform3DScale(CATransform3DIdentity, 1, 1, 1)
        }
    }
}

// MARK: - CollectionView Delegate-
extension GuideVC: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrCategory.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let obj = self.arrCategory[indexPath.row]
        let label = UILabel(frame: CGRect.zero)
        label.text = obj.titleEn
        label.sizeToFit()
        label.font = UIFont(name: "Open-Sans-SemiBold", size: 12)
        return CGSize(width: label.frame.width + 40, height: 45)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        collectionView.register(UINib(nibName: String(describing: CategoryCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: CategoryCell.self))
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: CategoryCell.self), for: indexPath as IndexPath) as! CategoryCell
        
        cell.btnCategory.tag = indexPath.row
        cell.btnCategory.addTarget(self, action: #selector(self.categorySelectTapped(_:)), for: .touchUpInside)
        
        let obj = self.arrCategory[indexPath.row]
        cell.btnCategory.setTitle(obj.titleEn, for: .normal)
        
        if currentIndex == indexPath.row {
            cell.btnCategory.endColor = COLORS.TABBAR_COLOR
            cell.btnCategory.startColor = COLORS.APP_THEME_COLOR
            cell.btnCategory.setTitleColor(.white, for: .normal)
        } else {
            cell.btnCategory.endColor = COLORS.UNSELECT_CATEGORY_COLOR
            cell.btnCategory.startColor = COLORS.UNSELECT_CATEGORY_COLOR
            cell.btnCategory.setTitleColor(COLORS.TABBAR_COLOR, for: .normal)
        }
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        currentIndex = indexPath.row
        collectionView.reloadData()
    }
    
}
