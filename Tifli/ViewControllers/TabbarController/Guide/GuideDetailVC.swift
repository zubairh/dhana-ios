//
//  GuideDetailVC.swift
//  Tifli
//
//  Created by zubair on 25/07/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

class GuideDetailVC: UIViewController {
    
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var tableComments: UITableView!
    @IBOutlet weak var imgArtcile: UIImageView!
    @IBOutlet weak var lablTitle: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var autherName: UIButton!
    @IBOutlet weak var lablDetail: UILabel!
    @IBOutlet weak var lablTotalComments: UILabel!
    @IBOutlet weak var lablTotalLikes: UILabel!
    @IBOutlet weak var layoutHeightTable: NSLayoutConstraint!
    @IBOutlet weak var tfComment: UITextField!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var lablNavTitle: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lablPostAnonumous: UILabel!
    @IBOutlet weak var btnAnounoums: UIButton!
    
    var objDetail = GuideDetail()
    var artticleID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        lablNavTitle.text = lablNavTitle.text?.localized()
        lablPostAnonumous.text = lablPostAnonumous.text?.localized()
        tfComment.placeholder = tfComment.placeholder?.localized()
        
        if appSceneDelegate.isArabic {
            tfComment.textAlignment = .right
            btnBack.setImage(UIImage(named: BACK_AR), for: .normal)
        }

        let titleTextAttributesNormal = [NSAttributedString.Key.foregroundColor: UIColor.lightGray]
        let titleTextAttributesSelected = [NSAttributedString.Key.foregroundColor: UIColor.white]
        segmentControl.setTitleTextAttributes(titleTextAttributesNormal, for: .normal)
        segmentControl.setTitleTextAttributes(titleTextAttributesSelected, for: .selected)
        
        segmentControl.setTitleColor(.lightGray, state: .normal)
        segmentControl.setTitleColor(.white, state: .selected)
        segmentControl.setTitleFont(UIFont(name: "OpenSans-SemiBold", size: 13)!)
        
        tableComments.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
        
        self.getArticlesDetail()
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        layoutHeightTable.constant = tableComments.contentSize.height
    }
    
    //MARK:- Apis functions
    func getArticlesDetail() {
        var isAnonomus = 0
        if btnAnounoums.isSelected {
            isAnonomus = 1
        } else {
            isAnonomus = 0
        }
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "id":artticleID, "anonymous_comment":isAnonomus]
        APIRequestUtil.getGuideDetail(parameters: paramDict) { (result, error) in
            
            if(result != nil) {
                
                let swiftyJsonVar = JSON(result!)
                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.objDetail = GuideDetail.init(withDictionary: dict)
                    
                    self.lablTitle.text = self.objDetail.titleEng.htmlToString
                    self.lablDetail.text = self.objDetail.detailEng.htmlToString
                    self.lablTotalLikes.text = String(self.objDetail.totalLike)
                    self.lablTotalComments.text = "Comments".localized()+": "+String(self.objDetail.totalComment)
                    AppUtility.loadImage(imageView: self.imgArtcile, urlString: self.objDetail.image, placeHolderImageString: "bb")
                    AppUtility.loadImage(imageView: self.imgUser, urlString: self.objDetail.autherImage, placeHolderImageString: "bb")
                    self.autherName.setTitle(self.objDetail.autherName, for: .normal)
                    
                    if self.objDetail.isLiked == 1 {
                        self.btnLike.isSelected = true
                    } else {
                        self.btnLike.isSelected = false
                    }
                    
                    self.tableComments.reloadData()
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    func addComment() {
        var isAnonomus = 0
        if btnAnounoums.isSelected {
            isAnonomus = 1
        } else {
            isAnonomus = 0
        }
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "id":artticleID, "comment":tfComment.text!, "anonymous_comment":isAnonomus]
        APIRequestUtil.addArticleComment(parameters: paramDict) { (result, error) in
            
            if(result != nil) {
                
                let swiftyJsonVar = JSON(result!)
                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.tfComment.text = ""
                    self.objDetail = GuideDetail.init(withDictionary: dict)
                    
                    self.lablTitle.text = self.objDetail.titleEng.htmlToString
                    self.lablDetail.text = self.objDetail.detailEng.htmlToString
                    self.lablTotalLikes.text = String(self.objDetail.totalLike)
                    self.lablTotalComments.text = "Comments".localized()+": "+String(self.objDetail.totalComment)
                    AppUtility.loadImage(imageView: self.imgArtcile, urlString: self.objDetail.image, placeHolderImageString: "bb")
                    AppUtility.loadImage(imageView: self.imgUser, urlString: self.objDetail.autherImage, placeHolderImageString: "bb")
                    
                    if self.objDetail.isLiked == 1 {
                        self.btnLike.isSelected = true
                    } else {
                        self.btnLike.isSelected = false
                    }
                    self.tableComments.reloadData()
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    func addLike() {
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "id":artticleID]
        APIRequestUtil.likeArticle(parameters: paramDict) { (result, error) in
            
            if(result != nil) {
                
                let swiftyJsonVar = JSON(result!)
                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.tfComment.text = ""
                    self.objDetail = GuideDetail.init(withDictionary: dict)
                    
                    self.lablTitle.text = self.objDetail.titleEng.htmlToString
                    self.lablDetail.text = self.objDetail.detailEng.htmlToString
                    self.lablTotalLikes.text = String(self.objDetail.totalLike)
                    self.lablTotalComments.text = "Comments".localized()+": "+String(self.objDetail.totalComment)
                    AppUtility.loadImage(imageView: self.imgArtcile, urlString: self.objDetail.image, placeHolderImageString: "bb")
                    AppUtility.loadImage(imageView: self.imgUser, urlString: self.objDetail.autherImage, placeHolderImageString: "bb")
                    
                    if self.objDetail.isLiked == 1 {
                        self.btnLike.isSelected = true
                    } else {
                        self.btnLike.isSelected = false
                    }
                    self.tableComments.reloadData()
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    //MARK:- Button Actions
    @IBAction func segmentTapped(_ sender: UISegmentedControl) {
        
    }
    
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func commentCheckTapped(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
        } else {
            sender.isSelected = true
        }
    }
    
    @IBAction func addCommentTapped(_ sender: Any) {
        if self.tfComment.text?.count ?? 0 > 0 {
            self.addComment()
        }
    }
    
    @IBAction func likeTapped(_ sender: UIButton) {
        self.addLike()
    }
    
    @IBAction func shareTapped(_ sender: Any) {
        let items = [objDetail.articleShareUrl]
        let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
        present(ac, animated: true)
    }
}

// MARK: - Tableview Delegate-
extension GuideDetailVC : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.objDetail.arrCommment.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        tableView.register(UINib(nibName: "CommentCell", bundle: nil), forCellReuseIdentifier: "CommentCell")
        var cell : CommentCell! = tableView.dequeueReusableCell(withIdentifier: "CommentCell") as? CommentCell
        
        if (cell == nil) {
            cell = CommentCell.init(style: UITableViewCell.CellStyle.default,
                                          reuseIdentifier:"CommentCell");
        }
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        let obj = self.objDetail.arrCommment[indexPath.row]
        cell.lablUserName.text = "By".localized()+" "+obj.userName
        cell.lablComment.text = obj.comment
        cell.lablDate.text = AppUtility.getElapsedInterval(fromDate: obj.createdDate)
        AppUtility.loadImage(imageView: cell.imgUser, urlString: obj.userImage, placeHolderImageString: "bb")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
}
