//
//  MilestoneVC.swift
//  Dhana
//
//  Created by zubair on 22/06/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON
import AVFoundation
import AVKit

class MilestoneVC: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    @IBOutlet weak var indicator: ANSegmentIndicator!
    @IBOutlet weak var tableProgress: UITableView!
    @IBOutlet weak var collectionMilestone: UICollectionView!
    @IBOutlet weak var tableMilestone: UITableView!
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var imgBaby: UIImageView!
    @IBOutlet weak var lablBabyName: UILabel!
    @IBOutlet weak var lablYear: UILabel!
    @IBOutlet weak var layoutHeightTableProgress: NSLayoutConstraint!
    @IBOutlet weak var lablProgress: UILabel!
    @IBOutlet weak var viewProgress: UIView!
    @IBOutlet weak var viewMilestone: UIView!
    @IBOutlet weak var imgBg: UIImageView!
    @IBOutlet weak var lablCurrentAge: UILabel!
    @IBOutlet weak var lablNextAge: UILabel!
    @IBOutlet weak var lablBackAge: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var tableBaby: UITableView!
    @IBOutlet weak var layoutHeightTableMilestone: NSLayoutConstraint!
    @IBOutlet weak var lablMilestoneTitle: UILabel!
    
    var objMilestone = Milestones()
    var isCameraOpen = false
    var UIPicker = UIImagePickerController()
    var audioPlayer = AVAudioPlayer()
    var player: AVPlayer!
    var selectedIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lablMilestoneTitle.text = lablMilestoneTitle.text?.localized()
        UIPicker.delegate = self
        DispatchQueue.main.async {
            self.viewBg.roundCorners(corners: [.topRight, .topLeft], radius: 24)
        }
        
        AppUtility.showProgress()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        appSceneDelegate.navController = self.navigationController!
        
        for obj in AppManager.shared.arrBabies {
            if appDelegate.babyId != "0" {
                if obj.babyID == appDelegate.babyId {
                    appDelegate.babyId = "0"
                    AppUtility.loadImage(imageView: imgBaby, urlString: obj.babyImage, placeHolderImageString: "bb")
                    lablBabyName.text = obj.babyName
                    lablYear.text = obj.age
                    break
                }
            } else {
                if obj.babyID == AppUtility.getBabyId() {
                    AppUtility.loadImage(imageView: imgBaby, urlString: obj.babyImage, placeHolderImageString: "bb")
                    lablBabyName.text = obj.babyName
                    lablYear.text = obj.age
                }
            }
        }
        if isCameraOpen {
            self.isCameraOpen = false
        } else {
            self.getMilestone(categoryID: "", ageGroupID: "")
        }
        self.tableBaby.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        layoutHeightTableProgress.constant = tableProgress.contentSize.height
        layoutHeightTableMilestone.constant = tableMilestone.contentSize.height
    }
    
    //MARK:- Functions
    func loadData() {
        var settings = ANSegmentIndicatorSettings()
        settings.defaultSegmentColor = COLORS.COLOR_PROGRESS_COLOR
        settings.segmentBorderType = .square
        if objMilestone.total > 0 {
            settings.segmentsCount = Int(objMilestone.total)
        } else {
            settings.segmentsCount = 1
        }
        
        settings.segmentWidth = 16
        settings.segmentColor = .white
        indicator.settings = settings
        let totalPercent:Float = (objMilestone.totalAchieve/objMilestone.total)*100
        if totalPercent == 0 || totalPercent > 0 {
            indicator.updateProgress(percent: Degrees(totalPercent))
        } else {
            indicator.updateProgress(percent: Degrees(0))
        }
        
        //lablCurrentAge.text = 
        
        lablProgress.text = "Progress".localized()+" "+String(Int(objMilestone.totalAchieve))+"/"+String(Int(objMilestone.total))
        let color = UIColor.init(hexString: objMilestone.objSelectedCategory.color)
//
//        viewProgress.backgroundColor = color
        imgBg.backgroundColor = color
        imgBg.alpha = 0.6
        lablCurrentAge.text = objMilestone.objSelectedAgeGroup.startMonth+" - "+objMilestone.objSelectedAgeGroup.endMonth+" months"
        if objMilestone.objNextAgeGroup.groupId.count > 0 {
            btnNext.isHidden = false
            lablNextAge.text = objMilestone.objNextAgeGroup.startMonth+" - "+objMilestone.objNextAgeGroup.endMonth
        } else {
            lablNextAge.text = ""
            btnNext.isHidden = true
        }
        if objMilestone.objPreviousAgeGroup.groupId.count > 0 {
            btnBack.isHidden = false
            lablBackAge.text = objMilestone.objPreviousAgeGroup.startMonth+" - "+objMilestone.objPreviousAgeGroup.endMonth
        } else {
            lablBackAge.text = ""
            btnBack.isHidden = true
        }
        
        tableProgress.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
        tableMilestone.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
//        viewBg.backgroundColor = color
    }
    
    //MARK:- Apis Functions
    func getMilestone(categoryID:String, ageGroupID:String) {
        self.view.endEditing(true)
        var paramDict: [String: Any]?
        if categoryID.count > 0 {
            paramDict = ["milestoneCategoryId":categoryID, "appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":AppUtility.getBabyId()]
        } else if ageGroupID.count > 0 {
            paramDict = ["ageGroupId":ageGroupID, "appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":AppUtility.getBabyId()]
        } else if ageGroupID.count > 0 && categoryID.count > 0 {
            paramDict = ["ageGroupId":ageGroupID, "appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":AppUtility.getBabyId(), "milestoneCategoryId":categoryID]
        } else {
            paramDict = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":AppUtility.getBabyId()]
        }
        APIRequestUtil.getMilestones(parameters: paramDict!) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.objMilestone = Milestones.init(withDictionary: dict)
                    self.tableProgress.reloadData()
                    self.tableMilestone.reloadData()
                    self.collectionMilestone.reloadData()
                    self.loadData()
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    func saveMilestone(objMilestone:Milestones) {
        self.view.endEditing(true)
        let paramDict: [String: Any] = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":AppUtility.getBabyId(), "milestoneId":objMilestone.ID, "babyMilestoneAnswer":objMilestone.answer, "babyMilestoneNotes":objMilestone.babyNotes]
        if objMilestone.isAudio {
            APIRequestUtil.addMilestoneArchive(parameters: paramDict, data:objMilestone.audioData, extensionString:"audio/m4a" , fileNameKey: "babyMilestoneMedia") { (result, error) in
                if(result != nil) {
                    let swiftyJsonVar = JSON(result!)

                    let dict = swiftyJsonVar.dictionaryValue
                    let status = dict["status"]?.intValue
                    let message = dict["message"]?.string
                    
                    if status == SUCESS_CODE {
                        
                        AppUtility.showSuccessMessage(message: message ?? "Milestone saved successfully!".localized())
                        self.getMilestone(categoryID: self.objMilestone.arrCategory[self.selectedIndex].catID, ageGroupID: objMilestone.objSelectedAgeGroup.groupId)
                        
                    } else {
                        
                        AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                        return
                    }
                }
            }
        } else {
            let data = objMilestone.imageMedia.jpegData(compressionQuality: 0.5)
            APIRequestUtil.addMilestoneArchive(parameters: paramDict, data:data ?? Data() , extensionString:"image/jpeg" , fileNameKey: "babyMilestoneMedia") { (result, error) in
                if(result != nil) {
                    let swiftyJsonVar = JSON(result!)

                    let dict = swiftyJsonVar.dictionaryValue
                    let status = dict["status"]?.intValue
                    let message = dict["message"]?.string
                    
                    if status == SUCESS_CODE {
                        
                        AppUtility.showSuccessMessage(message: message ?? "Milestone saved successfully!".localized())
                        self.getMilestone(categoryID: self.objMilestone.arrCategory[self.selectedIndex].catID, ageGroupID: objMilestone.objSelectedAgeGroup.groupId)
                        
                    } else {
                        
                        AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                        return
                    }
                }
            }
        }
    }
    
    func getThumbnailImageFromVideoUrl(url: URL, completion: @escaping ((_ image: UIImage?)->Void)) {
        DispatchQueue.global().async { //1
            let asset = AVAsset(url: url) //2
            let avAssetImageGenerator = AVAssetImageGenerator(asset: asset) //3
            avAssetImageGenerator.appliesPreferredTrackTransform = true //4
            let thumnailTime = CMTimeMake(value: 2, timescale: 1) //5
            do {
                let cgThumbImage = try avAssetImageGenerator.copyCGImage(at: thumnailTime, actualTime: nil) //6
                let thumbNailImage = UIImage(cgImage: cgThumbImage) //7
                DispatchQueue.main.async { //8
                    completion(thumbNailImage) //9
                }
            } catch {
                print(error.localizedDescription) //10
                DispatchQueue.main.async {
                    completion(nil) //11
                }
            }
        }
    }
    
    //MARK: -ImagePickerView Delegates
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let chosenImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        self.objMilestone.arrMilestone[self.UIPicker.view.tag].imageMedia = chosenImage
        self.objMilestone.arrMilestone[self.UIPicker.view.tag].isAudio = false
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.isCameraOpen = false
        dismiss(animated: true, completion: nil)
    }
    
    //MARK:- Button Actions
    @objc func categorySelectTapped(_ sender:UIButton) {
        selectedIndex = sender.tag
        self.getMilestone(categoryID: self.objMilestone.arrCategory[sender.tag].catID, ageGroupID: "")
    }
    
    @IBAction func sideMenuTapped(_ sender: Any) {
        self.sideMenuViewController.presentRightMenuViewController()
    }
    
    @IBAction func notifyTapped(_ sender: Any) {
        let viewController = NotificationVC(nibName:String(describing: NotificationVC.self), bundle:nil)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func nextAgeTapped(_ sender: Any) {
        self.getMilestone(categoryID: "", ageGroupID: objMilestone.objNextAgeGroup.groupId)
    }
    
    @IBAction func backAgeTapped(_ sender: Any) {
        self.getMilestone(categoryID: "", ageGroupID: objMilestone.objPreviousAgeGroup.groupId)
    }
    
    @IBAction func yesArchiveTapped(_ sender: UIButton) {
        objMilestone.arrMilestone[sender.tag].answer = "Yes"
        
        let indexPath = IndexPath(row: sender.tag, section: 0)
        let cell = tableMilestone.cellForRow(at: indexPath) as! MilestoneMediaCell
        cell.btnYes.backgroundColor = .darkGray
        cell.btnYes.setTitleColor(.white, for: .normal)
        cell.btnNo.backgroundColor = #colorLiteral(red: 0.9568627451, green: 0.9568627451, blue: 0.9568627451, alpha: 1)
        cell.btnNo.setTitleColor(.black, for: .normal)
        cell.btnNotSure.backgroundColor = #colorLiteral(red: 0.9568627451, green: 0.9568627451, blue: 0.9568627451, alpha: 1)
        cell.btnNotSure.setTitleColor(.black, for: .normal)
    }
    
    @IBAction func noArchiveTapped(_ sender: UIButton) {
        objMilestone.arrMilestone[sender.tag].answer = "No"
        
        let indexPath = IndexPath(row: sender.tag, section: 0)
        let cell = tableMilestone.cellForRow(at: indexPath) as! MilestoneMediaCell
        cell.btnNo.backgroundColor = .darkGray
        cell.btnNo.setTitleColor(.white, for: .normal)
        cell.btnYes.backgroundColor = #colorLiteral(red: 0.9568627451, green: 0.9568627451, blue: 0.9568627451, alpha: 1)
        cell.btnYes.setTitleColor(.black, for: .normal)
        cell.btnNotSure.backgroundColor = #colorLiteral(red: 0.9568627451, green: 0.9568627451, blue: 0.9568627451, alpha: 1)
        cell.btnNotSure.setTitleColor(.black, for: .normal)
    }
    
    @IBAction func notsureArchiveTapped(_ sender: UIButton) {
        objMilestone.arrMilestone[sender.tag].answer = "Not Sure"
        
        let indexPath = IndexPath(row: sender.tag, section: 0)
        let cell = tableMilestone.cellForRow(at: indexPath) as! MilestoneMediaCell
        cell.btnNotSure.backgroundColor = .darkGray
        cell.btnNotSure.setTitleColor(.white, for: .normal)
        cell.btnNo.backgroundColor = #colorLiteral(red: 0.9568627451, green: 0.9568627451, blue: 0.9568627451, alpha: 1)
        cell.btnNo.setTitleColor(.black, for: .normal)
        cell.btnYes.backgroundColor = #colorLiteral(red: 0.9568627451, green: 0.9568627451, blue: 0.9568627451, alpha: 1)
        cell.btnYes.setTitleColor(.black, for: .normal)
    }
    
    @IBAction func archiveTapped(_ sender: UIButton) {
        if objMilestone.arrMilestone[sender.tag].answer.count == 0 {
            AppUtility.showInfoMessage(message: "please select your answer".localized())
            return
        }
        
        self.saveMilestone(objMilestone: objMilestone.arrMilestone[sender.tag])
    }
    
    @IBAction func readMmoreTapped(_ sender: UIButton) {
        if objMilestone.arrMilestone[sender.tag].isAnswerExapnd {
            objMilestone.arrMilestone[sender.tag].isAnswerExapnd = false
        } else {
            objMilestone.arrMilestone[sender.tag].isAnswerExapnd = true
        }
        tableMilestone.reloadData()
    }
    
    @IBAction func mediaTapped(_ sender: UIButton) {
        self.isCameraOpen = true
        self.UIPicker.view.tag = sender.tag
        self.UIPicker.allowsEditing = true
        self.UIPicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.UIPicker.modalPresentationStyle = .fullScreen
        self.present(self.UIPicker, animated: true, completion: nil)
    }
    
    @IBAction func cameraTapped(_ sender: UIButton) {
        self.isCameraOpen = true
        self.UIPicker.view.tag = sender.tag
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            self.UIPicker.allowsEditing = false
            self.UIPicker.sourceType = UIImagePickerController.SourceType.camera
            self.UIPicker.cameraCaptureMode = .photo
            self.UIPicker.modalPresentationStyle = .fullScreen
            self.present(self.UIPicker, animated: true, completion: nil)
        }
    }
    
    @IBAction func audioTapped(_ sender: UIButton) {
        self.isCameraOpen = true
        let viewController = AddAudioVC(nibName:String(describing: AddAudioVC.self), bundle:nil)
        viewController.delegate = self
        viewController.index = sender.tag
        self.navigationController?.pushViewController(viewController, animated: false)
    }
    
    @IBAction func playMileStoneVideoTapped(_ sender:UIButton) {
        let obj = self.objMilestone.arrMilestone[sender.tag]
        if obj.video.count > 0 {
            let player = AVPlayer(url: URL(string:obj.video)!)
            let vc = AVPlayerViewController()
            vc.player = player
            self.present(vc, animated: true) { vc.player?.play() }
        }
    }
    
    @IBAction func playAudioTapped(_ sender: UIButton) {
        if self.objMilestone.arrMilestone[sender.tag].isPlay == true {
            self.objMilestone.arrMilestone[sender.tag].isPlay = false
        } else {
            self.objMilestone.arrMilestone[sender.tag].isPlay = true
        }
        self.tableMilestone.tag = sender.tag
        self.tableMilestone.reloadData()
    }
    
    @IBAction func dropDownBabyTapped(_ sender: UIButton) {
        if sender.tag == 0 {
            sender.tag = 1
            tableBaby.isHidden = false
        } else {
            sender.tag = 0
            tableBaby.isHidden = true
        }
    }
    
    @objc func playerDidFinishPlaying(sender: Notification) {
        if self.objMilestone.arrMilestone[tableMilestone.tag].isPlay == true {
            self.objMilestone.arrMilestone[tableMilestone.tag].isPlay = false
        } else {
            self.objMilestone.arrMilestone[tableMilestone.tag].isPlay = true
        }
    }
}

// MARK: - Tableview Delegate-
extension MilestoneVC : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableProgress {
            return self.objMilestone.arrProgress.count
        } else if tableView == tableBaby {
            return AppManager.shared.arrBabies.count
        }
        return self.objMilestone.arrMilestone.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        if tableView == tableProgress {
            tableView.register(UINib(nibName: "MilestoneProgressCell", bundle: nil), forCellReuseIdentifier: "MilestoneProgressCell")
            var cell : MilestoneProgressCell! = tableView.dequeueReusableCell(withIdentifier: "MilestoneProgressCell") as? MilestoneProgressCell
            
            if (cell == nil) {
                cell = MilestoneProgressCell.init(style: UITableViewCell.CellStyle.default,
                                              reuseIdentifier:"MilestoneProgressCell");
            }
            
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            
            cell.lablTitle.text = self.objMilestone.arrProgress[indexPath.row].titleEn
    
            cell.stepProgress.tag = indexPath.row
            if let removable = cell.stepProgress.viewWithTag(indexPath.row) {
               removable.removeFromSuperview()
            }
            cell.stepProgress = StepIndicatorView()
            cell.stepProgress.frame = cell.viewStep.bounds
            cell.viewStep.addSubview(cell.stepProgress)
            cell.stepProgress.numberOfSteps = self.objMilestone.arrProgress[indexPath.row].totalCount
            cell.stepProgress.currentStep = self.objMilestone.arrProgress[indexPath.row].achieveCount
            let color = UIColor.init(hexString: self.objMilestone.arrProgress[indexPath.row].color)
            cell.stepProgress.circleColor = color
            cell.stepProgress.circleTintColor = color
            cell.stepProgress.lineColor = color
            cell.stepProgress.lineTintColor = color
            cell.lablAchievement.text = String(self.objMilestone.arrProgress[indexPath.row].achieveCount)+" "+"achievement".localized()
            cell.lablAchievement.textColor = color
            cell.lablTitle.textColor = color
            cell.lablStatus.textColor = color
            
            if self.objMilestone.arrProgress[indexPath.row].totalCount > self.objMilestone.arrProgress[indexPath.row].achieveCount {
                let tp = self.objMilestone.arrProgress[indexPath.row].totalCount-self.objMilestone.arrProgress[indexPath.row].achieveCount
                cell.lablStatus.text = String(tp)+" "+"to go".localized()
                cell.lablStatus.textColor = .lightGray
            } else {
                cell.lablAchievement.textColor = color
                cell.lablTitle.textColor = color
                cell.lablStatus.textColor = color
                cell.lablStatus.text = "completed".localized()
            }
            
            return cell
            
        } else if tableView == tableBaby {
                
            tableView.register(UINib(nibName: "BabyCell", bundle: nil), forCellReuseIdentifier: "BabyCell")
            var cell : BabyCell! = tableView.dequeueReusableCell(withIdentifier: "BabyCell") as? BabyCell
            
            if (cell == nil) {
                cell = BabyCell.init(style: UITableViewCell.CellStyle.default,
                                              reuseIdentifier:"BabyCell");
            }
            
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            
            let objG = AppManager.shared.arrBabies[indexPath.row]
            cell.lablBabyName.text = objG.babyName
            cell.lablYear.text = objG.age
            AppUtility.loadImage(imageView: cell.imgBaby, urlString: objG.babyImage, placeHolderImageString: "bb")
            
            return cell
            
        } else {
            
            let obj = self.objMilestone.arrMilestone[indexPath.row]
            
            if obj.babyMediaType == "audio" {

                tableView.register(UINib(nibName: "MilestoneAudioCell", bundle: nil), forCellReuseIdentifier: "MilestoneAudioCell")
                var cell : MilestoneAudioCell! = tableView.dequeueReusableCell(withIdentifier: "MilestoneAudioCell") as? MilestoneAudioCell

                if (cell == nil) {
                    cell = MilestoneAudioCell.init(style: UITableViewCell.CellStyle.default,
                                                  reuseIdentifier:"MilestoneAudioCell");
                }

                cell.selectionStyle = UITableViewCell.SelectionStyle.none
                
                if obj.isAchieved {
                    cell.layoutHeightNotArchived.constant = 0
                    cell.viewAchived.isHidden = true
                    cell.viewQuestion.isHidden = true
                    cell.imgArchive.image = UIImage(named: "tick")
                    cell.lablDate.text = AppUtility.convertDateFormater(obj.babyDate)
                    cell.lablMonth.text = AppUtility.getElapsedInterval(fromDate: obj.babyDate)
                    cell.tfNotes.text = obj.babyNotes
                } else {
                    cell.viewAchived.isHidden = false
                    cell.imgArchive.image = UIImage(named: "circle")
                    cell.lablDate.text = "Not achieved yet".localized()
                    cell.lablMonth.text = ""
                    cell.tfNotes.text = ""
                    
                    let totallQuestionLines = cell.lablQuestion.calculateMaxLines()
                    
                    if obj.isAnswerExapnd {
                        cell.btnReadMore.setTitle("Read less".localized(), for: .normal)
                        cell.viewQuestion.isHidden = false
                        cell.layoutHeightQuestion.constant = 70+(CGFloat(totallQuestionLines)*19.5)
                        cell.layoutHeightNotArchived.constant = 240+(CGFloat(totallQuestionLines)*19.5)
                    } else {
                        cell.btnReadMore.setTitle("Read more".localized(), for: .normal)
                        cell.viewQuestion.isHidden = true
                        cell.layoutHeightQuestion.constant = 0
                        cell.layoutHeightNotArchived.constant = 170
                    }
                }
                
                AppUtility.loadImage(imageView: cell.imgCareGiver, urlString: AppUtility.getUserLoggedIn().userImage, placeHolderImageString: "bb")
                cell.lablUsername.text = AppUtility.getUserLoggedIn().username
            
                cell.lablTitle.text = obj.titleEn
                cell.lablDetail.text = obj.descriptionEn
                cell.lablQuestion.text = obj.questionEn
            
//                if obj.babyMedia.count > 0 {
//                    cell.btnPlay.isHidden = true
//                    AppUtility.loadImage(imageView: cell.imgProduct, urlString: obj.babyMedia, placeHolderImageString: "bb")
//                } else {
//                    cell.btnPlay.isHidden = false
//                    AppUtility.loadImage(imageView: cell.imgProduct, urlString: obj.milestoneThumbnail, placeHolderImageString: "bb")
//                }
                
                let item = AVPlayerItem(url: URL(string: obj.babyMedia)!)
                let duration = Double(item.asset.duration.value) / Double(item.asset.duration.timescale)
                
                //let currentTime1 = Int((player?.currentTime)!)
                let minutes = duration/60
                let seconds = Int(duration) - Int(minutes) * 60
                cell.lablAudioTime.text = NSString(format: "%02d:%02d", minutes,seconds) as String
                cell.progressAudio.setProgress(Float(0), animated: false)
                
                if !obj.isPlay {
                    
                    obj.isPlay = false
                    if player != nil
                    {
                        player.pause()
                    }
                    cell.btnPlayAudio.isSelected = false
                    
                } else {
                    cell.btnPlayAudio.isSelected = true
                    obj.isPlay = true
                    player = AVPlayer()
                    if player != nil
                    {
                        player.pause()
                    }
                    
                    let url = URL(string: obj.babyMedia)
                    let playerItem = CachingPlayerItem(url: url!)
                    NotificationCenter.default.addObserver(self, selector: #selector(self.playerDidFinishPlaying(sender:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
                    playerItem.delegate = self
                    player = AVPlayer(playerItem: playerItem)
                    player.automaticallyWaitsToMinimizeStalling = true
                    player.play()
                    
                    player!.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(1, preferredTimescale: 1), queue: DispatchQueue.main) { (CMTime) -> Void in
                        if self.player!.currentItem?.status == .readyToPlay {
                            let time : Float64 = CMTimeGetSeconds(self.player!.currentTime());
                            let duration = Int(time)
                            let minutes2 = duration/60
                            let seconds2 = duration - minutes2 * 60
                            cell.lablAudioTime.text = NSString(format: "%02d:%02d", minutes2,seconds2) as String
                            
                            guard let currentTimes = self.player?.currentTime, let durations = self.player.currentItem?.asset.duration else {
                                  return
                                }
                            let seconds = CMTimeGetSeconds(durations)
                            let secondss = CMTimeGetSeconds(currentTimes())
                            let ff:Float = Float(secondss)
                            let pf:Float = Float(seconds)
                            let percentCompleted = ff/pf
                            cell.progressAudio.setProgress(Float(percentCompleted), animated: true)
                        }
                        
                        let playbackLikelyToKeepUp = self.player?.currentItem?.isPlaybackLikelyToKeepUp
                        if playbackLikelyToKeepUp == false {
                            print("IsBuffering")
//                            self.ButtonPlay.isHidden = true
//                            self.loadingView.isHidden = false
                        } else {
                            //stop the activity indicator
//                            print("Buffering completed")
//                            self.ButtonPlay.isHidden = false
//                            self.loadingView.isHidden = true
                        }
                        
                    }
                
                }
            
                if objMilestone.answer.count == 0 {
                    cell.btnNo.backgroundColor = #colorLiteral(red: 0.9568627451, green: 0.9568627451, blue: 0.9568627451, alpha: 1)
                    cell.btnNo.setTitleColor(.black, for: .normal)
                    cell.btnYes.backgroundColor = #colorLiteral(red: 0.9568627451, green: 0.9568627451, blue: 0.9568627451, alpha: 1)
                    cell.btnYes.setTitleColor(.black, for: .normal)
                    cell.btnNotSure.backgroundColor = #colorLiteral(red: 0.9568627451, green: 0.9568627451, blue: 0.9568627451, alpha: 1)
                    cell.btnNotSure.setTitleColor(.black, for: .normal)
                }
                
                cell.tfNotes.placeholder = "Notes".localized()
                cell.tfNotes.text = obj.babyNotes
                cell.tfNotes.tag = indexPath.row
                cell.tfNotes.delegate = self
                
                cell.btnMedia.tag = indexPath.row
                cell.btnAudio.tag = indexPath.row
                cell.btnCamera.tag = indexPath.row
                cell.btnNo.tag = indexPath.row
                cell.btnYes.tag = indexPath.row
                cell.btnAchieve.tag = indexPath.row
                cell.btnReadMore.tag = indexPath.row
                cell.btnNotSure.tag = indexPath.row
                cell.btnPlayAudio.tag = indexPath.row
                cell.progressAudio.tag = indexPath.row
                //cell.btnPlay.tag = indexPath.row
            
                cell.btnMedia.addTarget(self, action: #selector(self.mediaTapped(_:)), for: .touchUpInside)
                cell.btnCamera.addTarget(self, action: #selector(self.cameraTapped(_:)), for: .touchUpInside)
                cell.btnAudio.addTarget(self, action: #selector(self.audioTapped(_:)), for: .touchUpInside)
                cell.btnNo.addTarget(self, action: #selector(self.noArchiveTapped(_:)), for: .touchUpInside)
                cell.btnYes.addTarget(self, action: #selector(self.yesArchiveTapped(_:)), for: .touchUpInside)
                cell.btnNotSure.addTarget(self, action: #selector(self.notsureArchiveTapped(_:)), for: .touchUpInside)
                cell.btnAchieve.addTarget(self, action: #selector(self.archiveTapped(_:)), for: .touchUpInside)
                cell.btnReadMore.addTarget(self, action: #selector(self.readMmoreTapped(_:)), for: .touchUpInside)
                cell.btnPlayAudio.addTarget(self, action: #selector(self.playAudioTapped(_:)), for: .touchUpInside)
                //cell.btnPlay.addTarget(self, action: #selector(self.playMileStoneVideoTapped(_:)), for: .touchUpInside)

                return cell

            } else {
            
                tableView.register(UINib(nibName: "MilestoneMediaCell", bundle: nil), forCellReuseIdentifier: "MilestoneMediaCell")
                var cell : MilestoneMediaCell! = tableView.dequeueReusableCell(withIdentifier: "MilestoneMediaCell") as? MilestoneMediaCell
                
                if (cell == nil) {
                    cell = MilestoneMediaCell.init(style: UITableViewCell.CellStyle.default,
                                                  reuseIdentifier:"MilestoneMediaCell");
                }
                
                cell.selectionStyle = UITableViewCell.SelectionStyle.none
                
                if obj.isAchieved {
                    cell.layoutHeightNotArchived.constant = 0
                    cell.viewAchived.isHidden = true
                    cell.viewQuestion.isHidden = true
                    cell.imgArchive.image = UIImage(named: "tick")
                    cell.lablDate.text = AppUtility.convertDateFormater(obj.babyDate)
                    cell.lablMonth.text = AppUtility.getElapsedInterval(fromDate: obj.babyDate)
                    cell.tfNotes.text = obj.babyNotes
                } else {
                    cell.viewAchived.isHidden = false
                    cell.imgArchive.image = UIImage(named: "circle")
                    cell.lablDate.text = "Not achieved yet".localized()
                    cell.lablMonth.text = ""
                    cell.tfNotes.text = ""
                    
                    let totallQuestionLines = cell.lablQuestion.calculateMaxLines()
                    
                    if obj.isAnswerExapnd {
                        cell.btnReadMore.setTitle("Read less".localized(), for: .normal)
                        cell.viewQuestion.isHidden = false
                        cell.layoutHeightQuestion.constant = 70+(CGFloat(totallQuestionLines)*19.5)
                        cell.layoutHeightNotArchived.constant = 240+(CGFloat(totallQuestionLines)*19.5)
                    } else {
                        cell.btnReadMore.setTitle("Read more".localized(), for: .normal)
                        cell.viewQuestion.isHidden = true
                        cell.layoutHeightQuestion.constant = 0
                        cell.layoutHeightNotArchived.constant = 170
                    }
                }
                
                AppUtility.loadImage(imageView: cell.imgCareGiver, urlString: AppUtility.getUserLoggedIn().userImage, placeHolderImageString: "bb")
                cell.lablUsername.text = AppUtility.getUserLoggedIn().username
            
                cell.lablTitle.text = obj.titleEn
                cell.lablDetail.text = obj.descriptionEn
                cell.lablQuestion.text = obj.questionEn
            
                if obj.babyMedia.count > 0 {
                    cell.layoutHeightImage.constant = 168
                    cell.btnPlay.isHidden = true
                    AppUtility.loadImage(imageView: cell.imgProduct, urlString: obj.babyMedia, placeHolderImageString: "bb")
                } else {
                    if obj.milestoneThumbnail.count > 0 {
                        cell.btnPlay.isHidden = false
                        cell.layoutHeightImage.constant = 168
                    } else {
                        cell.btnPlay.isHidden = true
                        cell.layoutHeightImage.constant = 0
                    }
                    AppUtility.loadImage(imageView: cell.imgProduct, urlString: obj.milestoneThumbnail, placeHolderImageString: "bb")
                }
            
//                self.getThumbnailImageFromVideoUrl(url: URL(string: obj.video)!) { (thumbNailImage) in
//                    cell.imgProduct.image = thumbNailImage
//                }
            
                if objMilestone.answer.count == 0 {
                    cell.btnNo.backgroundColor = #colorLiteral(red: 0.9568627451, green: 0.9568627451, blue: 0.9568627451, alpha: 1)
                    cell.btnNo.setTitleColor(.black, for: .normal)
                    cell.btnYes.backgroundColor = #colorLiteral(red: 0.9568627451, green: 0.9568627451, blue: 0.9568627451, alpha: 1)
                    cell.btnYes.setTitleColor(.black, for: .normal)
                    cell.btnNotSure.backgroundColor = #colorLiteral(red: 0.9568627451, green: 0.9568627451, blue: 0.9568627451, alpha: 1)
                    cell.btnNotSure.setTitleColor(.black, for: .normal)
                }
                
                cell.tfNotes.placeholder = "Notes".localized()
                cell.tfNotes.text = obj.babyNotes
                cell.tfNotes.tag = indexPath.row
                cell.tfNotes.delegate = self
                
                cell.btnMedia.tag = indexPath.row
                cell.btnAudio.tag = indexPath.row
                cell.btnCamera.tag = indexPath.row
                cell.btnNo.tag = indexPath.row
                cell.btnYes.tag = indexPath.row
                cell.btnAchieve.tag = indexPath.row
                cell.btnReadMore.tag = indexPath.row
                cell.btnNotSure.tag = indexPath.row
                cell.btnPlay.tag = indexPath.row
            
                cell.btnMedia.addTarget(self, action: #selector(self.mediaTapped(_:)), for: .touchUpInside)
                cell.btnCamera.addTarget(self, action: #selector(self.cameraTapped(_:)), for: .touchUpInside)
                cell.btnAudio.addTarget(self, action: #selector(self.audioTapped(_:)), for: .touchUpInside)
                cell.btnNo.addTarget(self, action: #selector(self.noArchiveTapped(_:)), for: .touchUpInside)
                cell.btnYes.addTarget(self, action: #selector(self.yesArchiveTapped(_:)), for: .touchUpInside)
                cell.btnNotSure.addTarget(self, action: #selector(self.notsureArchiveTapped(_:)), for: .touchUpInside)
                cell.btnAchieve.addTarget(self, action: #selector(self.archiveTapped(_:)), for: .touchUpInside)
                cell.btnReadMore.addTarget(self, action: #selector(self.readMmoreTapped(_:)), for: .touchUpInside)
                cell.btnPlay.addTarget(self, action: #selector(self.playMileStoneVideoTapped(_:)), for: .touchUpInside)
                
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tableProgress {
           return 48
        } else if tableView == tableBaby {
            return 50
        } else {
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tableBaby {
            self.tableBaby.isHidden = true
            AppUtility.saveBabyId(babyID: AppManager.shared.arrBabies[indexPath.row].babyID)
            
            self.getMilestone(categoryID: objMilestone.objSelectedCategory.catID, ageGroupID: objMilestone.objSelectedAgeGroup.groupId)
            
            AppUtility.loadImage(imageView: self.imgBaby, urlString: AppManager.shared.arrBabies[indexPath.row].babyImage, placeHolderImageString: "bb")
            self.lablBabyName.text = AppManager.shared.arrBabies[indexPath.row].babyName
            self.lablYear.text = AppManager.shared.arrBabies[indexPath.row].age
        }
    }
}

// MARK: - CollectionView Delegate-
extension MilestoneVC: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.objMilestone.arrCategory.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return  CGSize(width: 130, height: 45)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        collectionView.register(UINib(nibName: String(describing: CategoryCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: CategoryCell.self))
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: CategoryCell.self), for: indexPath as IndexPath) as! CategoryCell
        
        cell.btnCategory.addTarget(self, action: #selector(self.categorySelectTapped(_:)), for: .touchUpInside)
        cell.btnCategory.tag = indexPath.row
        
        if objMilestone.objSelectedCategory.catID == self.objMilestone.arrCategory[indexPath.row].catID {
            cell.btnCategory.endColor = COLORS.TABBAR_COLOR
            cell.btnCategory.startColor = COLORS.APP_THEME_COLOR
            cell.btnCategory.setTitleColor(.white, for: .normal)
        } else {
            cell.btnCategory.endColor = COLORS.UNSELECT_CATEGORY_COLOR
            cell.btnCategory.startColor = COLORS.UNSELECT_CATEGORY_COLOR
            cell.btnCategory.setTitleColor(COLORS.TABBAR_COLOR, for: .normal)
        }
        
        cell.btnCategory.setTitle(self.objMilestone.arrCategory[indexPath.row].titleEn, for: .normal)
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}

// MARK: - textfield Delegate-
extension MilestoneVC:UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.objMilestone.arrMilestone[textField.tag].babyNotes = textField.text ?? ""
        self.tableMilestone.reloadData()
    }
}

// MARK: - textfield Delegate-
extension MilestoneVC:AddAudioVCDelegate {
    func addAudio(audioData: Data, index: Int) {
        self.objMilestone.arrMilestone[index].isAudio = true
        self.objMilestone.arrMilestone[index].audioData = audioData
    }
}

//MARK:- Audio delegate
extension MilestoneVC:CachingPlayerItemDelegate {
    func playerItem(_ playerItem: CachingPlayerItem, didFinishDownloadingData data: Data)
    {
        print("File is downloaded and ready for storing")
        
        player.play()
    }
    
    func playerItem(_ playerItem: CachingPlayerItem, didDownloadBytesSoFar bytesDownloaded: Int, outOf bytesExpected: Int)
    {
        print("\(bytesDownloaded)/\(bytesExpected)")
        player.play()
    }
    
    func playerItemPlaybackStalled(_ playerItem: CachingPlayerItem)
    {
        print("Not enough data for playback. Probably because of the poor network. Wait a bit and try to play later.")
    }
    
    func playerItem(_ playerItem: CachingPlayerItem, downloadingFailedWith error: Error)
    {
        print(error)
    }
}
