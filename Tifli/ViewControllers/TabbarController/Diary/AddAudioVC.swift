//
//  AddAudioVC.swift
//  Tifli
//
//  Created by zubair on 29/09/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import AVFoundation
import IQAudioRecorderController

protocol AddAudioVCDelegate {
    func addAudio(audioData:Data, index:Int)
}

class AddAudioVC: UIViewController {

    @IBOutlet weak var viewBg: UIImageView!
    @IBOutlet weak var lablTime: UILabel!
    @IBOutlet weak var progressAudio: UIProgressView!
    @IBOutlet weak var btnRecord: GradientButton!
    @IBOutlet weak var btnDone: GradientButton!
    
    @IBOutlet weak var viewAudioPlay: UIView!
    @IBOutlet var recordButton: UIButton!
    @IBOutlet var playButton: UIButton!
    @IBOutlet weak var statusLabel: UILabel!
    
    var recorder: AVAudioRecorder!
    var player: AVAudioPlayer!
    var meterTimer: Timer!
    var soundFileURL: URL!
    var delegate:AddAudioVCDelegate? = nil
    var index = 0
    var timerProgress = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.statusLabel.text = ""
        playButton.isEnabled = false
        
        setSessionPlayback()
        askForNotifications()
        checkHeadphones()
        
        btnRecord.isHidden = true
        btnDone.isHidden = true
        
        btnRecord.setTitle(btnRecord.titleLabel?.text?.localized(), for: .normal)
        btnDone.setTitle(btnDone.titleLabel?.text?.localized(), for: .normal)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        recorder = nil
        player = nil
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let blurEffect = UIBlurEffect(style: .light)
        let blurredEffectView = UIVisualEffectView(effect: blurEffect)
        blurredEffectView.frame = self.view.bounds
        self.viewBg.addSubview(blurredEffectView)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if timerProgress != nil {
            timerProgress.invalidate()
        }
    }
    
    //MARK:- Audio Funtions
    func play() {
        
        var url: URL?
        if self.recorder != nil {
            url = self.recorder.url
        } else {
            url = self.soundFileURL!
        }
        print("playing \(String(describing: url))")
        
        do {
            self.player = try AVAudioPlayer(contentsOf: url!)
            //stopButton.isEnabled = true
            player.delegate = self
            player.prepareToPlay()
            player.volume = 1.0
            player.play()
            
            timerProgress = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateAudioProgressView), userInfo: nil, repeats: true)
            progressAudio.setProgress(Float(player.currentTime/player.duration), animated: false)
            
        } catch {
            self.player = nil
            print(error.localizedDescription)
        }
    }
    
    func recordWithPermission(_ setup: Bool)
    {
        AVAudioSession.sharedInstance().requestRecordPermission
            {
            [unowned self] granted in
            if granted
            {
                DispatchQueue.main.async
                    {
                    print("Permission to record granted")
                    self.setSessionPlayAndRecord()
                    if setup
                    {
                        self.setupRecorder()
                    }
                    self.recorder.record()
                    
                    self.meterTimer = Timer.scheduledTimer(timeInterval: 0.1,
                                                           target: self,
                                                           selector: #selector(self.updateAudioMeter(_:)),
                                                           userInfo: nil,
                                                           repeats: true)
                }
            }
            else
            {
                print("Permission to record not granted")
            }
        }
        
        if AVAudioSession.sharedInstance().recordPermission == .denied
        {
            print("permission denied")
        }
    }
    
    func setSessionPlayback()
    {
        let session = AVAudioSession.sharedInstance()
        
        do {
            try session.setCategory(AVAudioSession.Category.playback, options: .defaultToSpeaker)
            
        } catch {
            print("could not set session category")
            print(error.localizedDescription)
        }
        
        do {
            try session.setActive(true)
        } catch {
            print("could not make session active")
            print(error.localizedDescription)
        }
    }
    
    func setSessionPlayAndRecord()
    {
        let session = AVAudioSession.sharedInstance()
        do {
            try session.setCategory(AVAudioSession.Category.playAndRecord, options: .defaultToSpeaker)
        } catch {
            print("could not set session category")
            print(error.localizedDescription)
        }
        
        do {
            try session.setActive(true)
        } catch {
            print("could not make session active")
            print(error.localizedDescription)
        }
    }
    
    func setupRecorder()
    {
        do {
            let tmpDirectory = try FileManager.default.contentsOfDirectory(atPath: NSTemporaryDirectory())
            try tmpDirectory.forEach { file in
                let path = String.init(format: "%@%@", NSTemporaryDirectory(), file)
                try FileManager.default.removeItem(atPath: path)
            }
        } catch {
            print(error)
        }
        
        var uuid: String = ProcessInfo().globallyUniqueString
        
        uuid = String(format:"%@.m4a",uuid)
        
        self.soundFileURL = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(uuid)

        let recordSettings: [String: Any] = [
            AVFormatIDKey: kAudioFormatMPEG4AAC,
            AVEncoderAudioQualityKey: AVAudioQuality.max.rawValue,
            AVEncoderBitRateKey: 32000,
            AVNumberOfChannelsKey: 1,
            AVSampleRateKey: 44100.0,
        ]
        
        
        do {
            recorder = try AVAudioRecorder(url: soundFileURL, settings: recordSettings)
            recorder.delegate = self
            recorder.isMeteringEnabled = true
            recorder.prepareToRecord() // creates/overwrites the file at soundFileURL
        } catch {
            recorder = nil
            print(error.localizedDescription)
        }
        
    }
    
    func askForNotifications() {
        print("\(#function)")
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(AddAudioVC.background(_:)),
                                               name: UIApplication.willResignActiveNotification,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(AddAudioVC.foreground(_:)),
                                               name: UIApplication.willEnterForegroundNotification,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(AddAudioVC.routeChange(_:)),
                                               name: AVAudioSession.routeChangeNotification,
                                               object: nil)
    }
    
    @objc func background(_ notification: Notification) {
        print("\(#function)")
        
    }
    
    @objc func foreground(_ notification: Notification) {
        print("\(#function)")
        
    }
    
    @objc func routeChange(_ notification: Notification)
    {
        if let userInfo = (notification as NSNotification).userInfo {
            print("routeChange \(userInfo)")
            if let reason = userInfo[AVAudioSessionRouteChangeReasonKey] as? UInt {
                //print("reason \(reason)")
                switch AVAudioSession.RouteChangeReason(rawValue: reason)! {
                case AVAudioSessionRouteChangeReason.newDeviceAvailable:
                    print("NewDeviceAvailable")
                    print("did you plug in headphones?")
                    checkHeadphones()
                case AVAudioSessionRouteChangeReason.oldDeviceUnavailable:
                    print("OldDeviceUnavailable")
                    print("did you unplug headphones?")
                    checkHeadphones()
                case AVAudioSessionRouteChangeReason.categoryChange:
                    print("CategoryChange")
                case AVAudioSessionRouteChangeReason.override:
                    print("Override")
                case AVAudioSessionRouteChangeReason.wakeFromSleep:
                    print("WakeFromSleep")
                case AVAudioSessionRouteChangeReason.unknown:
                    print("Unknown")
                case AVAudioSessionRouteChangeReason.noSuitableRouteForCategory:
                    print("NoSuitableRouteForCategory")
                case AVAudioSessionRouteChangeReason.routeConfigurationChange:
                    print("RouteConfigurationChange")
                    
                }
            }
        }
    }
    
    func checkHeadphones() {
        print("\(#function)")
        
        // check NewDeviceAvailable and OldDeviceUnavailable for them being plugged in/unplugged
        let currentRoute = AVAudioSession.sharedInstance().currentRoute
        if !currentRoute.outputs.isEmpty {
            for description in currentRoute.outputs {
                if description.portType == AVAudioSession.Port.headphones {
                    print("headphones are plugged in")
                    break
                } else {
                    print("headphones are unplugged")
                }
            }
        } else {
            print("checking headphones requires a connection to a device")
        }
    }
    
    @objc func updateAudioMeter(_ timer: Timer)
    {
        if let recorder = self.recorder
        {
            if recorder.isRecording
            {
                let min = Int(recorder.currentTime / 60)
                let sec = Int(recorder.currentTime.truncatingRemainder(dividingBy: 60))
                let s = String(format: "%02d:%02d", min, sec)
                statusLabel.text = s
                recorder.updateMeters()
            }
        }
    }
    
    @objc func updateAudioProgressView() {
        if player.isPlaying {
            let duration = Int(player!.duration - player!.currentTime)
            let minutes2 = duration/60
            let seconds2 = duration - minutes2 * 60
            self.lablTime.text = NSString(format: "%02d:%02d", minutes2,seconds2) as String
            progressAudio.setProgress(Float(player.currentTime/player.duration), animated: true)
        }
    }
    
    //MARK:- Button Actions
    @IBAction func clloseTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func addVoiceTapped(_ sender: Any) {
        if self.recordButton.isSelected {
            
            self.recordButton.isSelected = false
            
            recorder?.stop()
            player?.stop()
            
            meterTimer.invalidate()
            
            //recordButton.setTitle("Record", for: .normal)
            //recordButton.setImage(UIImage(named:"record"), for: .normal)
            let session = AVAudioSession.sharedInstance()
            do {
                try session.setActive(false)
                playButton.isEnabled = true
                viewAudioPlay.isHidden = false
                btnRecord.isHidden = false
                btnDone.isHidden = false
                recordButton.isEnabled = true
            } catch {
                print("could not make session inactive")
                print(error.localizedDescription)
            }
            
            var url: URL?
            if self.recorder != nil {
                url = self.recorder.url
            } else {
                url = self.soundFileURL!
            }
            let item = AVPlayerItem(url: url!)
            let duration = Double(item.asset.duration.value) / Double(item.asset.duration.timescale)
            
            //let currentTime1 = Int((player?.currentTime)!)
            let minutes = duration/60
            let seconds = Int(duration) - Int(minutes) * 60
            self.lablTime.text = NSString(format: "%02d:%02d", minutes,seconds) as String
    
        } else {
            
            self.recordButton.isSelected = true
            
            if player != nil && player.isPlaying
            {
                print("stopping")
                player.stop()
            }
            
            if recorder == nil
            {
                print("recording. recorder nil")
               // recordButton.setTitle("Pause", for: .normal)
                //recordButton.setImage(UIImage(named:"pause"), for: .normal)
                playButton.isEnabled = false
                //stopButton.isEnabled = true
                viewAudioPlay.isHidden = true
                
                btnRecord.isHidden = true
                btnDone.isHidden = true
                recordWithPermission(true)
                return
            }
            
            if recorder != nil && recorder.isRecording
            {
                print("pausing")
                recorder.pause()
                //recordButton.setImage(UIImage(named:"record"), for: .normal)
            }
            else
            {
                print("recording")
                //recordButton.setImage(UIImage(named:"pause"), for: .normal)
                playButton.isEnabled = false
                //stopButton.isEnabled = true
                viewAudioPlay.isHidden = false
                btnRecord.isHidden = false
                btnDone.isHidden = false
                //            recorder.record()
                recordWithPermission(false)
            }
        }
    }
    
    @IBAction func playTapped(_ sender: Any) {
        if self.playButton.isSelected {
            self.playButton.isSelected = false
            player.pause()
        } else {
            self.playButton.isSelected = true
            play()
        }
    }
    
    @IBAction func rerecordTapped(_ sender: Any) {
        timerProgress.invalidate()
        appDelegate.delay(0.5, closure: {
            self.recordButton.isSelected = false
            self.recorder = nil
            self.player = nil
            self.statusLabel.text = ""
            self.viewAudioPlay.isHidden = true
        })
    }
    
    @IBAction func doneTapped(_ sender: Any) {
        var url: URL?
        if self.recorder != nil {
            url = self.recorder.url
            do {
                let audioData = try NSData(contentsOf: url!, options: NSData.ReadingOptions())
                self.delegate?.addAudio(audioData: audioData as Data, index: index)
                self.navigationController?.popViewController(animated: false)
            } catch {
                print(error)
            }
        } else {
            AppUtility.showInfoMessage(message: "Please record your voice note first".localized())
        }
    }
}

// MARK: AVAudioRecorderDelegate
extension AddAudioVC: AVAudioRecorderDelegate {
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder,
                                         successfully flag: Bool) {
        
        print("\(#function)")
        
        print("finished recording \(flag)")
        //stopButton.isEnabled = false
        playButton.isEnabled = true
        //recordButton.setTitle("Record", for: UIControlState())
        //recordButton.setImage(UIImage(named:"record"), for: .normal)
        
//        // iOS8 and later
//        let alert = UIAlertController(title: "Recorder",
//                                      message: "Finished Recording",
//                                      preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: "Keep", style: .default) {[unowned self] _ in
//            print("keep was tapped")
//            self.recorder = nil
//        })
//        alert.addAction(UIAlertAction(title: "Delete", style: .default) {[unowned self] _ in
//            print("delete was tapped")
//            self.recorder.deleteRecording()
//        })
//
//        self.present(alert, animated: true, completion: nil)
    }
    
    func audioRecorderEncodeErrorDidOccur(_ recorder: AVAudioRecorder,
                                          error: Error?) {
        print("\(#function)")
        
        if let e = error {
            print("\(e.localizedDescription)")
        }
    }
    
}

// MARK: AVAudioPlayerDelegate
extension AddAudioVC: AVAudioPlayerDelegate {
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        print("\(#function)")
        
        print("finished playing \(flag)")
        recordButton.isEnabled = true
        //stopButton.isEnabled = false
    }
    
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        print("\(#function)")
        
        if let e = error {
            print("\(e.localizedDescription)")
        }
        
    }
}
