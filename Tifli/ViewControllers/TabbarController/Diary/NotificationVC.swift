//
//  NotificationVC.swift
//  Tifli
//
//  Created by zubair on 10/08/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

class NotificationVC: UIViewController {

    @IBOutlet weak var tableNotify: UITableView!
    @IBOutlet weak var btnClearAll: UIButton!
    @IBOutlet weak var lablTitle: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    
    var arrNotify = [Notifications]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lablTitle.text = lablTitle.text?.localized()
        btnClearAll.setTitle(btnClearAll.titleLabel?.text?.localized(), for: .normal)
        
        if appSceneDelegate.isArabic {
            btnBack.setImage(UIImage(named: BACK_AR), for: .normal)
        }
        
        self.getNotifications()
    }
    
    //MARK:- Apis Functions
    func getNotifications() {
        self.view.endEditing(true)
        let paramDict: [String: Any] = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":AppUtility.getBabyId()]
        
        APIRequestUtil.getNotifications(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    let valueArray: [JSON] = dict["data"]!.arrayValue
                    for dict in valueArray {
                        let dictValue: Dictionary<String, JSON> = dict.dictionaryValue
                        let obj = Notifications.init(withDictionary: dictValue)
                        self.arrNotify.append(obj)
                    }
                    self.tableNotify.reloadData()
                    
                    if self.arrNotify.count > 0 {
                        self.btnClearAll.setTitle("Clear all".localized(), for: .normal)
                    } else {
                        self.btnClearAll.setTitle("", for: .normal)
                    }
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    func clearNotifications() {
        self.view.endEditing(true)
        let paramDict: [String: Any] = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":AppUtility.getBabyId()]
        
        APIRequestUtil.clearAllNotifications(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.arrNotify.removeAll()
                    self.tableNotify.reloadData()
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }

    //MARK:- Button Actions
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clearAllTapped(_ sender: Any) {
        let alert = UIAlertController(title: "Hang On!", message: "Are you sure you want to clear all notifications?".localized(), preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "OK".localized(), style: .default, handler: { (action: UIAlertAction!) in
            self.clearNotifications()
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: { (action: UIAlertAction!) in
            print("Handle Cancel Logic here")
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func settingTapped(_ sender: Any) {
        let viewController = NotificationSettingVC(nibName:String(describing: NotificationSettingVC.self), bundle:nil)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

// MARK: - Tableview Delegate-
extension NotificationVC : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrNotify.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        tableView.register(UINib(nibName: "NotificationCell", bundle: nil), forCellReuseIdentifier: "NotificationCell")
        var cell : NotificationCell! = tableView.dequeueReusableCell(withIdentifier: "NotificationCell") as? NotificationCell
        
        if (cell == nil) {
            cell = NotificationCell.init(style: UITableViewCell.CellStyle.default,
                                          reuseIdentifier:"NotificationCell");
        }
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        cell.lablTitle.text = self.arrNotify[indexPath.row].babyTitle
        cell.labkTimme.text = AppUtility.getElapsedInterval(fromDate: self.arrNotify[indexPath.row].createdAt)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        tableView.register(UINib(nibName: "NotificationHeaderCell", bundle: nil), forCellReuseIdentifier: "NotificationHeaderCell")
        let header = tableView.dequeueReusableCell(withIdentifier: "NotificationHeaderCell")! as! NotificationHeaderCell
        
        return header.contentView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        //return 35
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
