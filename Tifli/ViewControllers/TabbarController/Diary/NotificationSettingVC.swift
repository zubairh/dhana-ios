//
//  NotificationSettingVC.swift
//  Tifli
//
//  Created by zubair on 11/08/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit

class NotificationSettingVC: UIViewController {

    @IBOutlet weak var lablTitle: UILabel!
    @IBOutlet weak var lablDiary: UILabel!
    @IBOutlet weak var lablAppUpdate: UILabel!
    @IBOutlet weak var lablMilestone: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    
    var arrTitle = ["9:30 AM Diaper", "12:00 PM Food", "09:30 PM Sleep", "12:00 PM Diaper"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lablTitle.text = lablTitle.text?.localized()
        lablDiary.text = lablDiary.text?.localized()
        lablMilestone.text = lablMilestone.text?.localized()
        lablAppUpdate.text = lablAppUpdate.text?.localized()
        if appSceneDelegate.isArabic {
            btnBack.setImage(UIImage(named: BACK_AR), for: .normal)
        }
    }
    
    //MARK:- Button Actions
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

// MARK: - Tableview Delegate-
extension NotificationSettingVC : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        tableView.register(UINib(nibName: "SettingCell", bundle: nil), forCellReuseIdentifier: "SettingCell")
        var cell : SettingCell! = tableView.dequeueReusableCell(withIdentifier: "SettingCell") as? SettingCell
        
        if (cell == nil) {
            cell = SettingCell.init(style: UITableViewCell.CellStyle.default,
                                          reuseIdentifier:"SettingCell");
        }
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        cell.lablTitle.text = self.arrTitle[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 58
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
