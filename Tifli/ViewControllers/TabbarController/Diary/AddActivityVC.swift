//
//  AddActivityVC.swift
//  Tifli
//
//  Created by zubair on 19/07/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit

protocol AddActivityVCDelegate {
    func addGrowthNew()
}

class AddActivityVC: UIViewController {
    
//    var arrIcons = ["medication", "vaccine", "appointment", "Thermometer", "teeth", "notes", "pumping", "activity", "growth", "sleep", "diaper", "food", "cancel"]
//
//    var arrTitle = ["Medification", "Vaccination", "Appointment", "Temperature", "Teething", "Notes", "Pumping", "Activity", "Growth", "Sleep", "Diaper", "Feeding", "Cancel"]
    
    var arrIcons = ["growth", "vaccine", "appointment", "Thermometer", "medication", "notes", "pumping", "activity", "teeth", "sleep", "diaper", "food", "cancel"]

    var arrTitle = ["Growth".localized(), "Vaccination".localized(), "Appointment".localized(), "Temperature".localized(), "Medification".localized(), "Notes".localized(), "Pumping".localized(), "Activity".localized(), "Teething".localized(), "Sleep".localized(), "Diaper".localized(), "Feeding".localized(), "Cancel".localized()]
    
//    var arrIcons = ["growth", "cancel"]
//    var arrTitle = ["Growth", "Cancel"]
    var delegate:AddActivityVCDelegate? = nil

    @IBOutlet weak var collectionActivity: UICollectionView!
    @IBOutlet weak var immgBg: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let blurEffect = UIBlurEffect(style: .light)
        let blurredEffectView = UIVisualEffectView(effect: blurEffect)
        blurredEffectView.frame = self.view.bounds
        immgBg.addSubview(blurredEffectView)
    }

}

extension AddActivityVC: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrIcons.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return  CGSize(width: collectionView.bounds.width/4, height: 105)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        collectionView.register(UINib(nibName: String(describing: AddAnalyticCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: AddAnalyticCell.self))
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: AddAnalyticCell.self), for: indexPath as IndexPath) as! AddAnalyticCell
        
        cell.imgIcon.image = UIImage(named: self.arrIcons[indexPath.row])
        cell.lablTitle.text = self.arrTitle[indexPath.row]
        
        if indexPath.row == self.arrTitle.count-1 {
            cell.viewBg.backgroundColor = COLORS.ADD_ACTIVITY_COLOR
            cell.lablTitle.textColor = .lightGray
        } else {
            cell.viewBg.backgroundColor = .white
            cell.lablTitle.textColor = .black
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == self.arrTitle.count-1 {
            self.dismiss(animated: true, completion: nil)
        } else if indexPath.row == 0 {
            let viewController = AddGrowthVC(nibName:String(describing: AddGrowthVC.self), bundle:nil)
            viewController.hidesBottomBarWhenPushed = true
            viewController.delegate = self
            self.navigationController?.pushViewController(viewController, animated: false)
        } else if indexPath.row == 6 {
            let viewController = AddPumpingVC(nibName:String(describing: AddPumpingVC.self), bundle:nil)
            viewController.delegate = self
            viewController.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(viewController, animated: false)
        } else if indexPath.row == 5 {
            let viewController = AddNotesVC(nibName:String(describing: AddNotesVC.self), bundle:nil)
            viewController.delegate = self
            viewController.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(viewController, animated: false)
        } else if indexPath.row == 9 {
            let viewController = AddSleepVC(nibName:String(describing: AddSleepVC.self), bundle:nil)
            viewController.delegate = self
            viewController.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(viewController, animated: false)
        } else if indexPath.row == 3 {
            let viewController = AddTempretaureVc(nibName:String(describing: AddTempretaureVc.self), bundle:nil)
            viewController.hidesBottomBarWhenPushed = true
            viewController.delegate = self
            self.navigationController?.pushViewController(viewController, animated: false)
        } else if indexPath.row == 2 {
            let viewController = AddAppointmentVC(nibName:String(describing: AddAppointmentVC.self), bundle:nil)
            viewController.delegate = self
            viewController.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(viewController, animated: false)
        } else if indexPath.row == 7 {
            let viewController = AddActivityNewVC(nibName:String(describing: AddActivityNewVC.self), bundle:nil)
            viewController.hidesBottomBarWhenPushed = true
            viewController.delegate = self
            self.navigationController?.pushViewController(viewController, animated: false)
        } else if indexPath.row == 11 {
            let viewController = AddFoodBottleVC(nibName:String(describing: AddFoodBottleVC.self), bundle:nil)
            viewController.hidesBottomBarWhenPushed = true
            viewController.delegate = self
            self.navigationController?.pushViewController(viewController, animated: false)
        } else if indexPath.row == 4 {
            let viewController = AddMedicationsVC(nibName:String(describing: AddMedicationsVC.self), bundle:nil)
            viewController.hidesBottomBarWhenPushed = true
            viewController.delegate = self
            self.navigationController?.pushViewController(viewController, animated: false)
        } else if indexPath.row == 1 {
            let viewController = AddVacineVC(nibName:String(describing: AddVacineVC.self), bundle:nil)
            viewController.hidesBottomBarWhenPushed = true
            viewController.delegate = self
            self.navigationController?.pushViewController(viewController, animated: false)
        } else if indexPath.row == 10 {
            let viewController = AddDiaperVC(nibName:String(describing: AddDiaperVC.self), bundle:nil)
            viewController.delegate = self
            viewController.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(viewController, animated: false)
        } else if indexPath.row == 8 {
            let viewController = AddTeetchVC(nibName:String(describing: AddTeetchVC.self), bundle:nil)
            viewController.hidesBottomBarWhenPushed = true
            viewController.delegate = self
            self.navigationController?.pushViewController(viewController, animated: false)
        }
    }
    
}

extension AddActivityVC: AddGrowthVCDelegate {
    func addGrowth() {
        self.dismiss(animated: true, completion: nil)
        self.delegate?.addGrowthNew()
    }
}

extension AddActivityVC: AddNotesVCDelegate {
    func addNotes() {
        self.dismiss(animated: true, completion: nil)
        self.delegate?.addGrowthNew()
    }
}

extension AddActivityVC: AddSleepVCDelegate {
    func addSleep() {
        self.dismiss(animated: true, completion: nil)
        self.delegate?.addGrowthNew()
    }
}

extension AddActivityVC: AddAppointmentVCDelegate {
    func addAppointment() {
        self.dismiss(animated: true, completion: nil)
        self.delegate?.addGrowthNew()
    }
}

extension AddActivityVC: AddDiaperVCDelegate {
    func addDiaper() {
        self.dismiss(animated: true, completion: nil)
        self.delegate?.addGrowthNew()
    }
}

extension AddActivityVC: AddPumpingVCDelegate {
    func addPumping() {
        self.dismiss(animated: true, completion: nil)
        self.delegate?.addGrowthNew()
    }
}

extension AddActivityVC: AddActivityNewVCDelegate {
    func addActivity() {
        self.dismiss(animated: true, completion: nil)
        self.delegate?.addGrowthNew()
    }
}

extension AddActivityVC: AddFoodBottleVCDelegate {
    func addFood() {
        self.dismiss(animated: true, completion: nil)
        self.delegate?.addGrowthNew()
    }
}

extension AddActivityVC: AddMedicationsVCDelegate {
    func addMedication() {
        self.dismiss(animated: true, completion: nil)
        self.delegate?.addGrowthNew()
    }
}

extension AddActivityVC: AddTeetchVCDelegate {
    func addTeeth() {
        self.dismiss(animated: true, completion: nil)
        self.delegate?.addGrowthNew()
    }
}

extension AddActivityVC: AddTempretaureVcDelegate {
    func addTemprature() {
        self.dismiss(animated: true, completion: nil)
        self.delegate?.addGrowthNew()
    }
}

extension AddActivityVC: AddVacineVCDelegate {
    func addVaccine() {
        self.dismiss(animated: true, completion: nil)
        self.delegate?.addGrowthNew()
    }
}
