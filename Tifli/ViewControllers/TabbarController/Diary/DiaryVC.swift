//
//  DiaryVC.swift
//  Dhana
//
//  Created by zubair on 22/06/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON
import AVFoundation
import AVKit
import ActionSheetPicker_3_0

class DiaryVC: UIViewController {

    @IBOutlet weak var collectionCalender: UICollectionView!
    @IBOutlet weak var tableListing: UITableView!
    @IBOutlet weak var imgBaby: UIImageView!
    @IBOutlet weak var lablBabyName: UILabel!
    @IBOutlet weak var lablYear: UILabel!
    @IBOutlet weak var tableBaby: UITableView!
    @IBOutlet weak var lablAddNew: UILabel!
    
    var arrGrowth = [Growth]()
    var audioPlayer = AVAudioPlayer()
    var player: AVPlayer!
    var arrDiary = [Diary]()
    var diaryDate = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lablAddNew.text = lablAddNew.text?.localized()
        AppUtility.showProgress()
        diaryDate = AppUtility.convertDateToString(date: NSDate())
        
        if AppUtility.isBabySelected() {
            for obj in AppManager.shared.arrBabies {
                if obj.babyID == AppUtility.getBabyId() {
                    AppUtility.loadImage(imageView: imgBaby, urlString: obj.babyImage, placeHolderImageString: "bb")
                    lablBabyName.text = obj.babyName
                    lablYear.text = obj.age
                }
            }
            if AppManager.shared.arrBabies.count == 0 {
                self.getBabiesList()
                return
            }
            //self.getGrowth(babyId: AppUtility.getBabyId())
            self.getDiary(date: diaryDate)
        } else {
            self.getBabiesList()
        }
        
        self.tableBaby.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        appSceneDelegate.navController = self.navigationController!
    }
    
    //MARK:- Apis Functions
    func getGrowth(babyId:String) {
        self.view.endEditing(true)
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":babyId]
        APIRequestUtil.getHomeListing(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.arrGrowth.removeAll()
                    
                    let valueArray: [JSON] = dict["data"]!.arrayValue
                    for dict in valueArray {
                        let dictValue: Dictionary<String, JSON> = dict.dictionaryValue
                        let obj = Growth.init(withDictionary: dictValue)
                        self.arrGrowth.append(obj)
                    }
                    self.tableListing.reloadData()
                    
                    if self.arrGrowth.count == 0 {
                        self.tableListing.isHidden = true
                    } else {
                        self.tableListing.isHidden = false
                    }
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    func getBabiesList() {
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken()]
        APIRequestUtil.getBabyList(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    var arrBaby = [Baby]()
                    let valueArray: [JSON] = dict["babies"]!.arrayValue
                    for dict in valueArray {
                        let dictValue: Dictionary<String, JSON> = dict.dictionaryValue
                        let obj = Baby.init(withDictionary: dictValue)
                        arrBaby.append(obj)
                    }
                    
                    AppManager.shared.arrBabies = arrBaby
                    if arrBaby.count > 0 {
                        
                        if AppUtility.isBabySelected() {
                            for obj in AppManager.shared.arrBabies {
                                if obj.babyID == AppUtility.getBabyId() {
                                    AppUtility.loadImage(imageView: self.imgBaby, urlString: obj.babyImage, placeHolderImageString: "bb")
                                    self.lablBabyName.text = obj.babyName
                                    self.lablYear.text = obj.age
                                }
                            }
                            //self.getGrowth(babyId: AppUtility.getBabyId())
                            self.getDiary(date: self.diaryDate)
                        } else {
                            AppUtility.saveBabyId(babyID: arrBaby[0].babyID)
                            //self.getGrowth(babyId: arrBaby[0].babyID)
                            
                            self.getDiary(date: self.diaryDate)
                            
                            AppUtility.loadImage(imageView: self.imgBaby, urlString: arrBaby[0].babyImage, placeHolderImageString: "bb")
                            self.lablBabyName.text = arrBaby[0].babyName
                            self.lablYear.text = arrBaby[0].age
                        }
                        
                    } else {
                        //self.getGrowth(babyId: AppUtility.getBabyId())
                        self.getDiary(date: self.diaryDate)
                    }
                    
                    self.tableBaby.reloadData()
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    func getDiary(date:String) {
        self.view.endEditing(true)
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":AppUtility.getBabyId(), "date":date]
        APIRequestUtil.getDiary(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.arrDiary.removeAll()
                    
                    let valueArray: [JSON] = dict["data"]!.arrayValue
                    for dict in valueArray {
                        let dictValue: Dictionary<String, JSON> = dict.dictionaryValue
                        let obj = Diary.init(withDictionary: dictValue)
                        self.arrDiary.append(obj)
                    }
                    appDelegate.delay(0.5, closure: {
                        self.tableListing.beginUpdates()
                        self.tableListing.endUpdates()
                    })
                    self.tableListing.reloadData()
                    
                    if self.arrDiary.count == 0 {
                        self.tableListing.isHidden = true
                    } else {
                        self.tableListing.isHidden = false
                    }
                    self.tableListing.setContentOffset(.zero, animated: true)
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    func getTeeth() {
        self.view.endEditing(true)
        let paramDict: [String: Any] = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":AppUtility.getBabyId()]
        
        APIRequestUtil.getTeeth(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    var arrTeeth = [Teeth]()
                    let valueArray: [JSON] = dict["data"]!.arrayValue
                    for dict in valueArray {
                        let dictValue: Dictionary<String, JSON> = dict.dictionaryValue
                        let obj = Teeth.init(withDictionary: dictValue)
                        arrTeeth.append(obj)
                    }
                    AppManager.shared.arrTeeth = arrTeeth
                    self.tableListing.reloadData()
                } else {
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    func deleteCategories(index:Int, obj:Diary, type:String, url:String, paramDict:[String: Any]) {
        self.view.endEditing(true)
        APIRequestUtil.deleteCategoryOnDiary(url: url, parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    AppUtility.showSuccessMessage(message: message ?? type+" "+"deleted successfully".localized())
                    self.arrDiary.remove(at: index)
                    self.tableListing.reloadData()
                    
                    if self.arrDiary.count == 0 {
                        self.tableListing.isHidden = true
                    } else {
                        self.tableListing.isHidden = false
                    }
                    
                } else {
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    func updateDiary(paramDict:[String: Any], url:String) {
        self.view.endEditing(true)
        APIRequestUtil.addSleep(url:url, parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    AppUtility.showSuccessMessage(message: message ?? "")
                    self.getDiary(date: self.diaryDate)
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    func audioPlayFromGrowthCell(cell:GrowthAudioCell) {
        player!.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(1, preferredTimescale: 1), queue: DispatchQueue.main) { (CMTime) -> Void in
            if self.player!.currentItem?.status == .readyToPlay {
                let time : Float64 = CMTimeGetSeconds(self.player!.currentTime());
                let duration = Int(time)
                let minutes2 = duration/60
                let seconds2 = duration - minutes2 * 60
                cell.lablAudioTime.text = NSString(format: "%02d:%02d", minutes2,seconds2) as String

                guard let currentTimes = self.player?.currentTime, let durations = self.player.currentItem?.asset.duration else {
                      return
                    }
                let seconds = CMTimeGetSeconds(durations)
                let secondss = CMTimeGetSeconds(currentTimes())
                let ff:Float = Float(secondss)
                let pf:Float = Float(seconds)
                let percentCompleted = ff/pf
                cell.progressAudio.setProgress(Float(percentCompleted), animated: true)
            }

            let playbackLikelyToKeepUp = self.player?.currentItem?.isPlaybackLikelyToKeepUp
            if playbackLikelyToKeepUp == false {
                print("IsBuffering")
//                            self.ButtonPlay.isHidden = true
//                            self.loadingView.isHidden = false
            } else {
                //stop the activity indicator
//                            print("Buffering completed")
//                            self.ButtonPlay.isHidden = false
//                            self.loadingView.isHidden = true
            }

        }
    }
    
    func audioPlayFromTeethCell(cell:TeethGrowthAudioCell) {
        player!.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(1, preferredTimescale: 1), queue: DispatchQueue.main) { (CMTime) -> Void in
            if self.player!.currentItem?.status == .readyToPlay {
                let time : Float64 = CMTimeGetSeconds(self.player!.currentTime());
                let duration = Int(time)
                let minutes2 = duration/60
                let seconds2 = duration - minutes2 * 60
                cell.lablAudioTime.text = NSString(format: "%02d:%02d", minutes2,seconds2) as String

                guard let currentTimes = self.player?.currentTime, let durations = self.player.currentItem?.asset.duration else {
                      return
                    }
                let seconds = CMTimeGetSeconds(durations)
                let secondss = CMTimeGetSeconds(currentTimes())
                let ff:Float = Float(secondss)
                let pf:Float = Float(seconds)
                let percentCompleted = ff/pf
                cell.progressAudio.setProgress(Float(percentCompleted), animated: true)
            }

            let playbackLikelyToKeepUp = self.player?.currentItem?.isPlaybackLikelyToKeepUp
            if playbackLikelyToKeepUp == false {
                print("IsBuffering")
//                            self.ButtonPlay.isHidden = true
//                            self.loadingView.isHidden = false
            } else {
                //stop the activity indicator
//                            print("Buffering completed")
//                            self.ButtonPlay.isHidden = false
//                            self.loadingView.isHidden = true
            }
        }
    }
    
    func audioPlayFromMedicationCell(cell:MedicationGrowthAudioCell) {
        player!.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(1, preferredTimescale: 1), queue: DispatchQueue.main) { (CMTime) -> Void in
            if self.player!.currentItem?.status == .readyToPlay {
                let time : Float64 = CMTimeGetSeconds(self.player!.currentTime());
                let duration = Int(time)
                let minutes2 = duration/60
                let seconds2 = duration - minutes2 * 60
                cell.lablAudioTime.text = NSString(format: "%02d:%02d", minutes2,seconds2) as String

                guard let currentTimes = self.player?.currentTime, let durations = self.player.currentItem?.asset.duration else {
                      return
                    }
                let seconds = CMTimeGetSeconds(durations)
                let secondss = CMTimeGetSeconds(currentTimes())
                let ff:Float = Float(secondss)
                let pf:Float = Float(seconds)
                let percentCompleted = ff/pf
                cell.progressAudio.setProgress(Float(percentCompleted), animated: true)
            }

            let playbackLikelyToKeepUp = self.player?.currentItem?.isPlaybackLikelyToKeepUp
            if playbackLikelyToKeepUp == false {
                print("IsBuffering")
//                            self.ButtonPlay.isHidden = true
//                            self.loadingView.isHidden = false
            } else {
                //stop the activity indicator
//                            print("Buffering completed")
//                            self.ButtonPlay.isHidden = false
//                            self.loadingView.isHidden = true
            }

        }
    }
    

    //MARK:-Button Actions
    @IBAction func addActivityTapped(_ sender: Any) {
        
        let viewController = AddActivityVC(nibName:String(describing: AddActivityVC.self), bundle:nil)
        viewController.delegate = self
        let navigationController = UINavigationController.init(rootViewController: viewController)
        navigationController.navigationBar.isHidden = true
        navigationController.modalPresentationStyle = .custom
        navigationController.modalTransitionStyle = .crossDissolve
        self.present(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func sideMenuTapped(_ sender: Any) {
        self.sideMenuViewController.presentRightMenuViewController()
    }
    
    @IBAction func notifyTapped(_ sender: Any) {
        let viewController = NotificationVC(nibName:String(describing: NotificationVC.self), bundle:nil)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func dropDownBabyTapped(_ sender: UIButton) {
        if sender.tag == 0 {
            sender.tag = 1
            tableBaby.isHidden = false
        } else {
            sender.tag = 0
            tableBaby.isHidden = true
        }
    }
    
    @IBAction func playAudioTapped(_ sender: UIButton) {
        let obj = self.arrDiary[sender.tag]
        if obj.type == DIARYTYPE.medication.rawValue {
            if obj.objMedication.isPlay == true {
                obj.objMedication.isPlay = false
            } else {
                obj.objMedication.isPlay = true
            }
            self.tableListing.tag = sender.tag
            self.tableListing.reloadData()
            appDelegate.delay(0.1, closure: {
                let indexPath = IndexPath(row: sender.tag, section: 0)
                let cell = self.tableListing.cellForRow(at: indexPath) as! MedicationGrowthAudioCell
                self.audioPlayFromMedicationCell(cell: cell)
            })
        } else if obj.type == DIARYTYPE.teething.rawValue {
            if obj.objTeething.isPlay == true {
                obj.objTeething.isPlay = false
            } else {
                obj.objTeething.isPlay = true
            }
            self.tableListing.tag = sender.tag
            self.tableListing.reloadData()
            appDelegate.delay(0.1, closure: {
                let indexPath = IndexPath(row: sender.tag, section: 0)
                let cell = self.tableListing.cellForRow(at: indexPath) as! TeethGrowthAudioCell
                self.audioPlayFromTeethCell(cell: cell)
            })
        } else if obj.type == DIARYTYPE.note.rawValue {
            if obj.objNote.isPlay == true {
                obj.objNote.isPlay = false
            } else {
                obj.objNote.isPlay = true
            }
            self.tableListing.tag = sender.tag
            self.tableListing.reloadData()
            appDelegate.delay(0.1, closure: {
                let indexPath = IndexPath(row: sender.tag, section: 0)
                let cell = self.tableListing.cellForRow(at: indexPath) as! GrowthAudioCell
                self.audioPlayFromGrowthCell(cell: cell)
            })
        } else if obj.type == DIARYTYPE.growth.rawValue {
            if obj.objGrowth.isPlay == true {
                obj.objGrowth.isPlay = false
            } else {
                obj.objGrowth.isPlay = true
            }
            self.tableListing.tag = sender.tag
            self.tableListing.reloadData()
            appDelegate.delay(0.1, closure: {
                let indexPath = IndexPath(row: sender.tag, section: 0)
                let cell = self.tableListing.cellForRow(at: indexPath) as! GrowthAudioCell
                self.audioPlayFromGrowthCell(cell: cell)
            })
        } else if obj.type == DIARYTYPE.diaper.rawValue {
            if obj.objDiaper.isPlay == true {
                obj.objDiaper.isPlay = false
            } else {
                obj.objDiaper.isPlay = true
            }
            self.tableListing.tag = sender.tag
            self.tableListing.reloadData()
            appDelegate.delay(0.1, closure: {
                let indexPath = IndexPath(row: sender.tag, section: 0)
                let cell = self.tableListing.cellForRow(at: indexPath) as! GrowthAudioCell
                self.audioPlayFromGrowthCell(cell: cell)
            })
        } else if obj.type == DIARYTYPE.activity.rawValue {
            if obj.objActivity.isPlay == true {
                obj.objActivity.isPlay = false
            } else {
                obj.objActivity.isPlay = true
            }
            self.tableListing.tag = sender.tag
            self.tableListing.reloadData()
            appDelegate.delay(0.1, closure: {
                let indexPath = IndexPath(row: sender.tag, section: 0)
                let cell = self.tableListing.cellForRow(at: indexPath) as! GrowthAudioCell
                self.audioPlayFromGrowthCell(cell: cell)
            })
        }
    }
    
    @objc func playerDidFinishPlaying(sender: Notification) {
        let indexPath = IndexPath(row: tableListing.tag, section: 0)
        let obj = self.arrDiary[tableListing.tag]
        if obj.type == DIARYTYPE.medication.rawValue {
            if obj.objMedication.isPlay == true {
                obj.objMedication.isPlay = false
            } else {
                obj.objMedication.isPlay = true
            }
        } else if obj.type == DIARYTYPE.teething.rawValue {
            if obj.objTeething.isPlay == true {
                obj.objTeething.isPlay = false
            } else {
                obj.objTeething.isPlay = true
            }
        } else if obj.type == DIARYTYPE.note.rawValue {
            if obj.objNote.isPlay == true {
                obj.objNote.isPlay = false
            } else {
                obj.objNote.isPlay = true
            }
        } else if obj.type == DIARYTYPE.growth.rawValue {
            if obj.objGrowth.isPlay == true {
                obj.objGrowth.isPlay = false
            } else {
                obj.objGrowth.isPlay = true
            }
        } else if obj.type == DIARYTYPE.diaper.rawValue {
            if obj.objDiaper.isPlay == true {
                obj.objDiaper.isPlay = false
            } else {
                obj.objDiaper.isPlay = true
            }
        } else if obj.type == DIARYTYPE.activity.rawValue {
            if obj.objActivity.isPlay == true {
                obj.objActivity.isPlay = false
            } else {
                obj.objActivity.isPlay = true
            }
        }
        UIView.performWithoutAnimation {
           self.tableListing.reloadRows(at: [indexPath], with: .none)
        }
    }
    
    @IBAction func deleteCategoryTapped(_ sender: UIButton) {
        let obj = self.arrDiary[sender.tag]
        let alert = UIAlertController(title: "Information!", message: "Are you sure you want to delete?", preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
            let paramDict: [String: Any] = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "diaryId":obj.diaryId, "babyId":AppUtility.getBabyId()]
            self.deleteCategories(index: sender.tag, obj: obj, type: obj.type, url: API.DELETE_DIARY, paramDict: paramDict)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func editCategoryTapped(_ sender: UIButton) {
        let obj = self.arrDiary[sender.tag]
       
        if obj.type == DIARYTYPE.growth.rawValue {
            let viewController = AddGrowthVC(nibName:String(describing: AddGrowthVC.self), bundle:nil)
            viewController.hidesBottomBarWhenPushed = true
            viewController.objDiary = obj
            viewController.isFromEdit = true
            viewController.delegate = self
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else if obj.type == DIARYTYPE.medication.rawValue {
            
            let viewController = AddMedicationsVC(nibName:String(describing: AddMedicationsVC.self), bundle:nil)
            viewController.hidesBottomBarWhenPushed = true
            viewController.delegate = self
            viewController.objDiary = obj
            viewController.isFromEdit = true
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else if obj.type == DIARYTYPE.vaccination.rawValue {
            let viewController = AddVacineVC(nibName:String(describing: AddVacineVC.self), bundle:nil)
            viewController.hidesBottomBarWhenPushed = true
            viewController.delegate = self
            viewController.objDiary = obj
            viewController.isFromEdit = true
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else if obj.type == DIARYTYPE.appointment.rawValue {
            
            let viewController = AddAppointmentVC(nibName:String(describing: AddAppointmentVC.self), bundle:nil)
            viewController.objDiary = obj
            viewController.isFromEdit = true
            viewController.delegate = self
            viewController.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else if obj.type == DIARYTYPE.temperature.rawValue {
            
            let viewController = AddTempretaureVc(nibName:String(describing: AddTempretaureVc.self), bundle:nil)
            viewController.hidesBottomBarWhenPushed = true
            viewController.objDiary = obj
            viewController.isFromEdit = true
            viewController.delegate = self
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else if obj.type == DIARYTYPE.teething.rawValue {
            
            let viewController = AddTeetchVC(nibName:String(describing: AddTeetchVC.self), bundle:nil)
            viewController.hidesBottomBarWhenPushed = true
            viewController.delegate = self
            viewController.objDiary = obj
            viewController.isFromEdit = true
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else if obj.type == DIARYTYPE.note.rawValue {
            
            let viewController = AddNotesVC(nibName:String(describing: AddNotesVC.self), bundle:nil)
            viewController.delegate = self
            viewController.hidesBottomBarWhenPushed = true
            viewController.objDiary = obj
            viewController.isFromEdit = true
            self.navigationController?.pushViewController(viewController, animated: true)
            
        } else if obj.type == DIARYTYPE.pumping.rawValue {
            
            let viewController = AddPumpingVC(nibName:String(describing: AddPumpingVC.self), bundle:nil)
            viewController.delegate = self
            viewController.objDiary = obj
            viewController.isFromEdit = true
            viewController.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else if obj.type == DIARYTYPE.sleep.rawValue {
            
            let viewController = AddSleepVC(nibName:String(describing: AddSleepVC.self), bundle:nil)
            viewController.delegate = self
            viewController.objDiary = obj
            viewController.isFromEdit = true
            viewController.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else if obj.type == DIARYTYPE.diaper.rawValue {
            
            let viewController = AddDiaperVC(nibName:String(describing: AddDiaperVC.self), bundle:nil)
            viewController.delegate = self
            viewController.objDiary = obj
            viewController.isFromEdit = true
            viewController.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else if obj.type == DIARYTYPE.feeding.rawValue {
            
            let viewController = AddFoodBottleVC(nibName:String(describing: AddFoodBottleVC.self), bundle:nil)
            viewController.hidesBottomBarWhenPushed = true
            viewController.delegate = self
            viewController.objDiary = obj
            viewController.isFromEdit = true
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else if obj.type == DIARYTYPE.activity.rawValue {
            
            let viewController = AddActivityNewVC(nibName:String(describing: AddActivityNewVC.self), bundle:nil)
            viewController.hidesBottomBarWhenPushed = true
            viewController.objDiary = obj
            viewController.isFromEdit = true
            viewController.delegate = self
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    @IBAction func doneDiaryTapped(_ sender:UIButton) {
        let obj = self.arrDiary[sender.tag]
        let dd = diaryDate+" "+AppUtility.convertDateToStringTime(date: NSDate())+":10"
        if obj.type == DIARYTYPE.medication.rawValue {
            
            let indexPath = IndexPath(row: sender.tag, section: 0)
            if obj.objMedication.mediaType == "audio" {
                let cell = self.tableListing.cellForRow(at: indexPath) as! MedicationGrowthActiveAudioCell
                let paramDict: [String: Any] = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":AppUtility.getBabyId(), "date":dd, "notes":obj.notes, "amount":cell.tfAmount.text!, "amountType":cell.medicationAmountType, "medicationId":cell.tfMedical.tag, "diaryId":obj.diaryId, "diary_type":obj.type]
                self.updateDiary(paramDict: paramDict, url: API.UPDATE_DIARY)
            } else {
                let cell = self.tableListing.cellForRow(at: indexPath) as! MedicationGrowthActiveCell
                let paramDict: [String: Any] = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":AppUtility.getBabyId(), "date":dd, "notes":obj.notes, "amount":cell.tfAmount.text!, "amountType":cell.medicationAmountType, "medicationId":cell.tfMedical.tag, "diaryId":obj.diaryId, "diary_type":obj.type]
                self.updateDiary(paramDict: paramDict, url: API.UPDATE_DIARY)
            }

        } else if obj.type == DIARYTYPE.sleep.rawValue {
            
            let indexPath = IndexPath(row: sender.tag, section: 0)
            let cell = self.tableListing.cellForRow(at: indexPath) as! SleepGrowthActiveCell
            let paramDict: [String: Any]?
            if cell.viewTimer.isHidden == true {
                paramDict = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":AppUtility.getBabyId(), "date":dd, "notes":obj.notes, "startTime":cell.tfStartTime.text!, "endTime":cell.tfEndTime .text!, "diaryId":obj.diaryId, "diary_type":obj.type]
            } else {
                paramDict = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":AppUtility.getBabyId(), "date":dd, "notes":obj.notes, "sleepDuration":cell.lablTimer.text!, "diaryId":obj.diaryId, "diary_type":obj.type]
            }
            self.updateDiary(paramDict: paramDict!, url: API.UPDATE_DIARY)
    
        } else if obj.type == DIARYTYPE.note.rawValue {
           
            let indexPath = IndexPath(row: sender.tag, section: 0)
            if obj.objNote.mediaType == "audio" {
                let cell = self.tableListing.cellForRow(at: indexPath) as! NotesGrowthAudioActiveCell
                let paramDict: [String: Any]?
                paramDict = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":AppUtility.getBabyId(), "date":dd, "notes":cell.tfNotes.text!, "diaryId":obj.diaryId, "diary_type":obj.type]
                self.updateDiary(paramDict: paramDict!, url: API.UPDATE_DIARY)
            } else {
                let cell = self.tableListing.cellForRow(at: indexPath) as! NotesGrowthCell
                let paramDict: [String: Any]?
                paramDict = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":AppUtility.getBabyId(), "date":dd, "notes":cell.tfNotes.text!, "diaryId":obj.diaryId, "diary_type":obj.type]
                self.updateDiary(paramDict: paramDict!, url: API.UPDATE_DIARY)
            }
        
        } else if obj.type == DIARYTYPE.diaper.rawValue {
            
            let indexPath = IndexPath(row: sender.tag, section: 0)
            if obj.objDiaper.mediaType == "audio" {
                let cell = self.tableListing.cellForRow(at: indexPath) as! DiaperGrowthActiveAudioCell
                let paramDict: [String: Any] = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":AppUtility.getBabyId(), "date":dd, "type":cell.arrTitle[cell.selectedDiaperLog], "form":cell.arrTitle[cell.selectedIndexDaiperForm], "color":cell.arrColorTitle[cell.selectedIndexDaiperColor], "diaryId":obj.diaryId, "diary_type":obj.type]
                self.updateDiary(paramDict: paramDict, url: API.UPDATE_DIARY)
            } else {
                let cell = self.tableListing.cellForRow(at: indexPath) as! DiaperGrowthActiveCell
                let paramDict: [String: Any] = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":AppUtility.getBabyId(), "date":dd, "type":cell.arrTitle[cell.selectedDiaperLog], "form":cell.arrTitle[cell.selectedIndexDaiperForm], "color":cell.arrColorTitle[cell.selectedIndexDaiperColor], "diaryId":obj.diaryId, "diary_type":obj.type]
                self.updateDiary(paramDict: paramDict, url: API.UPDATE_DIARY)
            }
        } else if obj.type == DIARYTYPE.activity.rawValue {
            
            let indexPath = IndexPath(row: sender.tag, section: 0)
            if obj.objActivity.mediaType == "audio" {
                let cell = self.tableListing.cellForRow(at: indexPath) as! ActivityGrowthActiveAudioCell
                let paramDict: [String: Any] = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":AppUtility.getBabyId(), "date":dd, "activityId":cell.tfActivity.tag, "notes":obj.notes, "diaryId":obj.diaryId, "diary_type":obj.type]
                self.updateDiary(paramDict: paramDict, url: API.UPDATE_DIARY)
            } else {
                let cell = self.tableListing.cellForRow(at: indexPath) as! ActivityGrowthActiveCell
                let paramDict: [String: Any] = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":AppUtility.getBabyId(), "date":dd, "activityId":cell.tfActivity.tag, "notes":obj.notes, "diaryId":obj.diaryId, "diary_type":obj.type]
                self.updateDiary(paramDict: paramDict, url: API.UPDATE_DIARY)
            }
            
        } else if obj.type == DIARYTYPE.feeding.rawValue {
           
            let indexPath = IndexPath(row: sender.tag, section: 0)
            if obj.objFeeding.type == "breast" {
                let cell = self.tableListing.cellForRow(at: indexPath) as! FoodGrowthCell
                //if cell.viewRightBreast.isHidden == true {
                let paramDict = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":AppUtility.getBabyId(), "date":dd, "type":"breast", "leftBreastDuration":cell.lablLeftBreast.text!, "rightBreastDuration":cell.lablRightBreast.text!, "notes":cell.tfNotes.text!, "diaryId":obj.diaryId, "diary_type":obj.type]
                self.updateDiary(paramDict: paramDict, url: API.UPDATE_DIARY)
//                } else {
//                    paramDict = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":self.babyID, "date":tfDate.text!+" "+tfTime.text!, "type":"breast", "leftBreastDuration":lablTimerLeftBreast.text!, "rightBreastDuration":lablTimerRightBreast.text!, "notes":tfNotes.text!, "diaryId":objDiary.diaryId, "diary_type":objDiary.type, "addToDailyRoutine":addToDailyRoutine, "setReminder":setReminder]
//                }
            } else if obj.objFeeding.type == "solids" {
                let cell = self.tableListing.cellForRow(at: indexPath) as! FoodGrowthIngrediantsActiveCell
                let paramDict: [String: Any] = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":AppUtility.getBabyId(), "date":dd, "type":"solids", "ingredients":cell.arrIngrediant.componentsJoined(by: ","), "notes":obj.notes, "diaryId":obj.diaryId, "diary_type":obj.type]
                self.updateDiary(paramDict: paramDict, url: API.UPDATE_DIARY)
            } else {
                let cell = self.tableListing.cellForRow(at: indexPath) as! FoodGrowthBotelActiveCell
                var type = ""
                if cell.selectedBotelIndex == 0 {
                    type = "breast_milk"
                } else if cell.selectedBotelIndex == 1 {
                    type = "formula"
                }
                let paramDict: [String: Any] = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":AppUtility.getBabyId(), "date":dd, "type":"bottle", "bottleType":type, "amount":cell.tfAmount.text!, "notes":obj.notes, "diaryId":obj.diaryId, "diary_type":obj.type]
                self.updateDiary(paramDict: paramDict, url: API.UPDATE_DIARY)
            }
        } else if obj.type == DIARYTYPE.appointment.rawValue {
            let paramDict: [String: Any] = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":AppUtility.getBabyId(), "date":dd, "notes":obj.notes, "diaryId":obj.diaryId, "diary_type":obj.type]
            self.updateDiary(paramDict: paramDict, url: API.UPDATE_DIARY)
        } else if obj.type == DIARYTYPE.vaccination.rawValue {
            let paramDict: [String: Any] = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":AppUtility.getBabyId(), "date":dd, "notes":obj.notes, "diaryId":obj.diaryId, "diary_type":obj.type]
            self.updateDiary(paramDict: paramDict, url: API.UPDATE_DIARY)
        }
    }
    
    @IBAction func addIngrediantsTapped(_ sender:UIButton) {
        let viewController = AddFoodVC(nibName:String(describing: AddFoodVC.self), bundle:nil)
        viewController.hidesBottomBarWhenPushed = true
        viewController.isFromFoodIngrediats = true
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

extension DiaryVC: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenRect = UIScreen.main.bounds
        return CGSize(width: screenRect.size.width, height: 39)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        collectionView.register(UINib(nibName: String(describing: CalenderCells.self), bundle: nil), forCellWithReuseIdentifier: String(describing: CalenderCells.self))
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: CalenderCells.self), for: indexPath as IndexPath) as! CalenderCells
        
        cell.lablDate.text = "("+AppUtility.get_MMM(dateString: diaryDate)+")"
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let datePicker = ActionSheetDatePicker(title: title, datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let date:NSDate = value as! NSDate
            
            self.diaryDate = AppUtility.convertDateToString(date: date)
            self.collectionCalender.reloadData()
            self.getDiary(date: self.diaryDate)
            
            return
            
        }, cancel: { ActionStringCancelBlock in return }, origin: collectionView.superview!.superview)
        datePicker?.show()
    }
    
}

// MARK: - Tableview Delegate-
extension DiaryVC : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableBaby {
            return AppManager.shared.arrBabies.count
        }
        return self.arrDiary.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        if tableView == tableListing {
            
            let obj = self.arrDiary[indexPath.row]

            if obj.type == DIARYTYPE.growth.rawValue {
                if obj.objGrowth.mediaType == "audio" {
                    tableView.register(UINib(nibName: "GrowthAudioCell", bundle: nil), forCellReuseIdentifier: "GrowthAudioCell")
                    var cell : GrowthAudioCell! = tableView.dequeueReusableCell(withIdentifier: "GrowthAudioCell") as? GrowthAudioCell

                    if (cell == nil) {
                        cell = GrowthAudioCell.init(style: UITableViewCell.CellStyle.default,
                                                      reuseIdentifier:"GrowthAudioCell");
                    }

                    cell.selectionStyle = UITableViewCell.SelectionStyle.none
                    
                    cell.lablCatName.text = "Growth".localized()
                    cell.imgCat.image = UIImage(named: "growth")

                    cell.layoutHeightCollection.constant = 24
                    cell.isDiaper = false
                    cell.isNotes = false
                    cell.isActivity = false
                    if obj.isActive == 0 {
                        cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.date)
                        cell.lablTimeAgo.text = ""
                    } else {
                        cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                        cell.lablTimeAgo.text = obj.newDifferDate
                    }
                    cell.lablUser.text = obj.userName
                    cell.objGrowth = obj.objGrowth
                    cell.lablDetail.text = obj.notes
                    AppUtility.loadImage(imageView: cell.imgUSer, urlString: obj.userImage, placeHolderImageString: "bb")
                    cell.collectionView.reloadData()

//                    let item = AVPlayerItem(url: URL(string: obj.objGrowth.media)!)
//                    let duration = Double(item.asset.duration.value) / Double(item.asset.duration.timescale)
//
//                    let minutes = duration/60
//                    let seconds = Int(duration) - Int(minutes) * 60
//                    cell.lablAudioTime.text = NSString(format: "%02d:%02d", minutes,seconds) as String
                    cell.progressAudio.setProgress(Float(0), animated: false)
                    cell.lablAudioTime.text = obj.objGrowth.mediaDuration

                    if !obj.objGrowth.isPlay {

                        obj.objGrowth.isPlay = false
                        if player != nil
                        {
                            player.pause()
                        }
                        cell.btnPlayAudio.isSelected = false

                    } else {
                        cell.btnPlayAudio.isSelected = true
                        obj.objGrowth.isPlay = true
                        player = AVPlayer()
                        if player != nil
                        {
                            player.pause()
                        }

                        let url = URL(string: obj.objGrowth.media)
                        let playerItem = CachingPlayerItem(url: url!)
                        NotificationCenter.default.addObserver(self, selector: #selector(self.playerDidFinishPlaying(sender:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
                        playerItem.delegate = self
                        player = AVPlayer(playerItem: playerItem)
                        player.automaticallyWaitsToMinimizeStalling = true
                        player.play()

//                        player!.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(1, preferredTimescale: 1), queue: DispatchQueue.main) { (CMTime) -> Void in
//                            if self.player!.currentItem?.status == .readyToPlay {
//                                let time : Float64 = CMTimeGetSeconds(self.player!.currentTime());
//                                let duration = Int(time)
//                                let minutes2 = duration/60
//                                let seconds2 = duration - minutes2 * 60
//                                cell.lablAudioTime.text = NSString(format: "%02d:%02d", minutes2,seconds2) as String
//
//                                guard let currentTimes = self.player?.currentTime, let durations = self.player.currentItem?.asset.duration else {
//                                      return
//                                    }
//                                let seconds = CMTimeGetSeconds(durations)
//                                let secondss = CMTimeGetSeconds(currentTimes())
//                                let ff:Float = Float(secondss)
//                                let pf:Float = Float(seconds)
//                                let percentCompleted = ff/pf
//                                cell.progressAudio.setProgress(Float(percentCompleted), animated: true)
//                            }
//
//                            let playbackLikelyToKeepUp = self.player?.currentItem?.isPlaybackLikelyToKeepUp
//                            if playbackLikelyToKeepUp == false {
//                                print("IsBuffering")
//    //                            self.ButtonPlay.isHidden = true
//    //                            self.loadingView.isHidden = false
//                            } else {
//                                //stop the activity indicator
//    //                            print("Buffering completed")
//    //                            self.ButtonPlay.isHidden = false
//    //                            self.loadingView.isHidden = true
//                            }
//
//                        }

                    }

                    cell.btnPlayAudio.tag = indexPath.row
                    cell.progressAudio.tag = indexPath.row
                    cell.btnEdit.tag = indexPath.row
                    cell.btnDelete.tag = indexPath.row
                    cell.btnDelete.addTarget(self, action: #selector(self.deleteCategoryTapped(_:)), for: .touchUpInside)
                    cell.btnEdit.addTarget(self, action: #selector(self.editCategoryTapped(_:)), for: .touchUpInside)
                    cell.btnPlayAudio.addTarget(self, action: #selector(self.playAudioTapped(_:)), for: .touchUpInside)

                    return cell
                    
                } else {
                    
                    tableView.register(UINib(nibName: "GrowthCell", bundle: nil), forCellReuseIdentifier: "GrowthCell")
                    var cell : GrowthCell! = tableView.dequeueReusableCell(withIdentifier: "GrowthCell") as? GrowthCell

                    if (cell == nil) {
                        cell = GrowthCell.init(style: UITableViewCell.CellStyle.default,
                                                      reuseIdentifier:"GrowthCell");
                    }

                    cell.selectionStyle = UITableViewCell.SelectionStyle.none

                    cell.lablCatName.text = "Growth".localized()
                    cell.imgCat.image = UIImage(named: "growth")
                    
                    cell.layoutHeightCollection.constant = 24
                    cell.isDiaper = false
                    cell.isNotes = false
                    cell.isActivity = false
                    if obj.isActive == 0 {
                        cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                        cell.lablTimeAgo.text = ""
                    } else {
                        cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                        cell.lablTimeAgo.text = obj.newDifferDate
                    }
                    cell.lablUser.text = obj.userName
                    cell.objGrowth = obj.objGrowth
                    cell.lablDetail.text = obj.notes
                    if obj.objGrowth.media.count == 0 {
                        cell.layoutHeightImage.constant = 0
                    } else {
                        cell.layoutHeightImage.constant = 171
                    }
                    AppUtility.loadImage(imageView: cell.imgItem, urlString: obj.objGrowth.media, placeHolderImageString: "bb")
                    AppUtility.loadImage(imageView: cell.imgUSer, urlString: obj.userImage, placeHolderImageString: "bb")
                    
                    cell.btnEdit.tag = indexPath.row
                    cell.btnDelete.tag = indexPath.row
                    cell.btnDelete.addTarget(self, action: #selector(self.deleteCategoryTapped(_:)), for: .touchUpInside)
                    cell.btnEdit.addTarget(self, action: #selector(self.editCategoryTapped(_:)), for: .touchUpInside)
                    
                    cell.collectionView.reloadData()

                    return cell
                }
                
            } else if obj.type == DIARYTYPE.medication.rawValue {
                if obj.isActive == 1 {
                    if obj.objMedication.mediaType == "audio" {
                        tableView.register(UINib(nibName: "MedicationGrowthActiveAudioCell", bundle: nil), forCellReuseIdentifier: "MedicationGrowthActiveAudioCell")
                        var cell : MedicationGrowthActiveAudioCell! = tableView.dequeueReusableCell(withIdentifier: "MedicationGrowthActiveAudioCell") as? MedicationGrowthActiveAudioCell

                        if (cell == nil) {
                            cell = MedicationGrowthActiveAudioCell.init(style: UITableViewCell.CellStyle.default,
                                                          reuseIdentifier:"MedicationGrowthActiveAudioCell");
                        }

                        cell.selectionStyle = UITableViewCell.SelectionStyle.none

                        cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                        cell.lablTimeAgo.text = obj.newDifferDate
                        cell.lablUser.text = obj.userName
                        
                        cell.objMed = obj.objMedication
                        AppUtility.loadImage(imageView: cell.imgUSer, urlString: obj.userImage, placeHolderImageString: "bb")
                        cell.lablDetail.text = obj.notes

                        cell.progressAudio.setProgress(Float(0), animated: false)
                        cell.lablAudioTime.text = obj.objMedication.mediaDuration

                        if !obj.objMedication.isPlay {

                            obj.objMedication.isPlay = false
                            if player != nil
                            {
                                player.pause()
                            }
                            cell.btnPlayAudio.isSelected = false

                        } else {
                            cell.btnPlayAudio.isSelected = true
                            obj.objMedication.isPlay = true
                            player = AVPlayer()
                            if player != nil
                            {
                                player.pause()
                            }

                            let url = URL(string: obj.objMedication.media)
                            let playerItem = CachingPlayerItem(url: url!)
                            NotificationCenter.default.addObserver(self, selector: #selector(self.playerDidFinishPlaying(sender:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
                            playerItem.delegate = self
                            player = AVPlayer(playerItem: playerItem)
                            player.automaticallyWaitsToMinimizeStalling = true
                            player.play()

                        }
                        
                        cell.setData()
                        
                        cell.btnPlayAudio.tag = indexPath.row
                        cell.progressAudio.tag = indexPath.row
                        cell.btnEdit.tag = indexPath.row
                        cell.btnDelete.tag = indexPath.row
                        cell.btnDone.tag = indexPath.row
                        cell.btnDone.addTarget(self, action: #selector(self.doneDiaryTapped(_:)), for: .touchUpInside)
                        cell.btnDelete.addTarget(self, action: #selector(self.deleteCategoryTapped(_:)), for: .touchUpInside)
                        cell.btnEdit.addTarget(self, action: #selector(self.editCategoryTapped(_:)), for: .touchUpInside)
                        cell.btnPlayAudio.addTarget(self, action: #selector(self.playAudioTapped(_:)), for: .touchUpInside)

                        return cell
                        
                    } else {
                        
                        tableView.register(UINib(nibName: "MedicationGrowthActiveCell", bundle: nil), forCellReuseIdentifier: "MedicationGrowthActiveCell")
                        var cell : MedicationGrowthActiveCell! = tableView.dequeueReusableCell(withIdentifier: "MedicationGrowthActiveCell") as? MedicationGrowthActiveCell

                        if (cell == nil) {
                            cell = MedicationGrowthActiveCell.init(style: UITableViewCell.CellStyle.default,
                                                          reuseIdentifier:"MedicationGrowthActiveCell");
                        }

                        cell.selectionStyle = UITableViewCell.SelectionStyle.none

                        cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                        cell.lablTimeAgo.text = obj.newDifferDate
                        
                        cell.lablUser.text = obj.userName
                        cell.objMed = obj.objMedication
                        cell.lablDetail.text = obj.notes
                        if obj.objMedication.media.count == 0 {
                            cell.layoutHeightImage.constant = 0
                        } else {
                            cell.layoutHeightImage.constant = 171
                        }
                        AppUtility.loadImage(imageView: cell.imgItem, urlString: obj.objMedication.media, placeHolderImageString: "bb")
                        AppUtility.loadImage(imageView: cell.imgUSer, urlString: obj.userImage, placeHolderImageString: "bb")
                        
                        cell.setData()
                        
                        cell.btnEdit.tag = indexPath.row
                        cell.btnDelete.tag = indexPath.row
                        cell.btnDone.tag = indexPath.row
                        cell.btnDone.addTarget(self, action: #selector(self.doneDiaryTapped(_:)), for: .touchUpInside)
                        cell.btnDelete.addTarget(self, action: #selector(self.deleteCategoryTapped(_:)), for: .touchUpInside)
                        cell.btnEdit.addTarget(self, action: #selector(self.editCategoryTapped(_:)), for: .touchUpInside)
                        
                        return cell
                    }
                } else {
                    if obj.objMedication.mediaType == "audio" {
                        tableView.register(UINib(nibName: "MedicationGrowthAudioCell", bundle: nil), forCellReuseIdentifier: "MedicationGrowthAudioCell")
                        var cell : MedicationGrowthAudioCell! = tableView.dequeueReusableCell(withIdentifier: "MedicationGrowthAudioCell") as? MedicationGrowthAudioCell

                        if (cell == nil) {
                            cell = MedicationGrowthAudioCell.init(style: UITableViewCell.CellStyle.default,
                                                          reuseIdentifier:"MedicationGrowthAudioCell");
                        }

                        cell.selectionStyle = UITableViewCell.SelectionStyle.none

                        if obj.isActive == 0 {
                            cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                            cell.lablTimeAgo.text = ""
                        } else {
                            cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                            cell.lablTimeAgo.text = obj.newDifferDate
                        }
                        cell.lablUser.text = obj.userName
                        cell.objMed = obj.objMedication
                        AppUtility.loadImage(imageView: cell.imgUSer, urlString: obj.userImage, placeHolderImageString: "bb")
                        cell.collectionView.reloadData()
                        cell.lablDetail.text = obj.notes

    //                    let item = AVPlayerItem(url: URL(string: obj.objMedication.media)!)
    //                    let duration = Double(item.asset.duration.value) / Double(item.asset.duration.timescale)
    //
    //                    //let currentTime1 = Int((player?.currentTime)!)
    //                    let minutes = duration/60
    //                    let seconds = Int(duration) - Int(minutes) * 60
    //                    cell.lablAudioTime.text = NSString(format: "%02d:%02d", minutes,seconds) as String
                        
                        cell.progressAudio.setProgress(Float(0), animated: false)
                        cell.lablAudioTime.text = obj.objMedication.mediaDuration

                        if !obj.objMedication.isPlay {

                            obj.objMedication.isPlay = false
                            if player != nil
                            {
                                player.pause()
                            }
                            cell.btnPlayAudio.isSelected = false

                        } else {
                            cell.btnPlayAudio.isSelected = true
                            obj.objMedication.isPlay = true
                            player = AVPlayer()
                            if player != nil
                            {
                                player.pause()
                            }

                            let url = URL(string: obj.objMedication.media)
                            let playerItem = CachingPlayerItem(url: url!)
                            NotificationCenter.default.addObserver(self, selector: #selector(self.playerDidFinishPlaying(sender:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
                            playerItem.delegate = self
                            player = AVPlayer(playerItem: playerItem)
                            player.automaticallyWaitsToMinimizeStalling = true
                            player.play()

                        }

                        cell.btnPlayAudio.tag = indexPath.row
                        cell.progressAudio.tag = indexPath.row
                        cell.btnEdit.tag = indexPath.row
                        cell.btnDelete.tag = indexPath.row
                        cell.btnDelete.addTarget(self, action: #selector(self.deleteCategoryTapped(_:)), for: .touchUpInside)
                        cell.btnEdit.addTarget(self, action: #selector(self.editCategoryTapped(_:)), for: .touchUpInside)
                        cell.btnPlayAudio.addTarget(self, action: #selector(self.playAudioTapped(_:)), for: .touchUpInside)

                        return cell
                        
                    } else {
                        
                        tableView.register(UINib(nibName: "MedicationGrowthCell", bundle: nil), forCellReuseIdentifier: "MedicationGrowthCell")
                        var cell : MedicationGrowthCell! = tableView.dequeueReusableCell(withIdentifier: "MedicationGrowthCell") as? MedicationGrowthCell

                        if (cell == nil) {
                            cell = MedicationGrowthCell.init(style: UITableViewCell.CellStyle.default,
                                                          reuseIdentifier:"MedicationGrowthCell");
                        }

                        cell.selectionStyle = UITableViewCell.SelectionStyle.none

                        if obj.isActive == 0 {
                            cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                            cell.lablTimeAgo.text = ""
                        } else {
                            cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                            cell.lablTimeAgo.text = obj.newDifferDate
                        }
                        cell.lablUser.text = obj.userName
                        cell.objMed = obj.objMedication
                        cell.lablDetail.text = obj.notes
                        if obj.objMedication.media.count == 0 {
                            cell.layoutHeightImage.constant = 0
                        } else {
                            cell.layoutHeightImage.constant = 171
                        }
                        AppUtility.loadImage(imageView: cell.imgItem, urlString: obj.objMedication.media, placeHolderImageString: "bb")
                        AppUtility.loadImage(imageView: cell.imgUSer, urlString: obj.userImage, placeHolderImageString: "bb")
                        
                        cell.btnEdit.tag = indexPath.row
                        cell.btnDelete.tag = indexPath.row
                        cell.btnDelete.addTarget(self, action: #selector(self.deleteCategoryTapped(_:)), for: .touchUpInside)
                        cell.btnEdit.addTarget(self, action: #selector(self.editCategoryTapped(_:)), for: .touchUpInside)
                        
                        cell.collectionView.reloadData()

                        return cell
                    }
                }
            } else if obj.type == DIARYTYPE.vaccination.rawValue {
                
                tableView.register(UINib(nibName: "VaccinationGrowthCell", bundle: nil), forCellReuseIdentifier: "VaccinationGrowthCell")
                var cell : VaccinationGrowthCell! = tableView.dequeueReusableCell(withIdentifier: "VaccinationGrowthCell") as? VaccinationGrowthCell

                if (cell == nil) {
                    cell = VaccinationGrowthCell.init(style: UITableViewCell.CellStyle.default,
                                                  reuseIdentifier:"VaccinationGrowthCell");
                }

                cell.selectionStyle = UITableViewCell.SelectionStyle.none

                cell.lablCatName.text = "Vaccination".localized()
                cell.imgCat.image = UIImage(named: "vaccine")
                if obj.isActive == 0 {
                    cell.btnDone.isEnabled = false
                    cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                    cell.lablTimeAgo.text = ""
                    cell.imgNonActive.isHidden = false
                } else {
                    cell.btnDone.isEnabled = true
                    cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                    cell.lablTimeAgo.text = obj.newDifferDate
                    cell.imgNonActive.isHidden = true
                }
                cell.switchOneday.isOn = (obj.objVaccination.oneDayBeforeReminder != 0)
                cell.switchOneHour.isOn = (obj.objVaccination.oneHourBeforeReminder != 0)
                cell.lablUser.text = obj.userName
                cell.objVaccination = obj.objVaccination
                cell.lablDetail.text = obj.objVaccination.detail
                AppUtility.loadImage(imageView: cell.imgUSer, urlString: obj.userImage, placeHolderImageString: "bb")
                
                cell.btnEdit.tag = indexPath.row
                cell.btnDelete.tag = indexPath.row
                cell.btnDone.tag = indexPath.row
                cell.btnDone.addTarget(self, action: #selector(self.doneDiaryTapped(_:)), for: .touchUpInside)
                cell.btnEdit.addTarget(self, action: #selector(self.editCategoryTapped(_:)), for: .touchUpInside)
                cell.btnDelete.addTarget(self, action: #selector(self.deleteCategoryTapped(_:)), for: .touchUpInside)
                
                cell.collectionView.reloadData()

                return cell
                
            } else if obj.type == DIARYTYPE.appointment.rawValue {
                
                tableView.register(UINib(nibName: "VaccinationGrowthCell", bundle: nil), forCellReuseIdentifier: "VaccinationGrowthCell")
                var cell : VaccinationGrowthCell! = tableView.dequeueReusableCell(withIdentifier: "VaccinationGrowthCell") as? VaccinationGrowthCell

                if (cell == nil) {
                    cell = VaccinationGrowthCell.init(style: UITableViewCell.CellStyle.default,
                                                  reuseIdentifier:"VaccinationGrowthCell");
                }

                cell.selectionStyle = UITableViewCell.SelectionStyle.none

                cell.lablCatName.text = "Appointment".localized()
                cell.imgCat.image = UIImage(named: "appointment")
                if obj.isActive == 0 {
                    cell.btnDone.isEnabled = false
                    cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                    cell.lablTimeAgo.text = ""
                    cell.imgNonActive.isHidden = false
                } else {
                    cell.btnDone.isEnabled = true
                    cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                    cell.lablTimeAgo.text = obj.newDifferDate
                    cell.imgNonActive.isHidden = true
                }
                cell.lablUser.text = obj.userName
                cell.objAppointment = obj.objAppointment
                cell.isAppointment = true
                cell.lablDetail.text = obj.notes
                cell.switchOneday.isOn = (obj.objAppointment.oneDayBeforeReminder != 0)
                cell.switchOneHour.isOn = (obj.objAppointment.oneHourBeforeReminder != 0)
                
                AppUtility.loadImage(imageView: cell.imgUSer, urlString: obj.userImage, placeHolderImageString: "bb")
                
                cell.btnEdit.tag = indexPath.row
                cell.btnDelete.tag = indexPath.row
                cell.btnDone.tag = indexPath.row
                cell.btnDone.addTarget(self, action: #selector(self.doneDiaryTapped(_:)), for: .touchUpInside)
                cell.btnEdit.addTarget(self, action: #selector(self.editCategoryTapped(_:)), for: .touchUpInside)
                cell.btnDelete.addTarget(self, action: #selector(self.deleteCategoryTapped(_:)), for: .touchUpInside)
                
                cell.collectionView.reloadData()

                return cell
                
            } else if obj.type == DIARYTYPE.temperature.rawValue {
                
                tableView.register(UINib(nibName: "SleepGrowthCell", bundle: nil), forCellReuseIdentifier: "SleepGrowthCell")
                var cell : SleepGrowthCell! = tableView.dequeueReusableCell(withIdentifier: "SleepGrowthCell") as? SleepGrowthCell

                if (cell == nil) {
                    cell = SleepGrowthCell.init(style: UITableViewCell.CellStyle.default,
                                                  reuseIdentifier:"SleepGrowthCell");
                }

                cell.selectionStyle = UITableViewCell.SelectionStyle.none

                cell.lablCatName.text = "Temperature".localized()
                cell.imgCat.image = UIImage(named: "Thermometer")
                if obj.isActive == 0 {
                    cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                    cell.lablTimeAgo.text = ""
                } else {
                    cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                    cell.lablTimeAgo.text = obj.newDifferDate
                }
                cell.lablUser.text = obj.userName
                cell.objTemp = obj.objTemprature
                cell.lablDetail.text = obj.notes
                cell.isTempretaure = true
                AppUtility.loadImage(imageView: cell.imgUSer, urlString: obj.userImage, placeHolderImageString: "bb")
                
                cell.btnEdit.tag = indexPath.row
                cell.btnDelete.tag = indexPath.row
                cell.btnEdit.addTarget(self, action: #selector(self.editCategoryTapped(_:)), for: .touchUpInside)
                cell.btnDelete.addTarget(self, action: #selector(self.deleteCategoryTapped(_:)), for: .touchUpInside)
                
                cell.collectionView.reloadData()

                return cell
                
            } else if obj.type == DIARYTYPE.sleep.rawValue {
                if obj.isActive == 1 {
                    
                    tableView.register(UINib(nibName: "SleepGrowthActiveCell", bundle: nil), forCellReuseIdentifier: "SleepGrowthActiveCell")
                    var cell : SleepGrowthActiveCell! = tableView.dequeueReusableCell(withIdentifier: "SleepGrowthActiveCell") as? SleepGrowthActiveCell

                    if (cell == nil) {
                        cell = SleepGrowthActiveCell.init(style: UITableViewCell.CellStyle.default,
                                                      reuseIdentifier:"SleepGrowthActiveCell");
                    }

                    cell.selectionStyle = UITableViewCell.SelectionStyle.none

                    cell.lablCatName.text = "Sleep".localized()
                    cell.imgCat.image = UIImage(named: "sleep")
                    cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                    cell.lablTimeAgo.text = obj.newDifferDate
                    cell.lablUser.text = obj.userName
                    cell.objSleep = obj.objSleep
                    cell.lablDetail.text = obj.notes
                    AppUtility.loadImage(imageView: cell.imgUSer, urlString: obj.userImage, placeHolderImageString: "bb")
                    
                    cell.setData()
                    
                    cell.btnEdit.tag = indexPath.row
                    cell.btnDelete.tag = indexPath.row
                    cell.btnDone.tag = indexPath.row
                    cell.btnDone.addTarget(self, action: #selector(self.doneDiaryTapped(_:)), for: .touchUpInside)
                    cell.btnEdit.addTarget(self, action: #selector(self.editCategoryTapped(_:)), for: .touchUpInside)
                    cell.btnDelete.addTarget(self, action: #selector(self.deleteCategoryTapped(_:)), for: .touchUpInside)
            
                    return cell
                    
                } else {
                    
                    tableView.register(UINib(nibName: "SleepGrowthCell", bundle: nil), forCellReuseIdentifier: "SleepGrowthCell")
                    var cell : SleepGrowthCell! = tableView.dequeueReusableCell(withIdentifier: "SleepGrowthCell") as? SleepGrowthCell

                    if (cell == nil) {
                        cell = SleepGrowthCell.init(style: UITableViewCell.CellStyle.default,
                                                      reuseIdentifier:"SleepGrowthCell");
                    }

                    cell.selectionStyle = UITableViewCell.SelectionStyle.none

                    cell.lablCatName.text = "Sleep".localized()
                    cell.imgCat.image = UIImage(named: "sleep")
                    if obj.isActive == 0 {
                        cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                        cell.lablTimeAgo.text = ""
                    } else {
                        cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                        cell.lablTimeAgo.text = obj.newDifferDate
                    }
                    cell.lablUser.text = obj.userName
                    cell.objSleep = obj.objSleep
                    cell.lablDetail.text = obj.notes
                    cell.isTempretaure = false
                    AppUtility.loadImage(imageView: cell.imgUSer, urlString: obj.userImage, placeHolderImageString: "bb")
                    
                    cell.btnEdit.tag = indexPath.row
                    cell.btnDelete.tag = indexPath.row
                    cell.btnEdit.addTarget(self, action: #selector(self.editCategoryTapped(_:)), for: .touchUpInside)
                    cell.btnDelete.addTarget(self, action: #selector(self.deleteCategoryTapped(_:)), for: .touchUpInside)
                    
                    cell.collectionView.reloadData()

                    return cell
                }
                
            } else if obj.type == DIARYTYPE.note.rawValue {
                
                if obj.isActive == 1 {
                    if obj.objNote.mediaType == "audio" {
                        
                        tableView.register(UINib(nibName: "NotesGrowthAudioActiveCell", bundle: nil), forCellReuseIdentifier: "NotesGrowthAudioActiveCell")
                        var cell : NotesGrowthAudioActiveCell! = tableView.dequeueReusableCell(withIdentifier: "NotesGrowthAudioActiveCell") as? NotesGrowthAudioActiveCell

                        if (cell == nil) {
                            cell = NotesGrowthAudioActiveCell.init(style: UITableViewCell.CellStyle.default,
                                                          reuseIdentifier:"NotesGrowthAudioActiveCell");
                        }

                        cell.selectionStyle = UITableViewCell.SelectionStyle.none
                        
                        cell.tfNotes.text = obj.notes
                        cell.objNotes = obj.objNote
                        cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                        cell.lablTimeAgo.text = obj.newDifferDate
    
                        cell.lablUser.text = obj.userName
                        AppUtility.loadImage(imageView: cell.imgUSer, urlString: obj.userImage, placeHolderImageString: "bb")

                        cell.progressAudio.setProgress(Float(0), animated: false)
                        cell.lablAudioTime.text = obj.objNote.mediaDuration

                        if !obj.objNote.isPlay {

                            obj.objNote.isPlay = false
                            if player != nil
                            {
                                player.pause()
                            }
                            cell.btnPlayAudio.isSelected = false

                        } else {
                            
                            cell.btnPlayAudio.isSelected = true
                            obj.objNote.isPlay = true
                            player = AVPlayer()
                            if player != nil
                            {
                                player.pause()
                            }

                            let url = URL(string: obj.objNote.media)
                            let playerItem = CachingPlayerItem(url: url!)
                            NotificationCenter.default.addObserver(self, selector: #selector(self.playerDidFinishPlaying(sender:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
                            playerItem.delegate = self
                            player = AVPlayer(playerItem: playerItem)
                            player.automaticallyWaitsToMinimizeStalling = true
                            player.play()

                        }

                        cell.btnPlayAudio.tag = indexPath.row
                        cell.progressAudio.tag = indexPath.row
                        cell.btnEdit.tag = indexPath.row
                        cell.btnDelete.tag = indexPath.row
                        cell.btnDone.tag = indexPath.row
                        cell.btnDone.addTarget(self, action: #selector(self.doneDiaryTapped(_:)), for: .touchUpInside)
                        cell.btnDelete.addTarget(self, action: #selector(self.deleteCategoryTapped(_:)), for: .touchUpInside)
                        cell.btnEdit.addTarget(self, action: #selector(self.editCategoryTapped(_:)), for: .touchUpInside)
                        cell.btnPlayAudio.addTarget(self, action: #selector(self.playAudioTapped(_:)), for: .touchUpInside)

                        return cell
                        
                    } else {
                        
                        tableView.register(UINib(nibName: "NotesGrowthCell", bundle: nil), forCellReuseIdentifier: "NotesGrowthCell")
                        var cell : NotesGrowthCell! = tableView.dequeueReusableCell(withIdentifier: "NotesGrowthCell") as? NotesGrowthCell

                        if (cell == nil) {
                            cell = NotesGrowthCell.init(style: UITableViewCell.CellStyle.default,
                                                          reuseIdentifier:"NotesGrowthCell");
                        }

                        cell.selectionStyle = UITableViewCell.SelectionStyle.none
                        
                        cell.objNotes = obj.objNote
                        cell.tfNotes.text = obj.notes
                        cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                        cell.lablTimeAgo.text = obj.newDifferDate
                        cell.lablUser.text = obj.userName
                        
                        if obj.objNote.media.count == 0 {
                            cell.layoutHeightImage.constant = 0
                        } else {
                            cell.layoutHeightImage.constant = 171
                        }
                        AppUtility.loadImage(imageView: cell.imgItem, urlString: obj.objNote.media, placeHolderImageString: "bb")
                        AppUtility.loadImage(imageView: cell.imgUSer, urlString: obj.userImage, placeHolderImageString: "bb")
                        
                        cell.btnEdit.tag = indexPath.row
                        cell.btnDelete.tag = indexPath.row
                        cell.btnDone.tag = indexPath.row
                        cell.btnDone.addTarget(self, action: #selector(self.doneDiaryTapped(_:)), for: .touchUpInside)
                        cell.btnDelete.addTarget(self, action: #selector(self.deleteCategoryTapped(_:)), for: .touchUpInside)
                        cell.btnEdit.addTarget(self, action: #selector(self.editCategoryTapped(_:)), for: .touchUpInside)
                        
                        return cell
                    }
                } else {
                    if obj.objNote.mediaType == "audio" {
                        
                        tableView.register(UINib(nibName: "GrowthAudioCell", bundle: nil), forCellReuseIdentifier: "GrowthAudioCell")
                        var cell : GrowthAudioCell! = tableView.dequeueReusableCell(withIdentifier: "GrowthAudioCell") as? GrowthAudioCell

                        if (cell == nil) {
                            cell = GrowthAudioCell.init(style: UITableViewCell.CellStyle.default,
                                                          reuseIdentifier:"GrowthAudioCell");
                        }

                        cell.selectionStyle = UITableViewCell.SelectionStyle.none
                        
                        cell.lablCatName.text = "Notes".localized()
                        cell.imgCat.image = UIImage(named: "notes")
                        
                        cell.layoutHeightCollection.constant = 0

                        if obj.isActive == 0 {
                            cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                            cell.lablTimeAgo.text = ""
                        } else {
                            cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                            cell.lablTimeAgo.text = obj.newDifferDate
                        }
                        cell.lablUser.text = obj.userName
                        cell.isDiaper = false
                        cell.isNotes = true
                        cell.isActivity = false
                        cell.lablDetail.text = obj.notes
                        AppUtility.loadImage(imageView: cell.imgUSer, urlString: obj.userImage, placeHolderImageString: "bb")
                        cell.collectionView.reloadData()

                        cell.progressAudio.setProgress(Float(0), animated: false)
                        cell.lablAudioTime.text = obj.objNote.mediaDuration

                        if !obj.objNote.isPlay {

                            obj.objNote.isPlay = false
                            if player != nil
                            {
                                player.pause()
                            }
                            cell.btnPlayAudio.isSelected = false

                        } else {
                            cell.btnPlayAudio.isSelected = true
                            obj.objNote.isPlay = true
                            player = AVPlayer()
                            if player != nil
                            {
                                player.pause()
                            }

                            let url = URL(string: obj.objNote.media)
                            let playerItem = CachingPlayerItem(url: url!)
                            NotificationCenter.default.addObserver(self, selector: #selector(self.playerDidFinishPlaying(sender:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
                            playerItem.delegate = self
                            player = AVPlayer(playerItem: playerItem)
                            player.automaticallyWaitsToMinimizeStalling = true
                            player.play()

                        }

                        cell.btnPlayAudio.tag = indexPath.row
                        cell.progressAudio.tag = indexPath.row
                        cell.btnEdit.tag = indexPath.row
                        cell.btnDelete.tag = indexPath.row
                        cell.btnDelete.addTarget(self, action: #selector(self.deleteCategoryTapped(_:)), for: .touchUpInside)
                        cell.btnEdit.addTarget(self, action: #selector(self.editCategoryTapped(_:)), for: .touchUpInside)
                        cell.btnPlayAudio.addTarget(self, action: #selector(self.playAudioTapped(_:)), for: .touchUpInside)

                        return cell
                        
                    } else {
                        
                        tableView.register(UINib(nibName: "GrowthCell", bundle: nil), forCellReuseIdentifier: "GrowthCell")
                        var cell : GrowthCell! = tableView.dequeueReusableCell(withIdentifier: "GrowthCell") as? GrowthCell

                        if (cell == nil) {
                            cell = GrowthCell.init(style: UITableViewCell.CellStyle.default,
                                                          reuseIdentifier:"GrowthCell");
                        }

                        cell.selectionStyle = UITableViewCell.SelectionStyle.none
                        
                        cell.lablCatName.text = "Notes".localized()
                        cell.imgCat.image = UIImage(named: "notes")
                        
                        cell.layoutHeightCollection.constant = 0

                        if obj.isActive == 0 {
                            cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                            cell.lablTimeAgo.text = ""
                        } else {
                            cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                            cell.lablTimeAgo.text = obj.newDifferDate
                        }
                        cell.lablUser.text = obj.userName
                        cell.isDiaper = false
                        cell.isNotes = true
                        cell.isActivity = false
                        cell.lablDetail.text = obj.notes
                        if obj.objNote.media.count == 0 {
                            cell.layoutHeightImage.constant = 0
                        } else {
                            cell.layoutHeightImage.constant = 171
                        }
                        AppUtility.loadImage(imageView: cell.imgItem, urlString: obj.objNote.media, placeHolderImageString: "bb")
                        AppUtility.loadImage(imageView: cell.imgUSer, urlString: obj.userImage, placeHolderImageString: "bb")
                        
                        cell.btnEdit.tag = indexPath.row
                        cell.btnDelete.tag = indexPath.row
                        cell.btnDelete.addTarget(self, action: #selector(self.deleteCategoryTapped(_:)), for: .touchUpInside)
                        cell.btnEdit.addTarget(self, action: #selector(self.editCategoryTapped(_:)), for: .touchUpInside)
                        
                        cell.collectionView.reloadData()

                        return cell
                    }
                }
            } else if obj.type == DIARYTYPE.pumping.rawValue {
                
                tableView.register(UINib(nibName: "PumpingGrowthCell", bundle: nil), forCellReuseIdentifier: "PumpingGrowthCell")
                var cell : PumpingGrowthCell! = tableView.dequeueReusableCell(withIdentifier: "PumpingGrowthCell") as? PumpingGrowthCell

                if (cell == nil) {
                    cell = PumpingGrowthCell.init(style: UITableViewCell.CellStyle.default,
                                                  reuseIdentifier:"PumpingGrowthCell");
                }

                cell.selectionStyle = UITableViewCell.SelectionStyle.none

                if obj.isActive == 0 {
                    cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                    cell.lablTimeAgo.text = ""
                } else {
                    cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                    cell.lablTimeAgo.text = obj.newDifferDate
                }
                cell.lablDetail.text = obj.notes
                cell.lablUser.text = obj.userName
                cell.objPump = obj.objPumping
                AppUtility.loadImage(imageView: cell.imgUSer, urlString: obj.userImage, placeHolderImageString: "bb")
                
                cell.btnEdit.tag = indexPath.row
                cell.btnDelete.tag = indexPath.row
                cell.btnEdit.addTarget(self, action: #selector(self.editCategoryTapped(_:)), for: .touchUpInside)
                cell.btnDelete.addTarget(self, action: #selector(self.deleteCategoryTapped(_:)), for: .touchUpInside)
                
                cell.collectionView.reloadData()

                return cell
                
            } else if obj.type == DIARYTYPE.diaper.rawValue {
                if obj.isActive == 1 {
                    if obj.objDiaper.mediaType == "audio" {
                        tableView.register(UINib(nibName: "DiaperGrowthActiveAudioCell", bundle: nil), forCellReuseIdentifier: "DiaperGrowthActiveAudioCell")
                        var cell : DiaperGrowthActiveAudioCell! = tableView.dequeueReusableCell(withIdentifier: "DiaperGrowthActiveAudioCell") as? DiaperGrowthActiveAudioCell

                        if (cell == nil) {
                            cell = DiaperGrowthActiveAudioCell.init(style: UITableViewCell.CellStyle.default,
                                                          reuseIdentifier:"DiaperGrowthActiveAudioCell");
                        }

                        cell.selectionStyle = UITableViewCell.SelectionStyle.none
                        
                        cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                        cell.lablTimeAgo.text = obj.newDifferDate
                    
                        cell.lablDetail.text = obj.notes
                        cell.lablUser.text = obj.userName
                        cell.objDiaper = obj.objDiaper
                        AppUtility.loadImage(imageView: cell.imgUSer, urlString: obj.userImage, placeHolderImageString: "bb")
                        cell.collectionTypes.reloadData()
                        cell.collectionColor.reloadData()
                        cell.progressAudio.setProgress(Float(0), animated: false)
                        cell.lablAudioTime.text = obj.objDiaper.mediaDuration

                        if !obj.objDiaper.isPlay {

                            obj.objDiaper.isPlay = false
                            if player != nil
                            {
                                player.pause()
                            }
                            cell.btnPlayAudio.isSelected = false

                        } else {
                            cell.btnPlayAudio.isSelected = true
                            obj.objDiaper.isPlay = true
                            player = AVPlayer()
                            if player != nil
                            {
                                player.pause()
                            }

                            let url = URL(string: obj.objDiaper.media)
                            let playerItem = CachingPlayerItem(url: url!)
                            NotificationCenter.default.addObserver(self, selector: #selector(self.playerDidFinishPlaying(sender:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
                            playerItem.delegate = self
                            player = AVPlayer(playerItem: playerItem)
                            player.automaticallyWaitsToMinimizeStalling = true
                            player.play()

                        }

                        cell.setData()
                        
                        cell.btnPlayAudio.tag = indexPath.row
                        cell.progressAudio.tag = indexPath.row
                        cell.btnEdit.tag = indexPath.row
                        cell.btnDelete.tag = indexPath.row
                        cell.btnDone.tag = indexPath.row
                        cell.btnDone.addTarget(self, action: #selector(self.doneDiaryTapped(_:)), for: .touchUpInside)
                        cell.btnDelete.addTarget(self, action: #selector(self.deleteCategoryTapped(_:)), for: .touchUpInside)
                        cell.btnEdit.addTarget(self, action: #selector(self.editCategoryTapped(_:)), for: .touchUpInside)
                        cell.btnPlayAudio.addTarget(self, action: #selector(self.playAudioTapped(_:)), for: .touchUpInside)

                        return cell
                        
                    } else {
                        
                        tableView.register(UINib(nibName: "DiaperGrowthActiveCell", bundle: nil), forCellReuseIdentifier: "DiaperGrowthActiveCell")
                        var cell : DiaperGrowthActiveCell! = tableView.dequeueReusableCell(withIdentifier: "DiaperGrowthActiveCell") as? DiaperGrowthActiveCell

                        if (cell == nil) {
                            cell = DiaperGrowthActiveCell.init(style: UITableViewCell.CellStyle.default,
                                                          reuseIdentifier:"DiaperGrowthActiveCell");
                        }

                        cell.selectionStyle = UITableViewCell.SelectionStyle.none
                        
                        cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                        cell.lablTimeAgo.text = obj.newDifferDate

                        cell.lablUser.text = obj.userName
                        cell.objDiaper = obj.objDiaper
                        if obj.objDiaper.media.count == 0 {
                            cell.layoutHeightImage.constant = 0
                        } else {
                            cell.layoutHeightImage.constant = 171
                        }
                        cell.lablDetail.text = obj.notes
                        
                        AppUtility.loadImage(imageView: cell.imgItem, urlString: obj.objDiaper.media, placeHolderImageString: "bb")
                        AppUtility.loadImage(imageView: cell.imgUSer, urlString: obj.userImage, placeHolderImageString: "bb")
                        
                        cell.btnEdit.tag = indexPath.row
                        cell.btnDelete.tag = indexPath.row
                        cell.btnDone.tag = indexPath.row
                        cell.btnDone.addTarget(self, action: #selector(self.doneDiaryTapped(_:)), for: .touchUpInside)
                        cell.btnDelete.addTarget(self, action: #selector(self.deleteCategoryTapped(_:)), for: .touchUpInside)
                        cell.btnEdit.addTarget(self, action: #selector(self.editCategoryTapped(_:)), for: .touchUpInside)
                        
                        cell.collectionTypes.reloadData()
                        cell.collectionColor.reloadData()

                        cell.setData()
                        return cell
                    }
                } else {
                    if obj.objDiaper.mediaType == "audio" {
                        tableView.register(UINib(nibName: "GrowthAudioCell", bundle: nil), forCellReuseIdentifier: "GrowthAudioCell")
                        var cell : GrowthAudioCell! = tableView.dequeueReusableCell(withIdentifier: "GrowthAudioCell") as? GrowthAudioCell

                        if (cell == nil) {
                            cell = GrowthAudioCell.init(style: UITableViewCell.CellStyle.default,
                                                          reuseIdentifier:"GrowthAudioCell");
                        }

                        cell.selectionStyle = UITableViewCell.SelectionStyle.none
                        cell.lablCatName.text = "Diaper".localized()
                        cell.imgCat.image = UIImage(named: "diaper")
                        cell.layoutHeightCollection.constant = 24

                        if obj.isActive == 0 {
                            cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                            cell.lablTimeAgo.text = ""
                        } else {
                            cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                            cell.lablTimeAgo.text = obj.newDifferDate
                        }
                        cell.lablUser.text = obj.userName
                        cell.objDiaper = obj.objDiaper
                        cell.isDiaper = true
                        cell.isNotes = false
                        cell.isActivity = false
                        cell.lablDetail.text = obj.notes
                        AppUtility.loadImage(imageView: cell.imgUSer, urlString: obj.userImage, placeHolderImageString: "bb")
                        cell.collectionView.reloadData()

    //                    let item = AVPlayerItem(url: URL(string: obj.objDiaper.media)!)
    //                    let duration = Double(item.asset.duration.value) / Double(item.asset.duration.timescale)
    //
    //                    //let currentTime1 = Int((player?.currentTime)!)
    //                    let minutes = duration/60
    //                    let seconds = Int(duration) - Int(minutes) * 60
    //                    cell.lablAudioTime.text = NSString(format: "%02d:%02d", minutes,seconds) as String
                        
                        cell.progressAudio.setProgress(Float(0), animated: false)
                        cell.lablAudioTime.text = obj.objDiaper.mediaDuration

                        if !obj.objDiaper.isPlay {

                            obj.objDiaper.isPlay = false
                            if player != nil
                            {
                                player.pause()
                            }
                            cell.btnPlayAudio.isSelected = false

                        } else {
                            cell.btnPlayAudio.isSelected = true
                            obj.objDiaper.isPlay = true
                            player = AVPlayer()
                            if player != nil
                            {
                                player.pause()
                            }

                            let url = URL(string: obj.objDiaper.media)
                            let playerItem = CachingPlayerItem(url: url!)
                            NotificationCenter.default.addObserver(self, selector: #selector(self.playerDidFinishPlaying(sender:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
                            playerItem.delegate = self
                            player = AVPlayer(playerItem: playerItem)
                            player.automaticallyWaitsToMinimizeStalling = true
                            player.play()

    //                        player!.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(1, preferredTimescale: 1), queue: DispatchQueue.main) { (CMTime) -> Void in
    //                            if self.player!.currentItem?.status == .readyToPlay {
    //                                let time : Float64 = CMTimeGetSeconds(self.player!.currentTime());
    //                                let duration = Int(time)
    //                                let minutes2 = duration/60
    //                                let seconds2 = duration - minutes2 * 60
    //                                cell.lablAudioTime.text = NSString(format: "%02d:%02d", minutes2,seconds2) as String
    //
    //                                guard let currentTimes = self.player?.currentTime, let durations = self.player.currentItem?.asset.duration else {
    //                                      return
    //                                    }
    //                                let seconds = CMTimeGetSeconds(durations)
    //                                let secondss = CMTimeGetSeconds(currentTimes())
    //                                let ff:Float = Float(secondss)
    //                                let pf:Float = Float(seconds)
    //                                let percentCompleted = ff/pf
    //                                cell.progressAudio.setProgress(Float(percentCompleted), animated: true)
    //                            }
    //
    //                            let playbackLikelyToKeepUp = self.player?.currentItem?.isPlaybackLikelyToKeepUp
    //                            if playbackLikelyToKeepUp == false {
    //                                print("IsBuffering")
    //    //                            self.ButtonPlay.isHidden = true
    //    //                            self.loadingView.isHidden = false
    //                            } else {
    //                                //stop the activity indicator
    //    //                            print("Buffering completed")
    //    //                            self.ButtonPlay.isHidden = false
    //    //                            self.loadingView.isHidden = true
    //                            }
    //                        }
                        }

                        cell.btnPlayAudio.tag = indexPath.row
                        cell.progressAudio.tag = indexPath.row
                        cell.btnEdit.tag = indexPath.row
                        cell.btnDelete.tag = indexPath.row
                        cell.btnDelete.addTarget(self, action: #selector(self.deleteCategoryTapped(_:)), for: .touchUpInside)
                        cell.btnEdit.addTarget(self, action: #selector(self.editCategoryTapped(_:)), for: .touchUpInside)
                        cell.btnPlayAudio.addTarget(self, action: #selector(self.playAudioTapped(_:)), for: .touchUpInside)

                        return cell
                        
                    } else {
                        
                        tableView.register(UINib(nibName: "GrowthCell", bundle: nil), forCellReuseIdentifier: "GrowthCell")
                        var cell : GrowthCell! = tableView.dequeueReusableCell(withIdentifier: "GrowthCell") as? GrowthCell

                        if (cell == nil) {
                            cell = GrowthCell.init(style: UITableViewCell.CellStyle.default,
                                                          reuseIdentifier:"GrowthCell");
                        }

                        cell.selectionStyle = UITableViewCell.SelectionStyle.none
                        
                        cell.lablCatName.text = "Diaper".localized()
                        cell.imgCat.image = UIImage(named: "diaper")
                        cell.layoutHeightCollection.constant = 24

                        if obj.isActive == 0 {
                            cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                            cell.lablTimeAgo.text = ""
                        } else {
                            cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                            cell.lablTimeAgo.text = obj.newDifferDate
                        }
                        cell.lablUser.text = obj.userName
                        cell.objDiaper = obj.objDiaper
                        cell.isDiaper = true
                        cell.isNotes = false
                        cell.isActivity = false
                        cell.lablDetail.text = obj.notes
                        if obj.objDiaper.media.count == 0 {
                            cell.layoutHeightImage.constant = 0
                        } else {
                            cell.layoutHeightImage.constant = 171
                        }
                        AppUtility.loadImage(imageView: cell.imgItem, urlString: obj.objDiaper.media, placeHolderImageString: "bb")
                        AppUtility.loadImage(imageView: cell.imgUSer, urlString: obj.userImage, placeHolderImageString: "bb")
                        
                        cell.btnEdit.tag = indexPath.row
                        cell.btnDelete.tag = indexPath.row
                        cell.btnDelete.addTarget(self, action: #selector(self.deleteCategoryTapped(_:)), for: .touchUpInside)
                        cell.btnEdit.addTarget(self, action: #selector(self.editCategoryTapped(_:)), for: .touchUpInside)
                        
                        cell.collectionView.reloadData()

                        return cell
                    }
                }
                
            } else if obj.type == DIARYTYPE.feeding.rawValue {
                if obj.isActive == 1 {
                    if obj.objFeeding.type == "breast" {
                        tableView.register(UINib(nibName: "FoodGrowthCell", bundle: nil), forCellReuseIdentifier: "FoodGrowthCell")
                        var cell : FoodGrowthCell! = tableView.dequeueReusableCell(withIdentifier: "FoodGrowthCell") as? FoodGrowthCell

                        if (cell == nil) {
                            cell = FoodGrowthCell.init(style: UITableViewCell.CellStyle.default,
                                                          reuseIdentifier:"FoodGrowthCell");
                        }

                        cell.selectionStyle = UITableViewCell.SelectionStyle.none
                        
                        cell.objFood = obj.objFeeding
//                        cell.totalSecondRight = obj.objFeeding.rightBreastDuration.secondFromString
//                        cell.totalSecondLeft = obj.objFeeding.leftBreastDuration.secondFromString
                        cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                        cell.lablTimeAgo.text = obj.newDifferDate
                        cell.imgNonActive.isHidden = true
                        cell.tfNotes.text = obj.notes
                        cell.lablUser.text = obj.userName
                        AppUtility.loadImage(imageView: cell.imgUSer, urlString: obj.userImage, placeHolderImageString: "bb")
                        cell.lablLeftBreast.text = obj.objFeeding.leftBreastDuration
                        cell.lablRightBreast.text = obj.objFeeding.rightBreastDuration
                        cell.btnPlayLeftBreast.isEnabled = true
                        cell.btnPlayRightBreast.isEnabled = true
                        
                        cell.btnEdit.tag = indexPath.row
                        cell.btnDelete.tag = indexPath.row
                        cell.btnDone.tag = indexPath.row
                        cell.btnDone.addTarget(self, action: #selector(self.doneDiaryTapped(_:)), for: .touchUpInside)
                        cell.btnDelete.addTarget(self, action: #selector(self.deleteCategoryTapped(_:)), for: .touchUpInside)
                        cell.btnEdit.addTarget(self, action: #selector(self.editCategoryTapped(_:)), for: .touchUpInside)
                        
                        return cell
                        
                    } else if obj.objFeeding.type == "bottle" {
                        tableView.register(UINib(nibName: "FoodGrowthBotelActiveCell", bundle: nil), forCellReuseIdentifier: "FoodGrowthBotelActiveCell")
                        var cell : FoodGrowthBotelActiveCell! = tableView.dequeueReusableCell(withIdentifier: "FoodGrowthBotelActiveCell") as? FoodGrowthBotelActiveCell

                        if (cell == nil) {
                            cell = FoodGrowthBotelActiveCell.init(style: UITableViewCell.CellStyle.default,
                                                          reuseIdentifier:"FoodGrowthBotelActiveCell");
                        }

                        cell.selectionStyle = UITableViewCell.SelectionStyle.none

                        cell.objFood = obj.objFeeding
                        cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                        cell.lablTimeAgo.text = obj.newDifferDate
            
                        cell.lablDetail.text = obj.notes
                        cell.lablUser.text = obj.userName
                        AppUtility.loadImage(imageView: cell.imgUSer, urlString: obj.userImage, placeHolderImageString: "bb")
                        cell.collectionTypes.reloadData()
                        cell.collectionBottel.reloadData()
                        
                        cell.setData()
                        
                        cell.btnEdit.tag = indexPath.row
                        cell.btnDelete.tag = indexPath.row
                        cell.btnDone.tag = indexPath.row
                        cell.btnDone.addTarget(self, action: #selector(self.doneDiaryTapped(_:)), for: .touchUpInside)
                        cell.btnDelete.addTarget(self, action: #selector(self.deleteCategoryTapped(_:)), for: .touchUpInside)
                        cell.btnEdit.addTarget(self, action: #selector(self.editCategoryTapped(_:)), for: .touchUpInside)
                        
                        return cell
                        
                    } else {
                        tableView.register(UINib(nibName: "FoodGrowthIngrediantsActiveCell", bundle: nil), forCellReuseIdentifier: "FoodGrowthIngrediantsActiveCell")
                        var cell : FoodGrowthIngrediantsActiveCell! = tableView.dequeueReusableCell(withIdentifier: "FoodGrowthIngrediantsActiveCell") as? FoodGrowthIngrediantsActiveCell

                        if (cell == nil) {
                            cell = FoodGrowthIngrediantsActiveCell.init(style: UITableViewCell.CellStyle.default,
                                                          reuseIdentifier:"FoodGrowthIngrediantsActiveCell");
                        }

                        cell.selectionStyle = UITableViewCell.SelectionStyle.none

                        cell.objFood = obj.objFeeding
                        cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                        cell.lablTimeAgo.text = obj.newDifferDate
            
                        cell.lablDetail.text = obj.notes
                        cell.lablUser.text = obj.userName
                        AppUtility.loadImage(imageView: cell.imgUSer, urlString: obj.userImage, placeHolderImageString: "bb")
                        cell.collectionTypes.reloadData()
                        cell.collectionIngrediants.reloadData()
                        
                        cell.setData()
                        
                        cell.btnEdit.tag = indexPath.row
                        cell.btnDelete.tag = indexPath.row
                        cell.btnDone.tag = indexPath.row
                        cell.btnAddIngrediants.tag = indexPath.row
                        cell.btnAddIngrediants.addTarget(self, action: #selector(self.addIngrediantsTapped(_:)), for: .touchUpInside)
                        cell.btnDone.addTarget(self, action: #selector(self.doneDiaryTapped(_:)), for: .touchUpInside)
                        cell.btnDelete.addTarget(self, action: #selector(self.deleteCategoryTapped(_:)), for: .touchUpInside)
                        cell.btnEdit.addTarget(self, action: #selector(self.editCategoryTapped(_:)), for: .touchUpInside)
                        
                        return cell
                    }
                } else {
                    if obj.objFeeding.type == "breast" {
                        tableView.register(UINib(nibName: "FeedingGrowthCell", bundle: nil), forCellReuseIdentifier: "FeedingGrowthCell")
                        var cell : FeedingGrowthCell! = tableView.dequeueReusableCell(withIdentifier: "FeedingGrowthCell") as? FeedingGrowthCell

                        if (cell == nil) {
                            cell = FeedingGrowthCell.init(style: UITableViewCell.CellStyle.default,
                                                          reuseIdentifier:"FeedingGrowthCell");
                        }

                        cell.selectionStyle = UITableViewCell.SelectionStyle.none

                        if obj.isActive == 0 {
                            cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                            cell.lablTimeAgo.text = ""
                        } else {
                            cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                            cell.lablTimeAgo.text = obj.newDifferDate
                        }
                        cell.lablUser.text = obj.userName
                        cell.objFeed = obj.objFeeding
                        cell.lablDetail.text = obj.notes
                        AppUtility.loadImage(imageView: cell.imgUSer, urlString: obj.userImage, placeHolderImageString: "bb")
                        
                        cell.btnEdit.tag = indexPath.row
                        cell.btnDelete.tag = indexPath.row
                        cell.btnDelete.addTarget(self, action: #selector(self.deleteCategoryTapped(_:)), for: .touchUpInside)
                        cell.btnEdit.addTarget(self, action: #selector(self.editCategoryTapped(_:)), for: .touchUpInside)
                        
                        cell.collectionView.reloadData()

                        return cell
                        
                    } else {
                        
                        tableView.register(UINib(nibName: "FeedingGrowthCell", bundle: nil), forCellReuseIdentifier: "FeedingGrowthCell")
                        var cell : FeedingGrowthCell! = tableView.dequeueReusableCell(withIdentifier: "FeedingGrowthCell") as? FeedingGrowthCell

                        if (cell == nil) {
                            cell = FeedingGrowthCell.init(style: UITableViewCell.CellStyle.default,
                                                          reuseIdentifier:"FeedingGrowthCell");
                        }

                        cell.selectionStyle = UITableViewCell.SelectionStyle.none

                        if obj.isActive == 0 {
                            cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                            cell.lablTimeAgo.text = ""
                        } else {
                            cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                            cell.lablTimeAgo.text = obj.newDifferDate
                        }
                        cell.lablUser.text = obj.userName
                        cell.objFeed = obj.objFeeding
                        cell.lablDetail.text = obj.notes
                        AppUtility.loadImage(imageView: cell.imgUSer, urlString: obj.userImage, placeHolderImageString: "bb")
                        
                        cell.btnEdit.tag = indexPath.row
                        cell.btnDelete.tag = indexPath.row
                        cell.btnDelete.addTarget(self, action: #selector(self.deleteCategoryTapped(_:)), for: .touchUpInside)
                        cell.btnEdit.addTarget(self, action: #selector(self.editCategoryTapped(_:)), for: .touchUpInside)
                        
                        cell.collectionView.reloadData()

                        return cell
                    }
                }
            } else if obj.type == DIARYTYPE.teething.rawValue {
                if obj.objTeething.mediaType == "audio" {
                    tableView.register(UINib(nibName: "TeethGrowthAudioCell", bundle: nil), forCellReuseIdentifier: "TeethGrowthAudioCell")
                    var cell : TeethGrowthAudioCell! = tableView.dequeueReusableCell(withIdentifier: "TeethGrowthAudioCell") as? TeethGrowthAudioCell

                    if (cell == nil) {
                        cell = TeethGrowthAudioCell.init(style: UITableViewCell.CellStyle.default,
                                                      reuseIdentifier:"TeethGrowthAudioCell");
                    }

                    cell.selectionStyle = UITableViewCell.SelectionStyle.none
                    
                    if obj.isActive == 0 {
                        cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                        cell.lablTimeAgo.text = ""
                    } else {
                        cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                        cell.lablTimeAgo.text = obj.newDifferDate
                    }
                    cell.lablUser.text = obj.userName
                    cell.lablDetail.text = obj.objTeething.name
                    cell.objTeeth = obj.objTeething
                    AppUtility.loadImage(imageView: cell.imgUSer, urlString: obj.userImage, placeHolderImageString: "bb")
                    cell.collectionView.reloadData()

//                    let item = AVPlayerItem(url: URL(string: obj.objTeething.media)!)
//                    let duration = Double(item.asset.duration.value) / Double(item.asset.duration.timescale)
//
//                    //let currentTime1 = Int((player?.currentTime)!)
//                    let minutes = duration/60
//                    let seconds = Int(duration) - Int(minutes) * 60
//                    cell.lablAudioTime.text = NSString(format: "%02d:%02d", minutes,seconds) as String
                    
                    cell.progressAudio.setProgress(Float(0), animated: false)
                    cell.lablAudioTime.text = obj.objTeething.mediaDuration

                    if !obj.objTeething.isPlay {

                        obj.objTeething.isPlay = false
                        if player != nil
                        {
                            player.pause()
                        }
                        cell.btnPlayAudio.isSelected = false

                    } else {
                        cell.btnPlayAudio.isSelected = true
                        obj.objTeething.isPlay = true
                        player = AVPlayer()
                        if player != nil
                        {
                            player.pause()
                        }

                        let url = URL(string: obj.objTeething.media)
                        let playerItem = CachingPlayerItem(url: url!)
                        NotificationCenter.default.addObserver(self, selector: #selector(self.playerDidFinishPlaying(sender:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
                        playerItem.delegate = self
                        player = AVPlayer(playerItem: playerItem)
                        player.automaticallyWaitsToMinimizeStalling = true
                        player.play()

//                        player!.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(1, preferredTimescale: 1), queue: DispatchQueue.main) { (CMTime) -> Void in
//                            if self.player!.currentItem?.status == .readyToPlay {
//                                let time : Float64 = CMTimeGetSeconds(self.player!.currentTime());
//                                let duration = Int(time)
//                                let minutes2 = duration/60
//                                let seconds2 = duration - minutes2 * 60
//                                cell.lablAudioTime.text = NSString(format: "%02d:%02d", minutes2,seconds2) as String
//
//                                guard let currentTimes = self.player?.currentTime, let durations = self.player.currentItem?.asset.duration else {
//                                      return
//                                    }
//                                let seconds = CMTimeGetSeconds(durations)
//                                let secondss = CMTimeGetSeconds(currentTimes())
//                                let ff:Float = Float(secondss)
//                                let pf:Float = Float(seconds)
//                                let percentCompleted = ff/pf
//                                cell.progressAudio.setProgress(Float(percentCompleted), animated: true)
//                            }
//
//                            let playbackLikelyToKeepUp = self.player?.currentItem?.isPlaybackLikelyToKeepUp
//                            if playbackLikelyToKeepUp == false {
//                                print("IsBuffering")
//    //                            self.ButtonPlay.isHidden = true
//    //                            self.loadingView.isHidden = false
//                            } else {
//                                //stop the activity indicator
//    //                            print("Buffering completed")
//    //                            self.ButtonPlay.isHidden = false
//    //                            self.loadingView.isHidden = true
//                            }
//
//                        }

                    }

                    cell.btnPlayAudio.tag = indexPath.row
                    cell.progressAudio.tag = indexPath.row
                    cell.btnEdit.tag = indexPath.row
                    cell.btnDelete.tag = indexPath.row
                    cell.btnDelete.addTarget(self, action: #selector(self.deleteCategoryTapped(_:)), for: .touchUpInside)
                    cell.btnEdit.addTarget(self, action: #selector(self.editCategoryTapped(_:)), for: .touchUpInside)
                    cell.btnPlayAudio.addTarget(self, action: #selector(self.playAudioTapped(_:)), for: .touchUpInside)

                    return cell
                    
                } else {
                    
                    tableView.register(UINib(nibName: "TeethGrowthCell", bundle: nil), forCellReuseIdentifier: "TeethGrowthCell")
                    var cell : TeethGrowthCell! = tableView.dequeueReusableCell(withIdentifier: "TeethGrowthCell") as? TeethGrowthCell

                    if (cell == nil) {
                        cell = TeethGrowthCell.init(style: UITableViewCell.CellStyle.default,
                                                      reuseIdentifier:"TeethGrowthCell");
                    }

                    cell.selectionStyle = UITableViewCell.SelectionStyle.none
                    
                    if obj.isActive == 0 {
                        cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                        cell.lablTimeAgo.text = ""
                    } else {
                        cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                        cell.lablTimeAgo.text = obj.newDifferDate
                    }
                    cell.lablUser.text = obj.userName
                    cell.lablDetail.text = obj.objTeething.name
                    cell.objTeeth = obj.objTeething
                    if obj.objTeething.media.count == 0 {
                        cell.layoutHeightImage.constant = 0
                    } else {
                        cell.layoutHeightImage.constant = 171
                    }
                    AppUtility.loadImage(imageView: cell.imgItem, urlString: obj.objTeething.media, placeHolderImageString: "bb")
                    AppUtility.loadImage(imageView: cell.imgUSer, urlString: obj.userImage, placeHolderImageString: "bb")
                    
                    cell.btnEdit.tag = indexPath.row
                    cell.btnDelete.tag = indexPath.row
                    cell.btnDelete.addTarget(self, action: #selector(self.deleteCategoryTapped(_:)), for: .touchUpInside)
                    cell.btnEdit.addTarget(self, action: #selector(self.editCategoryTapped(_:)), for: .touchUpInside)
                    
                    cell.collectionView.reloadData()

                    return cell
                }
            } else if obj.type == DIARYTYPE.activity.rawValue {
                if obj.isActive == 1 {
                    
                    if obj.objActivity.mediaType == "audio" {
                        tableView.register(UINib(nibName: "ActivityGrowthActiveAudioCell", bundle: nil), forCellReuseIdentifier: "ActivityGrowthActiveAudioCell")
                        var cell : ActivityGrowthActiveAudioCell! = tableView.dequeueReusableCell(withIdentifier: "ActivityGrowthActiveAudioCell") as? ActivityGrowthActiveAudioCell

                        if (cell == nil) {
                            cell = ActivityGrowthActiveAudioCell.init(style: UITableViewCell.CellStyle.default,
                                                          reuseIdentifier:"ActivityGrowthActiveAudioCell");
                        }

                        cell.selectionStyle = UITableViewCell.SelectionStyle.none
                        
                        cell.objActivity = obj.objActivity
                        cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                        cell.lablTimeAgo.text = obj.newDifferDate
                        cell.lablUser.text = obj.userName
                        cell.lablDetail.text = obj.notes
                        
                        cell.objActivity = obj.objActivity
                        AppUtility.loadImage(imageView: cell.imgUSer, urlString: obj.userImage, placeHolderImageString: "bb")

                        cell.progressAudio.setProgress(Float(0), animated: false)
                        cell.lablAudioTime.text = obj.objActivity.mediaDuration

                        if !obj.objActivity.isPlay {

                            obj.objActivity.isPlay = false
                            if player != nil
                            {
                                player.pause()
                            }
                            cell.btnPlayAudio.isSelected = false

                        } else {
                            cell.btnPlayAudio.isSelected = true
                            obj.objActivity.isPlay = true
                            player = AVPlayer()
                            if player != nil
                            {
                                player.pause()
                            }

                            let url = URL(string: obj.objActivity.media)
                            let playerItem = CachingPlayerItem(url: url!)
                            NotificationCenter.default.addObserver(self, selector: #selector(self.playerDidFinishPlaying(sender:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
                            playerItem.delegate = self
                            player = AVPlayer(playerItem: playerItem)
                            player.automaticallyWaitsToMinimizeStalling = true
                            player.play()

                        }

                        cell.setData()
                        
                        cell.btnPlayAudio.tag = indexPath.row
                        cell.progressAudio.tag = indexPath.row
                        cell.btnEdit.tag = indexPath.row
                        cell.btnDelete.tag = indexPath.row
                        cell.btnDone.tag = indexPath.row
                        cell.btnDone.addTarget(self, action: #selector(self.doneDiaryTapped(_:)), for: .touchUpInside)
                        cell.btnDelete.addTarget(self, action: #selector(self.deleteCategoryTapped(_:)), for: .touchUpInside)
                        cell.btnEdit.addTarget(self, action: #selector(self.editCategoryTapped(_:)), for: .touchUpInside)
                        cell.btnPlayAudio.addTarget(self, action: #selector(self.playAudioTapped(_:)), for: .touchUpInside)

                        return cell
                        
                    } else {
                        
                        tableView.register(UINib(nibName: "ActivityGrowthActiveCell", bundle: nil), forCellReuseIdentifier: "ActivityGrowthActiveCell")
                        var cell : ActivityGrowthActiveCell! = tableView.dequeueReusableCell(withIdentifier: "ActivityGrowthActiveCell") as? ActivityGrowthActiveCell

                        if (cell == nil) {
                            cell = ActivityGrowthActiveCell.init(style: UITableViewCell.CellStyle.default,
                                                          reuseIdentifier:"ActivityGrowthActiveCell");
                        }

                        cell.selectionStyle = UITableViewCell.SelectionStyle.none
                       
                        cell.objActivity = obj.objActivity
                        cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                        cell.lablTimeAgo.text = obj.newDifferDate
                        cell.lablUser.text = obj.userName
                        if obj.objActivity.media.count == 0 {
                            cell.layoutHeightImage.constant = 0
                        } else {
                            cell.layoutHeightImage.constant = 171
                        }
                        
                        cell.lablDetail.text = obj.notes
                        AppUtility.loadImage(imageView: cell.imgItem, urlString: obj.objActivity.media, placeHolderImageString: "bb")
                        AppUtility.loadImage(imageView: cell.imgUSer, urlString: obj.userImage, placeHolderImageString: "bb")
                        
                        cell.setData()
                        
                        cell.btnEdit.tag = indexPath.row
                        cell.btnDelete.tag = indexPath.row
                        cell.btnDone.tag = indexPath.row
                        cell.btnDone.addTarget(self, action: #selector(self.doneDiaryTapped(_:)), for: .touchUpInside)
                        cell.btnDelete.addTarget(self, action: #selector(self.deleteCategoryTapped(_:)), for: .touchUpInside)
                        cell.btnEdit.addTarget(self, action: #selector(self.editCategoryTapped(_:)), for: .touchUpInside)

                        return cell
                    }
                    
                } else {
                    if obj.objActivity.mediaType == "audio" {
                        tableView.register(UINib(nibName: "GrowthAudioCell", bundle: nil), forCellReuseIdentifier: "GrowthAudioCell")
                        var cell : GrowthAudioCell! = tableView.dequeueReusableCell(withIdentifier: "GrowthAudioCell") as? GrowthAudioCell

                        if (cell == nil) {
                            cell = GrowthAudioCell.init(style: UITableViewCell.CellStyle.default,
                                                          reuseIdentifier:"GrowthAudioCell");
                        }

                        cell.selectionStyle = UITableViewCell.SelectionStyle.none
                        cell.lablCatName.text = "Activity".localized()
                        cell.imgCat.image = UIImage(named: "activity")
                        cell.layoutHeightCollection.constant = 24

                        if obj.isActive == 0 {
                            cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                            cell.lablTimeAgo.text = ""
                        } else {
                            cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                            cell.lablTimeAgo.text = obj.newDifferDate
                        }
                        cell.lablDetail.text = obj.notes
                        
                        cell.lablUser.text = obj.userName
                        cell.objActivity = obj.objActivity
                        cell.isDiaper = false
                        cell.isNotes = false
                        cell.isActivity = true
                        cell.lablDetail.text = ""
                        AppUtility.loadImage(imageView: cell.imgUSer, urlString: obj.userImage, placeHolderImageString: "bb")
                        cell.collectionView.reloadData()

                        cell.progressAudio.setProgress(Float(0), animated: false)
                        cell.lablAudioTime.text = obj.objActivity.mediaDuration

                        if !obj.objActivity.isPlay {

                            obj.objActivity.isPlay = false
                            if player != nil
                            {
                                player.pause()
                            }
                            cell.btnPlayAudio.isSelected = false

                        } else {
                            cell.btnPlayAudio.isSelected = true
                            obj.objActivity.isPlay = true
                            player = AVPlayer()
                            if player != nil
                            {
                                player.pause()
                            }

                            let url = URL(string: obj.objActivity.media)
                            let playerItem = CachingPlayerItem(url: url!)
                            NotificationCenter.default.addObserver(self, selector: #selector(self.playerDidFinishPlaying(sender:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
                            playerItem.delegate = self
                            player = AVPlayer(playerItem: playerItem)
                            player.automaticallyWaitsToMinimizeStalling = true
                            player.play()

                        }

                        cell.btnPlayAudio.tag = indexPath.row
                        cell.progressAudio.tag = indexPath.row
                        cell.btnEdit.tag = indexPath.row
                        cell.btnDelete.tag = indexPath.row
                        cell.btnDelete.addTarget(self, action: #selector(self.deleteCategoryTapped(_:)), for: .touchUpInside)
                        cell.btnEdit.addTarget(self, action: #selector(self.editCategoryTapped(_:)), for: .touchUpInside)
                        cell.btnPlayAudio.addTarget(self, action: #selector(self.playAudioTapped(_:)), for: .touchUpInside)

                        return cell
                        
                    } else {
                        
                        tableView.register(UINib(nibName: "GrowthCell", bundle: nil), forCellReuseIdentifier: "GrowthCell")
                        var cell : GrowthCell! = tableView.dequeueReusableCell(withIdentifier: "GrowthCell") as? GrowthCell

                        if (cell == nil) {
                            cell = GrowthCell.init(style: UITableViewCell.CellStyle.default,
                                                          reuseIdentifier:"GrowthCell");
                        }

                        cell.selectionStyle = UITableViewCell.SelectionStyle.none
                        
                        cell.lablCatName.text = "Activity".localized()
                        cell.imgCat.image = UIImage(named: "activity")
                        cell.layoutHeightCollection.constant = 24

                        if obj.isActive == 0 {
                            cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                            cell.lablTimeAgo.text = ""
                        } else {
                            cell.lablTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdDate)
                            cell.lablTimeAgo.text = obj.newDifferDate
                        }
                        cell.lablUser.text = obj.userName
                        cell.objActivity = obj.objActivity
                        cell.isDiaper = false
                        cell.isNotes = false
                        cell.isActivity = true
                        cell.lablDetail.text = obj.notes
                        if obj.objActivity.media.count == 0 {
                            cell.layoutHeightImage.constant = 0
                        } else {
                            cell.layoutHeightImage.constant = 171
                        }
                        AppUtility.loadImage(imageView: cell.imgItem, urlString: obj.objActivity.media, placeHolderImageString: "bb")
                        AppUtility.loadImage(imageView: cell.imgUSer, urlString: obj.userImage, placeHolderImageString: "bb")
                        
                        cell.btnEdit.tag = indexPath.row
                        cell.btnDelete.tag = indexPath.row
                        cell.btnDelete.addTarget(self, action: #selector(self.deleteCategoryTapped(_:)), for: .touchUpInside)
                        cell.btnEdit.addTarget(self, action: #selector(self.editCategoryTapped(_:)), for: .touchUpInside)
                        
                        cell.collectionView.reloadData()

                        return cell
                    }
                }
            } else {
                return UITableViewCell()
            }
            
        } else {
            
            tableView.register(UINib(nibName: "BabyCell", bundle: nil), forCellReuseIdentifier: "BabyCell")
            var cell : BabyCell! = tableView.dequeueReusableCell(withIdentifier: "BabyCell") as? BabyCell
            
            if (cell == nil) {
                cell = BabyCell.init(style: UITableViewCell.CellStyle.default,
                                              reuseIdentifier:"BabyCell");
            }
            
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            
            let objG = AppManager.shared.arrBabies[indexPath.row]
            cell.lablBabyName.text = objG.babyName
            cell.lablYear.text = objG.age
            AppUtility.loadImage(imageView: cell.imgBaby, urlString: objG.babyImage, placeHolderImageString: "bb")
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tableBaby {
            return 50
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tableBaby {
            self.tableBaby.isHidden = true
            AppUtility.saveBabyId(babyID: AppManager.shared.arrBabies[indexPath.row].babyID)
            
            self.getDiary(date: diaryDate)
            
            //self.getGrowth(babyId: AppManager.shared.arrBabies[indexPath.row].babyID)
            
            AppUtility.loadImage(imageView: self.imgBaby, urlString: AppManager.shared.arrBabies[indexPath.row].babyImage, placeHolderImageString: "bb")
            self.lablBabyName.text = AppManager.shared.arrBabies[indexPath.row].babyName
            self.lablYear.text = AppManager.shared.arrBabies[indexPath.row].age
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0
        cell.layer.transform = CATransform3DMakeScale(0.5, 0.5, 0.5)
        UIView.animate(withDuration: 0.5) {
            cell.alpha = 1
            cell.layer.transform = CATransform3DScale(CATransform3DIdentity, 1, 1, 1)
        }
    }
}

extension DiaryVC:AddActivityVCDelegate {
    func addGrowthNew() {
        //self.getGrowth(babyId: AppUtility.getBabyId())
        self.getDiary(date: diaryDate)
    }
}

//MARK:- Audio delegate
extension DiaryVC:CachingPlayerItemDelegate {
    func playerItem(_ playerItem: CachingPlayerItem, didFinishDownloadingData data: Data)
    {
        print("File is downloaded and ready for storing")
        
        player.play()
    }
    
    func playerItem(_ playerItem: CachingPlayerItem, didDownloadBytesSoFar bytesDownloaded: Int, outOf bytesExpected: Int)
    {
        print("\(bytesDownloaded)/\(bytesExpected)")
        player.play()
    }
    
    func playerItemPlaybackStalled(_ playerItem: CachingPlayerItem)
    {
        print("Not enough data for playback. Probably because of the poor network. Wait a bit and try to play later.")
    }
    
    func playerItem(_ playerItem: CachingPlayerItem, downloadingFailedWith error: Error)
    {
        print(error)
    }
}

extension DiaryVC: AddSleepVCDelegate {
    func addSleep() {
        self.getDiary(date: self.diaryDate)
    }
}

extension DiaryVC: AddAppointmentVCDelegate {
    func addAppointment() {
        self.getDiary(date: self.diaryDate)
    }
}

extension DiaryVC: AddDiaperVCDelegate {
    func addDiaper() {
        self.getDiary(date: self.diaryDate)
    }
}

extension DiaryVC: AddPumpingVCDelegate {
    func addPumping() {
        self.getDiary(date: self.diaryDate)
    }
}

extension DiaryVC: AddActivityNewVCDelegate {
    func addActivity() {
        self.getDiary(date: self.diaryDate)
    }
}

extension DiaryVC: AddFoodBottleVCDelegate {
    func addFood() {
        self.getDiary(date: self.diaryDate)
    }
}

extension DiaryVC: AddMedicationsVCDelegate {
    func addMedication() {
        self.getDiary(date: self.diaryDate)
    }
}

extension DiaryVC: AddTeetchVCDelegate {
    func addTeeth() {
        self.getDiary(date: self.diaryDate)
    }
}

extension DiaryVC: AddTempretaureVcDelegate {
    func addTemprature() {
        self.getDiary(date: self.diaryDate)
    }
}

extension DiaryVC: AddVacineVCDelegate {
    func addVaccine() {
        self.getDiary(date: self.diaryDate)
    }
}

extension DiaryVC: AddNotesVCDelegate {
    func addNotes() {
        self.getDiary(date: self.diaryDate)
    }
}

extension DiaryVC: AddGrowthVCDelegate {
    func addGrowth() {
        self.getDiary(date: self.diaryDate)
    }
}
