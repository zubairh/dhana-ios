//
//  AddSleepVC.swift
//  Tifli
//
//  Created by zubair on 24/10/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import SwiftyJSON

protocol AddSleepVCDelegate {
    func addSleep()
}

class AddSleepVC: UIViewController {
    
    @IBOutlet weak var tfDate: UITextField!
    @IBOutlet weak var tfTime: UITextField!
    @IBOutlet weak var tfStartTime: UITextField!
    @IBOutlet weak var tfEndTime: UITextField!
    @IBOutlet weak var viewBg: UIImageView!
    @IBOutlet weak var tableSelectBaby: UITableView!
    @IBOutlet weak var layoutTableBaby: NSLayoutConstraint!
    @IBOutlet weak var tfNotes: UITextField!
    @IBOutlet weak var lablSetTimer: UILabel!
    @IBOutlet weak var lablSetManual: UILabel!
    @IBOutlet weak var btnPlayPause: UIButton!
    @IBOutlet weak var lablTimer: UILabel!
    @IBOutlet weak var viewTimer: UIView!
    @IBOutlet weak var btnManual: UIButton!
    @IBOutlet weak var lablTitle: UILabel!
    @IBOutlet weak var lablTimerTitle: UILabel!
    @IBOutlet weak var lablBaby: UILabel!
    @IBOutlet weak var lablNotes: UILabel!
    @IBOutlet weak var lablThisPart: UILabel!
    @IBOutlet weak var lablSetReminder: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnAdd: GradientButton!
    @IBOutlet weak var switchDaily: UISwitch!
    @IBOutlet weak var switchSetReminder: UISwitch!
    
    var selectedIndex = -1
    var delegate:AddSleepVCDelegate? = nil
    var babyID = ""
    var timerSleep = Timer()
    var totalSecond:Int = 0
    var objDiary = Diary()
    var isFromEdit = false
    var addToDailyRoutine = "1"
    var setReminder = "0"

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isFromEdit {
            self.viewBg.backgroundColor = #colorLiteral(red: 0.9137254902, green: 0.9098039216, blue: 0.9490196078, alpha: 1)
            let dd = objDiary.date.components(separatedBy: " ")
            self.tfDate.text = dd[0]
            self.tfTime.text = dd[1]
            self.tfNotes.text = objDiary.notes
            if objDiary.objSleep.startTime == "00:00:00" && objDiary.objSleep.endTime == "00:00:00" {
                self.setManualTapped(btnManual)
                self.lablTimer.text = objDiary.objSleep.duration
            } else {
                self.tfStartTime.text = objDiary.objSleep.startTime
                self.tfEndTime.text = objDiary.objSleep.endTime
            }
            if self.objDiary.objSleep.setReminder == 1 {
                switchSetReminder.isOn = true
                setReminder = "1"
            } else {
                switchSetReminder.isOn = false
                setReminder = "0"
            }
            if self.objDiary.objSleep.dailyRoutineIsActive == 1 {
                switchDaily.isOn = true
                addToDailyRoutine = "1"
            } else {
                switchDaily.isOn = false
                addToDailyRoutine = "0"
            }
            for i in 0 ..< AppManager.shared.arrBabies.count {
                let obj = AppManager.shared.arrBabies[i]
                if AppUtility.getBabyId() == obj.babyID {
                    selectedIndex = i
                    babyID = obj.babyID
                    tableSelectBaby.reloadData()
                    break
                }
            }
        }
        
        tableSelectBaby.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
        
        if appSceneDelegate.isArabic {
            tfDate.textAlignment = .right
            tfTime.textAlignment = .right
            tfNotes.textAlignment = .right
            tfStartTime.textAlignment = .right
            tfEndTime.textAlignment = .right
        }
        lablTitle.text = lablTitle.text?.localized()
        lablThisPart.text = lablThisPart.text?.localized()
        lablSetReminder.text = lablSetReminder.text?.localized()
        lablNotes.text = lablNotes.text?.localized()
        lablBaby.text = lablBaby.text?.localized()
        lablTimer.text = lablTimer.text?.localized()
        lablSetTimer.text = lablSetTimer.text?.localized()
        lablTimerTitle.text = lablTimerTitle.text?.localized()
    
        btnAdd.setTitle(self.btnAdd.titleLabel?.text?.localized(), for: .normal)
        btnCancel.setTitle(self.btnCancel.titleLabel?.text?.localized(), for: .normal)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let blurEffect = UIBlurEffect(style: .light)
        let blurredEffectView = UIVisualEffectView(effect: blurEffect)
        blurredEffectView.frame = self.view.bounds
        self.viewBg.addSubview(blurredEffectView)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        layoutTableBaby.constant = tableSelectBaby.contentSize.height
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    func CheckFields() -> Bool {
        if (tfDate.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            
            AppUtility.showInfoMessage(message: "Please enter date".localized())
            let test = false
            return test
        }
        else if (tfTime.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            AppUtility.showInfoMessage(message: "Please enter time".localized())
            let test = false
            return test
        }
        else if (tfStartTime.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            AppUtility.showInfoMessage(message: "Please enter start time".localized())
            let test = false
            return test
        }
        else if (tfEndTime.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            AppUtility.showInfoMessage(message: "Please enter end time".localized())
            let test = false
            return test
        }
        else if babyID.count == 0 {
            AppUtility.showInfoMessage(message: "Please select baby".localized())
            let test = false
            return test
        }
        
        let test = true
        return test
    }
    
    func CheckFieldsTimer() -> Bool {
        if (tfDate.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            
            AppUtility.showInfoMessage(message: "Please enter date".localized())
            let test = false
            return test
        }
        else if (tfTime.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            AppUtility.showInfoMessage(message: "Please enter time".localized())
            let test = false
            return test
        }
        else if (lablTimer.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            AppUtility.showInfoMessage(message: "Please enter your manual time".localized())
            let test = false
            return test
        }
        else if babyID.count == 0 {
            AppUtility.showInfoMessage(message: "Please select baby".localized())
            let test = false
            return test
        }
        
        let test = true
        return test
    }
    
    @objc func addSleep() {
        self.view.endEditing(true)
        var paramDict: [String: Any]?
        var url = API.ADD_SLEEP
        if isFromEdit {
            url = API.ADD_SLEEP
            if self.viewTimer.isHidden == true {
                paramDict = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":self.babyID, "sleepLogDate":tfDate.text!+" "+tfTime.text!, "sleepLogNotes":tfNotes.text!, "sleepLogStartTime":tfStartTime.text!, "sleepLogEndTime":tfEndTime .text!, "sleepLogId":objDiary.objSleep.ID, "addToDailyRoutine":addToDailyRoutine, "setReminder":setReminder]
            } else {
                paramDict = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":self.babyID, "sleepLogDate":tfDate.text!+" "+tfTime.text!, "sleepLogNotes":tfNotes.text!, "sleepLogDuration":lablTimer.text!, "sleepLogId":objDiary.objSleep.ID, "addToDailyRoutine":addToDailyRoutine, "setReminder":setReminder]
            }
        } else {
            url = API.ADD_SLEEP
            if self.viewTimer.isHidden == true {
                paramDict = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":self.babyID, "sleepLogDate":tfDate.text!+" "+tfTime.text!, "sleepLogNotes":tfNotes.text!, "sleepLogStartTime":tfStartTime.text!, "sleepLogEndTime":tfEndTime .text!, "addToDailyRoutine":addToDailyRoutine, "setReminder":setReminder]
            } else {
                paramDict = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":self.babyID, "sleepLogDate":tfDate.text!+" "+tfTime.text!, "sleepLogNotes":tfNotes.text!, "sleepLogDuration":lablTimer.text!, "addToDailyRoutine":addToDailyRoutine, "setReminder":setReminder]
            }
        }
        APIRequestUtil.addSleep(url:url, parameters: paramDict!) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    AppUtility.showSuccessMessage(message: "Sleep added successfully".localized())
                    self.delegate?.addSleep()
                    self.cancelTapped(self)
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    @objc func sleepTimeUpdate() {
        var hours: Int
        var minutes: Int
        var seconds: Int

        totalSecond = totalSecond + 1
        hours = totalSecond / 3600
        minutes = (totalSecond % 3600) / 60
        seconds = (totalSecond % 3600) % 60
        lablTimer.text = String(format: "%02d:%02d:%02d", hours, minutes, seconds)
    }
    
    //MARK:- Button Action
    @IBAction func cancelTapped(_ sender: Any) {
        timerSleep.invalidate()
        self.navigationController?.popViewController(animated: false)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addTapped(_ sender: Any) {
        if self.viewTimer.isHidden == true {
            if CheckFields() {
                self.addSleep()
            }
        } else {
            if CheckFieldsTimer() {
                self.addSleep()
            }
        }
    }
    
    @IBAction func setManualTapped(_ sender: UIButton) {
        timerSleep.invalidate()
        if sender.tag == 0 {
            sender.tag = 1
            lablSetTimer.text = "Set by manual".localized()
            viewTimer.isHidden = false
            self.tfStartTime.text = ""
            self.tfEndTime.text = ""
            lablTimer.text = "00:00:00"
        } else {
            sender.tag = 0
            lablSetTimer.text = "Set by timer".localized()
            self.tfStartTime.text = ""
            self.tfEndTime.text = ""
            lablTimer.text = "00:00:00"
            viewTimer.isHidden = true
        }
    }
    
    @IBAction func playTimeTapped(_ sender: Any) {
        if btnPlayPause.isSelected {
            btnPlayPause.isSelected = false
            timerSleep.invalidate()
        } else {
            let backgroundTaskIdentifier =
                          UIApplication.shared.beginBackgroundTask(expirationHandler: nil)

                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                            self.timerSleep = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.sleepTimeUpdate), userInfo: nil, repeats: true)
                           
                            UIApplication.shared.endBackgroundTask(backgroundTaskIdentifier)
                        }
            btnPlayPause.isSelected = true
            
        }
    }
    
    @IBAction func setSwitchBabyRoutineTapped(_ sender: UISwitch) {
        if sender.isOn {
            addToDailyRoutine = "1"
        } else {
            addToDailyRoutine = "0"
        }
    }
    
    @IBAction func setReminderTapped(_ sender: UISwitch) {
        if sender.isOn {
            setReminder = "1"
        } else {
            setReminder = "0"
        }
    }
    
    @IBAction func dayTapped(_ sender: UIButton) {
        let datePicker = ActionSheetDatePicker(title: "Select Date".localized(), datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let date:NSDate = value as! NSDate
            
            self.tfDate.text = AppUtility.convertDateToString(date: date)
            
            return
            
        }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview!.superview)
        datePicker?.minimumDate = Date()
        datePicker?.show()
    }
    
    @IBAction func timeTapped(_ sender: UIButton) {
        let datePicker = ActionSheetDatePicker(title: "Select Time".localized(), datePickerMode: UIDatePicker.Mode.time, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let date:NSDate = value as! NSDate
            
            self.tfTime.text = AppUtility.convertDateToStringTime(date: date)
            
            return
            
        }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview!.superview)
        datePicker?.show()
    }
    
    @IBAction func startTimeTapped(_ sender: UIButton) {
        let datePicker = ActionSheetDatePicker(title: "Select Start Time".localized(), datePickerMode: UIDatePicker.Mode.dateAndTime, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let date:NSDate = value as! NSDate
            
//            self.tfStartTime.text = AppUtility.convertDateToStringTime(date: date)
            self.tfStartTime.text = AppUtility.convertDateToStringTimeSleep(date: date)
            
            return
            
        }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview!.superview)
        datePicker?.show()
    }
    
    @IBAction func endTimeTapped(_ sender: UIButton) {
        if tfStartTime.text?.count ?? 0 > 0 {
            let datePicker = ActionSheetDatePicker(title: "Select End Time".localized(), datePickerMode: UIDatePicker.Mode.dateAndTime, selectedDate: Date(), doneBlock: {
                picker, value, index in
                let date:NSDate = value as! NSDate
                
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

                let date1 = formatter.date(from: self.tfStartTime.text!)
                let date2 = formatter.date(from: AppUtility.convertDateToStringTimeSleep(date: date))

                var result: ComparisonResult? = nil
                if let date2 = date2 {
                    result = date1?.compare(date2)
                }
                if result == .orderedDescending {
                    print("date1 is later than date2")
                    AppUtility.showInfoMessage(message: "end time should be next time of start time".localized())
                    return
                } else if result == .orderedAscending {
                    self.tfEndTime.text = AppUtility.convertDateToStringTimeSleep(date: date)
                    print("date2 is later than date1")
                } else {
                    print("date1 is equal to date2")
                    AppUtility.showInfoMessage(message: "end time should be next time of start time".localized())
                    return
                }
                
                return
                
//                let formatter = DateFormatter()
//                formatter.dateFormat = "HH:mm"
//
//                let date1 = formatter.date(from: self.tfStartTime.text!)
//                let date2 = formatter.date(from: AppUtility.convertDateToStringTime(date: date))
//
//                var result: ComparisonResult? = nil
//                if let date2 = date2 {
//                    result = date1?.compare(date2)
//                }
//                if result == .orderedDescending {
//                    print("date1 is later than date2")
//                    AppUtility.showInfoMessage(message: "end time should be next time of start time".localized())
//                    return
//                } else if result == .orderedAscending {
//                    self.tfEndTime.text = AppUtility.convertDateToStringTime(date: date)
//                    print("date2 is later than date1")
//                } else {
//                    print("date1 is equal to date2")
//                    AppUtility.showInfoMessage(message: "end time should be next time of start time".localized())
//                    return
//                }
//
//                return
                
            }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview!.superview)
            datePicker?.show()
        }
        else {
            AppUtility.showInfoMessage(message: "Please select first start time".localized())
        }
    }
}

// MARK: - Tableview Delegate-
extension AddSleepVC : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AppManager.shared.arrBabies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        tableView.register(UINib(nibName: "BabyCaregiverCell", bundle: nil), forCellReuseIdentifier: "BabyCaregiverCell")
        var cell : BabyCaregiverCell! = tableView.dequeueReusableCell(withIdentifier: "BabyCaregiverCell") as? BabyCaregiverCell
        
        if (cell == nil) {
            cell = BabyCaregiverCell.init(style: UITableViewCell.CellStyle.default,
                                          reuseIdentifier:"BabyCaregiverCell");
        }
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        let obj = AppManager.shared.arrBabies[indexPath.row]
        cell.lablTitle.text = obj.babyName
        
        if indexPath.row == selectedIndex {
            cell.imgCheck.image = UIImage(named: "checbox fill")
        } else {
            cell.imgCheck.image = UIImage(named: "checbox")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        self.babyID = AppManager.shared.arrBabies[indexPath.row].babyID
        tableView.reloadData()
    }
}
