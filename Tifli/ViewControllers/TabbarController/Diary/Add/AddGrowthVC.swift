//
//  AddGrowthVC.swift
//  Tifli
//
//  Created by zubair on 25/07/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol AddGrowthVCDelegate {
    func addGrowth()
}

class AddGrowthVC: UIViewController {

    @IBOutlet weak var viewBg: UIImageView!
    @IBOutlet weak var tfHeight: UITextField!
    @IBOutlet weak var tfWeight: UITextField!
    @IBOutlet weak var tfHeadCircle: UITextField!
    @IBOutlet weak var tfNotes: UITextField!
    @IBOutlet weak var lablTitle: UILabel!
    @IBOutlet weak var lablHeight: UILabel!
    @IBOutlet weak var lablWeight: UILabel!
    @IBOutlet weak var lablHeadCircumtnc: UILabel!
    @IBOutlet weak var lablNotes: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnAdd: GradientButton!
    
    var delegate:AddGrowthVCDelegate? = nil
    var isfromGraph = false
    var media = UIImage()
    var isAudio = false
    var audioDatas = Data()
    var objDiary = Diary()
    var isFromEdit = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isFromEdit {
            self.viewBg.backgroundColor = #colorLiteral(red: 0.9137254902, green: 0.9098039216, blue: 0.9490196078, alpha: 1)
            self.tfNotes.text = objDiary.objGrowth.notes
            self.tfHeight.text = objDiary.objGrowth.height
            self.tfWeight.text = objDiary.objGrowth.weight
            self.tfHeadCircle.text = objDiary.objGrowth.circle
            
        }
        if appSceneDelegate.isArabic {
            tfHeight.textAlignment = .right
            tfWeight.textAlignment = .right
            tfNotes.textAlignment = .right
            tfHeadCircle.textAlignment = .right
        }
        lablTitle.text = lablTitle.text?.localized()
        lablHeight.text = lablHeight.text?.localized()
        lablWeight.text = lablWeight.text?.localized()
        lablNotes.text = lablNotes.text?.localized()
        lablHeadCircumtnc.text = lablHeadCircumtnc.text?.localized()
        
        btnAdd.setTitle(self.btnAdd.titleLabel?.text?.localized(), for: .normal)
        btnCancel.setTitle(self.btnCancel.titleLabel?.text?.localized(), for: .normal)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let blurEffect = UIBlurEffect(style: .light)
        let blurredEffectView = UIVisualEffectView(effect: blurEffect)
        blurredEffectView.frame = self.view.bounds
        self.viewBg.addSubview(blurredEffectView)
    }
    
    func CheckFields() -> Bool {
        if (tfHeight.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            
            AppUtility.showInfoMessage(message: "Please enter height".localized())
            let test = false
            return test
        }
        else if (tfWeight.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            AppUtility.showInfoMessage(message: "Please enter weight".localized())
            let test = false
            return test
        }
        else if (tfHeadCircle.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            AppUtility.showInfoMessage(message: "Please enter head circle".localized())
            let test = false
            return test
        }
//        else if (tfNotes.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
//            AppUtility.showInfoMessage(message: "Please enter notes")
//            let test = false
//            return test
//        }
        
        let test = true
        return test
    }
    
    //MARK:- Apis Functions
    func addGrowth() {
        self.view.endEditing(true)
        var paramDict: [String: Any]?
        if self.isFromEdit {
            paramDict = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":AppUtility.getBabyId(), "height":tfHeight.text!, "weight":tfWeight.text!, "headCircle":tfHeadCircle.text!, "description":tfNotes.text!, "id":objDiary.objGrowth.growthID]
        } else {
            paramDict = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":AppUtility.getBabyId(), "height":tfHeight.text!, "weight":tfWeight.text!, "headCircle":tfHeadCircle.text!, "description":tfNotes.text!]
        }
        if isAudio {
            APIRequestUtil.addGrowth(parameters: paramDict!, data: audioDatas , extensionString: "audio/m4a", fileNameKey: "growthChartMedia", isEdit: self.isFromEdit) { (result, error) in
                if(result != nil) {
                    let swiftyJsonVar = JSON(result!)

                    let dict = swiftyJsonVar.dictionaryValue
                    let status = dict["status"]?.intValue
                    let message = dict["message"]?.string
                    
                    if status == SUCESS_CODE {
                        
                        AppUtility.showSuccessMessage(message: message ?? "Add growth data successfully!".localized())
                        self.delegate?.addGrowth()
                        self.cancelTapped(self)
                        
                    } else {
                        
                        AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                        return
                    }
                }
            }
        } else {
            let data = media.jpegData(compressionQuality: 0.5)
            APIRequestUtil.addGrowth(parameters: paramDict!, data: data ?? Data(), extensionString: "image/jpeg", fileNameKey: "growthChartMedia", isEdit: self.isFromEdit) { (result, error) in
                if(result != nil) {
                    let swiftyJsonVar = JSON(result!)

                    let dict = swiftyJsonVar.dictionaryValue
                    let status = dict["status"]?.intValue
                    let message = dict["message"]?.string
                    
                    if status == SUCESS_CODE {
                        
                        AppUtility.showSuccessMessage(message: message ?? "Add growth data successfully!".localized())
                        self.delegate?.addGrowth()
                        self.cancelTapped(self)
                        
                    } else {
                        
                        AppUtility.showInfoMessage(message: message ?? SERVER_ERROR)
                        return
                    }
                }
            }
        }
    }

    //MARK:- Button Action
    @IBAction func cancelTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func addTapped(_ sender: Any) {
        if CheckFields() {
            self.addGrowth()
        }
    }
    
    @IBAction func addHeightTapped(_ sender: Any) {
        var height = 0
        if tfHeight.text?.count ?? 0 > 0 {
            height = Int(tfHeight.text ?? "0") ?? 0
            height = height+1
            tfHeight.text = String(height)
        } else {
            height = height+1
            tfHeight.text = String(height)
        }
    }
    
    @IBAction func addWeightTapped(_ sender: Any) {
        var weight = 0
        if tfWeight.text?.count ?? 0 > 0 {
            weight = Int(tfWeight.text ?? "0") ?? 0
            weight = weight+1
            tfWeight.text = String(weight)
        } else {
            weight = weight+1
            tfWeight.text = String(weight)
        }
    }
    
    @IBAction func minusWeightTapped(_ sender: Any) {
        var weight = 0
        if tfWeight.text?.count ?? 0 > 0 {
            weight = Int(tfWeight.text ?? "0") ?? 0
            if weight >= 1 {
                weight = weight-1
            }
            tfWeight.text = String(weight)
        }
    }
    
    @IBAction func addHeadCircle(_ sender: Any) {
        var headCircle = 0
        if tfHeadCircle.text?.count ?? 0 > 0 {
            headCircle = Int(tfHeadCircle.text ?? "0") ?? 0
            headCircle = headCircle+1
            tfHeadCircle.text = String(headCircle)
        } else {
            headCircle = headCircle+1
            tfHeadCircle.text = String(headCircle)
        }
    }
    
    @IBAction func uploadMediaTapped(_ sender: UIButton) {
        if sender.tag == 0 {
            ImagePickerManager().pickImage(self) { image in
                self.isAudio = false
                self.media = image
            }
        } else {
            let viewController = AddAudioVC(nibName:String(describing: AddAudioVC.self), bundle:nil)
            viewController.delegate = self
            self.navigationController?.pushViewController(viewController, animated: false)
        }
    }
}

extension AddGrowthVC:AddAudioVCDelegate {
    func addAudio(audioData: Data, index: Int) {
        isAudio = true
        audioDatas = audioData
    }
}
