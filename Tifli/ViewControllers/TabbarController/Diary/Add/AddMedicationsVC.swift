//
//  AddMedicationsVC.swift
//  Tifli
//
//  Created by zubair on 25/10/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import SwiftyJSON

protocol AddMedicationsVCDelegate {
    func addMedication()
}

class AddMedicationsVC: UIViewController {

    @IBOutlet weak var tfDate: UITextField!
    @IBOutlet weak var tfTime: UITextField!
    @IBOutlet weak var tfMedical: UITextField!
    @IBOutlet weak var tfAmount: UITextField!
    @IBOutlet weak var tfNotes: UITextField!
    @IBOutlet weak var viewBg: UIImageView!
    @IBOutlet weak var tableSelectBaby: UITableView!
    @IBOutlet weak var layoutTableBaby: NSLayoutConstraint!
    @IBOutlet weak var btnMl: UIButton!
    @IBOutlet weak var btnDrops: UIButton!
    
    @IBOutlet weak var lablTitle: UILabel!
    @IBOutlet weak var lablMedical: UILabel!
    @IBOutlet weak var lablBaby: UILabel!
    @IBOutlet weak var lablNotes: UILabel!
    @IBOutlet weak var lablThisPart: UILabel!
    @IBOutlet weak var lablSetReminder: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnAdd: GradientButton!
    @IBOutlet weak var lablAmount: UILabel!
    @IBOutlet weak var switchDaily: UISwitch!
    @IBOutlet weak var switchSetReminder: UISwitch!
    
    
    var selectedIndex = -1
    var babyID = ""
    var medicationAmountType = "ml"
    var arrMedicationList = [BabyType]()
    var delegate:AddMedicationsVCDelegate? = nil
    var media = UIImage()
    var isAudio = false
    var audioDatas = Data()
    var objDiary = Diary()
    var isFromEdit = false
    var addToDailyRoutine = "1"
    var setReminder = "0"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isFromEdit {
            self.viewBg.backgroundColor = #colorLiteral(red: 0.9137254902, green: 0.9098039216, blue: 0.9490196078, alpha: 1)
            let dd = objDiary.date.components(separatedBy: " ")
            self.tfDate.text = dd[0]
            self.tfTime.text = dd[1]
            self.tfNotes.text = objDiary.notes
            self.tfMedical.text = objDiary.objMedication.titles
            self.tfMedical.tag = Int(objDiary.objMedication.medicationId) ?? 0
            self.tfAmount.text = objDiary.objMedication.amount
            if objDiary.objMedication.amountType == "drops" {
                self.dropTapped(btnDrops)
            } else {
                self.mlTapped(btnMl)
            }
            
            if self.objDiary.objMedication.setReminder == 1 {
                switchSetReminder.isOn = true
                setReminder = "1"
            } else {
                switchSetReminder.isOn = false
                setReminder = "0"
            }
            if self.objDiary.objMedication.dailyRoutineIsActive == 1 {
                switchDaily.isOn = true
                addToDailyRoutine = "1"
            } else {
                switchDaily.isOn = false
                addToDailyRoutine = "0"
            }
            
            for i in 0 ..< AppManager.shared.arrBabies.count {
                let obj = AppManager.shared.arrBabies[i]
                if AppUtility.getBabyId() == obj.babyID {
                    selectedIndex = i
                    babyID = obj.babyID
                    tableSelectBaby.reloadData()
                    break
                }
            }
        }
        
        tableSelectBaby.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
        
        if appSceneDelegate.isArabic {
            tfDate.textAlignment = .right
            tfTime.textAlignment = .right
            tfNotes.textAlignment = .right
            tfMedical.textAlignment = .right
            tfAmount.textAlignment = .right
        }
        lablTitle.text = lablTitle.text?.localized()
        lablThisPart.text = lablThisPart.text?.localized()
        lablSetReminder.text = lablSetReminder.text?.localized()
        lablNotes.text = lablNotes.text?.localized()
        lablBaby.text = lablBaby.text?.localized()
        lablMedical.text = lablMedical.text?.localized()
        lablAmount.text = lablAmount.text?.localized()
    
        btnDrops.setTitle(self.btnDrops.titleLabel?.text?.localized(), for: .normal)
        btnAdd.setTitle(self.btnAdd.titleLabel?.text?.localized(), for: .normal)
        btnCancel.setTitle(self.btnCancel.titleLabel?.text?.localized(), for: .normal)
        
        self.getMedications()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let blurEffect = UIBlurEffect(style: .light)
        let blurredEffectView = UIVisualEffectView(effect: blurEffect)
        blurredEffectView.frame = self.view.bounds
        self.viewBg.addSubview(blurredEffectView)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        layoutTableBaby.constant = tableSelectBaby.contentSize.height
    }
    
    func CheckFields() -> Bool {
        if (tfDate.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            
            AppUtility.showInfoMessage(message: "Please enter date".localized())
            let test = false
            return test
        }
        else if (tfTime.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            AppUtility.showInfoMessage(message: "Please enter time".localized())
            let test = false
            return test
        }
        else if (tfMedical.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            AppUtility.showInfoMessage(message: "Please select medications".localized())
            let test = false
            return test
        }
        else if (tfAmount.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            AppUtility.showInfoMessage(message: "Please enter amount".localized())
            let test = false
            return test
        }
        else if babyID.count == 0 {
            AppUtility.showInfoMessage(message: "Please select baby".localized())
            let test = false
            return test
        }
        
        let test = true
        return test
    }
    
    //MARK:- Apis Function
    func addMedical() {
        self.view.endEditing(true)
        var paramDict: [String: Any]?
        var url = API.ADD_MDEICATIONS
        if isFromEdit {
            url = API.ADD_MDEICATIONS
            paramDict = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":self.babyID, "medicationDate":tfDate.text!+" "+tfTime.text!, "medicationNotes":tfNotes.text!, "medicationAmount":tfAmount.text!, "medicationAmountType":medicationAmountType, "medicationId":self.tfMedical.tag, "babyMedicationId":objDiary.objMedication.ID, "addToDailyRoutine":addToDailyRoutine, "setReminder":setReminder]
        } else {
            url = API.ADD_MDEICATIONS
            paramDict = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":self.babyID, "medicationDate":tfDate.text!+" "+tfTime.text!, "medicationNotes":tfNotes.text!, "medicationAmount":tfAmount.text!, "medicationAmountType":medicationAmountType, "medicationId":self.tfMedical.tag, "addToDailyRoutine":addToDailyRoutine, "setReminder":setReminder]
        }
        
        if isAudio {
            APIRequestUtil.addMedical(url:url, parameters: paramDict!, data: audioDatas , extensionString: "audio/m4a", fileNameKey: "medicationFile") { (result, error) in
                if(result != nil) {
                    let swiftyJsonVar = JSON(result!)

                    let dict = swiftyJsonVar.dictionaryValue
                    let status = dict["status"]?.intValue
                    let message = dict["message"]?.string
                    
                    if status == SUCESS_CODE {
                        
                        AppUtility.showSuccessMessage(message: "Medication added successfully".localized())
                        self.delegate?.addMedication()
                        self.cancelTapped(self)
                        
                    } else {
                        
                        AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                        return
                    }
                }
            }
        } else {
            let data = media.jpegData(compressionQuality: 0.5)
            APIRequestUtil.addMedical(url:url, parameters: paramDict!, data: data ?? Data(), extensionString: "image/jpeg", fileNameKey: "medicationFile") { (result, error) in
                if(result != nil) {
                    let swiftyJsonVar = JSON(result!)

                    let dict = swiftyJsonVar.dictionaryValue
                    let status = dict["status"]?.intValue
                    let message = dict["message"]?.string
                    
                    if status == SUCESS_CODE {
                        
                        AppUtility.showSuccessMessage(message: "Medication added successfully".localized())
                        self.delegate?.addMedication()
                        self.cancelTapped(self)
                        
                    } else {
                        
                        AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                        return
                    }
                }
            }
        }
    }
    
    func getMedications() {
        self.view.endEditing(true)
        let paramDict: [String: Any] = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken()]
        
        APIRequestUtil.getMedicationList(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    let valueArray: [JSON] = dict["data"]!.arrayValue
                    for dict in valueArray {
                        let dictValue: Dictionary<String, JSON> = dict.dictionaryValue
                        let obj = BabyType.init(withDictionary: dictValue)
                        self.arrMedicationList.append(obj)
                    }

                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    //MARK:- Button Action
    @IBAction func cancelTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addTapped(_ sender: Any) {
        if self.CheckFields() {
            self.addMedical()
        }
    }
    
    @IBAction func addMedicationTapped(_ sender: Any) {
        var arr = [String]()
        for obj in self.arrMedicationList {
            arr.append(obj.typeTitle)
        }
        ActionSheetStringPicker.show(withTitle: "Select Medical List".localized(), rows: arr, initialSelection: 0, doneBlock: {
            picker, indexes, values in
            
            self.tfMedical.text = values as? String
            self.tfMedical.tag = Int(self.arrMedicationList[indexes].ID)!

            return
        }, cancel: { ActionSheetStringPicker in return},origin: sender)
    }
    
    @IBAction func dayTapped(_ sender: UIButton) {
        let datePicker = ActionSheetDatePicker(title: "Select Date".localized(), datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let date:NSDate = value as! NSDate
            
            self.tfDate.text = AppUtility.convertDateToString(date: date)
            
            return
            
        }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview!.superview)
        datePicker?.minimumDate = Date()
        datePicker?.show()
    }
    
    @IBAction func timeTapped(_ sender: UIButton) {
        let datePicker = ActionSheetDatePicker(title: "Select Time".localized(), datePickerMode: UIDatePicker.Mode.time, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let date:NSDate = value as! NSDate
            
            self.tfTime.text = AppUtility.convertDateToStringTime(date: date)
            
            return
            
        }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview!.superview)
        datePicker?.show()
    }
    
    @IBAction func setSwitchBabyRoutineTapped(_ sender: UISwitch) {
        if sender.isOn {
            addToDailyRoutine = "1"
        } else {
            addToDailyRoutine = "0"
        }
    }
    
    @IBAction func setReminderTapped(_ sender: UISwitch) {
        if sender.isOn {
            setReminder = "1"
        } else {
            setReminder = "0"
        }
    }
    
    @IBAction func addAmountTapped(_ sender: Any) {
        var amount = 0
        if tfAmount.text?.count ?? 0 > 0 {
            amount = Int(tfAmount.text ?? "0") ?? 0
            amount = amount+1
            tfAmount.text = String(amount)
        } else {
            amount = amount+1
            tfAmount.text = String(amount)
        }
    }
    
    @IBAction func minusAmountTapped(_ sender: Any) {
        var amount = 0
        if tfAmount.text?.count ?? 0 > 0 {
            amount = Int(tfAmount.text ?? "0") ?? 0
            if amount >= 1 {
                amount = amount-1
            }
            tfAmount.text = String(amount)
        }
    }
    
    @IBAction func mlTapped(_ sender: UIButton) {
        medicationAmountType = "ml"
        btnMl.backgroundColor = #colorLiteral(red: 0.4431372549, green: 0.2156862745, blue: 0.7568627451, alpha: 1)
        btnMl.setTitleColor(.white, for: .normal)
        btnDrops.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.9294117647, blue: 0.9294117647, alpha: 1)
        btnDrops.setTitleColor(.darkGray, for: .normal)
    }
    
    @IBAction func dropTapped(_ sender: UIButton) {
        medicationAmountType = "drops"
        btnDrops.backgroundColor = #colorLiteral(red: 0.4431372549, green: 0.2156862745, blue: 0.7568627451, alpha: 1)
        btnDrops.setTitleColor(.white, for: .normal)
        btnMl.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.9294117647, blue: 0.9294117647, alpha: 1)
        btnMl.setTitleColor(.darkGray, for: .normal)
    }
    
    @IBAction func uploadMediaTapped(_ sender: UIButton) {
        if sender.tag == 0 {
            ImagePickerManager().pickImage(self) { image in
                self.isAudio = false
                self.media = image
            }
        } else {
            let viewController = AddAudioVC(nibName:String(describing: AddAudioVC.self), bundle:nil)
            viewController.delegate = self
            self.navigationController?.pushViewController(viewController, animated: false)
        }
    }
}

extension AddMedicationsVC:AddAudioVCDelegate {
    func addAudio(audioData: Data, index: Int) {
        isAudio = true
        audioDatas = audioData
    }
}

// MARK: - Tableview Delegate-
extension AddMedicationsVC : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AppManager.shared.arrBabies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        tableView.register(UINib(nibName: "BabyCaregiverCell", bundle: nil), forCellReuseIdentifier: "BabyCaregiverCell")
        var cell : BabyCaregiverCell! = tableView.dequeueReusableCell(withIdentifier: "BabyCaregiverCell") as? BabyCaregiverCell
        
        if (cell == nil) {
            cell = BabyCaregiverCell.init(style: UITableViewCell.CellStyle.default,
                                          reuseIdentifier:"BabyCaregiverCell");
        }
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        let obj = AppManager.shared.arrBabies[indexPath.row]
        cell.lablTitle.text = obj.babyName
        
        if indexPath.row == selectedIndex {
            cell.imgCheck.image = UIImage(named: "checbox fill")
        } else {
            cell.imgCheck.image = UIImage(named: "checbox")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        self.babyID = AppManager.shared.arrBabies[indexPath.row].babyID
        tableView.reloadData()
    }
}
