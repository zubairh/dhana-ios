//
//  AddPumpingVC.swift
//  Tifli
//
//  Created by zubair on 21/10/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import SwiftyJSON

protocol AddPumpingVCDelegate {
    func addPumping()
}

class AddPumpingVC: UIViewController {
    
    @IBOutlet weak var viewBg: UIImageView!
    @IBOutlet weak var tfDate: UITextField!
    @IBOutlet weak var tfTime: UITextField!
    @IBOutlet weak var tfRightBreast: UITextField!
    @IBOutlet weak var tfLeftBreast: UITextField!
    @IBOutlet weak var tfNotes: UITextField!
    @IBOutlet weak var lablTitle: UILabel!
    @IBOutlet weak var lablRightBrst: UILabel!
    @IBOutlet weak var lablLeftBrst: UILabel!
    @IBOutlet weak var lablNotes: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnAdd: GradientButton!
    
    var delegate:AddPumpingVCDelegate? = nil
    var objDiary = Diary()
    var isFromEdit = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isFromEdit {
            self.viewBg.backgroundColor = #colorLiteral(red: 0.9137254902, green: 0.9098039216, blue: 0.9490196078, alpha: 1)
            self.tfLeftBreast.text = objDiary.objPumping.leftBreastDuration
            self.tfRightBreast.text = objDiary.objPumping.rightBreastDuration
            self.tfNotes.text = objDiary.notes
            let dd = objDiary.date.components(separatedBy: " ")
            self.tfDate.text = dd[0]
            self.tfTime.text = dd[1]
        }
        
        if appSceneDelegate.isArabic {
            tfDate.textAlignment = .right
            tfTime.textAlignment = .right
            tfNotes.textAlignment = .right
            tfRightBreast.textAlignment = .right
            tfLeftBreast.textAlignment = .right
        }
        lablTitle.text = lablTitle.text?.localized()
        lablLeftBrst.text = lablLeftBrst.text?.localized()
        lablRightBrst.text = lablRightBrst.text?.localized()
        lablNotes.text = lablNotes.text?.localized()
    
        btnAdd.setTitle(self.btnAdd.titleLabel?.text?.localized(), for: .normal)
        btnCancel.setTitle(self.btnCancel.titleLabel?.text?.localized(), for: .normal)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let blurEffect = UIBlurEffect(style: .light)
        let blurredEffectView = UIVisualEffectView(effect: blurEffect)
        blurredEffectView.frame = self.view.bounds
        self.viewBg.addSubview(blurredEffectView)
    }
    
    func CheckFields() -> Bool {
        if (tfDate.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            
            AppUtility.showInfoMessage(message: "Please enter date".localized())
            let test = false
            return test
        }
        else if (tfTime.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            AppUtility.showInfoMessage(message: "Please enter time".localized())
            let test = false
            return test
        }
        else if (tfRightBreast.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            AppUtility.showInfoMessage(message: "Please enter right breast data".localized())
            let test = false
            return test
        }
        else if (tfLeftBreast.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            AppUtility.showInfoMessage(message: "Please enter left breast data".localized())
            let test = false
            return test
        }
        
        let test = true
        return test
    }
    
      func addPumping() {
        self.view.endEditing(true)
        var paramDict: [String: Any]?
        if self.isFromEdit {
            paramDict = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":AppUtility.getBabyId(), "pumpingLogDate":tfDate.text!+" "+tfTime.text!, "leftBreastAmount":tfLeftBreast.text!, "rightBreastAmount":tfRightBreast.text!, "pumpingLogNotes":tfNotes.text!, "pumpingLogId":objDiary.objPumping.ID]
        } else {
            paramDict = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":AppUtility.getBabyId(), "pumpingLogDate":tfDate.text!+" "+tfTime.text!, "leftBreastAmount":tfLeftBreast.text!, "rightBreastAmount":tfRightBreast.text!, "pumpingLogNotes":tfNotes.text!]
        }
        APIRequestUtil.addPumping(parameters: paramDict!) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    AppUtility.showSuccessMessage(message: "Pumping added successfully".localized())
                    self.delegate?.addPumping()
                    self.cancelTapped(self)
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    //MARK:- Button Action
    @IBAction func cancelTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addTapped(_ sender: Any) {
        if self.CheckFields() {
            self.addPumping()
        }
    }
    
    @IBAction func addRightBreastTapped(_ sender: Any) {
        var right = 0
        if tfRightBreast.text?.count ?? 0 > 0 {
            right = Int(tfRightBreast.text ?? "0") ?? 0
            right = right+1
            tfRightBreast.text = String(right)
        } else {
            right = right+1
            tfRightBreast.text = String(right)
        }
    }
    
    @IBAction func addLeftBreastTapped(_ sender: Any) {
        var left = 0
        if tfLeftBreast.text?.count ?? 0 > 0 {
            left = Int(tfLeftBreast.text ?? "0") ?? 0
            left = left+1
            tfLeftBreast.text = String(left)
        } else {
            left = left+1
            tfLeftBreast.text = String(left)
        }
    }
    
    @IBAction func minusRightBreastTapped(_ sender: Any) {
        var right = 0
        if tfRightBreast.text?.count ?? 0 > 0 {
            right = Int(tfRightBreast.text ?? "0") ?? 0
            if right >= 1 {
                right = right-1
            }
            tfRightBreast.text = String(right)
        }
    }
    
    @IBAction func minusLeftBreastTapped(_ sender: Any) {
        var left = 0
        if tfLeftBreast.text?.count ?? 0 > 0 {
            left = Int(tfLeftBreast.text ?? "0") ?? 0
            left = left+1
            tfLeftBreast.text = String(left)
        } else {
            left = left+1
            tfLeftBreast.text = String(left)
        }
    }
    
    @IBAction func dayTapped(_ sender: UIButton) {
        let datePicker = ActionSheetDatePicker(title: "Select Date".localized(), datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let date:NSDate = value as! NSDate
            
            self.tfDate.text = AppUtility.convertDateToString(date: date)
            
            return
            
        }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview!.superview)
        datePicker?.minimumDate = Date()
        datePicker?.show()
    }
    
    @IBAction func timeTapped(_ sender: UIButton) {
        let datePicker = ActionSheetDatePicker(title: "Select Time".localized(), datePickerMode: UIDatePicker.Mode.time, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let date:NSDate = value as! NSDate
            
            self.tfTime.text = AppUtility.convertDateToStringTime(date: date)
            
            return
            
        }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview!.superview)
        datePicker?.show()
    }
    
}
