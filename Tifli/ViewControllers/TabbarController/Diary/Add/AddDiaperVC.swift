//
//  AddDiaperVC.swift
//  Tifli
//
//  Created by zubair on 26/10/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import SwiftyJSON

protocol AddDiaperVCDelegate {
    func addDiaper()
}

class AddDiaperVC: UIViewController {
    
    @IBOutlet weak var tfDate: UITextField!
    @IBOutlet weak var tfTime: UITextField!
    @IBOutlet weak var tfNotes: UITextField!
    @IBOutlet weak var viewBg: UIImageView!
    @IBOutlet weak var tableSelectBaby: UITableView!
    @IBOutlet weak var layoutTableBaby: NSLayoutConstraint!
    @IBOutlet weak var collectionTypes: UICollectionView!
    @IBOutlet weak var layoutCollectionHeightType: NSLayoutConstraint!
    @IBOutlet weak var collectionColor: UICollectionView!
    
    @IBOutlet weak var lablTitle: UILabel!
    @IBOutlet weak var lablBaby: UILabel!
    @IBOutlet weak var lablNotes: UILabel!
    @IBOutlet weak var lablThisPart: UILabel!
    @IBOutlet weak var lablSetReminder: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnAdd: GradientButton!
    @IBOutlet weak var switchDaily: UISwitch!
    @IBOutlet weak var switchSetReminder: UISwitch!
    
    var selectedIndex = -1
    var selectedDiaperLog = -1
    var selectedIndexDaiperForm = -1
    var selectedIndexDaiperColor = -1
    var arrTitle = ["Clean".localized(), "Poo".localized(), "Pee".localized(), "Mixed".localized(), "Liquid".localized(), "Soft".localized() , "Solid".localized()]
    var arrColor = [UIColor.brown, UIColor.yellow, UIColor.green, UIColor.black, UIColor.gray, UIColor.red]
    var arrColorTitle = ["brown", "yellow", "green", "black", "gray", "red"]
    var media = UIImage()
    var isAudio = false
    var audioDatas = Data()
    var babyID = ""
    var delegate:AddDiaperVCDelegate? = nil
    var objDiary = Diary()
    var isFromEdit = false
    var addToDailyRoutine = "1"
    var setReminder = "0"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isFromEdit {
            
            self.viewBg.backgroundColor = #colorLiteral(red: 0.9137254902, green: 0.9098039216, blue: 0.9490196078, alpha: 1)
            
            let dd = objDiary.date.components(separatedBy: " ")
            self.tfDate.text = dd[0]
            self.tfTime.text = dd[1]
            self.tfNotes.text = objDiary.notes
            
            if self.objDiary.objDiaper.setReminder == 1 {
                switchSetReminder.isOn = true
                setReminder = "1"
            } else {
                switchSetReminder.isOn = false
                setReminder = "0"
            }
            if self.objDiary.objDiaper.dailyRoutineIsActive == 1 {
                switchDaily.isOn = true
                addToDailyRoutine = "1"
            } else {
                switchDaily.isOn = false
                addToDailyRoutine = "0"
            }
            
            for i in 0 ..< arrTitle.count {
                let cc = self.arrTitle[i]
                if cc == objDiary.objDiaper.form {
                    selectedIndexDaiperForm = i
                }
            }
            for i in 0 ..< arrColorTitle.count {
                let cc = self.arrColorTitle[i]
                if cc == objDiary.objDiaper.color {
                    selectedIndexDaiperColor = i
                }
            }
            
            for i in 0 ..< AppManager.shared.arrBabies.count {
                let obj = AppManager.shared.arrBabies[i]
                if AppUtility.getBabyId() == obj.babyID {
                    selectedIndex = i
                    babyID = obj.babyID
                    tableSelectBaby.reloadData()
                    break
                }
            }
        }
        
        if appSceneDelegate.isArabic {
            tfDate.textAlignment = .right
            tfTime.textAlignment = .right
            tfNotes.textAlignment = .right
        }
        lablTitle.text = lablTitle.text?.localized()
        lablThisPart.text = lablThisPart.text?.localized()
        lablSetReminder.text = lablSetReminder.text?.localized()
        lablNotes.text = lablNotes.text?.localized()
        lablBaby.text = lablBaby.text?.localized()
    
        btnAdd.setTitle(self.btnAdd.titleLabel?.text?.localized(), for: .normal)
        btnCancel.setTitle(self.btnCancel.titleLabel?.text?.localized(), for: .normal)
        
        tableSelectBaby.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
        collectionTypes.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let blurEffect = UIBlurEffect(style: .light)
        let blurredEffectView = UIVisualEffectView(effect: blurEffect)
        blurredEffectView.frame = self.view.bounds
        self.viewBg.addSubview(blurredEffectView)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        layoutCollectionHeightType.constant = collectionTypes.contentSize.height
        layoutTableBaby.constant = tableSelectBaby.contentSize.height
    }
    
    func CheckFields() -> Bool {
        if (tfDate.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            
            AppUtility.showInfoMessage(message: "Please enter date".localized())
            let test = false
            return test
        }
        else if (tfTime.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            AppUtility.showInfoMessage(message: "Please enter time".localized())
            let test = false
            return test
        }
        else if babyID.count == 0 {
            AppUtility.showInfoMessage(message: "Please select baby".localized())
            let test = false
            return test
        }
        else if selectedDiaperLog == -1 {
            AppUtility.showInfoMessage(message: "Please select diaper log".localized())
            let test = false
            return test
        }
        else if selectedIndexDaiperForm == -1 {
            AppUtility.showInfoMessage(message: "Please select diaper form".localized())
            let test = false
            return test
        }
        else if selectedIndexDaiperColor == -1 {
            AppUtility.showInfoMessage(message: "Please select diaper color".localized())
            let test = false
            return test
        }
        
        let test = true
        return test
    }
    
    //MARK:- Apis Functions
    func addDiaper() {
        self.view.endEditing(true)
        var paramDict: [String: Any]?
        var url = API.ADD_DIAPER
        if isFromEdit {
            url = API.ADD_DIAPER
            paramDict = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":self.babyID, "diaperLogDate":tfDate.text!+" "+tfTime.text!, "diaperLogType":self.arrTitle[selectedDiaperLog], "diaperLogForm":self.arrTitle[selectedIndexDaiperForm], "diaperLogColor":self.arrColorTitle[selectedIndexDaiperColor], "diaperLogId":objDiary.objDiaper.ID, "addToDailyRoutine":addToDailyRoutine, "setReminder":setReminder]
        } else {
            url = API.ADD_DIAPER
            paramDict = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":self.babyID, "diaperLogDate":tfDate.text!+" "+tfTime.text!, "diaperLogType":self.arrTitle[selectedDiaperLog], "diaperLogForm":self.arrTitle[selectedIndexDaiperForm], "diaperLogColor":self.arrColorTitle[selectedIndexDaiperColor], "addToDailyRoutine":addToDailyRoutine, "setReminder":setReminder]
        }
        if isAudio {
            APIRequestUtil.addDaiper(url:url, parameters: paramDict!, data: audioDatas , extensionString: "audio/m4a", fileNameKey: "diaperFile") { (result, error) in
                if(result != nil) {
                    let swiftyJsonVar = JSON(result!)

                    let dict = swiftyJsonVar.dictionaryValue
                    let status = dict["status"]?.intValue
                    let message = dict["message"]?.string
                    
                    if status == SUCESS_CODE {
                        
                        AppUtility.showSuccessMessage(message: message ?? "Add notes successfully!".localized())
                        self.delegate?.addDiaper()
                        self.cancelTapped(self)
                        
                    } else {
                        
                        AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                        return
                    }
                }
            }
        } else {
            let data = media.jpegData(compressionQuality: 0.5)
            APIRequestUtil.addDaiper(url:url, parameters: paramDict!, data: data ?? Data(), extensionString: "image/jpeg", fileNameKey: "diaperFile") { (result, error) in
                if(result != nil) {
                    let swiftyJsonVar = JSON(result!)

                    let dict = swiftyJsonVar.dictionaryValue
                    let status = dict["status"]?.intValue
                    let message = dict["message"]?.string
                    
                    if status == SUCESS_CODE {
                        
                        AppUtility.showSuccessMessage(message: message ?? "Add notes successfully!".localized())
                        self.delegate?.addDiaper()
                        self.cancelTapped(self)
                        
                    } else {
                        
                        AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                        return
                    }
                }
            }
        }
    }
    
    //MARK:- Button Action
    @IBAction func cancelTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addTapped(_ sender: Any) {
        if self.CheckFields() {
            self.addDiaper()
        }
    }
    
    @IBAction func dayTapped(_ sender: UIButton) {
        let datePicker = ActionSheetDatePicker(title: "Select Date", datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let date:NSDate = value as! NSDate
            
            self.tfDate.text = AppUtility.convertDateToString(date: date)
            
            return
            
        }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview!.superview)
        datePicker?.minimumDate = Date()
        datePicker?.show()
    }
    
    @IBAction func timeTapped(_ sender: UIButton) {
        let datePicker = ActionSheetDatePicker(title: "Select Time".localized(), datePickerMode: UIDatePicker.Mode.time, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let date:NSDate = value as! NSDate
            
            self.tfTime.text = AppUtility.convertDateToStringTime(date: date)
            
            return
            
        }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview!.superview)
        datePicker?.show()
    }
    
    @IBAction func setSwitchBabyRoutineTapped(_ sender: UISwitch) {
        if sender.isOn {
            addToDailyRoutine = "1"
        } else {
            addToDailyRoutine = "0"
        }
    }
    
    @IBAction func setReminderTapped(_ sender: UISwitch) {
        if sender.isOn {
            setReminder = "1"
        } else {
            setReminder = "0"
        }
    }
    
    @IBAction func uploadMediaTapped(_ sender: UIButton) {
        if sender.tag == 0 {
            ImagePickerManager().pickImage(self) { image in
                self.isAudio = false
                self.media = image
            }
        } else {
            let viewController = AddAudioVC(nibName:String(describing: AddAudioVC.self), bundle:nil)
            viewController.delegate = self
            self.navigationController?.pushViewController(viewController, animated: false)
        }
    }
}

extension AddDiaperVC:AddAudioVCDelegate {
    func addAudio(audioData: Data, index: Int) {
        isAudio = true
        audioDatas = audioData
    }
}

// MARK: - Tableview Delegate-
extension AddDiaperVC : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AppManager.shared.arrBabies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        tableView.register(UINib(nibName: "BabyCaregiverCell", bundle: nil), forCellReuseIdentifier: "BabyCaregiverCell")
        var cell : BabyCaregiverCell! = tableView.dequeueReusableCell(withIdentifier: "BabyCaregiverCell") as? BabyCaregiverCell
        
        if (cell == nil) {
            cell = BabyCaregiverCell.init(style: UITableViewCell.CellStyle.default,
                                          reuseIdentifier:"BabyCaregiverCell");
        }
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        let obj = AppManager.shared.arrBabies[indexPath.row]
        cell.lablTitle.text = obj.babyName
        
        if indexPath.row == selectedIndex {
            cell.imgCheck.image = UIImage(named: "checbox fill")
        } else {
            cell.imgCheck.image = UIImage(named: "checbox")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        self.babyID = AppManager.shared.arrBabies[indexPath.row].babyID
        tableView.reloadData()
    }
}

extension AddDiaperVC: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionTypes {
            return self.arrTitle.count
        }
        return self.arrColor.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == collectionTypes {
            if indexPath.row > 3 {
                return CGSize(width: collectionTypes.frame.width/4, height: 50)
            } else {
                return CGSize(width: collectionTypes.frame.width/4, height: 50)
            }
//            let label = UILabel(frame: CGRect.zero)
//            label.text = self.arrTitle[indexPath.row]
//            label.sizeToFit()
//            label.font = UIFont(name: "Open-Sans-Regular", size: 12)
//            return CGSize(width: label.frame.size.width+60, height: 50)
        }
        return CGSize(width: 64, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collectionTypes {
            
            collectionView.register(UINib(nibName: String(describing: DiaperSelectionCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: DiaperSelectionCell.self))
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: DiaperSelectionCell.self), for: indexPath as IndexPath) as! DiaperSelectionCell
            
            cell.lablTitle.text = self.arrTitle[indexPath.row]
            
            if indexPath.row > 3 {
                if indexPath.row == selectedIndexDaiperForm {
                    cell.viewBg.layer.borderColor = COLORS.TABBAR_COLOR.cgColor
                    cell.lablTitle.textColor = COLORS.TABBAR_COLOR
                } else {
                    cell.viewBg.layer.borderColor = UIColor.lightGray.cgColor
                    cell.lablTitle.textColor = .lightGray
                }
            } else {
                if indexPath.row == selectedDiaperLog {
                    cell.viewBg.layer.borderColor = COLORS.TABBAR_COLOR.cgColor
                    cell.lablTitle.textColor = COLORS.TABBAR_COLOR
                } else {
                    cell.viewBg.layer.borderColor = UIColor.lightGray.cgColor
                    cell.lablTitle.textColor = .lightGray
                }
            }
            
            return cell
            
        } else {
            
            collectionView.register(UINib(nibName: String(describing: DiaperColorCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: DiaperColorCell.self))
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: DiaperColorCell.self), for: indexPath as IndexPath) as! DiaperColorCell
            
            cell.imgBg.backgroundColor = self.arrColor[indexPath.row]
            
            if indexPath.row == selectedIndexDaiperColor {
                cell.viewBg.layer.borderColor = COLORS.TABBAR_COLOR.cgColor
            } else {
                cell.viewBg.layer.borderColor = UIColor.lightGray.cgColor
            }
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectionTypes {
            if indexPath.row > 3 {
                selectedIndexDaiperForm = indexPath.row
            } else {
                selectedDiaperLog = indexPath.row
            }
        } else {
            selectedIndexDaiperColor = indexPath.row
        }
        collectionView.reloadData()
    }
    
}
