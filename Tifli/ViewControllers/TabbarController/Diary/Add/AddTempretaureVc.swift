//
//  AddTempretaureVc.swift
//  Tifli
//
//  Created by zubair on 24/10/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import SwiftyJSON

protocol AddTempretaureVcDelegate {
    func addTemprature()
}

class AddTempretaureVc: UIViewController {
    
    @IBOutlet weak var viewBg: UIImageView!
    @IBOutlet weak var tfDate: UITextField!
    @IBOutlet weak var tfTime: UITextField!
    @IBOutlet weak var tfTemp: UITextField!
    @IBOutlet weak var tfNotes: UITextField!
    @IBOutlet weak var lablTitle: UILabel!
    @IBOutlet weak var lablTemp: UILabel!
    @IBOutlet weak var lablNotes: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnAdd: GradientButton!
    
    var delegate:AddTempretaureVcDelegate? = nil
    var objDiary = Diary()
    var isFromEdit = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isFromEdit {
            self.viewBg.backgroundColor = #colorLiteral(red: 0.9137254902, green: 0.9098039216, blue: 0.9490196078, alpha: 1)
            let dd = objDiary.date.components(separatedBy: " ")
            self.tfDate.text = dd[0]
            self.tfTime.text = dd[1]
            self.tfNotes.text = objDiary.notes
            self.tfTemp.text = objDiary.objTemprature.temperature
        }
        
        if appSceneDelegate.isArabic {
            tfDate.textAlignment = .right
            tfTime.textAlignment = .right
            tfNotes.textAlignment = .right
            tfTemp.textAlignment = .right
        }
        lablTitle.text = lablTitle.text?.localized()
        lablTemp.text = lablTemp.text?.localized()
        lablNotes.text = lablNotes.text?.localized()
    
        btnAdd.setTitle(self.btnAdd.titleLabel?.text?.localized(), for: .normal)
        btnCancel.setTitle(self.btnCancel.titleLabel?.text?.localized(), for: .normal)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let blurEffect = UIBlurEffect(style: .light)
        let blurredEffectView = UIVisualEffectView(effect: blurEffect)
        blurredEffectView.frame = self.view.bounds
        self.viewBg.addSubview(blurredEffectView)
    }
    
    func CheckFields() -> Bool {
        if (tfDate.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            
            AppUtility.showInfoMessage(message: "Please enter date".localized())
            let test = false
            return test
        }
        else if (tfTime.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            AppUtility.showInfoMessage(message: "Please enter time".localized())
            let test = false
            return test
        }
        else if (tfTemp.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            AppUtility.showInfoMessage(message: "Please enter temperature value".localized())
            let test = false
            return test
        }
        
        let test = true
        return test
    }
    
      func addTemprature() {
        self.view.endEditing(true)
        var paramDict: [String: Any]?
        if isFromEdit {
            paramDict = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":AppUtility.getBabyId(), "temperatureLogDate":tfDate.text!+" "+tfTime.text!, "temperatureLogTemperature":tfTemp.text!, "temperatureLogNotes":tfNotes.text!, "temperatureLogId":objDiary.objTemprature.ID]
        } else {
            paramDict = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":AppUtility.getBabyId(), "temperatureLogDate":tfDate.text!+" "+tfTime.text!, "temperatureLogTemperature":tfTemp.text!, "temperatureLogNotes":tfNotes.text!]
        }
        APIRequestUtil.addTemprature(parameters: paramDict!) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    AppUtility.showSuccessMessage(message: "Temperature added Successfully".localized())
                    self.delegate?.addTemprature()
                    self.cancelTapped(self)
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    //MARK:- Button Action
    @IBAction func cancelTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addTapped(_ sender: Any) {
        if self.CheckFields() {
            self.addTemprature()
        }
    }
    
    @IBAction func addTempTapped(_ sender: Any) {
        var temp = 0
        if tfTemp.text?.count ?? 0 > 0 {
            temp = Int(tfTemp.text ?? "0") ?? 0
            temp = temp+1
            tfTemp.text = String(temp)
        } else {
            temp = temp+1
            tfTemp.text = String(temp)
        }
    }
    
    @IBAction func minusTempTapped(_ sender: Any) {
        var temp = 0
        if tfTemp.text?.count ?? 0 > 0 {
            temp = Int(tfTemp.text ?? "0") ?? 0
            if temp >= 1 {
                temp = temp-1
            }
            tfTemp.text = String(temp)
        }
    }
    
    @IBAction func dayTapped(_ sender: UIButton) {
        let datePicker = ActionSheetDatePicker(title: "Select Date".localized(), datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let date:NSDate = value as! NSDate
            
            self.tfDate.text = AppUtility.convertDateToString(date: date)
            
            return
            
        }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview!.superview)
        datePicker?.minimumDate = Date()
        datePicker?.show()
    }
    
    @IBAction func timeTapped(_ sender: UIButton) {
        let datePicker = ActionSheetDatePicker(title: "Select Time".localized(), datePickerMode: UIDatePicker.Mode.time, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let date:NSDate = value as! NSDate
            
            self.tfTime.text = AppUtility.convertDateToStringTime(date: date)
            
            return
            
        }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview!.superview)
        datePicker?.show()
    }
    
    @IBAction func setSwitchBabyRoutineTapped(_ sender: UISwitch) {
        
    }
    
    @IBAction func setReminderTapped(_ sender: Any) {
        
    }

}
