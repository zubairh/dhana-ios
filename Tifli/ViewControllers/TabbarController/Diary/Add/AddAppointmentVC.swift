//
//  AddAppointmentVC.swift
//  Tifli
//
//  Created by zubair on 24/10/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import SwiftyJSON

protocol AddAppointmentVCDelegate {
    func addAppointment()
}

class AddAppointmentVC: UIViewController {

    @IBOutlet weak var tfDate: UITextField!
    @IBOutlet weak var tfTime: UITextField!
    @IBOutlet weak var tfDoctorName: UITextField!
    @IBOutlet weak var tfHospital: UITextField!
    @IBOutlet weak var tfNotes: UITextField!
    @IBOutlet weak var viewBg: UIImageView!
    @IBOutlet weak var tableSelectBaby: UITableView!
    @IBOutlet weak var layoutTableBaby: NSLayoutConstraint!
    @IBOutlet weak var lablTitle: UILabel!
    @IBOutlet weak var lablHospital: UILabel!
    @IBOutlet weak var lablDoctorName: UILabel!
    @IBOutlet weak var lablNot: UILabel!
    @IBOutlet weak var lablBaby: UILabel!
    @IBOutlet weak var lablRemindOne: UILabel!
    @IBOutlet weak var lablOneDay: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnAdd: GradientButton!
    @IBOutlet weak var switchOneHour: UISwitch!
    @IBOutlet weak var switchOneDay: UISwitch!
    
    var selectedIndex = -1
    var babyID = ""
    var delegate:AddAppointmentVCDelegate? = nil
    var objDiary = Diary()
    var isFromEdit = false
    var oneHourBeforeReminder = 0
    var oneDayBeforeReminder = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isFromEdit {
            self.viewBg.backgroundColor = #colorLiteral(red: 0.9137254902, green: 0.9098039216, blue: 0.9490196078, alpha: 1)
            let dd = objDiary.date.components(separatedBy: " ")
            self.tfDate.text = dd[0]
            self.tfTime.text = dd[1]
            self.tfNotes.text = objDiary.notes
            self.tfDoctorName.text = objDiary.objAppointment.doctorName
            self.tfHospital.text = objDiary.objAppointment.clinic
            if self.objDiary.objAppointment.oneHourBeforeReminder == 1 {
                switchOneHour.isOn = true
                oneHourBeforeReminder = 1
            } else {
                switchOneHour.isOn = false
                oneHourBeforeReminder = 0
            }
            if self.objDiary.objAppointment.oneDayBeforeReminder == 1 {
                switchOneDay.isOn = true
                oneDayBeforeReminder = 1
            } else {
                switchOneDay.isOn = false
                oneDayBeforeReminder = 0
            }
            for i in 0 ..< AppManager.shared.arrBabies.count {
                let obj = AppManager.shared.arrBabies[i]
                if AppUtility.getBabyId() == obj.babyID {
                    selectedIndex = i
                    babyID = obj.babyID
                    tableSelectBaby.reloadData()
                    break
                }
            }
        }
        
        if appSceneDelegate.isArabic {
            tfDate.textAlignment = .right
            tfTime.textAlignment = .right
            tfNotes.textAlignment = .right
            tfDoctorName.textAlignment = .right
            tfHospital.textAlignment = .right
            tfDoctorName.textAlignment = .right
        }
        lablTitle.text = lablTitle.text?.localized()
        lablBaby.text = lablBaby.text?.localized()
        lablNot.text = lablNot.text?.localized()
        lablHospital.text = lablHospital.text?.localized()
        lablOneDay.text = lablOneDay.text?.localized()
        lablRemindOne.text = lablRemindOne.text?.localized()
        lablDoctorName.text = lablDoctorName.text?.localized()
    
        btnAdd.setTitle(self.btnAdd.titleLabel?.text?.localized(), for: .normal)
        btnCancel.setTitle(self.btnCancel.titleLabel?.text?.localized(), for: .normal)
        
        tableSelectBaby.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let blurEffect = UIBlurEffect(style: .light)
        let blurredEffectView = UIVisualEffectView(effect: blurEffect)
        blurredEffectView.frame = self.view.bounds
        self.viewBg.addSubview(blurredEffectView)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        layoutTableBaby.constant = tableSelectBaby.contentSize.height
    }
    
    func CheckFields() -> Bool {
        if (tfDate.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            
            AppUtility.showInfoMessage(message: "Please enter date".localized())
            let test = false
            return test
        }
        else if (tfTime.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            AppUtility.showInfoMessage(message: "Please enter time".localized())
            let test = false
            return test
        }
        else if (tfHospital.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            AppUtility.showInfoMessage(message: "Please enter hospital name".localized())
            let test = false
            return test
        }
        else if (tfDoctorName.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            AppUtility.showInfoMessage(message: "Please enter doctor name".localized())
            let test = false
            return test
        }
        else if babyID.count == 0 {
            AppUtility.showInfoMessage(message: "Please select baby".localized())
            let test = false
            return test
        }
        
        let test = true
        return test
    }
    
    
    @objc func addAppointment() {
        self.view.endEditing(true)
        var paramDict: [String: Any]?
        if isFromEdit {
            paramDict = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":self.babyID, "appointmentDate":tfDate.text!+" "+tfTime.text!, "appointmentClinic":tfHospital.text!, "appointmentDoctorName":tfDoctorName.text!, "appointmentNotes":tfNotes .text!, "appointmentId":objDiary.objAppointment.ID, "oneHourBeforeReminder":oneHourBeforeReminder, "oneDayBeforeReminder":oneDayBeforeReminder]
        } else {
            paramDict = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":self.babyID, "appointmentDate":tfDate.text!+" "+tfTime.text!, "appointmentClinic":tfHospital.text!, "appointmentDoctorName":tfDoctorName.text!, "appointmentNotes":tfNotes .text!, "oneHourBeforeReminder":oneHourBeforeReminder, "oneDayBeforeReminder":oneDayBeforeReminder]
        }
        APIRequestUtil.addAppointment(parameters: paramDict!) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    AppUtility.showSuccessMessage(message: "Appointment added successfully".localized())
                    self.delegate?.addAppointment()
                    self.cancelTapped(self)
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    //MARK:- Button Action
    @IBAction func cancelTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addTapped(_ sender: Any) {
        if self.CheckFields() {
            self.addAppointment()
        }
    }
    
    @IBAction func dayTapped(_ sender: UIButton) {
        let datePicker = ActionSheetDatePicker(title: "Select Date".localized(), datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let date:NSDate = value as! NSDate
            
            self.tfDate.text = AppUtility.convertDateToString(date: date)
            
            return
            
        }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview!.superview)
        datePicker?.minimumDate = Date()
        datePicker?.show()
    }
    
    @IBAction func timeTapped(_ sender: UIButton) {
        let datePicker = ActionSheetDatePicker(title: "Select Time".localized(), datePickerMode: UIDatePicker.Mode.time, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let date:NSDate = value as! NSDate
            
            self.tfTime.text = AppUtility.convertDateToStringTime(date: date)
            
            return
            
        }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview!.superview)
        datePicker?.show()
    }
    
    @IBAction func setSwitchBabyRoutineTapped(_ sender: UISwitch) {
        if sender.isOn {
            oneHourBeforeReminder = 1
        } else {
            oneHourBeforeReminder = 0
        }
    }
    
    @IBAction func setReminderTapped(_ sender: UISwitch) {
        if sender.isOn {
            oneDayBeforeReminder = 1
        } else {
            oneDayBeforeReminder = 0
        }
    }
}

// MARK: - Tableview Delegate-
extension AddAppointmentVC : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AppManager.shared.arrBabies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        tableView.register(UINib(nibName: "BabyCaregiverCell", bundle: nil), forCellReuseIdentifier: "BabyCaregiverCell")
        var cell : BabyCaregiverCell! = tableView.dequeueReusableCell(withIdentifier: "BabyCaregiverCell") as? BabyCaregiverCell
        
        if (cell == nil) {
            cell = BabyCaregiverCell.init(style: UITableViewCell.CellStyle.default,
                                          reuseIdentifier:"BabyCaregiverCell");
        }
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        let obj = AppManager.shared.arrBabies[indexPath.row]
        cell.lablTitle.text = obj.babyName
        
        if indexPath.row == selectedIndex {
            cell.imgCheck.image = UIImage(named: "checbox fill")
        } else {
            cell.imgCheck.image = UIImage(named: "checbox")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        self.babyID = AppManager.shared.arrBabies[indexPath.row].babyID
        tableView.reloadData()
    }
}
