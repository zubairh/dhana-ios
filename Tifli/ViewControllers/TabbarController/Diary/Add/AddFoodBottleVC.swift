//
//  AddFoodBottleVC.swift
//  Tifli
//
//  Created by zubair on 25/10/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import SwiftyJSON

protocol AddFoodBottleVCDelegate {
    func addFood()
}

class AddFoodBottleVC: UIViewController {
    
    @IBOutlet weak var lablSetManual: UIButton!
    @IBOutlet weak var tfStartTime: UITextField!
    @IBOutlet weak var tfEndTime: UITextField!
    @IBOutlet weak var tfDate: UITextField!
    @IBOutlet weak var tfTime: UITextField!
    @IBOutlet weak var tfAmount: UITextField!
    @IBOutlet weak var tfNotes: UITextField!
    @IBOutlet weak var viewBg: UIImageView!
    @IBOutlet weak var tableSelectBaby: UITableView!
    @IBOutlet weak var layoutTableBaby: NSLayoutConstraint!
    @IBOutlet weak var collectionTypes: UICollectionView!
    @IBOutlet weak var layoutCollectionHeightType: NSLayoutConstraint!
    @IBOutlet weak var layoutViewBottleHeight: NSLayoutConstraint!
    @IBOutlet weak var btnLeftManual: UIButton!
    @IBOutlet weak var btnRightManual: UIButton!
    @IBOutlet weak var viewBottle: UIView!
    @IBOutlet weak var viewBreast: UIView!
    @IBOutlet weak var layoutViewBreastHeight: NSLayoutConstraint!
    @IBOutlet weak var layoutViewHeightSolid: NSLayoutConstraint!
    @IBOutlet weak var collectionIngrediants: UICollectionView!
    @IBOutlet weak var viewSolid: UIView!
    @IBOutlet weak var btnPlayLeftBreast: UIButton!
    @IBOutlet weak var btnPlayRightBreast: UIButton!
    @IBOutlet weak var lablTimerLeftBreast: UILabel!
    @IBOutlet weak var lablTimerRightBreast: UILabel!
    @IBOutlet weak var viewAmount: UIView!
    @IBOutlet weak var viewLayoutAmountHeight: NSLayoutConstraint!
    @IBOutlet weak var collectionBottel: UICollectionView!
    @IBOutlet weak var layoutConsBottleHeight: NSLayoutConstraint!
    @IBOutlet weak var viewLeftBreast: UIView!
    @IBOutlet weak var viewRightBreast: UIView!
    
    @IBOutlet weak var lablTitle: UILabel!
    @IBOutlet weak var lablLeftBreast: UILabel!
    @IBOutlet weak var lablRightBreast: UILabel!
    @IBOutlet weak var lablSetManually: UILabel!
    @IBOutlet weak var lablBaby: UILabel!
    @IBOutlet weak var lablNotes: UILabel!
    @IBOutlet weak var lablThisPart: UILabel!
    @IBOutlet weak var lablSetReminder: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnAdd: GradientButton!
    @IBOutlet weak var lablAmount: UILabel!
    @IBOutlet weak var lablIngrediants: UILabel!
    @IBOutlet weak var switchDaily: UISwitch!
    @IBOutlet weak var switchSetReminder: UISwitch!
    
    var selectedIndex = -1
    var selectedFoodIndex = -1
    var selectedBotelIndex = -1
    var arrTitle = ["Breast".localized(), "Botel".localized(), "Solids".localized()]
    var arrIcon = ["breast", "Milk", "Solid"]
    var arrTitleBottle = [String]()
    var arrIconBottle = [String]()
    var timerLeftBreast = Timer()
    var timerRightBreast = Timer()
    var isLeftTimer = false
    var isRightTimer = false
    
    var leftStartTime = ""
    var rightStartTime = ""
    
    var totalSecondLeft:Int = 0
    var totalSecondRight:Int = 0
    
    var delegate:AddFoodBottleVCDelegate? = nil
    var babyID = ""
    var objDiary = Diary()
    var isFromEdit = false
    var addToDailyRoutine = "1"
    var setReminder = "0"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewBottle.isHidden = true
        layoutViewBottleHeight.constant = 0
        viewBreast.isHidden = true
        layoutViewBreastHeight.constant = 0
        viewSolid.isHidden = true
        layoutViewHeightSolid.constant = 0
        viewAmount.isHidden = true
        viewLayoutAmountHeight.constant = 0
        
        if isFromEdit {
            
            self.viewBg.backgroundColor = #colorLiteral(red: 0.9137254902, green: 0.9098039216, blue: 0.9490196078, alpha: 1)
            
            let dd = objDiary.date.components(separatedBy: " ")
            self.tfDate.text = dd[0]
            self.tfTime.text = dd[1]
            self.tfNotes.text = objDiary.notes
            
            if self.objDiary.objFeeding.setReminder == 1 {
                switchSetReminder.isOn = true
                setReminder = "1"
            } else {
                switchSetReminder.isOn = false
                setReminder = "0"
            }
            if self.objDiary.objFeeding.dailyRoutineIsActive == 1 {
                switchDaily.isOn = true
                addToDailyRoutine = "1"
            } else {
                switchDaily.isOn = false
                addToDailyRoutine = "0"
            }
            
            if objDiary.objFeeding.type == "breast" {
                
                selectedFoodIndex = 0
                
                self.tfAmount.text = objDiary.objFeeding.amount
                arrTitle = ["Breast".localized(), "Botel".localized(), "Solids".localized()]
                arrIcon = ["breast", "Milk", "Solid"]
                arrTitleBottle = [String]()
                arrIconBottle = [String]()
                viewBottle.isHidden = true
                layoutViewBottleHeight.constant = 0
                viewBreast.isHidden = false
                layoutViewBreastHeight.constant = 170
                viewSolid.isHidden = true
                layoutViewHeightSolid.constant = 0
                viewAmount.isHidden = true
                viewLayoutAmountHeight.constant = 0
                
                if objDiary.objFeeding.leftBreastDuration.count <= 5 {
                    self.setManualTapped(lablSetManual)
                    self.tfStartTime.text = objDiary.objFeeding.leftBreastDuration
                    self.tfEndTime.text = objDiary.objFeeding.rightBreastDuration
                } else {
                    self.lablTimerLeftBreast.text = objDiary.objFeeding.leftBreastDuration
                    self.lablTimerRightBreast.text = objDiary.objFeeding.rightBreastDuration
                }
                
            } else if objDiary.objFeeding.type == "solids" {
                
                selectedFoodIndex = 2
                
                let arr = objDiary.objFeeding.ingredients.components(separatedBy: ",")
                for v in arr {
                    AppManager.shared.arrIngrediants.add(v)
                }
                
                arrTitle = ["Breast".localized(), "Botel".localized(), "Solids".localized()]
                arrIcon = ["breast", "Milk", "Solid"]
                arrTitleBottle = [String]()
                arrIconBottle = [String]()
                viewBottle.isHidden = true
                layoutViewBottleHeight.constant = 0
                viewBreast.isHidden = true
                layoutViewBreastHeight.constant = 0
                viewSolid.isHidden = false
                layoutViewHeightSolid.constant = 135
                viewAmount.isHidden = true
                viewLayoutAmountHeight.constant = 0
                
                timerRightBreast.invalidate()
                timerLeftBreast.invalidate()
                lablTimerRightBreast.text = "00:00:00"
                lablTimerLeftBreast.text = "00:00:00"
                totalSecondLeft = 0
                totalSecondRight = 0
                btnPlayRightBreast.isSelected = false
                btnPlayLeftBreast.isSelected = false
                
            } else if objDiary.objFeeding.type == "bottle" {
                
                selectedFoodIndex = 1
                if objDiary.objFeeding.bottleType == "breast_milk" {
                    selectedBotelIndex = 0
                } else {
                    selectedBotelIndex = 1
                }
                arrTitle = ["Breast".localized(), "Botel".localized(), "Solids".localized()]
                arrIcon = ["breast", "Milk", "Solid"]
                arrTitleBottle = ["Breast Milk".localized(), "Formula".localized()]
                arrIconBottle = ["Breast Milk", "Formula"]
                viewBottle.isHidden = false
                layoutViewBottleHeight.constant = 170
                viewBreast.isHidden = true
                layoutViewBreastHeight.constant = 0
                viewSolid.isHidden = true
                layoutViewHeightSolid.constant = 0
                viewAmount.isHidden = false
                viewLayoutAmountHeight.constant = 100
                
                timerRightBreast.invalidate()
                timerLeftBreast.invalidate()
                lablTimerRightBreast.text = "00:00:00"
                lablTimerLeftBreast.text = "00:00:00"
                totalSecondLeft = 0
                totalSecondRight = 0
                btnPlayRightBreast.isSelected = false
                btnPlayLeftBreast.isSelected = false
            }
            
            for i in 0 ..< AppManager.shared.arrBabies.count {
                let obj = AppManager.shared.arrBabies[i]
                if AppUtility.getBabyId() == obj.babyID {
                    selectedIndex = i
                    babyID = obj.babyID
                    tableSelectBaby.reloadData()
                    break
                }
            }
        }
        
        if appSceneDelegate.isArabic {
            tfDate.textAlignment = .right
            tfTime.textAlignment = .right
            tfNotes.textAlignment = .right
            tfStartTime.textAlignment = .right
            tfEndTime.textAlignment = .right
            tfAmount.textAlignment = .right
        }
        lablTitle.text = lablTitle.text?.localized()
        lablThisPart.text = lablThisPart.text?.localized()
        lablSetReminder.text = lablSetReminder.text?.localized()
        lablNotes.text = lablNotes.text?.localized()
        lablBaby.text = lablBaby.text?.localized()
        lablLeftBreast.text = lablLeftBreast.text?.localized()
        lablRightBreast.text = lablRightBreast.text?.localized()
        lablAmount.text = lablAmount.text?.localized()
        lablRightBreast.text = lablRightBreast.text?.localized()
        lablSetManually.text = lablSetManually.text?.localized()
    
        lablSetManual.setTitle(self.lablSetManual.titleLabel?.text?.localized(), for: .normal)
        btnAdd.setTitle(self.btnAdd.titleLabel?.text?.localized(), for: .normal)
        btnCancel.setTitle(self.btnCancel.titleLabel?.text?.localized(), for: .normal)
        
        tableSelectBaby.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
        collectionTypes.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
        collectionBottel.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let blurEffect = UIBlurEffect(style: .light)
        let blurredEffectView = UIVisualEffectView(effect: blurEffect)
        blurredEffectView.frame = self.view.bounds
        self.viewBg.addSubview(blurredEffectView)
        
        self.collectionIngrediants.reloadData()
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        layoutCollectionHeightType.constant = collectionTypes.contentSize.height
        layoutConsBottleHeight.constant = collectionBottel.contentSize.height
        layoutTableBaby.constant = tableSelectBaby.contentSize.height
    }
    
    @objc func leftBreastTimeUpdate() {
        var hours: Int
        var minutes: Int
        var seconds: Int

        totalSecondLeft = totalSecondLeft + 1
        hours = totalSecondLeft / 3600
        minutes = (totalSecondLeft % 3600) / 60
        seconds = (totalSecondLeft % 3600) % 60
        lablTimerLeftBreast.text = String(format: "%02d:%02d:%02d", hours, minutes, seconds)
    }
    
    @objc func rightBreastTimeUpdate() {
        var hours: Int
        var minutes: Int
        var seconds: Int

        totalSecondRight = totalSecondRight + 1
        hours = totalSecondRight / 3600
        minutes = (totalSecondRight % 3600) / 60
        seconds = (totalSecondRight % 3600) % 60
        lablTimerRightBreast.text = String(format: "%02d:%02d:%02d", hours, minutes, seconds)
    }
    
    func CheckFieldsBreast() -> Bool {
        if (tfDate.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {

            AppUtility.showInfoMessage(message: "Please enter date".localized())
            let test = false
            return test
        }
        else if (tfTime.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            AppUtility.showInfoMessage(message: "Please enter time".localized())
            let test = false
            return test
        }
        else if babyID.count == 0 {
            AppUtility.showInfoMessage(message: "Please select baby".localized())
            let test = false
            return test
        }
        if self.viewLeftBreast.isHidden == false {
            if lablTimerLeftBreast.text == "00:00:00" {
                AppUtility.showInfoMessage(message: "Please put left breast time".localized())
                let test = false
                return test
            }
            else if lablTimerRightBreast.text == "00:00:00" {
                AppUtility.showInfoMessage(message: "Please put right breast time".localized())
                let test = false
                return test
            }
        }

        let test = true
        return test
    }
    
    func CheckFieldsBotel() -> Bool {
        if (tfDate.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {

            AppUtility.showInfoMessage(message: "Please enter date".localized())
            let test = false
            return test
        }
        else if (tfTime.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            AppUtility.showInfoMessage(message: "Please enter time".localized())
            let test = false
            return test
        }
        else if selectedBotelIndex == -1 {
            AppUtility.showInfoMessage(message: "Please select bottle type".localized())
            let test = false
            return test
        }
        else if (tfAmount.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            AppUtility.showInfoMessage(message: "Please enter amount in ml".localized())
            let test = false
            return test
        }
        else if babyID.count == 0 {
            AppUtility.showInfoMessage(message: "Please select baby".localized())
            let test = false
            return test
        }

        let test = true
        return test
    }
    
    func CheckFieldsSolid() -> Bool {
        if (tfDate.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {

            AppUtility.showInfoMessage(message: "Please enter date".localized())
            let test = false
            return test
        }
        else if (tfTime.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            AppUtility.showInfoMessage(message: "Please enter time".localized())
            let test = false
            return test
        }
        else if AppManager.shared.arrIngrediants.count == 0 {
            AppUtility.showInfoMessage(message: "Please add your Ingredients".localized())
            let test = false
            return test
        }
        else if babyID.count == 0 {
            AppUtility.showInfoMessage(message: "Please select baby".localized())
            let test = false
            return test
        }

        let test = true
        return test
    }

    //MARK:- Apis Function
    func addFoodBottel() {
        self.view.endEditing(true)
        var paramDict: [String: Any]?
        var url = API.ADD_FEEDRING
        if self.isFromEdit {
            url = API.ADD_FEEDRING
            if selectedFoodIndex == 0 {
                if self.viewRightBreast.isHidden == true {
                    paramDict = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":self.babyID, "date":tfDate.text!+" "+tfTime.text!, "type":"breast", "leftBreastDuration":tfStartTime.text!, "rightBreastDuration":tfEndTime.text!, "notes":tfNotes.text!, "feedingId":objDiary.objFeeding.ID, "addToDailyRoutine":addToDailyRoutine, "setReminder":setReminder]
                } else {
                    paramDict = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":self.babyID, "date":tfDate.text!+" "+tfTime.text!, "type":"breast", "leftBreastDuration":lablTimerLeftBreast.text!, "rightBreastDuration":lablTimerRightBreast.text!, "notes":tfNotes.text!, "feedingId":objDiary.objFeeding.ID, "addToDailyRoutine":addToDailyRoutine, "setReminder":setReminder]
                }
            } else if selectedFoodIndex == 1 {
                var type = ""
                if selectedBotelIndex == 0 {
                    type = "breast_milk"
                } else if selectedBotelIndex == 1 {
                    type = "formula"
                }
                paramDict = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":self.babyID, "date":tfDate.text!+" "+tfTime.text!, "type":"bottle", "bottleType":type, "amount":tfAmount.text!, "notes":tfNotes.text!, "feedingId":objDiary.objFeeding.ID, "addToDailyRoutine":addToDailyRoutine, "setReminder":setReminder]
            } else if selectedFoodIndex == 2 {
                paramDict = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":self.babyID, "date":tfDate.text!+" "+tfTime.text!, "type":"solids", "ingredients":AppManager.shared.arrIngrediants.componentsJoined(by: ","), "notes":tfNotes.text!, "feedingId":objDiary.objFeeding.ID, "addToDailyRoutine":addToDailyRoutine, "setReminder":setReminder]
            }
        } else {
            url = API.ADD_FEEDRING
            if selectedFoodIndex == 0 {
                if self.viewRightBreast.isHidden == true {
                    paramDict = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":self.babyID, "date":tfDate.text!+" "+tfTime.text!, "type":"breast", "leftBreastDuration":tfStartTime.text!, "rightBreastDuration":tfEndTime.text!, "notes":tfNotes.text!, "addToDailyRoutine":addToDailyRoutine, "setReminder":setReminder]
                } else {
                    paramDict = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":self.babyID, "date":tfDate.text!+" "+tfTime.text!, "type":"breast", "leftBreastDuration":lablTimerLeftBreast.text!, "rightBreastDuration":lablTimerRightBreast.text!, "notes":tfNotes.text!, "addToDailyRoutine":addToDailyRoutine, "setReminder":setReminder]
                }
            } else if selectedFoodIndex == 1 {
                var type = ""
                if selectedBotelIndex == 0 {
                    type = "breast_milk"
                } else if selectedBotelIndex == 1 {
                    type = "formula"
                }
                paramDict = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":self.babyID, "date":tfDate.text!+" "+tfTime.text!, "type":"bottle", "bottleType":type, "amount":tfAmount.text!, "notes":tfNotes.text!, "addToDailyRoutine":addToDailyRoutine, "setReminder":setReminder]
            } else if selectedFoodIndex == 2 {
                paramDict = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":self.babyID, "date":tfDate.text!+" "+tfTime.text!, "type":"solids", "ingredients":AppManager.shared.arrIngrediants.componentsJoined(by: ","), "notes":tfNotes.text!, "addToDailyRoutine":addToDailyRoutine, "setReminder":setReminder]
            }
        }
        APIRequestUtil.addFeedering(url:url, parameters: paramDict!) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string

                if status == SUCESS_CODE {

                    AppUtility.showSuccessMessage(message: "Food added successfully".localized())
                    self.delegate?.addFood()
                    self.cancelTapped(self)

                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    //MARK:- Button Action
    @IBAction func cancelTapped(_ sender: Any) {
        timerLeftBreast.invalidate()
        timerRightBreast.invalidate()
        AppManager.shared.arrIngrediants.removeAllObjects()
        self.navigationController?.popViewController(animated: false)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addTapped(_ sender: Any) {
        if selectedFoodIndex == 0 {
            if self.CheckFieldsBreast() {
                self.addFoodBottel()
            }
        } else if selectedFoodIndex == 1 {
            if CheckFieldsBotel() {
                self.addFoodBottel()
            }
        } else if selectedFoodIndex == 2 {
            if CheckFieldsSolid() {
                self.addFoodBottel()
            }
        }
    }
    
    @IBAction func dayTapped(_ sender: UIButton) {
        let datePicker = ActionSheetDatePicker(title: "Select Date".localized(), datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let date:NSDate = value as! NSDate
            
            self.tfDate.text = AppUtility.convertDateToString(date: date)
            
            return
            
        }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview!.superview)
        datePicker?.minimumDate = Date()
        datePicker?.show()
    }
    
    @IBAction func timeTapped(_ sender: UIButton) {
        let datePicker = ActionSheetDatePicker(title: "Select Time".localized(), datePickerMode: UIDatePicker.Mode.time, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let date:NSDate = value as! NSDate
            
            self.tfTime.text = AppUtility.convertDateToStringTime(date: date)
            
            return
            
        }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview!.superview)
        datePicker?.show()
    }
    
    @IBAction func setSwitchBabyRoutineTapped(_ sender: UISwitch) {
        if sender.isOn {
            addToDailyRoutine = "1"
        } else {
            addToDailyRoutine = "0"
        }
    }
    
    @IBAction func setReminderTapped(_ sender: UISwitch) {
        if sender.isOn {
            setReminder = "1"
        } else {
            setReminder = "0"
        }
    }
    
    @IBAction func addAmountTapped(_ sender: Any) {
        var amount = 0
        if tfAmount.text?.count ?? 0 > 0 {
            amount = Int(tfAmount.text ?? "0") ?? 0
            amount = amount+1
            tfAmount.text = String(amount)
        } else {
            amount = amount+1
            tfAmount.text = String(amount)
        }
    }
    
    @IBAction func minusAmountTapped(_ sender: Any) {
        var amount = 0
        if tfAmount.text?.count ?? 0 > 0 {
            amount = Int(tfAmount.text ?? "0") ?? 0
            if amount >= 1 {
                amount = amount-1
            }
            tfAmount.text = String(amount)
        }
    }
    
    @IBAction func addIngrediatsTapped(_ sender: Any) {
        let viewController = AddFoodVC(nibName:String(describing: AddFoodVC.self), bundle:nil)
        viewController.hidesBottomBarWhenPushed = true
        viewController.isFromFoodIngrediats = true
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func leftBreastTapped(_ sender: Any) {
        if btnPlayLeftBreast.isSelected {
            btnPlayLeftBreast.isSelected = false
            timerLeftBreast.invalidate()
        } else {
            btnPlayLeftBreast.isSelected = true
            timerLeftBreast = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.leftBreastTimeUpdate), userInfo: nil, repeats: true)
        }
    }
    
    @IBAction func rightBreastTapped(_ sender: Any) {
        if btnPlayRightBreast.isSelected {
            btnPlayRightBreast.isSelected = false
            timerRightBreast.invalidate()
        } else {
            btnPlayRightBreast.isSelected = true
            timerRightBreast = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.rightBreastTimeUpdate), userInfo: nil, repeats: true)
        }
    }
    
    @IBAction func setManualTapped(_ sender: UIButton) {
        timerRightBreast.invalidate()
        timerRightBreast.invalidate()
        if sender.tag == 1 {
            sender.tag = 0
            viewLeftBreast.isHidden = false
            viewRightBreast.isHidden = false
            lablSetManual.setTitle("Set by manual".localized(), for: .normal)
            self.tfStartTime.text = ""
            self.tfEndTime.text = ""
            lablTimerLeftBreast.text = "00:00:00"
            lablTimerRightBreast.text = "00:00:00"
        } else {
            sender.tag = 1
            viewLeftBreast.isHidden = true
            viewRightBreast.isHidden = true
            self.tfStartTime.text = ""
            self.tfEndTime.text = ""
            lablTimerLeftBreast.text = "00:00:00"
            lablTimerRightBreast.text = "00:00:00"
            lablSetManual.setTitle("Set by timer".localized(), for: .normal)
        }
    }
    
    @IBAction func LeftstartTimeTapped(_ sender: UIButton) {
        let datePicker = ActionSheetDatePicker(title: "Select Left Breast Start Time".localized(), datePickerMode: UIDatePicker.Mode.time, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let date:NSDate = value as! NSDate
            
            self.leftStartTime = AppUtility.convertDateToStringTime(date: date)
            appDelegate.delay(0.5, closure: {
                self.LeftendTimeTapped(self.btnLeftManual)
            })
            
            return
            
        }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview!.superview)
        datePicker?.show()
    }
    
    @IBAction func LeftendTimeTapped(_ sender: UIButton) {
        let datePicker = ActionSheetDatePicker(title: "Select Left Breast End Time".localized(), datePickerMode: UIDatePicker.Mode.time, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let date:NSDate = value as! NSDate
            
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm"

            let date1 = formatter.date(from: self.leftStartTime)
            let date2 = formatter.date(from: AppUtility.convertDateToStringTime(date: date))
            
            let difference = Calendar.current.dateComponents([.hour, .minute, .second], from: date1!, to: date2!)
            let formattedString = String(format: "%02ld:%02ld:%02ld", difference.hour!, difference.minute!, difference.second as! CVarArg)

            var result: ComparisonResult? = nil
            if let date2 = date2 {
                result = date1?.compare(date2)
            }
            if result == .orderedDescending {
                print("date1 is later than date2")
                AppUtility.showInfoMessage(message: "end time should be next time of start time".localized())
                return
            } else if result == .orderedAscending {
                self.tfStartTime.text = formattedString
                print("date2 is later than date1")
            } else {
                print("date1 is equal to date2")
                AppUtility.showInfoMessage(message: "end time should be next time of start time".localized())
                return
            }
            
            return
            
        }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview!.superview)
        datePicker?.show()
    }
    
    @IBAction func RightstartTimeTapped(_ sender: UIButton) {
        let datePicker = ActionSheetDatePicker(title: "Select Right Breast Start Time".localized(), datePickerMode: UIDatePicker.Mode.time, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let date:NSDate = value as! NSDate
            
            self.rightStartTime = AppUtility.convertDateToStringTime(date: date)
            appDelegate.delay(0.5, closure: {
                self.rightEndTimeTapped(self.btnRightManual)
            })
            
            return
            
        }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview!.superview)
        datePicker?.show()
    }
    
    @IBAction func rightEndTimeTapped(_ sender: UIButton) {
        
        let datePicker = ActionSheetDatePicker(title: "Select Right Breast End Time".localized(), datePickerMode: UIDatePicker.Mode.time, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let date:NSDate = value as! NSDate
            
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm"

            let date1 = formatter.date(from: self.rightStartTime)
            let date2 = formatter.date(from: AppUtility.convertDateToStringTime(date: date))
            
            let difference = Calendar.current.dateComponents([.hour, .minute, .second], from: date1!, to: date2!)
            let formattedString = String(format: "%02ld:%02ld:%02ld", difference.hour!, difference.minute!, difference.second!)

            var result: ComparisonResult? = nil
            if let date2 = date2 {
                result = date1?.compare(date2)
            }
            if result == .orderedDescending {
                print("date1 is later than date2")
                AppUtility.showInfoMessage(message: "end time should be next time of start time".localized())
                return
            } else if result == .orderedAscending {
                self.tfEndTime.text = formattedString
                print("date2 is later than date1")
            } else {
                print("date1 is equal to date2")
                AppUtility.showInfoMessage(message: "end time should be next time of start time".localized())
                return
            }
            
            return
            
        }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview!.superview)
        datePicker?.show()
    }
    
    @objc func deleteIngrediatsTapped(_ sender: UIButton) {
        AppManager.shared.arrIngrediants.removeObject(at: sender.tag)
        collectionIngrediants.reloadData()
    }
}

// MARK: - Tableview Delegate-
extension AddFoodBottleVC : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AppManager.shared.arrBabies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        tableView.register(UINib(nibName: "BabyCaregiverCell", bundle: nil), forCellReuseIdentifier: "BabyCaregiverCell")
        var cell : BabyCaregiverCell! = tableView.dequeueReusableCell(withIdentifier: "BabyCaregiverCell") as? BabyCaregiverCell
        
        if (cell == nil) {
            cell = BabyCaregiverCell.init(style: UITableViewCell.CellStyle.default,
                                          reuseIdentifier:"BabyCaregiverCell");
        }
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        let obj = AppManager.shared.arrBabies[indexPath.row]
        cell.lablTitle.text = obj.babyName
        
        if indexPath.row == selectedIndex {
            cell.imgCheck.image = UIImage(named: "checbox fill")
        } else {
            cell.imgCheck.image = UIImage(named: "checbox")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        self.babyID = AppManager.shared.arrBabies[indexPath.row].babyID
        tableView.reloadData()
    }
}

extension AddFoodBottleVC: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionIngrediants {
            return AppManager.shared.arrIngrediants.count
        }
        else if collectionView == collectionBottel {
            return self.arrIconBottle.count
        }
        return self.arrIcon.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == collectionIngrediants {
            let label = UILabel(frame: CGRect.zero)
            label.text = (AppManager.shared.arrIngrediants.object(at: indexPath.row) as! String)
            label.sizeToFit()
            label.font = UIFont(name: "Open-Sans-Regular", size: 12)
            return CGSize(width: label.frame.size.width+58, height: 42)
            
        } else if collectionView == collectionBottel {
//            let label = UILabel(frame: CGRect.zero)
//            label.text = self.arrTitle[indexPath.row]
//            label.sizeToFit()
//            label.font = UIFont(name: "Open-Sans-Regular", size: 12)
//            return CGSize(width: label.frame.size.width+60, height: 50)
          
            return CGSize(width: collectionView.bounds.width/2, height: 50)
        } else {
            return CGSize(width: collectionView.bounds.width/3, height: 50)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collectionTypes {
            
            collectionView.register(UINib(nibName: String(describing: FoodAddCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: FoodAddCell.self))
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: FoodAddCell.self), for: indexPath as IndexPath) as! FoodAddCell
            
            cell.imgIcon.image = UIImage(named: self.arrIcon[indexPath.row])
            cell.lablTitle.text = self.arrTitle[indexPath.row]
            
            if indexPath.row == selectedFoodIndex {
                cell.viewBg.layer.borderColor = COLORS.TABBAR_COLOR.cgColor
                cell.lablTitle.textColor = COLORS.TABBAR_COLOR
            } else {
                cell.viewBg.layer.borderColor = UIColor.lightGray.cgColor
                cell.lablTitle.textColor = .lightGray
            }
            
            return cell
            
        } else if collectionView == collectionBottel {
            
            collectionView.register(UINib(nibName: String(describing: FoodAddCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: FoodAddCell.self))
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: FoodAddCell.self), for: indexPath as IndexPath) as! FoodAddCell
            
            cell.imgIcon.image = UIImage(named: self.arrIconBottle[indexPath.row])
            cell.lablTitle.text = self.arrTitleBottle[indexPath.row]
            
            if indexPath.row == selectedBotelIndex {
                cell.viewBg.layer.borderColor = COLORS.TABBAR_COLOR.cgColor
                cell.lablTitle.textColor = COLORS.TABBAR_COLOR
            } else {
                cell.viewBg.layer.borderColor = UIColor.lightGray.cgColor
                cell.lablTitle.textColor = .lightGray
            }
            
            return cell
            
        } else {
            
            collectionView.register(UINib(nibName: String(describing: FoodSolidCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: FoodSolidCell.self))
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: FoodSolidCell.self), for: indexPath as IndexPath) as! FoodSolidCell
            
            cell.lablTitle.text = (AppManager.shared.arrIngrediants.object(at: indexPath.row) as! String)
            cell.btnClose.tag = indexPath.row
            cell.btnClose.addTarget(self, action: #selector(self.deleteIngrediatsTapped(_:)), for: .touchUpInside)
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectionTypes {
            if indexPath.row == 1 {
                arrTitle = ["Breast".localized(), "Botel".localized(), "Solids".localized()]
                arrIcon = ["breast", "Milk", "Solid"]
                arrTitleBottle = ["Breast Milk".localized(), "Formula".localized()]
                arrIconBottle = ["Breast Milk", "Formula"]
                viewBottle.isHidden = false
                layoutViewBottleHeight.constant = 170
                viewBreast.isHidden = true
                layoutViewBreastHeight.constant = 0
                viewSolid.isHidden = true
                layoutViewHeightSolid.constant = 0
                viewAmount.isHidden = false
                viewLayoutAmountHeight.constant = 100
                
                timerRightBreast.invalidate()
                timerLeftBreast.invalidate()
                lablTimerRightBreast.text = "00:00:00"
                lablTimerLeftBreast.text = "00:00:00"
                totalSecondLeft = 0
                totalSecondRight = 0
                btnPlayRightBreast.isSelected = false
                btnPlayLeftBreast.isSelected = false
                
            } else if indexPath.row == 0 {
                arrTitle = ["Breast".localized(), "Botel".localized(), "Solids".localized()]
                arrIcon = ["breast", "Milk", "Solid"]
                arrTitleBottle = [String]()
                arrIconBottle = [String]()
                viewBottle.isHidden = true
                layoutViewBottleHeight.constant = 0
                viewBreast.isHidden = false
                layoutViewBreastHeight.constant = 170
                viewSolid.isHidden = true
                layoutViewHeightSolid.constant = 0
                viewAmount.isHidden = true
                viewLayoutAmountHeight.constant = 0
            } else if indexPath.row == 2 {
                arrTitle = ["Breast".localized(), "Botel".localized(), "Solids".localized()]
                arrIcon = ["breast", "Milk", "Solid"]
                arrTitleBottle = [String]()
                arrIconBottle = [String]()
                viewBottle.isHidden = true
                layoutViewBottleHeight.constant = 0
                viewBreast.isHidden = true
                layoutViewBreastHeight.constant = 0
                viewSolid.isHidden = false
                layoutViewHeightSolid.constant = 135
                viewAmount.isHidden = true
                viewLayoutAmountHeight.constant = 0
                
                timerRightBreast.invalidate()
                timerLeftBreast.invalidate()
                lablTimerRightBreast.text = "00:00:00"
                lablTimerLeftBreast.text = "00:00:00"
                totalSecondLeft = 0
                totalSecondRight = 0
                btnPlayRightBreast.isSelected = false
                btnPlayLeftBreast.isSelected = false
            }
            selectedFoodIndex = indexPath.row
            collectionTypes.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
            collectionView.reloadData()
            collectionBottel.reloadData()
        }
        else if collectionView == collectionBottel {
            selectedBotelIndex = indexPath.row
            collectionBottel.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
            collectionView.reloadData()
        }
    }
    
}
