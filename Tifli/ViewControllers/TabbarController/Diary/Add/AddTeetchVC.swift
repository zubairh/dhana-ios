//
//  AddTeetchVC.swift
//  Tifli
//
//  Created by zubair on 28/10/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import SwiftyJSON

protocol AddTeetchVCDelegate {
    func addTeeth()
}

class AddTeetchVC: UIViewController {
    
    @IBOutlet weak var tfDate: UITextField!
    @IBOutlet weak var tfNotes: UITextField!
    @IBOutlet weak var viewBg: UIImageView!
    @IBOutlet weak var collectionTeeth: UICollectionView!
    
    @IBOutlet weak var lablTitle: UILabel!
    @IBOutlet weak var lablNotes: UILabel!
    @IBOutlet weak var lablDetail: UILabel!
    @IBOutlet weak var btnRefrence: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnAdd: GradientButton!
    
    var media = UIImage()
    var isAudio = false
    var audioDatas = Data()
    let circularLayout = DSCircularLayout()
    var arrTeeth = [Teeth]()
    var selectedIndex = -1
    var delegate:AddTeetchVCDelegate? = nil
    var objDiary = Diary()
    var isFromEdit = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        appDelegate.delay(0.1, closure: {
            self.setCircular()
        })
        
        if AppManager.shared.arrTeeth.count == 0 {
            self.getTeeth()
        } else {
            appDelegate.delay(0.1, closure: {
                self.arrTeeth = AppManager.shared.arrTeeth
                self.collectionTeeth.reloadData()
            })
        }
        
        if isFromEdit {
            self.viewBg.backgroundColor = #colorLiteral(red: 0.9137254902, green: 0.9098039216, blue: 0.9490196078, alpha: 1)
            let dd = objDiary.date.components(separatedBy: " ")
            self.tfDate.text = dd[0]
            self.tfNotes.text = objDiary.notes

            for i in 0 ..< objDiary.objTeething.arrTooth.count {
                let cc = self.objDiary.objTeething.arrTooth[i]
                if cc.ID == objDiary.objTeething.ID {
                    selectedIndex = i
                }
            }
        }
        
        if appSceneDelegate.isArabic {
            tfDate.textAlignment = .right
            tfNotes.textAlignment = .right
        }
        lablTitle.text = lablTitle.text?.localized()
        lablDetail.text = lablDetail.text?.localized()
        lablNotes.text = lablNotes.text?.localized()
    
        btnAdd.setTitle(self.btnAdd.titleLabel?.text?.localized(), for: .normal)
        btnCancel.setTitle(self.btnCancel.titleLabel?.text?.localized(), for: .normal)
        btnRefrence.setTitle(self.btnRefrence.titleLabel?.text?.localized(), for: .normal)
    }

    override func viewDidAppear(_ animated: Bool) {
        let blurEffect = UIBlurEffect(style: .light)
        let blurredEffectView = UIVisualEffectView(effect: blurEffect)
        blurredEffectView.frame = self.view.bounds
        self.viewBg.addSubview(blurredEffectView)
    }
    
    func setCircular() {
        if SCREEN_WIDTH > 375 {
            circularLayout.initWithCentre(CGPoint(x: collectionTeeth.bounds.width/2, y: collectionTeeth.bounds.height/2), radius: collectionTeeth.bounds.width / 2 - 70 / 2, itemSize: CGSize(width: 39, height: 39), andAngularSpacing: 5)
        } else {
            circularLayout.initWithCentre(CGPoint(x: collectionTeeth.bounds.width/2, y: collectionTeeth.bounds.height/2), radius: collectionTeeth.bounds.width / 2 - 38 / 2, itemSize: CGSize(width: 38, height: 38), andAngularSpacing: 0)
        }
        circularLayout.setStartAngle(.pi * 2, endAngle: 0)
        circularLayout.mirrorX = true
        circularLayout.mirrorY = true
        circularLayout.rotateItems = true
        circularLayout.scrollDirection = .horizontal
        collectionTeeth.collectionViewLayout = circularLayout
    }
    
    func CheckFields() -> Bool {
        if (tfDate.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            
            AppUtility.showInfoMessage(message: "Please enter date")
            let test = false
            return test
        }
        else if selectedIndex == -1 {
            AppUtility.showInfoMessage(message: "Please select teeth")
            let test = false
            return test
        }
        
        let test = true
        return test
    }
    
    //MARK:- Apis functions
    func getTeeth() {
        self.view.endEditing(true)
        let paramDict: [String: Any] = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":AppUtility.getBabyId()]
        
        APIRequestUtil.getTeeth(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)
                print(swiftyJsonVar)
                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    let valueArray: [JSON] = dict["data"]!.arrayValue
                    for dict in valueArray {
                        let dictValue: Dictionary<String, JSON> = dict.dictionaryValue
                        let obj = Teeth.init(withDictionary: dictValue)
                        self.arrTeeth.append(obj)
                    }
                    AppManager.shared.arrTeeth = self.arrTeeth
                    self.collectionTeeth.reloadData()

                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    func addTeeth() {
        self.view.endEditing(true)
        var paramDict: [String: Any]?
        if self.isFromEdit {
            paramDict = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":AppUtility.getBabyId(), "teethingDate":tfDate.text!, "toothId":self.arrTeeth[selectedIndex].ID, "teethingNotes":tfNotes.text!, "id":self.arrTeeth[selectedIndex].ID]
        } else {
            paramDict = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":AppUtility.getBabyId(), "teethingDate":tfDate.text!, "toothId":self.arrTeeth[selectedIndex].ID, "teethingNotes":tfNotes.text!]
        }
        if isAudio {
            APIRequestUtil.addTeeth(parameters: paramDict!, data: audioDatas , extensionString: "audio/m4a", fileNameKey: "teethingMedia") { (result, error) in
                if(result != nil) {
                    let swiftyJsonVar = JSON(result!)

                    let dict = swiftyJsonVar.dictionaryValue
                    let status = dict["status"]?.intValue
                    let message = dict["message"]?.string
                    
                    if status == SUCESS_CODE {
                        
                        AppUtility.showSuccessMessage(message: message ?? "Add activity successfully!")
                        self.delegate?.addTeeth()
                        self.cancelTapped(self)
                        
                    } else {
                        
                        AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                        return
                    }
                }
            }
        } else {
            let data = media.jpegData(compressionQuality: 0.5)
            APIRequestUtil.addTeeth(parameters: paramDict!, data: data ?? Data(), extensionString: "image/jpeg", fileNameKey: "teethingMedia") { (result, error) in
                if(result != nil) {
                    let swiftyJsonVar = JSON(result!)

                    let dict = swiftyJsonVar.dictionaryValue
                    let status = dict["status"]?.intValue
                    let message = dict["message"]?.string
                    
                    if status == SUCESS_CODE {
                        
                        AppUtility.showSuccessMessage(message: message ?? "Add teeth successfully!")
                        self.delegate?.addTeeth()
                        self.cancelTapped(self)
                        
                    } else {
                        
                        AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                        return
                    }
                }
            }
        }
    }
    
    //MARK:- Button Action
    @IBAction func cancelTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addTapped(_ sender: Any) {
        if self.CheckFields() {
            self.addTeeth()
        }
    }
    
    @IBAction func dayTapped(_ sender: UIButton) {
        let datePicker = ActionSheetDatePicker(title: "Select Date", datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let date:NSDate = value as! NSDate
            
            self.tfDate.text = AppUtility.convertDateToString(date: date)
            
            return
            
        }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview!.superview)
        datePicker?.minimumDate = Date()
        datePicker?.show()
    }
    
    @IBAction func uploadMediaTapped(_ sender: UIButton) {
        if sender.tag == 0 {
            ImagePickerManager().pickImage(self) { image in
                self.isAudio = false
                self.media = image
            }
        } else {
            let viewController = AddAudioVC(nibName:String(describing: AddAudioVC.self), bundle:nil)
            viewController.delegate = self
            self.navigationController?.pushViewController(viewController, animated: false)
        }
    }
}

extension AddTeetchVC: AddAudioVCDelegate {
    func addAudio(audioData: Data, index: Int) {
        isAudio = true
        audioDatas = audioData
    }
}

// MARK: - CollectionView Delegate-
extension AddTeetchVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrTeeth.count
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return  CGSize(width: 40, height: 40)
//    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        collectionView.register(UINib(nibName: String(describing: TeethCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: TeethCell.self))
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: TeethCell.self), for: indexPath as IndexPath) as! TeethCell
        
        AppUtility.loadImage(imageView: cell.imgTeeth, urlString: self.arrTeeth[indexPath.row].image, placeHolderImageString: "")
        
        if selectedIndex == indexPath.row {
            cell.imgTeeth.alpha = 1
        } else {
            cell.imgTeeth.alpha = 0.3
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        collectionTeeth.reloadData()
    }
}
