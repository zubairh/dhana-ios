//
//  AddVacineVC.swift
//  Tifli
//
//  Created by zubair on 26/10/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import SwiftyJSON

protocol AddVacineVCDelegate {
    func addVaccine()
}

class AddVacineVC: UIViewController {

    @IBOutlet weak var tableVaccine: UITableView!
    @IBOutlet weak var layoutTableHeightVaccine: NSLayoutConstraint!
    @IBOutlet weak var tfDate: UITextField!
    @IBOutlet weak var tfTime: UITextField!
    @IBOutlet weak var tfHospital: UITextField!
    @IBOutlet weak var tfNotes: UITextField!
    @IBOutlet weak var viewBg: UIImageView!
    @IBOutlet weak var tableSelectBaby: UITableView!
    @IBOutlet weak var layoutTableBaby: NSLayoutConstraint!
    
    @IBOutlet weak var lablTitle: UILabel!
    @IBOutlet weak var lablMedical: UILabel!
    @IBOutlet weak var lablBaby: UILabel!
    @IBOutlet weak var lablNotes: UILabel!
    @IBOutlet weak var lablThisPart: UILabel!
    @IBOutlet weak var lablSetReminder: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnAdd: GradientButton!
    @IBOutlet weak var switchOneHour: UISwitch!
    @IBOutlet weak var switchOneDay: UISwitch!
    
    var selectedIndex = -1
    var selectedIndexVaccine = -1
    var arrVaccine = [Vaccine]()
    var babyID = ""
    var delegate:AddVacineVCDelegate? = nil
    var objDiary = Diary()
    var isFromEdit = false
    var oneHourBeforeReminder = 0
    var oneDayBeforeReminder = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isFromEdit {
            self.viewBg.backgroundColor = #colorLiteral(red: 0.9137254902, green: 0.9098039216, blue: 0.9490196078, alpha: 1)
            let dd = objDiary.date.components(separatedBy: " ")
            self.tfDate.text = dd[0]
            self.tfTime.text = dd[1]
            self.tfNotes.text = objDiary.notes
            self.tfHospital.text = objDiary.objVaccination.clinic
            
            if self.objDiary.objVaccination.oneHourBeforeReminder == 1 {
                switchOneHour.isOn = true
                oneHourBeforeReminder = 1
            } else {
                switchOneHour.isOn = false
                oneHourBeforeReminder = 0
            }
            if self.objDiary.objVaccination.oneDayBeforeReminder == 1 {
                switchOneDay.isOn = true
                oneDayBeforeReminder = 1
            } else {
                switchOneDay.isOn = false
                oneDayBeforeReminder = 0
            }
            
            for i in 0 ..< AppManager.shared.arrBabies.count {
                let obj = AppManager.shared.arrBabies[i]
                if AppUtility.getBabyId() == obj.babyID {
                    selectedIndex = i
                    babyID = obj.babyID
                    tableSelectBaby.reloadData()
                    break
                }
            }
        }
        
        tableSelectBaby.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
        tableVaccine.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
        
        if appSceneDelegate.isArabic {
            tfDate.textAlignment = .right
            tfTime.textAlignment = .right
            tfNotes.textAlignment = .right
            tfHospital.textAlignment = .right
        }
        lablTitle.text = lablTitle.text?.localized()
        lablThisPart.text = lablThisPart.text?.localized()
        lablSetReminder.text = lablSetReminder.text?.localized()
        lablNotes.text = lablNotes.text?.localized()
        lablBaby.text = lablBaby.text?.localized()
        lablMedical.text = lablMedical.text?.localized()
    
        btnAdd.setTitle(self.btnAdd.titleLabel?.text?.localized(), for: .normal)
        btnCancel.setTitle(self.btnCancel.titleLabel?.text?.localized(), for: .normal)
        
        self.getVaccine()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let blurEffect = UIBlurEffect(style: .light)
        let blurredEffectView = UIVisualEffectView(effect: blurEffect)
        blurredEffectView.frame = self.view.bounds
        self.viewBg.addSubview(blurredEffectView)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        layoutTableBaby.constant = tableSelectBaby.contentSize.height
        layoutTableHeightVaccine.constant = tableVaccine.contentSize.height
    }
    
    func CheckFields() -> Bool {
        var isVaccine = false
        for obj in self.arrVaccine {
            if !obj.isTaken {
                isVaccine = true
            }
        }
        if (tfDate.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            
            AppUtility.showInfoMessage(message: "Please enter date".localized())
            let test = false
            return test
        }
        else if (tfTime.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            AppUtility.showInfoMessage(message: "Please enter time".localized())
            let test = false
            return test
        }
        else if (tfHospital.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            AppUtility.showInfoMessage(message: "Please select hospital or clinic".localized())
            let test = false
            return test
        }
        else if babyID.count == 0 {
            AppUtility.showInfoMessage(message: "Please select baby".localized())
            let test = false
            return test
        }
        else if isVaccine {
            if selectedIndexVaccine == -1 {
                AppUtility.showInfoMessage(message: "Please select vaccine".localized())
                let test = false
                return test
            }
        } else {
            AppUtility.showInfoMessage(message: "you already have all vaccine dose".localized())
            let test = false
            return test
        }
        
        let test = true
        return test
    }
    
    //MARK:- Apis Function
    func getVaccine() {
        self.view.endEditing(true)
        let paramDict: [String: Any] = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":AppUtility.getBabyId()]
        
        APIRequestUtil.getVaccine(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    let valueArray: [JSON] = dict["data"]!.arrayValue
                    var i = 0
                    for dict in valueArray {
                        let dictValue: Dictionary<String, JSON> = dict.dictionaryValue
                        let obj = Vaccine.init(withDictionary: dictValue)
                        if obj.titles == self.objDiary.objVaccination.titles {
                            obj.isTaken = true
                            self.selectedIndexVaccine = i
                        }
                        i = i+1
                        self.arrVaccine.append(obj)
                    }
                    self.tableVaccine.reloadData()

                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    func addVaccine() {
        self.view.endEditing(true)
        var paramDict: [String: Any]?
        if isFromEdit {
            paramDict = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":self.babyID, "date":tfDate.text!+" "+tfTime.text!, "notes":tfNotes.text!, "clinic":tfHospital.text!, "vaccinationId":self.arrVaccine[selectedIndexVaccine].ID, "id":objDiary.objVaccination.ID, "oneHourBeforeReminder":oneHourBeforeReminder, "oneDayBeforeReminder":oneDayBeforeReminder]
        } else {
            paramDict = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":self.babyID, "date":tfDate.text!+" "+tfTime.text!, "notes":tfNotes.text!, "clinic":tfHospital.text!, "vaccinationId":self.arrVaccine[selectedIndexVaccine].ID, "oneHourBeforeReminder":oneHourBeforeReminder, "oneDayBeforeReminder":oneDayBeforeReminder]
        }
        APIRequestUtil.AddBabyVaccine(parameters: paramDict!, isEdit: self.isFromEdit) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string

                if status == SUCESS_CODE {

                    AppUtility.showSuccessMessage(message: "Vaccine added successfully".localized())
                    self.delegate?.addVaccine()
                    self.cancelTapped(self)

                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    //MARK:- Button Action
    @IBAction func cancelTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addTapped(_ sender: Any) {
        if self.CheckFields() {
            self.addVaccine()
        }
    }
    
    @IBAction func dayTapped(_ sender: UIButton) {
        let datePicker = ActionSheetDatePicker(title: "Select Date".localized(), datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let date:NSDate = value as! NSDate
            
            self.tfDate.text = AppUtility.convertDateToString(date: date)
            
            return
            
        }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview!.superview)
        datePicker?.minimumDate = Date()
        datePicker?.show()
    }
    
    @IBAction func timeTapped(_ sender: UIButton) {
        let datePicker = ActionSheetDatePicker(title: "Select Time".localized(), datePickerMode: UIDatePicker.Mode.time, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let date:NSDate = value as! NSDate
            
            self.tfTime.text = AppUtility.convertDateToStringTime(date: date)
            
            return
            
        }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview!.superview)
        datePicker?.show()
    }
    
    @IBAction func setSwitchBabyRoutineTapped(_ sender: UISwitch) {
        if sender.isOn {
            oneHourBeforeReminder = 1
        } else {
            oneHourBeforeReminder = 0
        }
    }
    
    @IBAction func setReminderTapped(_ sender: UISwitch) {
        if sender.isOn {
            oneDayBeforeReminder = 1
        } else {
            oneDayBeforeReminder = 0
        }
    }
}

// MARK: - Tableview Delegate-
extension AddVacineVC : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableVaccine {
            return self.arrVaccine.count
        }
        return AppManager.shared.arrBabies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tableVaccine {
            
            tableView.register(UINib(nibName: "AddVaccineCell", bundle: nil), forCellReuseIdentifier: "AddVaccineCell")
            var cell : AddVaccineCell! = tableView.dequeueReusableCell(withIdentifier: "AddVaccineCell") as? AddVaccineCell
            
            if (cell == nil) {
                cell = AddVaccineCell.init(style: UITableViewCell.CellStyle.default,
                                              reuseIdentifier:"AddVaccineCell");
            }
            
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            
            let obj = self.arrVaccine[indexPath.row]
            cell.lablTitle.text = obj.titles
            cell.lablDetail.text = obj.detail
            if obj.isTaken {
                cell.lablStatus.text = "Taken".localized()
                cell.imgTick.image = UIImage(named: "tick")
            } else {
                cell.lablStatus.text = "Pending".localized()
                if self.selectedIndexVaccine == indexPath.row {
                    cell.imgTick.image = UIImage(named: "tick")
                } else {
                    cell.imgTick.image = UIImage(named: "circle")
                }
            }
            
            return cell
            
        } else {
            
            tableView.register(UINib(nibName: "BabyCaregiverCell", bundle: nil), forCellReuseIdentifier: "BabyCaregiverCell")
            var cell : BabyCaregiverCell! = tableView.dequeueReusableCell(withIdentifier: "BabyCaregiverCell") as? BabyCaregiverCell
            
            if (cell == nil) {
                cell = BabyCaregiverCell.init(style: UITableViewCell.CellStyle.default,
                                              reuseIdentifier:"BabyCaregiverCell");
            }
            
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            
            let obj = AppManager.shared.arrBabies[indexPath.row]
            cell.lablTitle.text = obj.babyName
            
            if indexPath.row == selectedIndex {
                cell.imgCheck.image = UIImage(named: "checbox fill")
            } else {
                cell.imgCheck.image = UIImage(named: "checbox")
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tableVaccine {
            self.selectedIndexVaccine = indexPath.row
            tableView.reloadData()
        } else {
            selectedIndex = indexPath.row
            self.babyID = AppManager.shared.arrBabies[indexPath.row].babyID
            tableView.reloadData()
        }
    }
}
