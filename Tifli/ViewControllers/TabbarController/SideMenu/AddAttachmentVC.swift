//
//  AddAttachmentVC.swift
//  Tifli
//
//  Created by zubair on 31/08/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol AddAttachmentVCDelegate {
    func reloadBabyProfiles()
}

class AddAttachmentVC: UIViewController {

    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lablTitle: UILabel!
    @IBOutlet weak var lablTitleField: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnAdd: GradientButton!
    
    var isFromDocument = false
    var babyId = "0"
    var isFromEdit = false
    var isFromTwins = false
    var delegate:AddAttachmentVCDelegate? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isFromDocument {
            lablTitle.text = "Document".localized()
            lablTitleField.text = "Document name".localized()
        } else {
            lablTitle.text = lablTitle.text?.localized()
            lablTitleField.text = lablTitleField.text?.localized()
        }
        
        if appSceneDelegate.isArabic {
            tfName.textAlignment = .right
            btnBack.setImage(UIImage(named: BACK_AR), for: .normal)
        }
        
        btnAdd.setTitle(btnAdd.titleLabel?.text?.localized(), for: .normal)
    }
    
    //MARK:- Apis Function
    func addDocument() {
        self.view.endEditing(true)
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":babyId, "documentTitle":tfName.text!]
        let data = imgProduct.image?.jpegData(compressionQuality: 0.5)
        APIRequestUtil.addDocument(parameters: paramDict, data: data ?? Data(), extensionString: "image/jpeg", fileNameKey: "documentFile") { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.delegate?.reloadBabyProfiles()
                    AppUtility.showSuccessMessage(message: message ?? "Add document successfully!".localized())
                    self.navigationController?.popViewController(animated: true)
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    func addToys() {
        self.view.endEditing(true)
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":babyId, "name":tfName.text!]
        let data = imgProduct.image?.jpegData(compressionQuality: 0.5)
        APIRequestUtil.addToys(parameters: paramDict, data: data ?? Data(), extensionString: "image/jpeg", fileNameKey: "image") { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.delegate?.reloadBabyProfiles()
                    AppUtility.showSuccessMessage(message: message ?? "Add toy successfully!".localized())
                    self.navigationController?.popViewController(animated: true)
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }

    //MARK:- Button Actions
    @IBAction func libraryTapped(_ sender: Any) {
        ImagePickerManager().pickImage(self) { image in
            self.imgProduct.image = image
        }
    }
    
    @IBAction func cameraTapped(_ sender: Any) {
        ImagePickerManager().pickImage(self) { image in
            self.imgProduct.image = image
        }
    }
    
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addTapped(_ sender: Any) {
        if self.imgProduct.image == nil {
            AppUtility.showInfoMessage(message: "Please select image".localized())
            return
        }
        if tfName.text?.count == 0 {
            AppUtility.showInfoMessage(message: "Please add name".localized())
            return
        }
        
        if isFromEdit {
            if isFromDocument {
                self.addDocument()
            } else {
                self.addToys()
            }
        } else {
            if self.isFromTwins {
                if isFromDocument {
                    AppManager.shared.arrBabyDocumentTitleTwins.add(tfName.text!)
                    AppManager.shared.arrBabyDocumentTwins.append(self.imgProduct.image!)
                } else {
                    AppManager.shared.arrBabyToysTitleTwins.add(tfName.text!)
                    AppManager.shared.arrBabyToysTwins.append(self.imgProduct.image!)
                }
            } else {
                if isFromDocument {
                    AppManager.shared.arrBabyDocumentTitle.add(tfName.text!)
                    AppManager.shared.arrBabyDocument.append(self.imgProduct.image!)
                } else {
                    AppManager.shared.arrBabyToysTitle.add(tfName.text!)
                    AppManager.shared.arrBabyToys.append(self.imgProduct.image!)
                }
            }
            self.navigationController?.popViewController(animated: true)
        }
    }
}
