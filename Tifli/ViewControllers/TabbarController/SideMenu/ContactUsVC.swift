//
//  ContactUsVC.swift
//  Tifli
//
//  Created by zubair on 01/08/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

class ContactUsVC: UIViewController {

    @IBOutlet weak var lablTitle: UILabel!
    @IBOutlet weak var lablSubject: UILabel!
    @IBOutlet weak var lablMessage: UILabel!
    @IBOutlet weak var lablEmail: UILabel!
    @IBOutlet weak var lablPhone: UILabel!
    @IBOutlet weak var lablInsta: UILabel!
    @IBOutlet weak var lablYoutube: UILabel!
    @IBOutlet weak var lablFb: UILabel!
    @IBOutlet weak var tfSubject: UITextField!
    @IBOutlet weak var textMessage: UITextView!
    @IBOutlet weak var btnSend: GradientButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lablTitle.text = lablTitle.text?.localized()
        lablSubject.text = lablSubject.text?.localized()
        lablMessage.text = lablMessage.text?.localized()
        
        if appSceneDelegate.isArabic {
            tfSubject.textAlignment = .right
            textMessage.textAlignment = .right
        }
        
        btnSend.setTitle(btnSend.titleLabel?.text?.localized(), for: .normal)
        
        self.navigationController?.isNavigationBarHidden = true
        
        self.getUserSettings()
    }
    
    //MARK:- Api Functions
    func getUserSettings() {
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken()]
        APIRequestUtil.getContactDetail(parameters: paramDict) { (result, error) in
            if(result != nil) {
                
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                print(dict)
                
                if status == SUCESS_CODE {

                    let dictionary: Dictionary<String, JSON> = dict["data"]!.dictionaryValue
                    if let email = dictionary["email"]?.stringValue {
                        self.lablEmail.text = email
                    }
                    if let phone = dictionary["phone"]?.stringValue {
                        self.lablPhone.text = phone
                    }
                    if let instagram = dictionary["instagram"]?.stringValue {
                        self.lablInsta.text = instagram
                    }
                    if let youtube = dictionary["youtube"]?.stringValue {
                        self.lablYoutube.text = youtube
                    }
                    if let facebook = dictionary["facebook"]?.stringValue {
                        self.lablFb.text = facebook
                    }
                    
                } else {

                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    func saveContactUsMessage() {
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "subject":tfSubject.text!, "message":textMessage.text!]
        APIRequestUtil.addContactUs(parameters: paramDict) { (result, error) in
            if(result != nil) {
                
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
            
                if status == SUCESS_CODE {

                    AppUtility.showSuccessMessage(message: message ?? "Thanks for contacting us".localized())
                    
                } else {

                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }

    //MARK:- Button Actions
    @IBAction func sendTapped(_ sender: Any) {
        if tfSubject.text?.count == 0 && textMessage.text.count == 0 {
            AppUtility.showInfoMessage(message: "Please input your subject and message".localized())
            return
        }
        self.saveContactUsMessage()
    }
    
    @IBAction func sideMenuTapped(_ sender: Any) {
        self.sideMenuViewController.presentRightMenuViewController()
    }
    
}
