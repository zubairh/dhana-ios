//
//  AddBabyVC.swift
//  Tifli
//
//  Created by zubair on 02/08/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import SwiftyJSON


protocol AddBabyVCDelegate {
    func babyReloadData()
}

class AddBabyVC: UIViewController {

    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var tfBabyType: UITextField!
    @IBOutlet weak var tfDob: UITextField!
    @IBOutlet weak var tfRelation: UITextField!
    @IBOutlet weak var tfPregnancy: UITextField!
    @IBOutlet weak var collectionMedical: UICollectionView!
    @IBOutlet weak var collectionAllergies: UICollectionView!
    @IBOutlet weak var collectionFavoriteFood: UICollectionView!
    @IBOutlet weak var collectionFavoriteToyes: UICollectionView!
    @IBOutlet weak var collectionDocuument: UICollectionView!
    @IBOutlet weak var collectionVaccine: UICollectionView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var btnGender: UIButton!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var layoutHeightCollectionMedical: NSLayoutConstraint!
    @IBOutlet weak var layoutHeightCollectionAlergy: NSLayoutConstraint!
    @IBOutlet weak var layoutHeightCollectionFood: NSLayoutConstraint!
    @IBOutlet weak var layoutHeightCollectionToys: NSLayoutConstraint!
    @IBOutlet weak var layoutHeightCollectionDocument: NSLayoutConstraint!
    @IBOutlet weak var layoutHeightCollectionVaccine: NSLayoutConstraint!
    @IBOutlet weak var viewOther: UIView!
    @IBOutlet weak var tfOther: UITextField!
    @IBOutlet weak var layoutOtherHeight: NSLayoutConstraint!
    @IBOutlet weak var viewTwins: UIStackView!
    @IBOutlet weak var layoutHeightPreterm: NSLayoutConstraint!
    
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lablTitle: UILabel!
    
    @IBOutlet weak var lablName: UILabel!
    @IBOutlet weak var lablType: UILabel!
    @IBOutlet weak var lablGender: UILabel!
    @IBOutlet weak var lablDobTitle: UILabel!
    @IBOutlet weak var lablWeekPregnancyTitle: UILabel!
    @IBOutlet weak var lablRelation: UILabel!
    @IBOutlet weak var lablChronic: UILabel!
    @IBOutlet weak var llablAlergy: UILabel!
    @IBOutlet weak var lablFavToys: UILabel!
    @IBOutlet weak var lablFavFood: UILabel!
    @IBOutlet weak var lablDoc: UILabel!
    @IBOutlet weak var lablCaregiver: UILabel!
    
    @IBOutlet weak var lablNameTwins: UILabel!
    @IBOutlet weak var lablGenderTwins: UILabel!
    @IBOutlet weak var lablDobTitleTwins: UILabel!
    @IBOutlet weak var lablWeekPregnancyTitleTwins: UILabel!
    @IBOutlet weak var lablRelationTwins: UILabel!
    @IBOutlet weak var lablChronicTwins: UILabel!
    @IBOutlet weak var llablAlergyTwins: UILabel!
    @IBOutlet weak var lablFavToysTwins: UILabel!
    @IBOutlet weak var lablFavFoodTwins: UILabel!
    @IBOutlet weak var lablDocTwins: UILabel!
    @IBOutlet weak var lablCaregiverTwins: UILabel!
    
    @IBOutlet weak var tfNameTwins: UITextField!
    @IBOutlet weak var tfDobTwins: UITextField!
    @IBOutlet weak var tfRelationTwins: UITextField!
    @IBOutlet weak var tfPregnancyTwins: UITextField!
    @IBOutlet weak var collectionMedicalTwins: UICollectionView!
    @IBOutlet weak var collectionAllergiesTwins: UICollectionView!
    @IBOutlet weak var collectionFavoriteFoodTwins: UICollectionView!
    @IBOutlet weak var collectionFavoriteToyesTwins: UICollectionView!
    @IBOutlet weak var collectionDocuumentTwins: UICollectionView!
    @IBOutlet weak var collectionVaccineTwins: UICollectionView!
    @IBOutlet weak var segmentControlTwins: UISegmentedControl!
    @IBOutlet weak var btnGenderTwins: UIButton!
    @IBOutlet weak var imgUserTwins: UIImageView!
    @IBOutlet weak var layoutHeightCollectionMedicalTwins: NSLayoutConstraint!
    @IBOutlet weak var layoutHeightCollectionAlergyTwins: NSLayoutConstraint!
    @IBOutlet weak var layoutHeightCollectionFoodTwins: NSLayoutConstraint!
    @IBOutlet weak var layoutHeightCollectionToysTwins: NSLayoutConstraint!
    @IBOutlet weak var layoutHeightCollectionDocumentTwins: NSLayoutConstraint!
    @IBOutlet weak var layoutHeightCollectionVaccineTwins: NSLayoutConstraint!
    @IBOutlet weak var viewOtherTwins: UIView!
    @IBOutlet weak var tfOtherTwins: UITextField!
    @IBOutlet weak var layoutOtherHeightTwins: NSLayoutConstraint!
    @IBOutlet weak var layoutHeightPretermTwin: NSLayoutConstraint!
    
    var babyGender: String?
    var babyGenderTwins: String?
    var babyDeliveryType:String?
    var arrRelation = [BabyType]()
    var relationID = ""
    var delegate:AddBabyVCDelegate? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lablTitle.text = lablTitle.text?.localized()
        lablType.text = lablType.text?.localized()
        
        lablName.text = lablName.text?.localized()
        lablGender.text = lablGender.text?.localized()
        lablDobTitle.text = lablDobTitle.text?.localized()
        lablWeekPregnancyTitle.text = lablWeekPregnancyTitle.text?.localized()
        lablRelation.text = lablRelation.text?.localized()
        lablChronic.text = lablChronic.text?.localized()
        llablAlergy.text = llablAlergy.text?.localized()
        lablFavToys.text = lablFavToys.text?.localized()
        lablFavFood.text = lablFavFood.text?.localized()
        lablDoc.text = lablDoc.text?.localized()
        lablCaregiver.text = lablCaregiver.text?.localized()
        
        lablNameTwins.text = lablNameTwins.text?.localized()
        lablGenderTwins.text = lablGenderTwins.text?.localized()
        lablDobTitleTwins.text = lablDobTitleTwins.text?.localized()
        lablWeekPregnancyTitleTwins.text = lablWeekPregnancyTitleTwins.text?.localized()
        lablRelationTwins.text = lablRelationTwins.text?.localized()
        lablChronicTwins.text = lablChronicTwins.text?.localized()
        llablAlergyTwins.text = llablAlergyTwins.text?.localized()
        lablFavToysTwins.text = lablFavToysTwins.text?.localized()
        lablFavFoodTwins.text = lablFavFoodTwins.text?.localized()
        lablDocTwins.text = lablDocTwins.text?.localized()
        lablCaregiverTwins.text = lablCaregiverTwins.text?.localized()
        
        if appSceneDelegate.isArabic {
            tfBabyType.textAlignment = .right
            
            tfName.textAlignment = .right
            tfDob.textAlignment = .right
            tfPregnancy.textAlignment = .right
            tfRelation.textAlignment = .right
            tfOther.textAlignment = .right
            
            tfNameTwins.textAlignment = .right
            tfDobTwins.textAlignment = .right
            tfPregnancyTwins.textAlignment = .right
            tfRelationTwins.textAlignment = .right
            tfOtherTwins.textAlignment = .right
            
            btnBack.setImage(UIImage(named: BACK_AR), for: .normal)
        }
        
        btnSave.setTitle(btnSave.titleLabel?.text?.localized(), for: .normal)
        
        layoutHeightPretermTwin.constant = 0
        layoutHeightPreterm.constant = 0
        
        viewTwins.isHidden = true
        
        let titleTextAttributesNormal = [NSAttributedString.Key.foregroundColor: UIColor.lightGray]
        let titleTextAttributesSelected = [NSAttributedString.Key.foregroundColor: UIColor.white]
        segmentControl.setTitleTextAttributes(titleTextAttributesNormal, for: .normal)
        segmentControl.setTitleTextAttributes(titleTextAttributesSelected, for: .selected)
        
        segmentControl.setTitleColor(.lightGray, state: .normal)
        segmentControl.setTitleColor(.white, state: .selected)
        segmentControl.setTitleFont(UIFont(name: "OpenSans-SemiBold", size: 13)!)
        
        segmentControlTwins.setTitleTextAttributes(titleTextAttributesNormal, for: .normal)
        segmentControlTwins.setTitleTextAttributes(titleTextAttributesSelected, for: .selected)
        
        segmentControlTwins.setTitleColor(.lightGray, state: .normal)
        segmentControlTwins.setTitleColor(.white, state: .selected)
        segmentControlTwins.setTitleFont(UIFont(name: "OpenSans-SemiBold", size: 13)!)
        
        self.tfDob.text = AppUtility.convertDateToString(date: NSDate())
        babyGender = "boy"
        babyGenderTwins = "boy"
        babyDeliveryType = "fullterm"
        self.tfBabyType.text = "Single"
        self.layoutOtherHeight.constant = 0
        self.viewOther.isHidden = true
        self.layoutOtherHeightTwins.constant = 0
        self.viewOtherTwins.isHidden = true
        
        collectionMedical.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
        collectionAllergies.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
        collectionFavoriteFood.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
        collectionFavoriteToyes.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
        collectionDocuument.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
        collectionVaccine.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
        
        // for twins
        collectionMedicalTwins.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
        collectionAllergiesTwins.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
        collectionFavoriteFoodTwins.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
        collectionFavoriteToyesTwins.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
        collectionDocuumentTwins.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
        collectionVaccineTwins.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
        
        self.getRelation()
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        layoutHeightCollectionMedical.constant = collectionMedical.contentSize.height
        layoutHeightCollectionAlergy.constant = collectionAllergies.contentSize.height
        layoutHeightCollectionFood.constant = collectionFavoriteFood.contentSize.height
        layoutHeightCollectionToys.constant = collectionFavoriteToyes.contentSize.height
        layoutHeightCollectionDocument.constant = collectionDocuument.contentSize.height
        layoutHeightCollectionVaccine.constant = collectionVaccine.contentSize.height
        
        // for twins
        layoutHeightCollectionMedicalTwins.constant = collectionMedicalTwins.contentSize.height
        layoutHeightCollectionAlergyTwins.constant = collectionAllergiesTwins.contentSize.height
        layoutHeightCollectionFoodTwins.constant = collectionFavoriteFoodTwins.contentSize.height
        layoutHeightCollectionToysTwins.constant = collectionFavoriteToyesTwins.contentSize.height
        layoutHeightCollectionDocumentTwins.constant = collectionDocuumentTwins.contentSize.height
        layoutHeightCollectionVaccineTwins.constant = collectionVaccineTwins.contentSize.height
    }
    
    override func viewWillAppear(_ animated: Bool) {
        collectionMedical.reloadData()
        collectionAllergies.reloadData()
        collectionFavoriteFood.reloadData()
        collectionVaccine.reloadData()
        collectionFavoriteToyes.reloadData()
        collectionDocuument.reloadData()
        
        // for twins
        collectionMedicalTwins.reloadData()
        collectionAllergiesTwins.reloadData()
        collectionFavoriteFoodTwins.reloadData()
        collectionVaccineTwins.reloadData()
        collectionFavoriteToyesTwins.reloadData()
        collectionDocuumentTwins.reloadData()
    }
    
    //MARK: - Functions
    @objc func checkTF() {
        if tfPregnancy.text?.count == 2 {
            self.view.endEditing(true)
        }
    }
    
    //MARK:- Apis Functions
    func getRelation() {
        self.view.endEditing(true)
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN]
        APIRequestUtil.getRelationList(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.arrRelation.removeAll()
                    
                    let valueArray: [JSON] = dict["relationships"]!.arrayValue
                    for dict in valueArray {
                        let dictValue: Dictionary<String, JSON> = dict.dictionaryValue
                        let obj = BabyType.init(withDictionary: dictValue)
                        self.arrRelation.append(obj)
                    }
                    let obj = BabyType()
                    obj.typeTitle = "Other"
                    obj.ID = "0"
                    self.arrRelation.append(obj)

                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    //MARK:- Apis Functions
    func addBaby(jsonString:String) {
        
        self.view.endEditing(true)
        
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "param":jsonString, "preference":AppUtility.getUserLoggedIn().preference, "toysTitle":AppManager.shared.arrBabyToysTitle.componentsJoined(by: ","), "documentsTtle":AppManager.shared.arrBabyDocumentTitle.componentsJoined(by: ","), "twinBabyToys":AppManager.shared.arrBabyToysTitleTwins.componentsJoined(by: ","), "twinDocumentsTitle":AppManager.shared.arrBabyDocumentTitleTwins.componentsJoined(by: ",")]
        let data = self.imgUser.image?.jpegData(compressionQuality: 0.5)
        let dataTwins = self.imgUserTwins.image?.jpegData(compressionQuality: 0.5)
        APIRequestUtil.addNewBabyProfile(parameters: paramDict, data: data ?? Data(), extensionString: "image/jpeg", fileNameKey: "babyImage", dataTwins: dataTwins ?? Data(), extensionStringTwins: "image/jpeg", fileNameKeyTwins: "twinBabyImage", arrDocument: AppManager.shared.arrBabyDocument, arrToys: AppManager.shared.arrBabyToys) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    AppUtility.showSuccessMessage(message: message ?? "Baby added successfully!".localized())
                    self.delegate?.babyReloadData()
                    self.backTapped(self)
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    //MARK:- Button Actions
    @IBAction func profileChangeTapped(_ sender: Any) {
        ImagePickerManager().pickImage(self) { image in
            self.imgUser.image = image
        }
    }
    
    @IBAction func profileChangeTwinsTapped(_ sender: Any) {
        ImagePickerManager().pickImage(self) { image in
            self.imgUserTwins.image = image
        }
    }
    
    @IBAction func saveTapped(_ sender: Any) {
        let dicBaby = NSMutableDictionary()
        dicBaby.setValue(tfName.text, forKey: "babyNameEn")
        dicBaby.setValue(babyGender, forKey: "gender")
        dicBaby.setValue(tfDob.text, forKey: "dob")
        dicBaby.setValue(tfDob.text, forKey: "dueDate")
        dicBaby.setValue(relationID, forKey: "relationId")
        dicBaby.setValue(tfPregnancy.text, forKey: "babyPregnancyWeeks")
        dicBaby.setValue("born", forKey: "bornType")
        //dicBaby.setValue(babyDeliveryType, forKey: "babyDeliveryType")
        dicBaby.setValue("yes", forKey: "firstLabour")
        dicBaby.setValue(tfBabyType.text?.lowercased(), forKey: "babyType")
        if relationID == "0" {
            dicBaby.setValue(tfOther.text, forKey: "babyRelationTitle")
        }
        dicBaby.setValue(AppManager.shared.arrBabyMedical, forKey: "medicalConditionId")
        dicBaby.setValue(AppManager.shared.arrBabyVaccine, forKey: "vaccinationId")
        dicBaby.setValue(AppManager.shared.arrBabyAllergy, forKey: "allergyId")
        dicBaby.setValue(AppManager.shared.arrBabyFoods, forKey: "favFood")
        
        if self.tfBabyType.text == "Twin" {
            dicBaby.setValue(tfNameTwins.text!, forKey: "twinBabyName")
            dicBaby.setValue(babyGenderTwins, forKey: "twinGender")
            dicBaby.setValue(AppManager.shared.arrBabyMedicalTwins, forKey: "twinMedicalConditions")
            dicBaby.setValue(AppManager.shared.arrBabyVaccineTwins, forKey: "twinVaccinations")
            dicBaby.setValue(AppManager.shared.arrBabyAllergyTwins, forKey: "twinAllergies")
            dicBaby.setValue(AppManager.shared.arrBabyFoodsTwins, forKey: "twinFavFoods")
        }
        
        let jsonData = try! JSONSerialization.data(withJSONObject: dicBaby, options: JSONSerialization.WritingOptions.prettyPrinted)
        let json = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)!
        
        print(json)
        self.addBaby(jsonString: json as String)
    }
    
    @IBAction func segmentTapped(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            babyDeliveryType = "preterm"
            self.segmentControlTwins.selectedSegmentIndex = 0
        } else {
            babyDeliveryType = "fullterm"
            self.segmentControlTwins.selectedSegmentIndex = 1
        }
    }
    
    @IBAction func chooseBabyTapped(_ sender: UIButton) {
        if sender.tag == 0 {
            sender.tag = 1
            sender.isSelected = true
            babyGender = "boy"
        } else {
            sender.tag = 0
            sender.isSelected = false
            babyGender = "girl"
        }
    }
    
    @IBAction func backTapped(_ sender: Any) {
        
        AppManager.shared.babyName = ""
        AppManager.shared.babyGender = ""
        AppManager.shared.babyImage = UIImage()
        AppManager.shared.babyDOB = ""
        AppManager.shared.babyDeliveryType = ""
        AppManager.shared.babyWeekPregnancy = ""
        AppManager.shared.otherRelationValue = ""
        
        AppManager.shared.arrBabyFoods = NSMutableArray.init()
        AppManager.shared.arrBabyToysTitle = NSMutableArray.init()
        AppManager.shared.arrBabyDocumentTitle = NSMutableArray.init()
        
        AppManager.shared.arrBabyVacc = [BabyType]()
        AppManager.shared.arrBabyMed = [BabyType]()
        AppManager.shared.arrBabyAller = [BabyType]()
        AppManager.shared.arrBabyRel = [BabyType]()
        AppManager.shared.arrBabyFood = [BabyType]()
        AppManager.shared.arrBabyToys = [UIImage]()
        AppManager.shared.arrBabyDocument = [UIImage]()
        
        //for twins
        AppManager.shared.babyNameTwins = ""
        AppManager.shared.babyGenderTwins = ""
        AppManager.shared.babyImageTwins = UIImage()
        
        AppManager.shared.arrBabyFoodsTwins = NSMutableArray.init()
        AppManager.shared.arrBabyToysTitleTwins = NSMutableArray.init()
        AppManager.shared.arrBabyDocumentTitleTwins = NSMutableArray.init()
        
        AppManager.shared.arrBabyVaccTwins = [BabyType]()
        AppManager.shared.arrBabyMedTwins = [BabyType]()
        AppManager.shared.arrBabyAllerTwins = [BabyType]()
        AppManager.shared.arrBabyRelTwins = [BabyType]()
        AppManager.shared.arrBabyFoodTwins = [BabyType]()
        AppManager.shared.arrBabyToysTwins = [UIImage]()
        AppManager.shared.arrBabyDocumentTwins = [UIImage]()
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func babyTypeTapped(_ sender: UIButton) {
        let arr = ["Single".localized(), "Twin".localized()]
        ActionSheetStringPicker.show(withTitle: "Select Type".localized(), rows: arr, initialSelection: 0, doneBlock: {
            picker, indexes, values in
            
            self.tfBabyType.text = values as? String
            if indexes == 1 {
                self.viewTwins.isHidden = false
            } else {
                self.viewTwins.isHidden = true
            }

            return
        }, cancel: { ActionSheetStringPicker in return},origin: sender)
    }
    
    @IBAction func dobTapped(_ sender: UIButton) {
        let datePicker = ActionSheetDatePicker(title: title, datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let date:NSDate = value as! NSDate
            self.tfDob.text = AppUtility.convertDateToString(date: date)
            self.tfDobTwins.text = AppUtility.convertDateToString(date: date)
            
            return
            
        }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview!.superview)
        datePicker?.maximumDate = Date()
        datePicker?.show()
    }
    
    @IBAction func relationTappe(_ sender: Any) {
        var arr = [String]()
        for obj in self.arrRelation {
            arr.append(obj.typeTitle)
        }
        ActionSheetStringPicker.show(withTitle: "Select Relation".localized(), rows: arr, initialSelection: 0, doneBlock: {
            picker, indexes, values in
            
            self.tfRelation.text = values as? String
            self.tfRelationTwins.text = values as? String
            self.relationID = self.arrRelation[indexes].ID
            
            if indexes == self.arrRelation.count-1 {
                self.layoutOtherHeight.constant = 40
                self.viewOther.isHidden = false
                self.layoutOtherHeightTwins.constant = 40
                self.viewOtherTwins.isHidden = false
            } else {
                self.layoutOtherHeight.constant = 0
                self.viewOther.isHidden = true
                self.layoutOtherHeightTwins.constant = 0
                self.viewOtherTwins.isHidden = true
            }

            return
        }, cancel: { ActionSheetStringPicker in return},origin: sender)
    }
    
    @IBAction func addMmedicalTapped(_ sender: Any) {
        let viewController = AddBabyTypeVC(nibName:String(describing: AddBabyTypeVC.self), bundle:nil)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func addAlergiesTapped(_ sender: Any) {
        let viewController = AddBabyTypeVC(nibName:String(describing: AddBabyTypeVC.self), bundle:nil)
        viewController.isFromAlergy = true
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func addFoodTapped(_ sender: Any) {
        let viewController = AddFoodVC(nibName:String(describing: AddFoodVC.self), bundle:nil)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func addToyTapped(_ sender: Any) {
        let viewController = AddAttachmentVC(nibName:String(describing: AddAttachmentVC.self), bundle:nil)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func addDocumentTapped(_ sender: Any) {
        let viewController = AddAttachmentVC(nibName:String(describing: AddAttachmentVC.self), bundle:nil)
        viewController.isFromDocument = true
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func addVaccineTapped(_ sender: Any) {
        let viewController = AddBabyTypeVC(nibName:String(describing: AddBabyTypeVC.self), bundle:nil)
        viewController.isFromVaccine = true
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    // for Twins
    @IBAction func addMmedicalTwinsTapped(_ sender: Any) {
        let viewController = AddBabyTypeVC(nibName:String(describing: AddBabyTypeVC.self), bundle:nil)
        viewController.isFromTwins = true
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func addAlergiesTwinsTapped(_ sender: Any) {
        let viewController = AddBabyTypeVC(nibName:String(describing: AddBabyTypeVC.self), bundle:nil)
        viewController.isFromAlergy = true
        viewController.isFromTwins = true
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func addFoodTwinsTapped(_ sender: Any) {
        let viewController = AddFoodVC(nibName:String(describing: AddFoodVC.self), bundle:nil)
        viewController.isFromTwins = true
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func addToyTwinsTapped(_ sender: Any) {
        let viewController = AddAttachmentVC(nibName:String(describing: AddAttachmentVC.self), bundle:nil)
        viewController.isFromTwins = true
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func addDocumentTwinsTapped(_ sender: Any) {
        let viewController = AddAttachmentVC(nibName:String(describing: AddAttachmentVC.self), bundle:nil)
        viewController.isFromDocument = true
        viewController.isFromTwins = true
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func addVaccineTwinsTapped(_ sender: Any) {
        let viewController = AddBabyTypeVC(nibName:String(describing: AddBabyTypeVC.self), bundle:nil)
        viewController.isFromVaccine = true
        viewController.isFromTwins = true
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func genderTwinsTapped(_ sender: UIButton) {
        if sender.tag == 0 {
            sender.tag = 1
            sender.isSelected = true
            babyGenderTwins = "boy"
        } else {
            sender.tag = 0
            sender.isSelected = false
            babyGenderTwins = "girl"
        }
    }
    
}

// MARK: - CollectionView Delegate-
extension AddBabyVC: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //if self.tfBabyType.text == "Twin" {
            if collectionView == collectionMedicalTwins {
                return AppManager.shared.arrBabyMedTwins.count
            } else if collectionView == collectionAllergiesTwins {
                return AppManager.shared.arrBabyAllerTwins.count
            } else if collectionView == collectionVaccineTwins {
                return AppManager.shared.arrBabyVaccTwins.count
            } else if collectionView == collectionFavoriteFoodTwins {
                return AppManager.shared.arrBabyFoodsTwins.count
            } else if collectionView == collectionFavoriteToyesTwins {
                return AppManager.shared.arrBabyToysTwins.count
            } else if collectionView == collectionDocuumentTwins {
                return AppManager.shared.arrBabyDocumentTwins.count
            } else if collectionView == collectionMedical {
                return AppManager.shared.arrBabyMed.count
            } else if collectionView == collectionAllergies {
                return AppManager.shared.arrBabyAller.count
            } else if collectionView == collectionVaccine {
                return AppManager.shared.arrBabyVacc.count
            } else if collectionView == collectionFavoriteFood {
                return AppManager.shared.arrBabyFoods.count
            } else if collectionView == collectionFavoriteToyes {
                return AppManager.shared.arrBabyToys.count
            }
            return AppManager.shared.arrBabyDocument.count
        //}
//        else {
//            if collectionView == collectionMedical {
//                return AppManager.shared.arrBabyMed.count
//            } else if collectionView == collectionAllergies {
//                return AppManager.shared.arrBabyAller.count
//            } else if collectionView == collectionVaccine {
//                return AppManager.shared.arrBabyVacc.count
//            } else if collectionView == collectionFavoriteFood {
//                return AppManager.shared.arrBabyFoods.count
//            } else if collectionView == collectionFavoriteToyes {
//                return AppManager.shared.arrBabyToys.count
//            } else if collectionView == collectionDocuument {
//                return AppManager.shared.arrBabyDocument.count
//            }
//            return 0
//        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collectionMedical || collectionView == collectionAllergies || collectionView == collectionVaccine || collectionView == collectionFavoriteFood || collectionView == collectionMedicalTwins || collectionView == collectionAllergiesTwins || collectionView == collectionVaccineTwins || collectionView == collectionFavoriteFoodTwins {
            
            collectionView.register(UINib(nibName: String(describing: AddBabyMedCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: AddBabyMedCell.self))
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: AddBabyMedCell.self), for: indexPath as IndexPath) as! AddBabyMedCell
            
            var obj = BabyType()
            
            //if self.tfBabyType.text == "Twin" {
                if collectionView == collectionMedicalTwins {
                    obj = AppManager.shared.arrBabyMedTwins[indexPath.row]
                    cell.lablTitle.text = obj.typeTitle
                } else if collectionView == collectionAllergiesTwins {
                    obj = AppManager.shared.arrBabyAllerTwins[indexPath.row]
                    cell.lablTitle.text = obj.typeTitle
                } else if collectionView == collectionVaccineTwins {
                    obj = AppManager.shared.arrBabyVaccTwins[indexPath.row]
                    cell.lablTitle.text = obj.typeTitle
                } else if collectionView == collectionFavoriteFoodTwins {
                    cell.lablTitle.text = AppManager.shared.arrBabyFoodsTwins[indexPath.row] as? String
                } else if collectionView == collectionMedical {
                    obj = AppManager.shared.arrBabyMed[indexPath.row]
                    cell.lablTitle.text = obj.typeTitle
                } else if collectionView == collectionAllergies {
                    obj = AppManager.shared.arrBabyAller[indexPath.row]
                    cell.lablTitle.text = obj.typeTitle
                } else if collectionView == collectionVaccine {
                    obj = AppManager.shared.arrBabyVacc[indexPath.row]
                    cell.lablTitle.text = obj.typeTitle
                } else if collectionView == collectionFavoriteFood {
                    cell.lablTitle.text = AppManager.shared.arrBabyFoods[indexPath.row] as? String
                }
//            } else {
//                if collectionView == collectionMedical {
//                    obj = AppManager.shared.arrBabyMed[indexPath.row]
//                    cell.lablTitle.text = obj.typeTitle
//                } else if collectionView == collectionAllergies {
//                    obj = AppManager.shared.arrBabyAller[indexPath.row]
//                    cell.lablTitle.text = obj.typeTitle
//                } else if collectionView == collectionVaccine {
//                    obj = AppManager.shared.arrBabyVacc[indexPath.row]
//                    cell.lablTitle.text = obj.typeTitle
//                } else if collectionView == collectionFavoriteFood {
//                    cell.lablTitle.text = AppManager.shared.arrBabyFoods[indexPath.row] as? String
//                }
//            }
            
            return cell
            
        } else if collectionView == collectionFavoriteToyes || collectionView == collectionFavoriteToyesTwins {
            
            collectionView.register(UINib(nibName: String(describing: ToyCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: ToyCell.self))
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: ToyCell.self), for: indexPath as IndexPath) as! ToyCell
            
           // if self.tfBabyType.text == "Twin" {
            if collectionView == collectionFavoriteToyes {
                cell.imgToy.image = AppManager.shared.arrBabyToys[indexPath.row]
            } else {
                cell.imgToy.image = AppManager.shared.arrBabyToysTwins[indexPath.row]
            }
                
//            } else {
//                cell.imgToy.image = AppManager.shared.arrBabyToys[indexPath.row]
//            }
            
            return cell
            
        } else if collectionView == collectionDocuument || collectionView == collectionDocuumentTwins {
            
            collectionView.register(UINib(nibName: String(describing: DocumentCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: DocumentCell.self))
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: DocumentCell.self), for: indexPath as IndexPath) as! DocumentCell
            
            //if self.tfBabyType.text == "Twin" {
            if collectionView == collectionDocuument {
                cell.imgDocument.image = AppManager.shared.arrBabyDocument[indexPath.row]
            } else {
                cell.imgDocument.image = AppManager.shared.arrBabyDocumentTwins[indexPath.row]
            }

//            } else {
//                cell.imgDocument.image = AppManager.shared.arrBabyDocument[indexPath.row]
//            }
            
            return cell
        }
        
        return UICollectionViewCell()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collectionMedical || collectionView == collectionAllergies || collectionView == collectionVaccine || collectionView == collectionMedicalTwins || collectionView == collectionAllergiesTwins || collectionView == collectionVaccineTwins {
            
            var obj = BabyType()
            
            //if self.tfBabyType.text == "Twin" {
                if collectionView == collectionMedicalTwins {
                    obj = AppManager.shared.arrBabyMedTwins[indexPath.row]
                } else if collectionView == collectionAllergiesTwins {
                    obj = AppManager.shared.arrBabyAllerTwins[indexPath.row]
                } else if collectionView == collectionVaccineTwins {
                    obj = AppManager.shared.arrBabyVaccTwins[indexPath.row]
                } else if collectionView == collectionMedical {
                    obj = AppManager.shared.arrBabyMed[indexPath.row]
                } else if collectionView == collectionAllergies {
                    obj = AppManager.shared.arrBabyAller[indexPath.row]
                } else if collectionView == collectionVaccine {
                    obj = AppManager.shared.arrBabyVacc[indexPath.row]
                }
//            } else {
//                if collectionView == collectionMedical {
//                    obj = AppManager.shared.arrBabyMed[indexPath.row]
//                } else if collectionView == collectionAllergies {
//                    obj = AppManager.shared.arrBabyAller[indexPath.row]
//                } else if collectionView == collectionVaccine {
//                    obj = AppManager.shared.arrBabyVacc[indexPath.row]
//                }
//            }
            
            let label = UILabel(frame: CGRect.zero)
            label.text = obj.typeTitle
            
            label.sizeToFit()
            label.font = UIFont(name: "Open-Sans-Regular", size: 12)
            return CGSize(width: label.frame.width + 14, height: 22)
            
        } else if collectionView == collectionFavoriteFood || collectionView == collectionFavoriteFoodTwins {
            
            let label = UILabel(frame: CGRect.zero)
            if collectionView == collectionFavoriteFood {
                label.text = AppManager.shared.arrBabyFoods[indexPath.row] as? String
            } else {
                label.text = AppManager.shared.arrBabyFoodsTwins[indexPath.row] as? String
            }

            label.sizeToFit()
            label.font = UIFont(name: "Open-Sans-Regular", size: 12)
            return CGSize(width: label.frame.width + 14, height: 22)
            
        } else if collectionView == collectionFavoriteToyes || collectionView == collectionFavoriteToyesTwins {
            
            return  CGSize(width: 70, height: 70)
        }
        return  CGSize(width: 66, height: 87)
    }
}

//MARK:- Textfield delegate
extension AddBabyVC: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == tfPregnancy {
            tfPregnancyTwins.text = tfPregnancy.text
        } else if textField == tfOther {
            tfOtherTwins.text = tfOther.text
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == tfPregnancy {
            if (tfPregnancy.text?.count)! >= 2 && range.length == 0 {
                return false
            } else {
                //self .perform(#selector(checkTF), with: nil, afterDelay: 0.1)
                return true
            }
        }
        return true
    }
}
