//
//  SubscriptionVC.swift
//  Tifli
//
//  Created by zubair on 27/12/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

class SubscriptionVC: UIViewController {

    @IBOutlet weak var viewPayment: UIView!
    @IBOutlet weak var tablePlans: UITableView!
    @IBOutlet weak var layouHeightPayment: NSLayoutConstraint!
    @IBOutlet weak var lablTitle: UILabel!
    @IBOutlet weak var lablPlan: UILabel!
    @IBOutlet weak var btnUpdate: GradientButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    var arrSubscription = [Subscription]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lablTitle.text = lablTitle.text?.localized()
        lablPlan.text = lablPlan.text?.localized()
        
        btnUpdate.setTitle(btnUpdate.titleLabel?.text?.localized(), for: .normal)
        btnCancel.setTitle(btnCancel.titleLabel?.text?.localized(), for: .normal)
        
        layouHeightPayment.constant = 0
        viewPayment.isHidden = true
        
        self.getSubscriptionList()
    }
    
    
    //MARK:- Apis Function
    func getSubscriptionList() {
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken()]
        APIRequestUtil.getSubscriptionList(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.arrSubscription.removeAll()
                    
                    let dictionary: Dictionary<String, JSON> = dict["data"]!.dictionaryValue
                    let valueArray: [JSON] = dictionary["subscription_plans"]!.arrayValue
                    for dict in valueArray {
                        let dictValue: Dictionary<String, JSON> = dict.dictionaryValue
                        let obj = Subscription.init(withDictionary: dictValue)
                        self.arrSubscription.append(obj)
                    }
                    self.tablePlans.reloadData()
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }

    //MARK:- Button Actions
    @IBAction func sidemenuTapped(_ sender: Any) {
        self.sideMenuViewController.presentRightMenuViewController()
    }
}

// MARK: - Tableview Delegate-
extension SubscriptionVC : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrSubscription.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        tableView.register(UINib(nibName: "PlansCell", bundle: nil), forCellReuseIdentifier: "PlansCell")
        var cell : PlansCell! = tableView.dequeueReusableCell(withIdentifier: "PlansCell") as? PlansCell
        
        if (cell == nil) {
            cell = PlansCell.init(style: UITableViewCell.CellStyle.default,
                                          reuseIdentifier:"PlansCell");
        }
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        let obj = self.arrSubscription[indexPath.row]
        
        cell.lablDuration.text = obj.planDuration
        cell.lablPrice.text = obj.planPrice
        cell.lablPerMonth.text = "per".localized()+" "+obj.planDuration
        
        if obj.currentPlan == 1 {
            cell.viewOuter.isHidden = false
            cell.viewInner.backgroundColor = #colorLiteral(red: 0.9843137255, green: 0.2901960784, blue: 0.2941176471, alpha: 1)
            cell.lablDuration.textColor = .white
            cell.lablPrice.textColor = .white
            cell.lablPerMonth.textColor = .white
            cell.btnPlan.setTitle("My Plan".localized(), for: .normal)
            cell.btnPlan.backgroundColor = #colorLiteral(red: 0.8156862745, green: 0.8156862745, blue: 0.8156862745, alpha: 1)
            cell.btnPlan.setTitleColor(.darkGray, for: .normal)
        } else {
            cell.viewOuter.isHidden = false
            cell.viewInner.backgroundColor = #colorLiteral(red: 0.831372549, green: 0.8156862745, blue: 0.8941176471, alpha: 1)
            cell.lablDuration.textColor = .black
            cell.lablPrice.textColor = .black
            cell.lablPerMonth.textColor = .black
            cell.btnPlan.setTitle("Upgrade".localized(), for: .normal)
            cell.btnPlan.backgroundColor = #colorLiteral(red: 0.3725490196, green: 0.2196078431, blue: 0.6392156863, alpha: 1)
            cell.btnPlan.setTitleColor(.white, for: .normal)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 78
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
