//
//  AboutVC.swift
//  Tifli
//
//  Created by zubair on 02/08/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit

class AboutVC: UIViewController {

    @IBOutlet weak var lablTitle: UILabel!
    @IBOutlet weak var btnAboutUs: UIButton!
    @IBOutlet weak var btnPrivacyPolicy: UIButton!
    @IBOutlet weak var btnTermsCondition: UIButton!
    @IBOutlet weak var imgAboutIcon: UIImageView!
    @IBOutlet weak var imgPrivacyPolicyIcon: UIImageView!
    @IBOutlet weak var imgTermsIcon: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lablTitle.text = lablTitle.text?.localized()
        
        if appSceneDelegate.isArabic {
            imgAboutIcon.image = UIImage(named: SIDE_ICON_AR)
            imgPrivacyPolicyIcon.image = UIImage(named: SIDE_ICON_AR)
            imgTermsIcon.image = UIImage(named: SIDE_ICON_AR)
        }
        
        btnAboutUs.setTitle(btnAboutUs.titleLabel?.text?.localized(), for: .normal)
        btnPrivacyPolicy.setTitle(btnPrivacyPolicy.titleLabel?.text?.localized(), for: .normal)
        btnTermsCondition.setTitle(btnTermsCondition.titleLabel?.text?.localized(), for: .normal)
    }

    @IBAction func sideMenuTapped(_ sender: Any) {
        self.sideMenuViewController.presentRightMenuViewController()
    }
    
    @IBAction func aboutusTapped(_ sender: Any) {
        let viewController = PrivacyPolicyViewController(nibName:String(describing: PrivacyPolicyViewController.self), bundle:nil)
        viewController.type = ABOUTUS.localized()
        viewController.webUrl = APPURL.ABOUT_US
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func privacyTapped(_ sender: Any) {
        let viewController = PrivacyPolicyViewController(nibName:String(describing: PrivacyPolicyViewController.self), bundle:nil)
        viewController.type = PRIVACY.localized()
        viewController.webUrl = APPURL.PRIVACY_POLICY
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func termsTapped(_ sender: Any) {
        let viewController = PrivacyPolicyViewController(nibName:String(describing: PrivacyPolicyViewController.self), bundle:nil)
        viewController.type = TERMSCONDITION.localized()
        viewController.webUrl = APPURL.TERMS_CONDITION
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}
