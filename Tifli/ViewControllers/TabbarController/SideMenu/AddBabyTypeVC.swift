//
//  AddBabyTypeVC.swift
//  Tifli
//
//  Created by zubair on 30/08/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol AddBabyTypeVCDelegate {
    func reloadBabyProfile()
}

class AddBabyTypeVC: UIViewController {

    @IBOutlet weak var tfSearch: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lablTitle: UILabel!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    
    var isFromAlergy = false
    var isFromVaccine = false
    var arrData = [BabyType]()
    var delegate:AddBabyTypeVCDelegate? = nil
    
    var babyId = "0"
    var isFromEdit = false
    var isFromTwins = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lablTitle.text = lablTitle.text?.localized()
        
        if appSceneDelegate.isArabic {
            tfSearch.textAlignment = .right
            btnBack.setImage(UIImage(named: BACK_AR), for: .normal)
        }
        
        btnDone.setTitle(btnDone.titleLabel?.text?.localized(), for: .normal)
        
        if isFromAlergy {
            self.lablTitle.text = "Allergies".localized()
            self.getAlergy()
        } else if isFromVaccine {
            self.lablTitle.text = "Vaccines".localized()
            self.getVaccine()
        } else {
            self.lablTitle.text = "Chronic medical conditions".localized()
            self.getMedical()
        }
    }
    
    //MARK:- Apis Functions
    func getMedical() {
        self.view.endEditing(true)
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN]
        APIRequestUtil.getMedicalList(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.arrData.removeAll()
                    
                    let valueArray: [JSON] = dict["medicalConditions"]!.arrayValue
                    for dict in valueArray {
                        let dictValue: Dictionary<String, JSON> = dict.dictionaryValue
                        let obj = BabyType.init(withDictionary: dictValue)
                        if self.isFromTwins {
                            for v in AppManager.shared.arrBabyMedicalTwins {
                                if v as! String == obj.ID {
                                    obj.isAdded = true
                                }
                            }
                        } else {
                            for v in AppManager.shared.arrBabyMedical {
                                if v as! String == obj.ID {
                                    obj.isAdded = true
                                }
                            }
                        }
                        self.arrData.append(obj)
                    }
                    self.tableView.reloadData()
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    func getAlergy() {
        self.view.endEditing(true)
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN]
        APIRequestUtil.getAlergyList(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.arrData.removeAll()
                    
                    let valueArray: [JSON] = dict["allergies"]!.arrayValue
                    for dict in valueArray {
                        let dictValue: Dictionary<String, JSON> = dict.dictionaryValue
                        let obj = BabyType.init(withDictionary: dictValue)
                        if self.isFromTwins {
                            for v in AppManager.shared.arrBabyAllergyTwins {
                                if v as! String == obj.ID {
                                    obj.isAdded = true
                                }
                            }
                        } else {
                            for v in AppManager.shared.arrBabyAllergy {
                                if v as! String == obj.ID {
                                    obj.isAdded = true
                                }
                            }
                        }
                        self.arrData.append(obj)
                    }
                    self.tableView.reloadData()
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    func getVaccine() {
        self.view.endEditing(true)
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN]
        APIRequestUtil.getVaccineList(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.arrData.removeAll()
                    
                    let valueArray: [JSON] = dict["vaccinations"]!.arrayValue
                    for dict in valueArray {
                        let dictValue: Dictionary<String, JSON> = dict.dictionaryValue
                        let obj = BabyType.init(withDictionary: dictValue)
                        if self.isFromTwins {
                            for v in AppManager.shared.arrBabyVaccineTwins {
                                if v as! String == obj.ID {
                                    obj.isAdded = true
                                }
                            }
                        } else {
                            for v in AppManager.shared.arrBabyVaccine {
                                if v as! String == obj.ID {
                                    obj.isAdded = true
                                }
                            }
                        }
                        self.arrData.append(obj)
                    }
                    self.tableView.reloadData()
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    func addVaccine() {
        self.view.endEditing(true)
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken": AppUtility.getUserToken(), "babyId":babyId, "medicalConditionId":AppManager.shared.arrBabyVaccine.componentsJoined(by: ",")]
        APIRequestUtil.addVaccine(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.delegate?.reloadBabyProfile()
                    AppUtility.showSuccessMessage(message: message ?? "Vaccine added successfully!".localized())
                    AppManager.shared.arrBabyVaccine.removeAllObjects()
                    AppManager.shared.arrBabyVacc.removeAll()
                    AppManager.shared.arrBabyVaccineTwins.removeAllObjects()
                    AppManager.shared.arrBabyVaccTwins.removeAll()
                    self.navigationController?.popViewController(animated: true)
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    func addMedical() {
        self.view.endEditing(true)
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken": AppUtility.getUserToken(), "babyId":babyId, "medicalConditionId":AppManager.shared.arrBabyMedical.componentsJoined(by: ",")]
        APIRequestUtil.addMecial(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.delegate?.reloadBabyProfile()
                    AppUtility.showSuccessMessage(message: message ?? "Medical condition added successfully!".localized())
                    AppManager.shared.arrBabyMedical.removeAllObjects()
                    AppManager.shared.arrBabyMed.removeAll()
                    AppManager.shared.arrBabyMedicalTwins.removeAllObjects()
                    AppManager.shared.arrBabyMedTwins.removeAll()
                    self.navigationController?.popViewController(animated: true)
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    func addAlergy() {
        self.view.endEditing(true)
        let paramDict: [String: Any] = ["appToken":APP_TOKEN, "userToken": AppUtility.getUserToken(), "babyId":babyId, "allergyId":AppManager.shared.arrBabyAllergy.componentsJoined(by: ",")]
        APIRequestUtil.addAllergy(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.delegate?.reloadBabyProfile()
                    AppUtility.showSuccessMessage(message: message ?? "Allergy added successfully!".localized())
                    AppManager.shared.arrBabyAllergy.removeAllObjects()
                    AppManager.shared.arrBabyAller.removeAll()
                    AppManager.shared.arrBabyAllergyTwins.removeAllObjects()
                    AppManager.shared.arrBabyAllerTwins.removeAll()
                    self.navigationController?.popViewController(animated: true)
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    //MARK:- Button Actions
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func selectTypeTapped(_ sender:UIButton) {
        if self.arrData[sender.tag].isAdded {
            self.arrData[sender.tag].isAdded = false
        } else {
            self.arrData[sender.tag].isAdded = true
        }
        tableView.reloadData()
    }
    
    @IBAction func doneTapped(_ sender: Any) {
        if !isFromAlergy && !isFromVaccine {
            if isFromTwins {
                AppManager.shared.arrBabyMedicalTwins.removeAllObjects()
                AppManager.shared.arrBabyMedTwins.removeAll()
                for obj in self.arrData {
                    if obj.isAdded {
                        AppManager.shared.arrBabyMedicalTwins.add(obj.ID)
                        AppManager.shared.arrBabyMedTwins.append(obj)
                    }
                }
                if AppManager.shared.arrBabyMedicalTwins.count == 0 {
                    AppUtility.showInfoMessage(message: "Please select medical".localized())
                    return
                }
            } else {
                AppManager.shared.arrBabyMedical.removeAllObjects()
                AppManager.shared.arrBabyMed.removeAll()
                for obj in self.arrData {
                    if obj.isAdded {
                        AppManager.shared.arrBabyMedical.add(obj.ID)
                        AppManager.shared.arrBabyMed.append(obj)
                    }
                }
                if AppManager.shared.arrBabyMedical.count == 0 {
                    AppUtility.showInfoMessage(message: "Please select medical".localized())
                    return
                }
            }
        } else if isFromAlergy {
            if isFromTwins {
                AppManager.shared.arrBabyAllergyTwins.removeAllObjects()
                AppManager.shared.arrBabyAllerTwins.removeAll()
                for obj in self.arrData {
                    if obj.isAdded {
                        AppManager.shared.arrBabyAllergyTwins.add(obj.ID)
                        AppManager.shared.arrBabyAllerTwins.append(obj)
                    }
                }
                if AppManager.shared.arrBabyAllergyTwins.count == 0 {
                    AppUtility.showInfoMessage(message: "Please select allergy".localized())
                    return
                }
            } else {
                AppManager.shared.arrBabyAllergy.removeAllObjects()
                AppManager.shared.arrBabyAller.removeAll()
                for obj in self.arrData {
                    if obj.isAdded {
                        AppManager.shared.arrBabyAllergy.add(obj.ID)
                        AppManager.shared.arrBabyAller.append(obj)
                    }
                }
                if AppManager.shared.arrBabyAllergy.count == 0 {
                    AppUtility.showInfoMessage(message: "Please select allergy".localized())
                    return
                }
            }
        } else if isFromVaccine {
            if isFromTwins {
                AppManager.shared.arrBabyVaccineTwins.removeAllObjects()
                AppManager.shared.arrBabyVaccTwins.removeAll()
                for obj in self.arrData {
                    if obj.isAdded {
                        AppManager.shared.arrBabyVaccineTwins.add(obj.ID)
                        AppManager.shared.arrBabyVaccTwins.append(obj)
                    }
                }
                if AppManager.shared.arrBabyVaccineTwins.count == 0 {
                    AppUtility.showInfoMessage(message: "Please select vaccine".localized())
                    return
                }
            } else {
                AppManager.shared.arrBabyVaccine.removeAllObjects()
                AppManager.shared.arrBabyVacc.removeAll()
                for obj in self.arrData {
                    if obj.isAdded {
                        AppManager.shared.arrBabyVaccine.add(obj.ID)
                        AppManager.shared.arrBabyVacc.append(obj)
                    }
                }
                if AppManager.shared.arrBabyVaccine.count == 0 {
                    AppUtility.showInfoMessage(message: "Please select vaccine".localized())
                    return
                }
            }
        }
        if isFromEdit {
            if !isFromAlergy && !isFromVaccine {
                self.addMedical()
            } else if isFromAlergy {
                self.addAlergy()
            } else if isFromVaccine {
                self.addVaccine()
            }
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
}

// MARK: - Tableview Delegate-
extension AddBabyTypeVC : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        tableView.register(UINib(nibName: "AlergyCell", bundle: nil), forCellReuseIdentifier: "AlergyCell")
        var cell : AlergyCell! = tableView.dequeueReusableCell(withIdentifier: "AlergyCell") as? AlergyCell
        
        if (cell == nil) {
            cell = AlergyCell.init(style: UITableViewCell.CellStyle.default,
                                          reuseIdentifier:"AlergyCell");
        }
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        let obj = self.arrData[indexPath.row]
        cell.lablTitle.text = obj.typeTitle
        
        cell.btnAdd.tag = indexPath.row
        cell.btnAdd.addTarget(self, action: #selector(selectTypeTapped(_:)), for: .touchUpInside)
        
        if !obj.isAdded {
            cell.btnAdd.setTitle("Add".localized(), for: .normal)
            cell.btnAdd.endColor = COLORS.TABBAR_COLOR
            cell.btnAdd.startColor = COLORS.APP_THEME_COLOR
            cell.btnAdd.layer.borderWidth = 0.0
            cell.btnAdd.borderColor = .clear
            cell.btnAdd.setTitleColor(.white, for: .normal)
        } else {
            cell.btnAdd.setTitle("Remove".localized(), for: .normal)
            cell.btnAdd.endColor = COLORS.UNSELECT_CATEGORY_COLOR
            cell.btnAdd.startColor = COLORS.UNSELECT_CATEGORY_COLOR
            cell.btnAdd.layer.borderWidth = 0.5
            cell.btnAdd.borderColor = .lightGray
            cell.btnAdd.setTitleColor(.lightGray, for: .normal)
        }
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    }
}
