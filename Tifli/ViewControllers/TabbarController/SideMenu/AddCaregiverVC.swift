//
//  AddCaregiverVC.swift
//  Tifli
//
//  Created by zubair on 23/09/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON
import ActionSheetPicker_3_0

protocol AddCaregiverVCDelegate {
    func reloadBabyProfiless()
}

class AddCaregiverVC: UIViewController {

    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var tfRelation: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfMobile: UITextField!
    @IBOutlet weak var viewOther: UIView!
    @IBOutlet weak var tfOther: UITextField!
    @IBOutlet weak var layoutOtherHeight: NSLayoutConstraint!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var lablTitle: UILabel!
    @IBOutlet weak var lablRelationShip: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lablEmail: UILabel!
    @IBOutlet weak var lablMobile: UILabel!
    @IBOutlet weak var lablPassword: UILabel!
    @IBOutlet weak var btnAdd: GradientButton!
    
    var arrRelation = [BabyType]()
    var relationID = ""
    var selectedIndex = -1
    var babyId = "0"
    var delegate:AddCaregiverVCDelegate? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.layoutOtherHeight.constant = 0
        self.viewOther.isHidden = true
        
        lablTitle.text = lablTitle.text?.localized()
        lablRelationShip.text = lablRelationShip.text?.localized()
        lablEmail.text = lablEmail.text?.localized()
        lablMobile.text = lablMobile.text?.localized()
        lablPassword.text = lablPassword.text?.localized()
        
        if appSceneDelegate.isArabic {
            tfRelation.textAlignment = .right
            tfEmail.textAlignment = .right
            tfMobile.textAlignment = .right
            tfOther.textAlignment = .right
            tfPassword.textAlignment = .right
            btnBack.setImage(UIImage(named: BACK_AR), for: .normal)
        }
        
        btnAdd.setTitle(btnAdd.titleLabel?.text?.localized(), for: .normal)
        
        self.getRelation()
    }
    
    func CheckFields() -> Bool {
        if imgUser.image?.size.width == 0 {
            
            AppUtility.showInfoMessage(message: "Please select photo".localized())
            let test = false
            return test
            
        } else if (tfRelation.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            
            AppUtility.showInfoMessage(message: "Please select relationship".localized())
            let test = false
            return test
            
        } else if (tfEmail.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            
            AppUtility.showInfoMessage(message: "Please enter valid Email".localized())
            let test = false
            return test
        }
        else if !(AppUtility.isValidEmail(testStr:tfEmail.text!)) {
            AppUtility.showInfoMessage(message: "Please enter valid Email".localized())
            let test = false
            return test
        }
        else if (tfMobile.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            AppUtility.showInfoMessage(message: "Please enter mobile number".localized())
            let test = false
            return test
        }
        else if (tfPassword.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            AppUtility.showInfoMessage(message: "Please enter password".localized())
            let test = false
            return test
        }
        
        let test = true
        return test
    }
    
    //MARK:- Apis Functions
    func getRelation() {
        self.view.endEditing(true)
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN]
        APIRequestUtil.getRelationList(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.arrRelation.removeAll()
                    
                    let valueArray: [JSON] = dict["relationships"]!.arrayValue
                    for dict in valueArray {
                        let dictValue: Dictionary<String, JSON> = dict.dictionaryValue
                        let obj = BabyType.init(withDictionary: dictValue)
                        self.arrRelation.append(obj)
                    }
                    let obj = BabyType()
                    obj.typeTitle = "Other"
                    obj.ID = "0"
                    self.arrRelation.append(obj)

                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    func addCaregiver() {
        self.view.endEditing(true)
        var paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":self.babyId, "email":tfEmail.text!, "phone":tfMobile.text!, "relationId":relationID, "password":tfPassword.text!]
        if relationID == "0" {
            paramDict = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":self.babyId, "email":tfEmail.text!, "phone":tfMobile.text!, "relationId":relationID, "babyRelationTitle":tfOther.text!, "password":tfPassword.text!]
        }
        let data = imgUser.image?.jpegData(compressionQuality: 0.5)
        APIRequestUtil.addCaregiver(parameters: paramDict, data: data ?? Data(), extensionString: "image/jpeg", fileNameKey: "image") { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.delegate?.reloadBabyProfiless()
                    AppUtility.showSuccessMessage(message: message ?? "Caregiver added successfully!".localized())
                    self.navigationController?.popViewController(animated: true)
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    //MARK:- Button Actions
    @IBAction func relationTapped(_ sender: UIButton) {
        var arr = [String]()
        for obj in self.arrRelation {
            arr.append(obj.typeTitle)
        }
        
        ActionSheetStringPicker.show(withTitle: "Select Relation".localized(), rows: arr, initialSelection: 0, doneBlock: {
            picker, indexes, values in
            
            self.tfRelation.text = values as? String
            self.relationID = self.arrRelation[indexes].ID
            
            if indexes == self.arrRelation.count-1 {
                self.layoutOtherHeight.constant = 40
                self.viewOther.isHidden = false
            } else {
                self.layoutOtherHeight.constant = 0
                self.viewOther.isHidden = true
            }

            return
        }, cancel: { ActionSheetStringPicker in return},origin: sender)
    }
    
    @IBAction func photoTapped(_ sender: Any) {
        ImagePickerManager().pickImage(self) { image in
            self.imgUser.image = image
        }
    }
    
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addTapped(_ sender: Any) {
        if self.CheckFields() {
            self.addCaregiver()
        }
    }
    
}
