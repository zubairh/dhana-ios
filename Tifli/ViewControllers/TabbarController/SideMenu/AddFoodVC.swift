//
//  AddFoodVC.swift
//  Tifli
//
//  Created by zubair on 31/08/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol AddFoodVCDelegate {
    func reloadBabyProfilesss()
}

class AddFoodVC: UIViewController {

    @IBOutlet weak var tfTitle: UITextField!
    @IBOutlet weak var lablNavTitle: UILabel!
    @IBOutlet weak var lablFoodName: UILabel!
    @IBOutlet weak var btnAdd: GradientButton!
    @IBOutlet weak var btnBack: UIButton!
    
    var babyId = "0"
    var isFromEdit = false
    var delegate:AddFoodVCDelegate? = nil
    var isFromTwins = false
    var isFromFoodIngrediats = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if isFromFoodIngrediats {
            lablNavTitle.text = "Add Ingredients".localized()
            lablFoodName.text = "Ingredient name".localized()
        } else {
            lablNavTitle.text = lablNavTitle.text?.localized()
            lablFoodName.text = lablFoodName.text?.localized()
        }
        if appSceneDelegate.isArabic {
            tfTitle.textAlignment = .right
            btnBack.setImage(UIImage(named: BACK_AR), for: .normal)
        }
        
        btnAdd.setTitle(btnAdd.titleLabel?.text?.localized(), for: .normal)
    }
    
    //MARK:- Apis Functions
    func addFood() {
        self.view.endEditing(true)
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken": AppUtility.getUserToken(), "babyId":babyId, "favouriteFoodName":tfTitle.text!]
        APIRequestUtil.addFood(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.delegate?.reloadBabyProfilesss()
                    AppUtility.showSuccessMessage(message: message ?? "Food added successfully!".localized())
                    AppManager.shared.arrBabyFoods.removeAllObjects()
                    AppManager.shared.arrBabyFoodsTwins.removeAllObjects()
                    self.navigationController?.popViewController(animated: true)
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }

    //MARK:- Button Actions
    @IBAction func addTapped(_ sender: Any) {
        if tfTitle.text?.count == 0 {
            AppUtility.showInfoMessage(message: "please enter title".localized())
            return
        }
        if isFromFoodIngrediats {
            AppManager.shared.arrIngrediants.add(tfTitle.text!)
            self.navigationController?.popViewController(animated: true)
        } else {
            if self.isFromEdit {
                self.addFood()
            } else {
                if self.isFromTwins {
                    AppManager.shared.arrBabyFoodsTwins.add(tfTitle.text!)
                } else {
                    AppManager.shared.arrBabyFoods.add(tfTitle.text!)
                }
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
