//
//  ChangePasswordVC.swift
//  Tifli
//
//  Created by zubair on 20/09/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

class ChangePasswordVC: UIViewController {

    @IBOutlet weak var tfOldPassword: UITextField!
    @IBOutlet weak var tfNewPassword: UITextField!
    @IBOutlet weak var tfConfirmPassword: UITextField!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lablTitle: UILabel!
    @IBOutlet weak var btnSubmit: GradientButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lablTitle.text = lablTitle.text?.localized()
        tfOldPassword.placeholder = tfOldPassword.placeholder?.localized()
        tfNewPassword.placeholder = tfNewPassword.placeholder?.localized()
        tfConfirmPassword.placeholder = tfConfirmPassword.placeholder?.localized()
        
        if appSceneDelegate.isArabic {
            btnBack.setImage(UIImage(named: BACK_AR), for: .normal)
            tfOldPassword.textAlignment = .right
            tfNewPassword.textAlignment = .right
            tfConfirmPassword.textAlignment = .right
        }
        
        btnSubmit.setTitle(btnSubmit.titleLabel?.text?.localized(), for: .normal)
    }
    
    //MARK:- Functions
    func CheckFields() -> Bool {
        if (tfOldPassword.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            
            AppUtility.showInfoMessage(message: "Please enter old password".localized())
            let test = false
            return test
        }
        else if (tfNewPassword.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            AppUtility.showInfoMessage(message: "Please enter new password".localized())
            let test = false
            return test
        }
        else if (tfConfirmPassword.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            AppUtility.showInfoMessage(message: "Please enter confirm password".localized())
            let test = false
            return test
        }
        else if (tfNewPassword.text != tfConfirmPassword.text) {
            AppUtility.showInfoMessage(message: "password and confirm password does't match".localized())
            let test = false
            return test
        }
        
        let test = true
        return test
    }
    
    //MARK:- Apis Functions
    func updatePassword() {
        self.view.endEditing(true)
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "oldPassword":tfOldPassword.text!, "password":tfNewPassword.text!, "confirmPassword":tfConfirmPassword.text!]
        APIRequestUtil.updatePassword(parameters: paramDict) { (result, error) in
            if(result != nil) {
                
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    AppUtility.showSuccessMessage(message: message ?? "Password updated successfully!".localized())
                    self.navigationController?.popViewController(animated: true)
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    //MARK:- Button Actions
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitTapped(_ sender: Any) {
        if CheckFields() {
            self.updatePassword()
        }
    }
}
