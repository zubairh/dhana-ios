//
//  ProfileVC.swift
//  Tifli
//
//  Created by zubair on 01/08/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON
import ActionSheetPicker_3_0

class ProfileVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lablUserName: UILabel!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var tfDOB: UITextField!
    @IBOutlet weak var layoutHeightTable: NSLayoutConstraint!
    @IBOutlet weak var lablTitle: UILabel!
    @IBOutlet weak var lablMyInfo: UILabel!
    @IBOutlet weak var lablEmail: UILabel!
    @IBOutlet weak var lablNae: UILabel!
    @IBOutlet weak var lablDob: UILabel!
    @IBOutlet weak var btnSave: GradientButton!
    @IBOutlet weak var btnChangePassword: UIButton!
    
    var arrBaby = [Baby]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        
        DispatchQueue.main.async {
            self.viewBg.roundCorners(corners: [.topRight, .topLeft], radius: 24)
        }
        
        lablUserName.text = AppUtility.getUserLoggedIn().username
        AppUtility.loadImage(imageView: self.imgProfile, urlString: AppUtility.getUserLoggedIn().userImage, placeHolderImageString: "bb")
        tfEmail.text = AppUtility.getUserLoggedIn().email
        tfName.text = AppUtility.getUserLoggedIn().username
        tfDOB.text = AppUtility.getUserLoggedIn().dob
        
        if tfEmail.text?.count == 0 {
            tfEmail.isUserInteractionEnabled = true
        } else {
            tfEmail.isUserInteractionEnabled = false
        }
        
        tableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
        
        lablTitle.text = lablTitle.text?.localized()
        lablEmail.text = lablEmail.text?.localized()
        lablNae.text = lablNae.text?.localized()
        lablDob.text = lablDob.text?.localized()
        lablMyInfo.text = lablMyInfo.text?.localized()
        
        if appSceneDelegate.isArabic {
            tfEmail.textAlignment = .right
            tfName.textAlignment = .right
            tfDOB.textAlignment = .right
        }
        btnSave.setTitle(btnSave.titleLabel?.text?.localized(), for: .normal)
        btnChangePassword.setTitle(btnChangePassword.titleLabel?.text?.localized(), for: .normal)
        
        self.getBabiesList()
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        layoutHeightTable.constant = tableView.contentSize.height
    }
    
    //MARK:- Apis Function
    func getBabiesList() {
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken()]
        APIRequestUtil.getBabyList(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.arrBaby.removeAll()
                    
                    let valueArray: [JSON] = dict["babies"]!.arrayValue
                    for dict in valueArray {
                        let dictValue: Dictionary<String, JSON> = dict.dictionaryValue
                        let obj = Baby.init(withDictionary: dictValue)
                        self.arrBaby.append(obj)
                    }
                    AppManager.shared.arrBabies = self.arrBaby
                    
                    var notFound = false
                    for obj in AppManager.shared.arrBabies {
                        if obj.babyID == AppUtility.getBabyId() {
                            notFound = true
                        }
                    }
                    if !notFound {
                        if self.arrBaby.count > 0 {
                            AppUtility.saveBabyId(babyID: self.arrBaby[0].babyID)
                        }
                    }
                    
                    self.tableView.reloadData()
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    func uploadPhoto() {
        self.view.endEditing(true)
        
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "userId":AppUtility.getUserLoggedIn().userID, "preference":AppUtility.getUserLoggedIn().preference]
        let data = imgProfile.image?.jpegData(compressionQuality: 0.5)
        APIRequestUtil.uploadPhoto(parameters: paramDict, data: data ?? Data(), extensionString: "image/jpeg", fileNameKey: "user_profile_image") { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    USER_DEFAUTS.set(result ?? "", forKey: USER_LOGIN)
                    USER_DEFAUTS.synchronize()
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    func updateProfile() {
        self.view.endEditing(true)
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "dob":self.tfDOB.text!, "name":self.tfName.text!, "userToken":AppUtility.getUserToken(), "preference":AppUtility.getUserLoggedIn().preference]
        APIRequestUtil.updateProfile(parameters: paramDict) { (result, error) in
            if(result != nil) {
                
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    USER_DEFAUTS.set(result ?? "", forKey: USER_LOGIN)
                    USER_DEFAUTS.synchronize()
                    AppUtility.showSuccessMessage(message: message ?? "Profile updated successfully".localized())
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    func deleteBaby(obj:Baby, index:NSInteger) {
        AppUtility.showProgress()
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":obj.babyID]
        APIRequestUtil.deleteBaby(parameters: paramDict) { (result, error) in
            if(result != nil) {
                
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.arrBaby.remove(at: index)
                    self.tableView.reloadData()
                    AppUtility.showSuccessMessage(message: message ?? "baby deleted successfully!".localized())
                    self.getBabiesList()
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }

    //MARK:- Button Actions
    @IBAction func sidemenuTapped(_ sender: Any) {
        self.sideMenuViewController.presentRightMenuViewController()
    }
    
    @IBAction func saveTapped(_ sender: Any) {
        self.updateProfile()
    }
    
    @IBAction func changePasswordTapped(_ sender: Any) {
        let viewController = ChangePasswordVC(nibName:String(describing: ChangePasswordVC.self), bundle:nil)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func addTapped(_ sender: Any) {
        let vc = AddBabyVC(nibName:"AddBabyVC", bundle:nil)
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func dobTapped(_ sender: UIButton) {
        let datePicker = ActionSheetDatePicker(title: title, datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let date:NSDate = value as! NSDate
            self.tfDOB.text = AppUtility.convertDateToString(date: date)
            
            return
            
        }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview!.superview)
        datePicker?.maximumDate = Date()
        datePicker?.show()
    }
    
    @IBAction func changePhotoTapped(_ sender: Any) {
        ImagePickerManager().pickImage(self) { image in
            self.imgProfile.image = image
            self.uploadPhoto()
        }
    }
    
}

// MARK: - Baby Add Delegate-
extension ProfileVC :AddBabyVCDelegate {
    func babyReloadData() {
        self.getBabiesList()
    }
}

extension ProfileVC :BabyProfileVCDelegate {
    func updateBaby() {
        self.getBabiesList()
    }
}

// MARK: - Tableview Delegate-
extension ProfileVC : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrBaby.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        tableView.register(UINib(nibName: "BabyProfileCell", bundle: nil), forCellReuseIdentifier: "BabyProfileCell")
        var cell : BabyProfileCell! = tableView.dequeueReusableCell(withIdentifier: "BabyProfileCell") as? BabyProfileCell
        
        if (cell == nil) {
            cell = BabyProfileCell.init(style: UITableViewCell.CellStyle.default,
                                          reuseIdentifier:"BabyProfileCell");
        }
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        let obj = self.arrBaby[indexPath.row]
        cell.lablRelation.text = obj.babyRelationship
        AppUtility.loadImage(imageView: cell.imgBaby, urlString: obj.babyImage, placeHolderImageString: "bb")
        cell.lablBabyAge.text = obj.age
        cell.lablBabyName.text = obj.babyName
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = self.arrBaby[indexPath.row]
        let vc = BabyProfileVC(nibName:"BabyProfileVC", bundle:nil)
        vc.babyID = obj.babyID
        vc.title = obj.babyName
        vc.hidesBottomBarWhenPushed = true
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCell.EditingStyle.delete) {
            let obj = self.arrBaby[indexPath.row]
            
            let alert = UIAlertController(title: "Information!".localized(), message: "Are you sure you want to delete your baby?".localized(), preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: "Yes".localized(), style: .default, handler: { (action: UIAlertAction!) in
                self.deleteBaby(obj: obj, index: indexPath.row)
            }))
            alert.addAction(UIAlertAction(title: "No".localized(), style: .cancel, handler: { (action: UIAlertAction!) in
                
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
}
