//
//  SettingVC.swift
//  Tifli
//
//  Created by zubair on 01/08/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON
import ActionSheetPicker_3_0
import IQKeyboardManagerSwift
import Localize_Swift

class SettingVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lablTitle: UILabel!
    @IBOutlet weak var lablDiary: UILabel!
    @IBOutlet weak var lablNotification: UILabel!
    @IBOutlet weak var imgIconNotify: UIImageView!
    @IBOutlet weak var lablLanguage: UILabel!
    @IBOutlet weak var lablDeleteAccount: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
    
    var arrTitle = ["Growth log".localized(), "Notes log".localized(), "Red flags log".localized()]
    var notesLog = "0"
    var growthLog = "0"
    var redFlagsLog = "0"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        
        lablTitle.text = lablTitle.text?.localized()
        lablDiary.text = lablDiary.text?.localized()
        lablLanguage.text = lablLanguage.text?.localized()
        lablNotification.text = lablNotification.text?.localized()
        lablDeleteAccount.text = lablDeleteAccount.text?.localized()
        
        if appSceneDelegate.isArabic {
            imgIconNotify.image = UIImage(named: SIDE_ICON_AR)
        }
        
        btnDelete.setTitle(btnDelete.titleLabel?.text?.localized(), for: .normal)
        
        self.getUserSettings()
    }
    
    //MARK:- Apis functions
    func deleteAccount() {
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken()]
        APIRequestUtil.deleteAccount(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                let domain = Bundle.main.bundleIdentifier!
                UserDefaults.standard.removePersistentDomain(forName: domain)
                UserDefaults.standard.synchronize()
                
                self.sideMenuViewController.hideViewController()
                appSceneDelegate.loadLanguage()
                
//                if status == SUCESS_CODE {
//
//
//                } else {
//
//                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
//                    return
//                }
            }
        }
    }
    
    func getUserSettings() {
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken()]
        APIRequestUtil.getUserSetting(parameters: paramDict) { (result, error) in
            if(result != nil) {
                
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                print(dict)
                
                if status == SUCESS_CODE {

                    let dictionary: Dictionary<String, JSON> = dict["data"]!.dictionaryValue
                    if let notesLog = dictionary["notesLog"]?.stringValue {
                        self.notesLog = notesLog
                    }
                    if let growthLog = dictionary["growthLog"]?.stringValue {
                        self.growthLog = growthLog
                    }
                    if let redFlagsLog = dictionary["redFlagsLog"]?.stringValue {
                        self.redFlagsLog = redFlagsLog
                    }
                    self.tableView.reloadData()
                    
                } else {

                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    func saveUserSettings(columnName: String, value: String, index:Int) {
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "columnName":columnName, "value":value]
        APIRequestUtil.updateUserSettings(parameters: paramDict) { (result, error) in
            if(result != nil) {
                
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                print(dict)
                
                if status == SUCESS_CODE {

                    let dictionary: Dictionary<String, JSON> = dict["data"]!.dictionaryValue
                    if let notesLog = dictionary["notesLog"]?.stringValue {
                        self.notesLog = notesLog
                    }
                    if let growthLog = dictionary["growthLog"]?.stringValue {
                        self.growthLog = growthLog
                    }
                    if let redFlagsLog = dictionary["redFlagsLog"]?.stringValue {
                        self.redFlagsLog = redFlagsLog
                    }
                    self.tableView.reloadData()
                    
                } else {

                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    //MARK:- Button Actions
    @IBAction func deleteTapped(_ sender: Any) {
        let alert = UIAlertController(title: "Hang On!".localized(), message: "Are you sure you want to delete your account?".localized(), preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "OK".localized(), style: .default, handler: { (action: UIAlertAction!) in
            self.deleteAccount()
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: { (action: UIAlertAction!) in
            print("Handle Cancel Logic here")
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func changeLanguageTapped(_ sender: UIButton) {
        let arr = ["English", "عربي"]
        ActionSheetStringPicker.show(withTitle: "Select Language".localized(), rows: arr, initialSelection: 0, doneBlock: {
            picker, indexes, values in
            
            if indexes == 1 {
                Localize.setCurrentLanguage("ar")
                ISENGLISH = "ar"
                appSceneDelegate.language = "2"
                appSceneDelegate.isArabic = true
                UILabel.appearance().substituteFontName = "Tajawal-Medium"
                IQKeyboardManager.shared.toolbarDoneBarButtonItemText = "Done".localized()
                UserDefaults.standard.set(true, forKey: LANGUAGE)
                if #available(iOS 9.0, *) {
                    UIView.appearance().semanticContentAttribute = UISemanticContentAttribute.forceRightToLeft
                }
                appSceneDelegate.loadTab()
            } else {
                Localize.setCurrentLanguage("en")
                IQKeyboardManager.shared.toolbarDoneBarButtonItemText = "Done".localized()
                ISENGLISH = "en"
                appSceneDelegate.language = "1"
                appSceneDelegate.isArabic = false
                UILabel.appearance().substituteFontName = "OpenSans-SemiBold"
                UserDefaults.standard.set(false, forKey: LANGUAGE)
                
                UIFont.overrideInitialize()
                
                if #available(iOS 9.0, *) {
                    UIView.appearance().semanticContentAttribute = UISemanticContentAttribute.forceLeftToRight
                }
                appSceneDelegate.loadTab()
            }
            sender.setTitle(values as? String, for: .normal)
            
            return
        }, cancel: { ActionSheetStringPicker in return},origin: sender)
    }
    
    @IBAction func sideMenuTapped(_ sender: Any) {
        self.sideMenuViewController.presentRightMenuViewController()
    }
    
    @IBAction func notificationSettingTapped(_ sender: Any) {
        let viewController = NotificationSettingVC(nibName:String(describing: NotificationSettingVC.self), bundle:nil)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @objc func switchValueDidChange(_ sender: UISwitch) {
        if sender.tag == 0 {
            if notesLog == "0" {
                self.saveUserSettings(columnName: "notesLog", value: "1", index: 0)
            } else {
                self.saveUserSettings(columnName: "notesLog", value: "0", index: 0)
            }
        } else if sender.tag == 1 {
            if growthLog == "0" {
                self.saveUserSettings(columnName: "growthLog", value: "1", index: 0)
            } else {
                self.saveUserSettings(columnName: "growthLog", value: "0", index: 0)
            }
        } else if sender.tag == 2 {
            if redFlagsLog == "0" {
                self.saveUserSettings(columnName: "redFlagsLog", value: "1", index: 0)
            } else {
                self.saveUserSettings(columnName: "redFlagsLog", value: "0", index: 0)
            }
        }
    }
}

// MARK: - Tableview Delegate-
extension SettingVC : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        tableView.register(UINib(nibName: "SettingCell", bundle: nil), forCellReuseIdentifier: "SettingCell")
        var cell : SettingCell! = tableView.dequeueReusableCell(withIdentifier: "SettingCell") as? SettingCell
        
        if (cell == nil) {
            cell = SettingCell.init(style: UITableViewCell.CellStyle.default,
                                          reuseIdentifier:"SettingCell");
        }
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        cell.btnSwitch.tag = indexPath.row
        cell.btnSwitch.addTarget(self, action: #selector(switchValueDidChange(_:)), for: .valueChanged)
        
        cell.lablTitle.text = self.arrTitle[indexPath.row]
        if indexPath.row == 0 {
            if self.notesLog == "0" {
                cell.btnSwitch.isOn = false
            } else {
                cell.btnSwitch.isOn = true
            }
        } else if indexPath.row == 1 {
            if self.growthLog == "0" {
                cell.btnSwitch.isOn = false
            } else {
                cell.btnSwitch.isOn = true
            }
        } else if indexPath.row == 2 {
            if self.redFlagsLog == "0" {
                cell.btnSwitch.isOn = false
            } else {
                cell.btnSwitch.isOn = true
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 56
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
}
