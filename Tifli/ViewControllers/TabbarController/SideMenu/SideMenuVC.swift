//
//  SideMenuVC.swift
//  Tifli
//
//  Created by zubair on 29/07/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON
import AnimatableReload

class SideMenuVC: UIViewController {

    @IBOutlet weak var tableMenu: UITableView!
    @IBOutlet weak var imgBaby: UIImageView!
    @IBOutlet weak var lablName: UILabel!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var lablFollowUs: UILabel!
    
    var arrSection = ["SWITCH BABY".localized(), "PREMIUM".localized(), "HELP".localized()]
    //var arrSection = ["SWITCH BABY", "HELP"]
    var arrTitlesPremium = ["Chat with a doctor".localized(), "Manage my subscription".localized()]
    var arrTitlesHelp = ["Home".localized(), "My Profile".localized(), "Settings".localized(), "Contact us".localized(), "About Dhana".localized(), "Logout".localized()]
    var arrImagesPremium = ["chat-1", "user subscrption"]
    var arrImagesHelp = ["home-1", "my profile", "settings-1", "Contact Us Copy", "about dhana", "logout"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnClose.setTitle(btnClose.titleLabel?.text?.localized(), for: .normal)
        lablFollowUs.text = lablFollowUs.text?.localized()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        for obj in AppManager.shared.arrBabies {
            if obj.babyID == AppUtility.getBabyId() {
                AppUtility.loadImage(imageView: imgBaby, urlString: obj.babyImage, placeHolderImageString: "bb")
                lablName.text = obj.babyName+" "+obj.age
            }
        }
        tableMenu.reloadData()
        AnimatableReload.reload(tableView: self.tableMenu, animationDirection: "up")
    }
    
    //MARK:- Apis functions
    func logout() {
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken()]
        APIRequestUtil.logout(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                let domain = Bundle.main.bundleIdentifier!
                UserDefaults.standard.removePersistentDomain(forName: domain)
                UserDefaults.standard.synchronize()
                
                self.sideMenuViewController.hideViewController()
                appSceneDelegate.loadLanguage()
                
//                if status == SUCESS_CODE {
//
//
//                } else {
//
//                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
//                    return
//                }
            }
        }
    }
    
    @IBAction func YoutubeAction() {

        let YoutubeQuery =  "dhana.channel"
        let escapedYoutubeQuery = YoutubeQuery.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        let appURL = NSURL(string: "youtube://www.youtube.com/results?search_query=\(escapedYoutubeQuery!)")!
        let webURL = NSURL(string: "https://www.youtube.com/results?search_query=\(escapedYoutubeQuery!)")!
        let application = UIApplication.shared

        if application.canOpenURL(appURL as URL) {
            application.open(appURL as URL)
        } else {
            application.open(webURL as URL)
        }
    }
    
    //MARK:- Button Actions
    @IBAction func closeTapped(_ sender: Any) {
        self.sideMenuViewController.hideViewController()
        appSceneDelegate.navController.popViewController(animated: true)
    }
    
    @IBAction func instaTapped(_ sender: Any) {
        let Username =  "dhana.kw"
        let appURL = URL(string: "instagram://user?username=\(Username)")!
        let application = UIApplication.shared

        if application.canOpenURL(appURL) {
            application.open(appURL)
        } else {
            // if Instagram app is not installed, open URL inside Safari
            let webURL = URL(string: "https://instagram.com/\(Username)")!
            application.open(webURL)
        }
    }
    
    @IBAction func youtubeTapped(_ sender: Any) {
        self.YoutubeAction()
    }
    
    @IBAction func fbTapped(_ sender: Any) {
        let Username =  "dhana.kw"
        let appURL = URL(string: "fb://profile/\(Username)")!
        let application = UIApplication.shared

        if application.canOpenURL(appURL) {
            application.open(appURL)
        } else {
            // if Instagram app is not installed, open URL inside Safari
            let webURL = URL(string: "https://instagram.com/\(Username)")!
            application.open(webURL)
        }
    }
}

// MARK: - Tableview Delegate-
extension SideMenuVC : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrSection.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return AppManager.shared.arrBabies.count
        }
        else if section == 1 {
            return arrTitlesPremium.count
        }
        return arrTitlesHelp.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        tableView.register(UINib(nibName: "MenuCell", bundle: nil), forCellReuseIdentifier: "MenuCell")
        var cell : MenuCell! = tableView.dequeueReusableCell(withIdentifier: "MenuCell") as? MenuCell
        
        if (cell == nil) {
            cell = MenuCell.init(style: UITableViewCell.CellStyle.default,
                                          reuseIdentifier:"MenuCell");
        }
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        if indexPath.section == 0 {
            cell.imgIcon.contentMode = .scaleAspectFill
            cell.imgIcon.cornerRadius = cell.imgIcon.frame.width/2
            cell.lablTitle.text = AppManager.shared.arrBabies[indexPath.row].babyName
            AppUtility.loadImage(imageView: cell.imgIcon, urlString: AppManager.shared.arrBabies[indexPath.row].babyImage, placeHolderImageString: "bb")
            if indexPath.row == AppManager.shared.arrBabies.count-1 {
                cell.imgSep.isHidden = false
            } else {
                cell.imgSep.isHidden = true
            }
        }
        else if indexPath.section == 1 {
            cell.imgIcon.contentMode = .scaleAspectFit
            cell.imgIcon.cornerRadius = 0
            cell.lablTitle.text = self.arrTitlesPremium[indexPath.row]
            cell.imgIcon.image = UIImage(named: self.arrImagesPremium[indexPath.row])

            if indexPath.row == self.arrTitlesPremium.count-1 {
                cell.imgSep.isHidden = false
            } else {
                cell.imgSep.isHidden = true
            }
        }
        else {
            cell.imgIcon.contentMode = .scaleAspectFit
            cell.imgIcon.cornerRadius = 0
            cell.lablTitle.text = self.arrTitlesHelp[indexPath.row]
            cell.imgIcon.image = UIImage(named: self.arrImagesHelp[indexPath.row])
            
            if indexPath.row == self.arrTitlesHelp.count-1 {
                cell.imgSep.isHidden = false
            } else {
                cell.imgSep.isHidden = true
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 46
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        tableView.register(UINib(nibName: "MenuSectionCell", bundle: nil), forCellReuseIdentifier: "MenuSectionCell")
        let header = tableView.dequeueReusableCell(withIdentifier: "MenuSectionCell")! as! MenuSectionCell
        
        header.lablTitle.text = self.arrSection[section]
        
        return header.contentView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 28
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 2 {
            
            if indexPath.row == 0 {
                
                self.sideMenuViewController.hideViewController()
                appSceneDelegate.navController.popViewController(animated: true)
                
            } else if indexPath.row == 1 {
                
                let profileVC = ProfileVC(nibName:"ProfileVC", bundle:nil)
//                appSceneDelegate.sideMenu.setContentViewController(UINavigationController.init(rootViewController:profileVC), animated: true)
                appSceneDelegate.navController.pushViewController(profileVC, animated: true)
                self.sideMenuViewController.hideViewController()
            } else if indexPath.row == 2 {
                let settingVC = SettingVC(nibName:"SettingVC", bundle:nil)
//                appSceneDelegate.sideMenu.setContentViewController(UINavigationController.init(rootViewController:settingVC), animated: true)
                appSceneDelegate.navController.pushViewController(settingVC, animated: true)
                self.sideMenuViewController.hideViewController()
            } else if indexPath.row == 3 {
                let contactUsVC = ContactUsVC(nibName:"ContactUsVC", bundle:nil)
//                appSceneDelegate.sideMenu.setContentViewController(UINavigationController.init(rootViewController:contactUsVC), animated: true)
                appSceneDelegate.navController.pushViewController(contactUsVC, animated: true)
                self.sideMenuViewController.hideViewController()
            } else if indexPath.row == 4 {
                let contactUsVC = AboutVC(nibName:"AboutVC", bundle:nil)
//                appSceneDelegate.sideMenu.setContentViewController(UINavigationController.init(rootViewController:contactUsVC), animated: true)
                appSceneDelegate.navController.pushViewController(contactUsVC, animated: true)
                self.sideMenuViewController.hideViewController()
            } else if indexPath.row == 5 {
                let alert = UIAlertController(title: "Hang On!".localized(), message: "Are you sure you want to Logout?".localized(), preferredStyle: UIAlertController.Style.alert)
                
                alert.addAction(UIAlertAction(title: "OK".localized(), style: .default, handler: { (action: UIAlertAction!) in
                    self.logout()
                }))
                
                alert.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: { (action: UIAlertAction!) in
                    print("Handle Cancel Logic here")
                }))
                
                self.present(alert, animated: true, completion: nil)
            }
        } else if indexPath.section == 0 {
            let vc = BabyProfileVC(nibName:"BabyProfileVC", bundle:nil)
            vc.babyID = AppManager.shared.arrBabies[indexPath.row].babyID
            vc.title = AppManager.shared.arrBabies[indexPath.row].babyName
            vc.hidesBottomBarWhenPushed = true
            appSceneDelegate.navController.pushViewController(vc, animated: true)
            self.sideMenuViewController.hideViewController()
        } else {
            if indexPath.row == 0 {
                let vc = MessagesVC(nibName:"MessagesVC", bundle:nil)
                vc.hidesBottomBarWhenPushed = true
                appSceneDelegate.navController.pushViewController(vc, animated: true)
                self.sideMenuViewController.hideViewController()
            }
            else if indexPath.row == 1 {
                let vc = SubscriptionVC(nibName:"SubscriptionVC", bundle:nil)
                vc.hidesBottomBarWhenPushed = true
                appSceneDelegate.navController.pushViewController(vc, animated: true)
                self.sideMenuViewController.hideViewController()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0
        cell.layer.transform = CATransform3DMakeScale(0.5, 0.5, 0.5)
        UIView.animate(withDuration: 0.5) {
            cell.alpha = 1
            cell.layer.transform = CATransform3DScale(CATransform3DIdentity, 1, 1, 1)
        }
    }
}
