//
//  BabyProfileVC.swift
//  Tifli
//
//  Created by zubair on 01/09/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON
import ActionSheetPicker_3_0

protocol BabyProfileVCDelegate {
    func updateBaby()
}

class BabyProfileVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var lablNavTitle: UILabel!
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var lablAge: UITextField!
    @IBOutlet weak var lablDob: UITextField!
    @IBOutlet weak var lablPregnancy: UITextField!
    @IBOutlet weak var collectionMedical: UICollectionView!
    @IBOutlet weak var collectionAllergies: UICollectionView!
    @IBOutlet weak var collectionFavoriteFood: UICollectionView!
    @IBOutlet weak var collectionFavoriteToyes: UICollectionView!
    @IBOutlet weak var collectionDocuument: UICollectionView!
    @IBOutlet weak var collectionCaregiver: UICollectionView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var layoutHeightCollectionMedical: NSLayoutConstraint!
    @IBOutlet weak var layoutHeightCollectionAlergy: NSLayoutConstraint!
    @IBOutlet weak var layoutHeightCollectionFood: NSLayoutConstraint!
    @IBOutlet weak var layoutHeightCollectionToys: NSLayoutConstraint!
    @IBOutlet weak var layoutHeightCollectionDocument: NSLayoutConstraint!
    @IBOutlet weak var layoutHeightCollectionCaregiver: NSLayoutConstraint!
    
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lablName: UILabel!
    
    @IBOutlet weak var lablAgeTitle: UILabel!
    @IBOutlet weak var lablDobTitle: UILabel!
    @IBOutlet weak var lablWeekPregnancyTitle: UILabel!
    @IBOutlet weak var lablChronic: UILabel!
    @IBOutlet weak var llablAlergy: UILabel!
    @IBOutlet weak var lablFavToys: UILabel!
    @IBOutlet weak var lablFavFood: UILabel!
    @IBOutlet weak var lablDoc: UILabel!
    @IBOutlet weak var lablCaregiver: UILabel!
    
    var arrMedical = [BabyType]()
    var arrAlergy = [BabyType]()
    var arrVaccine = [BabyType]()
    var arrToys = [BabyType]()
    var arrFood = [BabyType]()
    var arrDocuments = [BabyType]()
    var arrCaregiver = [BabyType]()
    
    var babyID = ""
    var delegate:BabyProfileVCDelegate? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lablName.text = lablName.text?.localized()
        lablAgeTitle.text = lablAgeTitle.text?.localized()
        lablDobTitle.text = lablDobTitle.text?.localized()
        lablWeekPregnancyTitle.text = lablWeekPregnancyTitle.text?.localized()
        lablChronic.text = lablChronic.text?.localized()
        llablAlergy.text = llablAlergy.text?.localized()
        lablFavToys.text = lablFavToys.text?.localized()
        lablFavFood.text = lablFavFood.text?.localized()
        lablDoc.text = lablDoc.text?.localized()
        lablCaregiver.text = lablCaregiver.text?.localized()
        
        if appSceneDelegate.isArabic {
            tfName.textAlignment = .right
            lablAge.textAlignment = .right
            lablDob.textAlignment = .right
            lablPregnancy.textAlignment = .right
            btnBack.setImage(UIImage(named: BACK_AR), for: .normal)
        }
        btnSave.setTitle(btnSave.titleLabel?.text?.localized(), for: .normal)
        
        collectionMedical.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
        collectionAllergies.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
        collectionFavoriteFood.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
        collectionFavoriteToyes.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
        collectionDocuument.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
        collectionCaregiver.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
        
        lablNavTitle.text = self.title
        self.getBabyDetail()
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        layoutHeightCollectionMedical.constant = collectionMedical.contentSize.height
        layoutHeightCollectionAlergy.constant = collectionAllergies.contentSize.height
        layoutHeightCollectionFood.constant = collectionFavoriteFood.contentSize.height
        layoutHeightCollectionToys.constant = collectionFavoriteToyes.contentSize.height
        layoutHeightCollectionDocument.constant = collectionDocuument.contentSize.height
        layoutHeightCollectionCaregiver.constant = collectionCaregiver.contentSize.height
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    //MARK: Textfiled Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == lablPregnancy {
            if (lablPregnancy.text?.count)! >= 2 && range.length == 0 {
                return false
            } else {
                self .perform(#selector(checkTF), with: nil, afterDelay: 0.1)
                return true
            }
        }
        return true
    }
    
    //MARK: - Functions
    @objc func checkTF() {
        if lablPregnancy.text?.count == 2 {
            self.view.endEditing(true)
        }
    }
    
    //MARK:- Apis Function
    func getBabyDetail() {
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":babyID]
        APIRequestUtil.getBabyDetail(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    let obj = BabyDetail.init(withDictionary: dict)
                    self.arrMedical = obj.arrMedical
                    self.arrAlergy = obj.arrAlergy
                    self.arrFood = obj.arrFood
                    self.arrVaccine = obj.arrVaccine
                    self.arrToys = obj.arrToys
                    self.arrDocuments = obj.arrDocuments
                    self.arrCaregiver = obj.arrCaregiver
                    
                    self.collectionDocuument.reloadData()
                    self.collectionMedical.reloadData()
                    self.collectionCaregiver.reloadData()
                    self.collectionAllergies.reloadData()
                    self.collectionFavoriteFood.reloadData()
                    self.collectionFavoriteToyes.reloadData()
                    
                    self.lablAge.text = obj.age
                    self.lablDob.text = obj.babyDob
                    self.lablPregnancy.text = obj.pregnancyWeek
                    self.tfName.text = obj.babyName
                    AppUtility.loadImage(imageView: self.imgUser, urlString: obj.babyImage, placeHolderImageString: "bb")
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    func updateProfile() {
        self.view.endEditing(true)
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":babyID, "dob":lablDob.text!, "fetalAge":lablAge.text!, "babyPregnancyWeeks":lablPregnancy.text!, "babyNameEn":tfName.text!]
        let data = imgUser.image?.jpegData(compressionQuality: 0.5)
        APIRequestUtil.updateNewBaby(parameters: paramDict, data: data ?? Data(), extensionString: "image/jpeg", fileNameKey: "babyImage") { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.delegate?.updateBaby()
                    AppUtility.showSuccessMessage(message: message ?? "Updated profile successfully!")
                    self.navigationController?.popViewController(animated: true)
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }

    //MARK:- Button Actions
    @IBAction func addMmedicalTapped(_ sender: Any) {
        let viewController = AddBabyTypeVC(nibName:String(describing: AddBabyTypeVC.self), bundle:nil)
        viewController.isFromEdit = true
        viewController.babyId = self.babyID
        viewController.delegate = self
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func addAlergiesTapped(_ sender: Any) {
        let viewController = AddBabyTypeVC(nibName:String(describing: AddBabyTypeVC.self), bundle:nil)
        viewController.isFromAlergy = true
        viewController.isFromEdit = true
        viewController.babyId = self.babyID
        viewController.delegate = self
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func addFoodTapped(_ sender: Any) {
        let viewController = AddFoodVC(nibName:String(describing: AddFoodVC.self), bundle:nil)
        viewController.babyId = self.babyID
        viewController.isFromEdit = true
        viewController.delegate = self
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func addToyTapped(_ sender: Any) {
        let viewController = AddAttachmentVC(nibName:String(describing: AddAttachmentVC.self), bundle:nil)
        viewController.babyId = self.babyID
        viewController.isFromEdit = true
        viewController.delegate = self
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func addDocumentTapped(_ sender: Any) {
        let viewController = AddAttachmentVC(nibName:String(describing: AddAttachmentVC.self), bundle:nil)
        viewController.isFromDocument = true
        viewController.babyId = self.babyID
        viewController.isFromEdit = true
        viewController.delegate = self
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func addVaccineTapped(_ sender: Any) {
        let viewController = AddBabyTypeVC(nibName:String(describing: AddBabyTypeVC.self), bundle:nil)
        viewController.isFromVaccine = true
        viewController.isFromEdit = true
        viewController.babyId = self.babyID
        viewController.delegate = self
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func addCaregiverTapped(_ sender: Any) {
        let viewController = AddCaregiverVC(nibName:String(describing: AddCaregiverVC.self), bundle:nil)
        viewController.babyId = self.babyID
        viewController.delegate = self
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func editTapped(_ sender: Any) {
        self.updateProfile()
    }
    
    @IBAction func dobTapped(_ sender: UIButton) {
        let datePicker = ActionSheetDatePicker(title: title, datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let date:NSDate = value as! NSDate
            self.lablDob.text = AppUtility.convertDateToString(date: date)
            
            return
            
        }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview!.superview)
        datePicker?.maximumDate = Date()
        datePicker?.show()
    }
    
    @IBAction func changeProfileTapped(_ sender: Any) {
        ImagePickerManager().pickImage(self) { image in
            self.imgUser.image = image
        }
    }
}

// MARK: - CollectionView Delegate-
extension BabyProfileVC: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionMedical {
            return arrMedical.count
        } else if collectionView == collectionAllergies {
            return arrAlergy.count
        }
//        else if collectionView == collectionVaccine {
//            return arrVaccine.count
//        }
        else if collectionView == collectionFavoriteFood {
            return arrFood.count
        } else if collectionView == collectionCaregiver {
            return arrCaregiver.count
        } else if collectionView == collectionFavoriteToyes {
            return arrToys.count
        }
        return arrDocuments.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collectionMedical || collectionView == collectionAllergies || collectionView == collectionFavoriteFood {
            
            var obj = BabyType()
            
            if collectionView == collectionMedical {
                obj = arrMedical[indexPath.row]
            } else if collectionView == collectionAllergies {
                obj = arrAlergy[indexPath.row]
            } else if collectionView == collectionFavoriteFood {
                obj = arrFood[indexPath.row]
            }
//            else if collectionView == collectionVaccine {
//                obj = arrVaccine[indexPath.row]
//            }
            let label = UILabel(frame: CGRect.zero)
            label.text = obj.typeTitle
            
            label.sizeToFit()
            label.font = UIFont(name: "Open-Sans-Regular", size: 12)
            return CGSize(width: label.frame.width + 14, height: 22)
            
        } else if collectionView == collectionFavoriteToyes {
            
            return  CGSize(width: 70, height: 70)
            
        } else if collectionView == collectionCaregiver {
            
            return  CGSize(width: 70, height: 70)
        }
        return  CGSize(width: 66, height: 87)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collectionMedical || collectionView == collectionAllergies || collectionView == collectionFavoriteFood {
            
            collectionView.register(UINib(nibName: String(describing: AddBabyMedCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: AddBabyMedCell.self))
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: AddBabyMedCell.self), for: indexPath as IndexPath) as! AddBabyMedCell
            
            var obj = BabyType()
            
            if collectionView == collectionMedical {
                obj = arrMedical[indexPath.row]
                cell.lablTitle.text = obj.typeTitle
            } else if collectionView == collectionAllergies {
                obj = arrAlergy[indexPath.row]
                cell.lablTitle.text = obj.typeTitle
            } else if collectionView == collectionFavoriteFood {
                obj = arrFood[indexPath.row]
            }
//            else if collectionView == collectionVaccine {
//                obj = arrVaccine[indexPath.row]
//            }
            
            cell.lablTitle.text = obj.typeTitle
            
            return cell
            
        } else if collectionView == collectionCaregiver {
            
            collectionView.register(UINib(nibName: String(describing: CaregiverCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: CaregiverCell.self))
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: CaregiverCell.self), for: indexPath as IndexPath) as! CaregiverCell
            
            AppUtility.loadImage(imageView: cell.imgUser, urlString: arrCaregiver[indexPath.row].file, placeHolderImageString: "bb")
            
            return cell
            
        } else if collectionView == collectionFavoriteToyes {
            
            collectionView.register(UINib(nibName: String(describing: ToyCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: ToyCell.self))
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: ToyCell.self), for: indexPath as IndexPath) as! ToyCell
            
            AppUtility.loadImage(imageView: cell.imgToy, urlString: arrToys[indexPath.row].file, placeHolderImageString: "bb")
            
            return cell
            
        } else {
            
            collectionView.register(UINib(nibName: String(describing: DocumentCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: DocumentCell.self))
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: DocumentCell.self), for: indexPath as IndexPath) as! DocumentCell
            
            AppUtility.loadImage(imageView: cell.imgDocument, urlString: arrDocuments[indexPath.row].file, placeHolderImageString: "bb")
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}


// MARK: - Baby Reload Delegate-
extension BabyProfileVC: AddBabyTypeVCDelegate {
    func reloadBabyProfile() {
        self.getBabyDetail()
    }
}

extension BabyProfileVC: AddCaregiverVCDelegate {
    func reloadBabyProfiless() {
        self.getBabyDetail()
    }
}

extension BabyProfileVC: AddAttachmentVCDelegate {
    func reloadBabyProfiles() {
        self.getBabyDetail()
    }
}

extension BabyProfileVC: AddFoodVCDelegate {
    func reloadBabyProfilesss() {
        self.getBabyDetail()
    }
}
