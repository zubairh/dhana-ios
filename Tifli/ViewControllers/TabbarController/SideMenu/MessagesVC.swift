//
//  MessagesVC.swift
//  Tifli
//
//  Created by zubair on 23/12/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON
import GrowingTextView
import IQKeyboardManagerSwift

class MessagesVC: UIViewController, GrowingTextViewDelegate {
    
    @IBOutlet weak var tableMessages: UITableView!
    @IBOutlet var commentTextView: GrowingTextView!
    @IBOutlet var scrollViewBottomConstraint:NSLayoutConstraint!
    @IBOutlet var scrollViewContentHeight:NSLayoutConstraint!
    @IBOutlet var btnAddImage: UIButton!
    @IBOutlet var btnSend: UIButton!
    @IBOutlet weak var viewMoreBottomCons: NSLayoutConstraint!
    @IBOutlet weak var lablTitle: UILabel!
    
    var arrMessages = [Messages]()
    var keyboardHeight:CGFloat = 0
    var timer = Timer()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        lablTitle.text = lablTitle.text?.localized()
        commentTextView.placeholder = commentTextView.placeholder?.localized()
        if appSceneDelegate.isArabic {
            commentTextView.textAlignment = .right
        }
        
        btnSend.setTitle(btnSend.titleLabel?.text?.localized(), for: .normal)
        
        btnSend.isEnabled = false
        btnSend.isSelected = false
        commentTextView.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        self.getLatestChat()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
        
        timer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(self.getLatestChat), userInfo: nil, repeats: true)
        RunLoop.current.add(self.timer, forMode: RunLoop.Mode.common)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        timer.invalidate()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }
    
    func scrollToBottom(animated:Bool) {
        
        DispatchQueue.main.async {
            
            var yOffset: CGFloat = 0.0;

            if (self.tableMessages.contentSize.height > self.tableMessages.bounds.size.height) {
                yOffset = self.tableMessages.contentSize.height - self.tableMessages.bounds.size.height;
            }
            
            self.tableMessages.setContentOffset(CGPoint(x: 0, y: yOffset), animated: animated)
        }
    }
    
    //MARK:- Keybboard handle notifications
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if #available(iOS 11.0, *) {
                
                keyboardHeight = keyboardSize.height - self.view.safeAreaInsets.bottom
                
            } else {
                
                keyboardHeight = keyboardSize.height
            }
            
            let scrollPoint = CGPoint(x: 0, y: self.tableMessages.contentSize.height - self.tableMessages.frame.size.height)
            self.tableMessages.setContentOffset(scrollPoint, animated: true)
            
            self.viewMoreBottomCons.constant = 177
            print(keyboardHeight)
            scrollViewBottomConstraint.constant = keyboardHeight
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        keyboardHeight = 0
        scrollViewBottomConstraint.constant = 0
        self.view.layoutIfNeeded()
    }
    
    // MARK: - CSGrowingTextViewDelegate
    func textViewDidChange(_ textView: UITextView) {
        if (textView.text.count) > 0 {
            btnSend.isEnabled = true
        } else {
            btnSend.isEnabled = false
        }
    }
    
    //MARK:- Apis Function
    @objc func getLatestChat() {
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken()]
        APIRequestUtil.getMessages(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.arrMessages.removeAll()
                    
                    if let valueArray: [JSON] = dict["data"]?.arrayValue {
                        for dict in valueArray {
                            let dictValue: Dictionary<String, JSON> = dict.dictionaryValue
                            let obj = Messages.init(withDictionary: dictValue)
                            self.arrMessages.append(obj)
                        }
                    }
                    
                    self.tableMessages.reloadData()
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    func sendMessage(obj:Messages) {
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "message":obj.message]
        APIRequestUtil.sendMessages(parameters: paramDict, data: obj.data, extensionString: "image/jpeg", fileNameKey: "chatMedia") { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.arrMessages.removeAll()
                    
                    let valueArray: [JSON] = dict["data"]!.arrayValue
                    for dict in valueArray {
                        let dictValue: Dictionary<String, JSON> = dict.dictionaryValue
                        let obj = Messages.init(withDictionary: dictValue)
                        self.arrMessages.append(obj)
                    }
                    
                    self.tableMessages.reloadData()
                    self.scrollToBottom(animated: true)
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    //MARK:- Button Actions
    @IBAction func sidemenuTapped(_ sender: Any) {
        self.sideMenuViewController.presentRightMenuViewController()
    }
    
    @IBAction func sendMessageTapped(_ sender: Any) {
        if commentTextView.text != "" {
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:s"
            let result = formatter.string(from: date)
            let seed = Int(Date().timeIntervalSince1970)
            
            let obj = Messages()
            obj.message = commentTextView.text!
            obj.createdAt = result
            obj.objUser.userID = AppUtility.getUserLoggedIn().userID
            obj.chatID = String(seed)+AppUtility.getUserLoggedIn().userID
            self.arrMessages.append(obj)
            self.tableMessages.reloadData()
            self.scrollToBottom(animated: true)
            self.commentTextView.text = ""
            self.sendMessage(obj: obj)
        }
    }
    
    @IBAction func addAttachmentTapped(_ sender: Any) {
        ImagePickerManager().pickImage(self) { image in
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:s"
            let result = formatter.string(from: date)
            let seed = Int(Date().timeIntervalSince1970)
            
            let obj = Messages()
            obj.createdAt = result
            obj.objUser.userID = AppUtility.getUserLoggedIn().userID
            obj.chatID = String(seed)+AppUtility.getUserLoggedIn().userID
            //obj.image = image
            obj.mediaFileType = "image"
            obj.data = image.jpegData(compressionQuality: 0.2)!
            self.tableMessages.reloadData()
            self.commentTextView.text = ""
            AppUtility.showProgress()
            self.sendMessage(obj: obj)
        }
    }
    
}

// MARK: - Tableview Delegate-
extension MessagesVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrMessages.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        var nibName = ""
        let obj = self.arrMessages[indexPath.row]

        if (obj.mediaFileType == "image") {
            if obj.objUser.userID == AppUtility.getUserLoggedIn().userID {
                nibName = "ChatTableViewImageCellRight"
            } else {
                nibName = "ChatTableViewImageCellLeft"
            }
        } else {
            if obj.objUser.userID == AppUtility.getUserLoggedIn().userID {
                nibName = "ChatTableViewCellRight"
            } else {
                nibName = "ChatTableViewCellLeft"
            }
        }
        
        tableView.register(UINib(nibName: "ChatTableViewCellRight", bundle:nil),
                                forCellReuseIdentifier: "ChatTableViewCellRight")
        tableView.register(UINib(nibName: "ChatTableViewCellLeft", bundle:nil),
                                forCellReuseIdentifier: "ChatTableViewCellLeft")
        
        tableView.register(UINib(nibName: "ChatTableViewImageCellRight", bundle:nil),
                                forCellReuseIdentifier: "ChatTableViewImageCellRight")
        tableView.register(UINib(nibName: "ChatTableViewImageCellLeft", bundle:nil),
                                forCellReuseIdentifier: "ChatTableViewImageCellLeft")

        tableView.register(UINib(nibName: nibName, bundle: nil), forCellReuseIdentifier: nibName)
        var cell : ChatTableViewCell! = tableView.dequeueReusableCell(withIdentifier: nibName) as? ChatTableViewCell

        if (cell == nil) {
            cell = ChatTableViewCell.init(style: UITableViewCell.CellStyle.default,
                                       reuseIdentifier:nibName);
        }

        cell.selectionStyle = UITableViewCell.SelectionStyle.none

        if obj.mediaFileType == "image" {
            if obj.image.size.width > 0 {
                cell.imageView?.image = obj.image
            } else {
                AppUtility.loadImage(imageView: cell.imageViewChat, urlString: obj.media, placeHolderImageString: "bb")
            }
            cell.lblDateTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdAt)
        } else {
            cell.lblMessage.text = obj.message
            cell.lblDateTime.text = AppUtility.getDateStringFormat_monthname_date_year_hour_min(date: obj.createdAt)
        }

        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
        let obj = self.arrMessages[indexPath.row]
//        if obj.type == "Attachment" {
//            let browser = SKPhotoBrowser(photos: images)
//            browser.initializePageIndex(indexPath.row)
//            present(browser, animated: true, completion: {})
//        }
    }
}
