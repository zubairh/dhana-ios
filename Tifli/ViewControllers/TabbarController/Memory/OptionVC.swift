//
//  OptionVC.swift
//  Tifli
//
//  Created by zubair on 24/11/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit

protocol OptionVCDelegate {
    func addNewAlbumMemory()
}

class OptionVC: UIViewController {
    
    var delegate:OptionVCDelegate? = nil
    
    @IBOutlet weak var btnNewAlbum: UIButton!
    @IBOutlet weak var btnOrder: UIButton!
    @IBOutlet weak var btnSlideShow: GradientButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnNewAlbum.setTitle(btnNewAlbum.titleLabel?.text?.localized(), for: .normal)
        btnOrder.setTitle(btnOrder.titleLabel?.text?.localized(), for: .normal)
        btnSlideShow.setTitle(btnSlideShow.titleLabel?.text?.localized(), for: .normal)
    }

    //MARK:- Button Actions
    @IBAction func dismissTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func createNewAlbumTapped(_ sender: Any) {
        let vc = AddNewAlbumVC(nibName:String(describing: AddNewAlbumVC.self), bundle:nil)
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func orderAlbumTapped(_ sender: Any) {
        
    }
    
    @IBAction func slideShowTapped(_ sender: Any) {
        
    }
}

extension OptionVC:AddNewAlbumVCDelegate {
    func addNewAlbum() {
        self.delegate?.addNewAlbumMemory()
    }
}
