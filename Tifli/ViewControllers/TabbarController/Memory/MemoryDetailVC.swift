//
//  MemoryDetailVC.swift
//  Tifli
//
//  Created by zubair on 29/11/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

protocol MemoryDetailVCDelegate {
    func editDeleteMemory()
}

class MemoryDetailVC: UIViewController {

    @IBOutlet weak var lablTitle: UILabel!
    @IBOutlet weak var lablDate: UILabel!
    @IBOutlet weak var lablDetail: UILabel!
    @IBOutlet weak var imgMemory: UIImageView!
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    
    var objMemory = Memory()
    var albumId = "0"
    var delegate:MemoryDetailVCDelegate? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lablTitle.text = objMemory.memoryTitle
        self.lablDate.text = AppUtility.get_memory_date_time(dateString: objMemory.memoryDate)
        self.lablDetail.text = objMemory.memoryDetail
        if objMemory.fileType == "image" {
            btnPlay.isHidden = true
            AppUtility.setImage(url: objMemory.memoryFile, image: self.imgMemory)
        } else {
            btnPlay.isHidden = false
            AppUtility.setImage(url: objMemory.memoryFileThumbnail, image: self.imgMemory)
        }
//        if objMemory.fileType == "image" {
//            cell.btnPlay.isHidden = true
//        } else {
//            cell.btnPlay.isHidden = false
//        }
        
        if appSceneDelegate.isArabic {
            btnBack.setImage(UIImage(named: BACK_AR), for: .normal)
        }
        
    }
    
    //MARK:- Button Actions
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func editTapped(_ sender: Any) {
        let viewController = EditAlbumVC(nibName:String(describing: EditAlbumVC.self), bundle:nil)
        viewController.objMemory = self.objMemory
        viewController.albumId = self.albumId
        viewController.delegate = self
        let navigationController = UINavigationController.init(rootViewController: viewController)
        navigationController.navigationBar.isHidden = true
        navigationController.modalPresentationStyle = .custom
        navigationController.modalTransitionStyle = .crossDissolve
        self.present(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func shareTapped(_ sender: Any) {
        
    }
    
    @IBAction func playTapped(_ sender: Any) {
        let player = AVPlayer(url: URL(string:objMemory.memoryFile)!)
        let vc = AVPlayerViewController()
        vc.player = player
        self.present(vc, animated: true) { vc.player?.play() }
    }
}

extension MemoryDetailVC:EditAlbumVCDelegate {
    func deleteSaveMemoryAlbum() {
        self.delegate?.editDeleteMemory()
        self.navigationController?.popViewController(animated: false)
    }
}
