//
//  AddNewMemoryVC.swift
//  Tifli
//
//  Created by zubair on 24/11/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import SwiftyJSON
import AVKit
import AVFoundation
import MobileCoreServices
import Photos

protocol AddNewMemoryVCDelegate {
    func addNewMemory()
}

class AddNewMemoryVC: UIViewController, UIImagePickerControllerDelegate,UITextViewDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var tfTitle: UITextField!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var tfDate: UITextField!
    @IBOutlet weak var tfTime: UITextField!
    @IBOutlet weak var tfCaption: UITextField!
    @IBOutlet weak var tfAlbum: UITextField!
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var lablTitle: UILabel!
    @IBOutlet weak var btnAdd: GradientButton!
    @IBOutlet weak var btnBAck: UIButton!
    
    var arrAlbum = [MemoryAlbum]()
    var delegate:AddNewMemoryVCDelegate? = nil
    let imagePicker = UIImagePickerController()
    var type = -1
    var videoData = NSData()
    var player = AVPlayer()
    var playerController = AVPlayerViewController()
    var playerLayer = AVPlayerLayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker.delegate = self
        
        let dates = Date()
        
        self.tfDate.text = AppUtility.convertDateToString(date: dates as NSDate)
        self.tfTime.text = AppUtility.convertDateToStringTime(date: dates as NSDate)
        
        lablTitle.text = lablTitle.text?.localized()
        btnAdd.setTitle(btnAdd.titleLabel?.text?.localized(), for: .normal)
        tfTitle.placeholder = tfTitle.placeholder?.localized()
        tfAlbum.placeholder = tfTitle.placeholder?.localized()
        tfCaption.placeholder = tfCaption.placeholder?.localized()
       
        if appSceneDelegate.isArabic {
            tfDate.textAlignment = .right
            tfTime.textAlignment = .right
            tfTitle.textAlignment = .right
            tfAlbum.textAlignment = .right
            tfCaption.textAlignment = .right
            btnBAck.setImage(UIImage(named: BACK_AR), for: .normal)
        }
    }
    
    func CheckFields() -> Bool {
        if imgProduct.image?.size.width == 0 {
            
            AppUtility.showInfoMessage(message: "Please select photo".localized())
            let test = false
            return test
            
        } else if (tfDate.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            
            AppUtility.showInfoMessage(message: "Please select date".localized())
            let test = false
            return test
            
        } else if (tfTime.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            
            AppUtility.showInfoMessage(message: "Please select time".localized())
            let test = false
            return test
        }
        else if (tfTitle.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            AppUtility.showInfoMessage(message: "Please enter title".localized())
            let test = false
            return test
        }
        else if (tfCaption.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            AppUtility.showInfoMessage(message: "Please enter caption".localized())
            let test = false
            return test
        }
        
        let test = true
        return test
    }
    
    func openCamera() {
        let actionSheet = UIAlertController(title: nil, message: "Select Picture".localized(), preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: "Take Photo From Camera".localized(), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.imagePicker.allowsEditing = false
                self.imagePicker.sourceType = UIImagePickerController.SourceType.camera
                self.imagePicker.cameraCaptureMode = .photo
                self.imagePicker.modalPresentationStyle = .fullScreen
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        })
        
        let videoAction = UIAlertAction(title: "Take Video From Camera".localized(), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
            {
                self.imagePicker.sourceType = UIImagePickerController.SourceType.camera
                //imagePicker.cameraCaptureMode = UIImagePickerController.CameraCaptureMode.video
                self.imagePicker.mediaTypes = [kUTTypeMovie as String]
                self.imagePicker.allowsEditing = true
                self.imagePicker.videoMaximumDuration = 30
                self.imagePicker.showsCameraControls = true
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        })
        
        let libraryAction = UIAlertAction(title: "Choose From Library".localized(), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.imagePicker.sourceType = .photoLibrary
            self.imagePicker.allowsEditing = true
            self.imagePicker.mediaTypes = ["public.image", "public.movie"]
            self.present(self.imagePicker, animated: true, completion: nil)
        
        })
        
        let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
        })
        actionSheet.addAction(cameraAction)
        actionSheet.addAction(videoAction)
        actionSheet.addAction(libraryAction)
        actionSheet.addAction(cancelAction)
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    //Image Picker Delegates
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let mediaType = info[.mediaType] as? String
        print(info)
        if mediaType  == "public.image" {
            
            if let image = info[.editedImage] as? UIImage {
                self.imgProduct.image = image
            }
            if let image = info[.originalImage] as? UIImage {
                self.imgProduct.image = image
            }
            self.type = 1
            self.btnPlay.isHidden = true
        }
        
        if mediaType == "public.movie" {
            
            if let media =  info[UIImagePickerController.InfoKey.mediaURL] as? URL {
                appDelegate.delay(0.0) {
                    do {
                        self.videoData = try NSData(contentsOf: media, options: NSData.ReadingOptions())
                    } catch {
                        print(error)
                    }
                    self.player = AVPlayer(url: media)
                    self.playerLayer = AVPlayerLayer(player: self.player)
                    self.playerLayer.frame = self.imgProduct.bounds
                    self.playerLayer.videoGravity = .resizeAspectFill
                    self.imgProduct.layer.addSublayer(self.playerLayer)
                    self.player.seek(to: CMTime.zero)
                }
                self.type = 2
                self.btnPlay.isHidden = false

            } else if let ref =  info[UIImagePickerController.InfoKey.referenceURL] as? URL {
                let fetchResult = PHAsset.fetchAssets(withALAssetURLs: [ref], options: nil)
                if let phAsset = fetchResult.firstObject {
                    PHImageManager.default().requestAVAsset(forVideo: phAsset, options: PHVideoRequestOptions(), resultHandler: { (asset, audioMix, info) -> Void in
                            if let asset = asset as? AVURLAsset {
                                do {
                                    let urls = URL(string: ref.absoluteString)
                                    self.videoData = try NSData(contentsOf: asset.url, options: NSData.ReadingOptions())
                                } catch {
                                    print(error)
                                }
                                appDelegate.delay(0.0) {
                                    self.player = AVPlayer(url: ref.absoluteURL)
                                    self.playerLayer = AVPlayerLayer(player: self.player)
                                    self.playerLayer.frame = self.imgProduct.bounds
                                    self.playerLayer.videoGravity = .resizeAspectFill
                                    self.imgProduct.layer.addSublayer(self.playerLayer)
                                    self.player.seek(to: CMTime.zero)
                                }
                                self.type = 2
                                self.btnPlay.isHidden = false
                            }
                        })
                    }
            }
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    
    @objc func playerStalled(note: NSNotification) {
        self.player.seek(to: CMTime.zero)
        self.player.play()
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
         dismiss(animated: true, completion: nil)
    }
    
    //MARK:- APIs Function
    func addNewMemory() {
        self.view.endEditing(true)
        var paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":AppUtility.getBabyId(), "memoryTitle":tfTitle.text!, "memoryDate":tfDate.text!+" "+tfTime.text!, "memoryDetail":tfCaption.text!]
        if tfAlbum.tag > 0 {
            paramDict = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":AppUtility.getBabyId(), "memoryTitle":tfTitle.text!, "memoryDate":tfDate.text!+" "+tfTime.text!, "memoryDetail":tfCaption.text!, "memoryAlbumId":tfAlbum.tag]
        }
       
        if type == 1 {
            let data = imgProduct.image?.jpegData(compressionQuality: 0.5)
            APIRequestUtil.addEditMemory(parameters: paramDict, data: data ?? Data(), extensionString: "image/jpeg", fileNameKey: "memoryMedia") { (result, error) in
                if(result != nil) {
                    
                    let swiftyJsonVar = JSON(result!)

                    let dict = swiftyJsonVar.dictionaryValue
                    let status = dict["status"]?.intValue
                    let message = dict["message"]?.string
                    
                    if status == SUCESS_CODE {
                        
                        self.delegate?.addNewMemory()
                        AppUtility.showSuccessMessage(message: message ?? "Memory added successfully!".localized())
                        self.navigationController?.popViewController(animated: true)
                        
                    } else {
                        
                        AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                        return
                    }
                }
            }
        } else if type == 2 {
            
            APIRequestUtil.addEditMemory(parameters: paramDict, data: videoData as Data, extensionString: "video/quicktime", fileNameKey: "memoryMedia") { (result, error) in
                if(result != nil) {
                    
                    let swiftyJsonVar = JSON(result!)

                    let dict = swiftyJsonVar.dictionaryValue
                    let status = dict["status"]?.intValue
                    let message = dict["message"]?.string
                    
                    if status == SUCESS_CODE {
                        
                        self.delegate?.addNewMemory()
                        AppUtility.showSuccessMessage(message: message ?? "Memory added successfully!".localized())
                        self.navigationController?.popViewController(animated: true)
                        
                    } else {
                        
                        AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                        return
                    }
                }
            }
        }
    }
    
    //MARK:- Button Actions
    @IBAction func libraryTapped(_ sender: Any) {
        self.openCamera()
//        ImagePickerManager().pickImage(self) { image in
//            self.imgProduct.image = image
//        }
    }
    
    @IBAction func cameraTapped(_ sender: Any) {
        self.openCamera()
//        ImagePickerManager().pickImage(self) { image in
//            self.imgProduct.image = image
//        }
    }
    
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func dayTapped(_ sender: UIButton) {
        let datePicker = ActionSheetDatePicker(title: "Select Date".localized(), datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let date:NSDate = value as! NSDate
            
            self.tfDate.text = AppUtility.convertDateToString(date: date)
            
            return
            
        }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview!.superview)
        datePicker?.minimumDate = Date()
        datePicker?.show()
    }
    
    @IBAction func timeTapped(_ sender: UIButton) {
        let datePicker = ActionSheetDatePicker(title: "Select Time".localized(), datePickerMode: UIDatePicker.Mode.time, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let date:NSDate = value as! NSDate
            
            self.tfTime.text = AppUtility.convertDateToStringTime(date: date)
            
            return
            
        }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview!.superview)
        datePicker?.show()
    }
    
    @IBAction func addAlbumTapped(_ sender: Any) {
        var arr = [String]()
        for obj in self.arrAlbum {
            arr.append(obj.albumTitle)
        }
        if arr.count > 0 {
            ActionSheetStringPicker.show(withTitle: "Select album".localized(), rows: arr, initialSelection: 0, doneBlock: {
                picker, indexes, values in
                
                self.tfAlbum.tag = Int(self.arrAlbum[indexes].albumId)!
                self.tfAlbum.text = values as? String

                return
            }, cancel: { ActionSheetStringPicker in return},origin: sender)
        } else {
            AppUtility.showInfoMessage(message: "Please add album first".localized())
        }
    }

    @IBAction func addTapped(_ sender: Any) {
        if self.CheckFields() {
            self.addNewMemory()
        }
    }
    
    @IBAction func playTapped(_ sender: Any) {
        self.playerController = AVPlayerViewController()
        self.playerController.player = self.player
        self.present(self.playerController, animated: true) {
            self.playerController.player!.play()
        }
    }
    
}
