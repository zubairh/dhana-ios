//
//  MemoryV.swift
//  Dhana
//
//  Created by zubair on 22/06/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

class MemoryV: UIViewController {

    @IBOutlet weak var tfSearch: UITextField!
    @IBOutlet weak var collectionViews: UICollectionView!
    @IBOutlet weak var tableViews: UITableView!
    @IBOutlet weak var imgBaby: UIImageView!
    @IBOutlet weak var lablBabyName: UILabel!
    @IBOutlet weak var lablYear: UILabel!
    @IBOutlet weak var tableBaby: UITableView!
    @IBOutlet weak var segmentConreol: SegmentedControl!
    @IBOutlet weak var VIEWEMPTY: UIView!
    @IBOutlet weak var lablAddNew: UILabel!
    
    var arrMemoryList = [Memory]()
    var arrMemoryAlbum = [MemoryAlbum]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lablAddNew.text = lablAddNew.text?.localized()
        segmentConreol.setTitle(segmentConreol.titleForSegment(at: 0), forSegmentAt: 0)
        segmentConreol.setTitle(segmentConreol.titleForSegment(at: 1), forSegmentAt: 1)
        tableViews.isHidden = true
        
        self.getMemoryList()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        appSceneDelegate.navController = self.navigationController!
        for obj in AppManager.shared.arrBabies {
            if appDelegate.babyId != "0" {
                if obj.babyID == appDelegate.babyId {
                    appDelegate.babyId = "0"
                    AppUtility.loadImage(imageView: imgBaby, urlString: obj.babyImage, placeHolderImageString: "bb")
                    lablBabyName.text = obj.babyName
                    lablYear.text = obj.age
                    break
                }
            } else {
                if obj.babyID == AppUtility.getBabyId() {
                    AppUtility.loadImage(imageView: imgBaby, urlString: obj.babyImage, placeHolderImageString: "bb")
                    lablBabyName.text = obj.babyName
                    lablYear.text = obj.age
                }
            }
        }
    }
    
    //MARK:- Apis Functions
    func getMemoryList() {
        self.view.endEditing(true)
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":AppUtility.getBabyId()]
        APIRequestUtil.getMemoriesList(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)
                print(swiftyJsonVar)
                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.arrMemoryList.removeAll()

                    let valueArray: [JSON] = dict["data"]!.arrayValue
                    for dict in valueArray {
                        let dictValue: Dictionary<String, JSON> = dict.dictionaryValue
                        let obj = Memory.init(withDictionary: dictValue)
                        self.arrMemoryList.append(obj)
                    }
                    if self.arrMemoryList.count > 0 {
                        self.VIEWEMPTY.isHidden = true
                    } else {
                        self.VIEWEMPTY.isHidden = false
                    }
                    self.collectionViews.reloadData()
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    func getMemoryAlbumList() {
        self.view.endEditing(true)
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":AppUtility.getBabyId()]
        APIRequestUtil.getMemoriesAlbumList(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)
                print(swiftyJsonVar)
                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.arrMemoryAlbum.removeAll()

                    let valueArray: [JSON] = dict["data"]!.arrayValue
                    for dict in valueArray {
                        let dictValue: Dictionary<String, JSON> = dict.dictionaryValue
                        let obj = MemoryAlbum.init(withDictionary: dictValue)
                        self.arrMemoryAlbum.append(obj)
                    }
                    if self.arrMemoryAlbum.count > 0 {
                        self.VIEWEMPTY.isHidden = true
                    } else {
                        self.VIEWEMPTY.isHidden = false
                    }
                    self.tableViews.reloadData()
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }

    //MARK:- Button Actions
    @IBAction func addNewTapped(_ sender: Any) {
        let viewController = AddNewMemoryVC(nibName:String(describing: AddNewMemoryVC.self), bundle:nil)
        viewController.delegate = self
        viewController.arrAlbum = self.arrMemoryAlbum
        viewController.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(viewController, animated: false)
    }
    
    @IBAction func segmentTapped(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            collectionViews.isHidden = false
            tableViews.isHidden = true
            getMemoryList()
        } else {
            collectionViews.isHidden = true
            tableViews.isHidden = false
            getMemoryAlbumList()
        }
    }
    
    @IBAction func moreTapped(_ sender: Any) {
        let viewController = OptionVC(nibName:String(describing: OptionVC.self), bundle:nil)
        viewController.delegate = self
        let navigationController = UINavigationController.init(rootViewController: viewController)
        navigationController.navigationBar.isHidden = true
        navigationController.modalPresentationStyle = .custom
        navigationController.modalTransitionStyle = .crossDissolve
        self.present(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func dropDownBabyTapped(_ sender: UIButton) {
        if sender.tag == 0 {
            sender.tag = 1
            tableBaby.isHidden = false
        } else {
            sender.tag = 0
            tableBaby.isHidden = true
        }
    }
    
    @IBAction func sideMenuTapped(_ sender: Any) {
        self.sideMenuViewController.presentRightMenuViewController()
    }
    
    @IBAction func notifyTapped(_ sender: Any) {
        let viewController = NotificationVC(nibName:String(describing: NotificationVC.self), bundle:nil)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}

// MARK: - Tableview Delegate-
extension MemoryV : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableBaby {
            return AppManager.shared.arrBabies.count
        }
        return self.arrMemoryAlbum.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tableBaby {
            
            tableView.register(UINib(nibName: "BabyCell", bundle: nil), forCellReuseIdentifier: "BabyCell")
            var cell : BabyCell! = tableView.dequeueReusableCell(withIdentifier: "BabyCell") as? BabyCell
            
            if (cell == nil) {
                cell = BabyCell.init(style: UITableViewCell.CellStyle.default,
                                              reuseIdentifier:"BabyCell");
            }
            
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            
            let objG = AppManager.shared.arrBabies[indexPath.row]
            cell.lablBabyName.text = objG.babyName
            cell.lablYear.text = objG.age
            AppUtility.loadImage(imageView: cell.imgBaby, urlString: objG.babyImage, placeHolderImageString: "bb")
            
            return cell
            
        } else {
            
            tableView.register(UINib(nibName: "MemoryAlbumCell", bundle: nil), forCellReuseIdentifier: "MemoryAlbumCell")
            var cell : MemoryAlbumCell! = tableView.dequeueReusableCell(withIdentifier: "MemoryAlbumCell") as? MemoryAlbumCell
            
            if (cell == nil) {
                cell = MemoryAlbumCell.init(style: UITableViewCell.CellStyle.default,
                                              reuseIdentifier:"MemoryAlbumCell");
            }
            
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            cell.lablTitle.text = self.arrMemoryAlbum[indexPath.row].albumTitle+" ("+self.arrMemoryAlbum[indexPath.row].memoryCount+")"
            cell.arrMemory = self.arrMemoryAlbum[indexPath.row].arrMemory
            cell.delegate = self
            cell.collectionMemory.reloadData()
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tableBaby {
            return 50
        }
        return 230
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tableBaby {
            
            self.tableBaby.isHidden = true
            AppUtility.saveBabyId(babyID: AppManager.shared.arrBabies[indexPath.row].babyID)
            
            AppUtility.loadImage(imageView: self.imgBaby, urlString: AppManager.shared.arrBabies[indexPath.row].babyImage, placeHolderImageString: "bb")
            self.lablBabyName.text = AppManager.shared.arrBabies[indexPath.row].babyName
            self.lablYear.text = AppManager.shared.arrBabies[indexPath.row].age
            
            if segmentConreol.selectedSegmentIndex == 0 {
                getMemoryList()
            } else {
                getMemoryAlbumList()
            }
            
        } else {
            
            let vc = MemoryListingVC(nibName:String(describing: MemoryListingVC.self), bundle:nil)
            vc.delegate = self
            vc.arrMemoryList = self.arrMemoryAlbum[indexPath.row].arrMemory
            vc.title = self.arrMemoryAlbum[indexPath.row].albumTitle
            vc.albumId = self.arrMemoryAlbum[indexPath.row].albumId
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

// MARK: - CollectionView Delegate-
extension MemoryV: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrMemoryList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return  CGSize(width: collectionView.bounds.width/3, height: 171)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        collectionView.register(UINib(nibName: String(describing: MemoryCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: MemoryCell.self))
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: MemoryCell.self), for: indexPath as IndexPath) as! MemoryCell
        
        let obj = self.arrMemoryList[indexPath.row]
        cell.lablTitle.text = obj.memoryTitle
        cell.lablDate.text = AppUtility.get_memory_date_time(dateString: obj.memoryDate)
        if obj.fileType == "image" {
            AppUtility.setImage(url: obj.memoryFile, image: cell.imgMemory)
            cell.btnPlay.isHidden = true
        } else {
            AppUtility.setImage(url: obj.memoryFileThumbnail, image: cell.imgMemory)
            cell.btnPlay.isHidden = false
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = MemoryDetailVC(nibName:String(describing: MemoryDetailVC.self), bundle:nil)
        vc.objMemory = self.arrMemoryList[indexPath.row]
        vc.delegate = self
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension MemoryV:AddNewMemoryVCDelegate {
    func addNewMemory() {
        if self.segmentConreol.selectedSegmentIndex == 0 {
            self.getMemoryList()
        } else {
            self.getMemoryAlbumList()
        }
    }
}

extension MemoryV:MemoryDetailVCDelegate {
    func editDeleteMemory() {
        if self.segmentConreol.selectedSegmentIndex == 0 {
            self.getMemoryList()
        } else {
            self.getMemoryAlbumList()
        }
    }
}

extension MemoryV: MemoryAlbumCellDelegate {
    func openDetailAlbum(objAlbum: Memory) {
        let vc = MemoryDetailVC(nibName:String(describing: MemoryDetailVC.self), bundle:nil)
        vc.objMemory = objAlbum
        vc.delegate = self
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension MemoryV: OptionVCDelegate {
    func addNewAlbumMemory() {
        if self.segmentConreol.selectedSegmentIndex == 0 {
            self.getMemoryList()
        } else {
            self.getMemoryAlbumList()
        }
    }
}

extension MemoryV: MemoryListingVCDelegate {
    func deleteAlbumNewMemory() {
        if self.segmentConreol.selectedSegmentIndex == 0 {
            self.getMemoryList()
        } else {
            self.getMemoryAlbumList()
        }
    }
}
