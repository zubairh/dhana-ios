//
//  MemoryListingVC.swift
//  Tifli
//
//  Created by zubair on 29/11/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol MemoryListingVCDelegate {
    func deleteAlbumNewMemory()
}

class MemoryListingVC: UIViewController {
    
    @IBOutlet weak var tfSearch: UITextField!
    @IBOutlet weak var collectionViews: UICollectionView!
    @IBOutlet weak var tfAlbumName: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    
    var arrMemoryList = [Memory]()
    var albumId = "0"
    var isEdit = false
    var delegate:MemoryListingVCDelegate? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tfAlbumName.text = self.title
        
        btnEdit.setTitle(btnEdit.titleLabel?.text?.localized(), for: .normal)
        tfSearch.placeholder = tfSearch.placeholder?.localized()
       
        if appSceneDelegate.isArabic {
            tfSearch.textAlignment = .right
            btnBack.setImage(UIImage(named: BACK_AR), for: .normal)
        }
    }
    
    //MARK:- Apis Function
    func deleteMemory() {
        self.view.endEditing(true)
        var arrDeleteMemoriesId = NSMutableArray()
        for obj in self.arrMemoryList {
            if obj.isSelect {
                arrDeleteMemoriesId.add(obj.ID)
            }
        }
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "memoryId":arrDeleteMemoriesId.componentsJoined(by: ",")]
        APIRequestUtil.deleteMemory(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)
                print(swiftyJsonVar)
                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.delegate?.deleteAlbumNewMemory()
                    AppUtility.showSuccessMessage(message: message ?? "Memory deleted successfully!".localized())
                    self.navigationController?.popViewController(animated: true)
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    //MARK:- Button Actions
    @IBAction func editTapped(_ sender: UIButton) {
        if !isEdit {
            isEdit = true
            sender.setTitle("Done", for: .normal)
            for obj in self.arrMemoryList {
                obj.isSelect = false
            }
        } else {
            var isSelect = false
            for obj in self.arrMemoryList {
                if obj.isSelect {
                    isSelect = true
                }
            }
            if isSelect {
                let alert = UIAlertController(title: "Information!".localized(), message: "Are you sure you want to delete all selected memories?".localized(), preferredStyle: UIAlertController.Style.alert)
                
                alert.addAction(UIAlertAction(title: "OK".localized(), style: .default, handler: { (action: UIAlertAction!) in
                    self.isEdit = false
                    sender.setTitle("Edit".localized(), for: .normal)
                    self.deleteMemory()
                }))
                alert.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: { (action: UIAlertAction!) in
                    for obj in self.arrMemoryList {
                        obj.isSelect = false
                    }
                    self.isEdit = false
                    sender.setTitle("Edit".localized(), for: .normal)
                }))
                
                self.present(alert, animated: true, completion: nil)
                
            } else {
                
                self.isEdit = false
                sender.setTitle("Edit".localized(), for: .normal)
            }
        }
        collectionViews.reloadData()
    }
    
    @IBAction func backTapped(_ sender: Any) {
        for obj in self.arrMemoryList {
            obj.isSelect = false
        }
        self.navigationController?.popViewController(animated: true)
    }
}

// MARK: - CollectionView Delegate-
extension MemoryListingVC: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrMemoryList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return  CGSize(width: collectionView.bounds.width/3, height: 171)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        collectionView.register(UINib(nibName: String(describing: MemoryCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: MemoryCell.self))
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: MemoryCell.self), for: indexPath as IndexPath) as! MemoryCell
        
        let obj = self.arrMemoryList[indexPath.row]
        cell.lablTitle.text = obj.memoryTitle
        cell.lablDate.text = AppUtility.get_memory_date_time(dateString: obj.memoryDate)
        if obj.fileType == "image" {
            AppUtility.setImage(url: obj.memoryFile, image: cell.imgMemory)
            cell.btnPlay.isHidden = true
        } else {
            AppUtility.setImage(url: obj.memoryFileThumbnail, image: cell.imgMemory)
            cell.btnPlay.isHidden = false
        }
        if isEdit {
            cell.imgSelectUnselect.isHidden = false
            if obj.isSelect {
                cell.imgSelectUnselect.image = UIImage(named: "tick")
            } else {
                cell.imgSelectUnselect.image = UIImage(named: "circle")
            }
        } else {
            cell.imgSelectUnselect.isHidden = true
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if isEdit {
            let obj = self.arrMemoryList[indexPath.row]
            if obj.isSelect {
                obj.isSelect = false
            } else {
                obj.isSelect = true
            }
            collectionViews.reloadData()
        } else {
            let vc = MemoryDetailVC(nibName:String(describing: MemoryDetailVC.self), bundle:nil)
            vc.objMemory = self.arrMemoryList[indexPath.row]
            vc.albumId = self.albumId
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
