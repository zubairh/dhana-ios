//
//  EditAlbumVC.swift
//  Tifli
//
//  Created by zubair on 30/11/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import SwiftyJSON

protocol EditAlbumVCDelegate {
    func deleteSaveMemoryAlbum()
}

class EditAlbumVC: UIViewController {

    @IBOutlet weak var tfTitle: UITextField!
    @IBOutlet weak var tfDetail: UITextField!
    @IBOutlet weak var tfAlbum: UITextField!
    @IBOutlet weak var btnSave: GradientButton!
    @IBOutlet weak var btnDelete: UIButton!
    
    var delegate:EditAlbumVCDelegate? = nil
    var objMemory = Memory()
    var albumId = "0"
    var arrMemoryAlbum = [MemoryAlbum]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tfTitle.text = objMemory.memoryTitle
        self.tfDetail.text = objMemory.memoryDetail
        
        btnSave.setTitle(btnSave.titleLabel?.text?.localized(), for: .normal)
        btnDelete.setTitle(btnDelete.titleLabel?.text?.localized(), for: .normal)
        tfTitle.placeholder = tfTitle.placeholder?.localized()
        tfAlbum.placeholder = tfAlbum.placeholder?.localized()
        tfDetail.placeholder = tfDetail.placeholder?.localized()
       
        if appSceneDelegate.isArabic {
            tfTitle.textAlignment = .right
            tfAlbum.textAlignment = .right
            tfDetail.textAlignment = .right
        }
        
        self.getMemoryAlbumList()
    }
    
    //MARK:- Apis Function
    func getMemoryAlbumList() {
        self.view.endEditing(true)
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":AppUtility.getBabyId()]
        APIRequestUtil.getMemoriesAlbumList(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)
                print(swiftyJsonVar)
                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.arrMemoryAlbum.removeAll()

                    let valueArray: [JSON] = dict["data"]!.arrayValue
                    for dict in valueArray {
                        let dictValue: Dictionary<String, JSON> = dict.dictionaryValue
                        let obj = MemoryAlbum.init(withDictionary: dictValue)
                        self.arrMemoryAlbum.append(obj)
                    }
                    
                    if self.albumId != "0" {
                        for obj in self.arrMemoryAlbum {
                            if self.albumId == obj.albumId {
                                self.tfAlbum.text = obj.albumTitle
                                self.tfAlbum.tag = Int(self.albumId)!
                                break
                            }
                        }
                    }
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    func editNewMemory() {
        self.view.endEditing(true)
        var paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":AppUtility.getBabyId(), "memoryTitle":tfTitle.text!, "memoryDate":objMemory.memoryDate, "memoryDetail":tfDetail.text!, "memoryId":objMemory.ID]
        if tfAlbum.tag > 0 {
            paramDict = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":AppUtility.getBabyId(), "memoryTitle":tfTitle.text!, "memoryDate":objMemory.memoryDate, "memoryDetail":tfDetail.text!, "memoryId":objMemory.ID, "memoryAlbumId":tfAlbum.tag]
        }
       
        APIRequestUtil.addEditMemory(parameters: paramDict, data: Data(), extensionString: "image/jpeg", fileNameKey: "memoryMedia") { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.delegate?.deleteSaveMemoryAlbum()
                    AppUtility.showSuccessMessage(message: message ?? "Memory edit successfully!")
                    self.dismiss(animated: true, completion: nil)
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    func deleteMemory() {
        self.view.endEditing(true)
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "memoryId":self.objMemory.ID]
        APIRequestUtil.deleteMemory(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)
                print(swiftyJsonVar)
                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.delegate?.deleteSaveMemoryAlbum()
                    AppUtility.showSuccessMessage(message: message ?? "Memory deleted successfully!")
                    self.dismiss(animated: true, completion: nil)
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }

    //MARK:- Button Actions
    @IBAction func addAlbumTapped(_ sender: Any) {
        var arr = [String]()
        for obj in self.arrMemoryAlbum {
            arr.append(obj.albumTitle)
        }
        if arr.count > 0 {
            ActionSheetStringPicker.show(withTitle: "Select album", rows: arr, initialSelection: 0, doneBlock: {
                picker, indexes, values in
                
                self.tfAlbum.tag = Int(self.arrMemoryAlbum[indexes].albumId)!
                self.tfAlbum.text = values as? String

                return
            }, cancel: { ActionSheetStringPicker in return},origin: sender)
        }
    }
    
    @IBAction func saveTapped(_ sender: Any) {
        self.editNewMemory()
    }
    
    @IBAction func deleteTapped(_ sender: Any) {
        self.deleteMemory()
    }
    
    @IBAction func dismissTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
