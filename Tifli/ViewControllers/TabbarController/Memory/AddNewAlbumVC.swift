//
//  AddNewAlbumVC.swift
//  Tifli
//
//  Created by zubair on 29/11/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol AddNewAlbumVCDelegate {
    func addNewAlbum()
}

class AddNewAlbumVC: UIViewController {
    
    @IBOutlet weak var viewBg: UIImageView!
    @IBOutlet weak var tfTitle: UITextField!
    @IBOutlet weak var lablTitle: UILabel!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnCreate: GradientButton!
    
    var delegate:AddNewAlbumVCDelegate? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnCancel.setTitle(btnCancel.titleLabel?.text?.localized(), for: .normal)
        btnCreate.setTitle(btnCreate.titleLabel?.text?.localized(), for: .normal)
        
        tfTitle.placeholder = tfTitle.placeholder?.localized()
        lablTitle.text = lablTitle.text?.localized()
       
        if appSceneDelegate.isArabic {
            tfTitle.textAlignment = .right
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let blurEffect = UIBlurEffect(style: .light)
        let blurredEffectView = UIVisualEffectView(effect: blurEffect)
        blurredEffectView.frame = self.view.bounds
        self.viewBg.addSubview(blurredEffectView)
    }
    
    //MARK:- Apis Functions
    func addNewAlbum() {
        self.view.endEditing(true)
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":AppUtility.getBabyId(), "memoryAlbumTitle":tfTitle.text!]
        APIRequestUtil.addEditAlbum(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)
                print(swiftyJsonVar)
                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    AppUtility.showSuccessMessage(message: message ?? "New album added successfully!")
                    self.delegate?.addNewAlbum()
                    self.dismiss(animated: true, completion: nil)
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    //MARK:- Button Actions
    @IBAction func cancelTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func createTapped(_ sender: Any) {
        if tfTitle.text!.count > 0 {
            self.addNewAlbum()
        } else {
            AppUtility.showErrorMessage(message: "Please enter album title")
        }
    }
    
}
