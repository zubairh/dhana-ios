//
//  GrowthViews.swift
//  Tifli
//
//  Created by zubair on 18/11/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol GrowthViewsDelegate {
    func addNewGrowthTapped()
}

class GrowthViews: UIView {
    
    @IBOutlet weak var tableGrapsh: UITableView!
    @IBOutlet weak var lablAddMeasurment: UILabel!
    
    var view: UIView!
    var arrLength = [GrowthChart]()
    var arrWeight = [GrowthChart]()
    var arrHeadCircle = [GrowthChart]()
    var arrRedFL = [String]()
    var arrRedFW = [String]()
    var arrRedFH = [String]()
    var arrTitles = [String]()
    var delegate:GrowthViewsDelegate? = nil
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        backgroundColor = .clear
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = true
        addSubview(view)
        setupLanguage()
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        return nibView
    }
    
    func setupLanguage() {
        lablAddMeasurment.text = lablAddMeasurment.text?.localized()
    }
    
    //MARK:- Apis Function
    func getGrowthChart() {
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "babyId":AppUtility.getBabyId(), "category":"Growth New"]
        APIRequestUtil.getAnalysis(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.arrLength.removeAll()
                    self.arrWeight.removeAll()
                    self.arrHeadCircle.removeAll()
                    
                    if let dictData: Dictionary<String, JSON> = dict["data"]?.dictionaryValue {
                        if let dictLength: Dictionary<String, JSON> = dictData["height"]?.dictionaryValue {
                            let valueArray: [JSON] = dictLength["data"]!.arrayValue
                            for dict in valueArray {
                                let dictValue: Dictionary<String, JSON> = dict.dictionaryValue
                                let obj = GrowthChart.init(withDictionary: dictValue)
                                obj.totalLength = dictLength["latest"]?.stringValue ?? "0"
                                self.arrLength.append(obj)
                            }
                            let flagArray: [JSON] = dictLength["redFlags"]!.arrayValue
                            for dict in flagArray {
                                self.arrRedFL.append(dict.stringValue)
                            }
                        }
                        if let dictWeight: Dictionary<String, JSON> = dictData["weight"]?.dictionaryValue {
                            let valueArray: [JSON] = dictWeight["data"]!.arrayValue
                            for dict in valueArray {
                                let dictValue: Dictionary<String, JSON> = dict.dictionaryValue
                                let obj = GrowthChart.init(withDictionary: dictValue)
                                obj.totalWeight = dictWeight["latest"]?.stringValue ?? "0"
                                self.arrWeight.append(obj)
                            }
                            let flagArray: [JSON] = dictWeight["redFlags"]!.arrayValue
                            for dict in flagArray {
                                self.arrRedFW.append(dict.stringValue)
                            }
                        }
                        if let dictHeadCircle: Dictionary<String, JSON> = dictData["headCircle"]?.dictionaryValue {
                            let valueArray: [JSON] = dictHeadCircle["data"]!.arrayValue
                            for dict in valueArray {
                                let dictValue: Dictionary<String, JSON> = dict.dictionaryValue
                                let obj = GrowthChart.init(withDictionary: dictValue)
                                obj.totalCircle = dictHeadCircle["latest"]?.stringValue ?? "0"
                                self.arrHeadCircle.append(obj)
                            }
                            let flagArray: [JSON] = dictHeadCircle["redFlags"]!.arrayValue
                            for dict in flagArray {
                                self.arrRedFH.append(dict.stringValue)
                            }
                        }
                    }
                    self.arrTitles = ["Height".localized(), "Weight".localized(), "Head Circumference".localized()]
                    self.tableGrapsh.reloadData()
                    AppManager.shared.arrDataChart = self.arrLength
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    //MARK:- Button Actions
    @IBAction func addNewTapped(_ sender: Any) {
        self.delegate?.addNewGrowthTapped()
    }
}

// MARK: - Tableview Delegate-
extension GrowthViews : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrTitles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        tableView.register(UINib(nibName: "GraphCell", bundle: nil), forCellReuseIdentifier: "GraphCell")
        var cell : GraphCell! = tableView.dequeueReusableCell(withIdentifier: "GraphCell") as? GraphCell
        
        if (cell == nil) {
            cell = GraphCell.init(style: UITableViewCell.CellStyle.default,
                                          reuseIdentifier:"GraphCell");
        }
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.lablTitle.text = self.arrTitles[indexPath.row]
        
        if indexPath.row == 0 {
            cell.arrData = self.arrLength
            if self.arrLength.count > 0 {
                cell.lablValues.text = cell.arrData[0].totalLength+" cm"
                if self.arrRedFL.count > 0 {
                    cell.lablNotes.text = self.arrRedFL[self.arrRedFL.count-1]
                } else {
                    cell.lablNotes.text = ""
                }
            } else {
                cell.lablValues.text = "0 cm"
                cell.lablNotes.text = ""
            }
            if self.arrRedFL.count > 0 {
                cell.lablNotes.textColor = .red
                cell.lablValues.textColor = .red
            } else {
                cell.lablNotes.textColor = #colorLiteral(red: 0.1098039216, green: 0.737254902, blue: 0.7803921569, alpha: 1)
                cell.lablValues.textColor = #colorLiteral(red: 0.1098039216, green: 0.737254902, blue: 0.7803921569, alpha: 1)
            }
        } else if indexPath.row == 1 {
            cell.arrData = self.arrWeight
            if self.arrWeight.count > 0 {
                cell.lablValues.text = cell.arrData[1].totalWeight+" kg"
                if self.arrRedFW.count > 0 {
                    cell.lablNotes.text = self.arrRedFW[self.arrRedFW.count-1]
                } else {
                    cell.lablNotes.text = ""
                }
            } else {
                cell.lablValues.text = "0 kg"
                cell.lablNotes.text = ""
            }
            if self.arrRedFW.count > 0 {
                cell.lablNotes.textColor = .red
                cell.lablValues.textColor = .red
            } else {
                cell.lablNotes.textColor = #colorLiteral(red: 0.1098039216, green: 0.737254902, blue: 0.7803921569, alpha: 1)
                cell.lablValues.textColor = #colorLiteral(red: 0.1098039216, green: 0.737254902, blue: 0.7803921569, alpha: 1)
            }
        } else if indexPath.row == 2 {
            cell.arrData = self.arrHeadCircle
            if self.arrHeadCircle.count > 0 {
                cell.lablValues.text = cell.arrData[2].totalCircle+" cm"
                if self.arrRedFH.count > 0 {
                    cell.lablNotes.text = self.arrRedFH[self.arrRedFH.count-1]
                } else {
                    cell.lablNotes.text = ""
                }
            } else {
                cell.lablValues.text = "0 cm"
                cell.lablNotes.text = ""
            }
            
            if self.arrRedFH.count > 0 {
                cell.lablNotes.textColor = .red
                cell.lablValues.textColor = .red
            } else {
                cell.lablNotes.textColor = #colorLiteral(red: 0.1098039216, green: 0.737254902, blue: 0.7803921569, alpha: 1)
                cell.lablValues.textColor = #colorLiteral(red: 0.1098039216, green: 0.737254902, blue: 0.7803921569, alpha: 1)
            }
        }
        cell.updateGraph()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
