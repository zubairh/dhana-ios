//
//  AnalysisVC.swift
//  Dhana
//
//  Created by zubair on 22/06/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

enum CAT_TYPE : String {
    case Growth = "Growth"
    case Teething = "Teething"
    case Milestones = "Milestones"
    case Sleep = "Sleep"
    case Feeding = "Feeding"
    case Diaper = "Diaper"
    case Pumping = "Pumping"
    case Temperature = "Temperature"
    case Vaccination = "Vaccination"
    case Medication = "Medication"
    case Liquid = "Liquid food"
}

class AnalysisVC: UIViewController {

    @IBOutlet weak var collectionAnalysis: UICollectionView!
    //@IBOutlet weak var tableGrapsh: UITableView!
    @IBOutlet weak var imgBaby: UIImageView!
    @IBOutlet weak var lablBabyName: UILabel!
    @IBOutlet weak var lablYear: UILabel!
    @IBOutlet weak var tableBaby: UITableView!
    @IBOutlet weak var containerView: UIView!
    
    var currentIndex = 0
    var arrLength = [GrowthChart]()
    var arrWeight = [GrowthChart]()
    var arrHeadCircle = [GrowthChart]()
    var arrRedFL = [String]()
    var arrRedFW = [String]()
    var arrRedFH = [String]()
    
    var arrCategory = [Categories]()
    
    var arrTitles = [String]()
    
    //child view
    var growthView = GrowthViews()
    var teethView = TeethView()
    var milstoneView = MilestoneView()
    var diaperView = DiaperView()
    var liquidFoodView = LiquidFoodView()
    var temperatureView = TemperatureView()
    var medicationView = MedicationView()
    var pumpingView = PumpingView()
    var feedingView = FeedView()
    var vaccineView = VaccinationView()
    var sleepView = SleepView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupContainerViews()
        
        self.showHideCategories(type: CAT_TYPE.Growth.rawValue)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        appSceneDelegate.navController = self.navigationController!
        
        for obj in AppManager.shared.arrBabies {
            if obj.babyID == AppUtility.getBabyId() {
                AppUtility.loadImage(imageView: imgBaby, urlString: obj.babyImage, placeHolderImageString: "bb")
                lablBabyName.text = obj.babyName
                lablYear.text = obj.age
            }
        }
        self.tableBaby.reloadData()
        if self.arrCategory.count > 0 {
            self.showHideCategories(type: self.arrCategory[currentIndex].titleEn)
        } else {
            self.getCategories()
        }
        //self.getGrowthChart()
    }
    
    func setupContainerViews () {

        self.containerView.layoutIfNeeded()
        
        DispatchQueue.main.async {
            self.growthView.frame = CGRect.init(x: 0, y: 0, width: self.containerView.bounds.width, height: self.containerView.bounds.height)
            self.growthView.delegate = self
            self.teethView.frame = CGRect.init(x: 0, y: 0, width: self.containerView.bounds.width, height: self.containerView.bounds.height)
            //self.teethView.delegate = self
            self.milstoneView.frame = CGRect.init(x: 0, y: 0, width: self.containerView.bounds.width, height: self.containerView.bounds.height)
            self.diaperView.frame = CGRect.init(x: 0, y: 0, width: self.containerView.bounds.width, height: self.containerView.bounds.height)
            self.liquidFoodView.frame = CGRect.init(x: 0, y: 0, width: self.containerView.bounds.width, height: self.containerView.bounds.height)
            self.temperatureView.frame = CGRect.init(x: 0, y: 0, width: self.containerView.bounds.width, height: self.containerView.bounds.height)
            self.medicationView.frame = CGRect.init(x: 0, y: 0, width: self.containerView.bounds.width, height: self.containerView.bounds.height)
            self.pumpingView.frame = CGRect.init(x: 0, y: 0, width: self.containerView.bounds.width, height: self.containerView.bounds.height)
            self.feedingView.frame = CGRect.init(x: 0, y: 0, width: self.containerView.bounds.width, height: self.containerView.bounds.height)
            self.vaccineView.frame = CGRect.init(x: 0, y: 0, width: self.containerView.bounds.width, height: self.containerView.bounds.height)
            self.sleepView.frame = CGRect.init(x: 0, y: 0, width: self.containerView.bounds.width, height: self.containerView.bounds.height)
            
            self.temperatureView.delegate = self
            self.containerView.addSubview(self.growthView)
            self.containerView.addSubview(self.teethView)
            self.containerView.addSubview(self.milstoneView)
            self.containerView.addSubview(self.diaperView)
            self.containerView.addSubview(self.liquidFoodView)
            self.containerView.addSubview(self.temperatureView)
            self.containerView.addSubview(self.medicationView)
            self.containerView.addSubview(self.pumpingView)
            self.containerView.addSubview(self.feedingView)
            self.containerView.addSubview(self.vaccineView)
            self.containerView.addSubview(self.sleepView)
        }
    }
    
    func showHideCategories(type : String) {
        
        if type == CAT_TYPE.Growth.rawValue {
            
            growthView.isHidden = false
            growthView.getGrowthChart()
            self.containerView.bringSubviewToFront(growthView)
            
            teethView.isHidden = true
            milstoneView.isHidden = true
            diaperView.isHidden = true
            liquidFoodView.isHidden = true
            temperatureView.isHidden = true
            medicationView.isHidden = true
            pumpingView.isHidden = true
            feedingView.isHidden = true
            vaccineView.isHidden = true
            sleepView.isHidden = true
            
        }
        else if type == CAT_TYPE.Teething.rawValue {
            
            growthView.isHidden = true
            milstoneView.isHidden = true
            diaperView.isHidden = true
            liquidFoodView.isHidden = true
            temperatureView.isHidden = true
            medicationView.isHidden = true
            pumpingView.isHidden = true
            feedingView.isHidden = true
            vaccineView.isHidden = true
            sleepView.isHidden = true
            
            teethView.isHidden = false
            appDelegate.delay(0.1, closure: {
                self.teethView.setCircular()
                self.teethView.getTeethChart()
            })
            self.containerView.bringSubviewToFront(teethView)
        }
        else if type == CAT_TYPE.Milestones.rawValue {
            
            growthView.isHidden = true
            teethView.isHidden = true
            diaperView.isHidden = true
            liquidFoodView.isHidden = true
            temperatureView.isHidden = true
            medicationView.isHidden = true
            pumpingView.isHidden = true
            feedingView.isHidden = true
            vaccineView.isHidden = true
            sleepView.isHidden = true
            
            milstoneView.getMilestoneChart(year: "1")
            milstoneView.isHidden = false
            self.containerView.bringSubviewToFront(milstoneView)
        }
        else if type == CAT_TYPE.Diaper.rawValue {
            
            growthView.isHidden = true
            milstoneView.isHidden = true
            teethView.isHidden = true
            liquidFoodView.isHidden = true
            temperatureView.isHidden = true
            medicationView.isHidden = true
            pumpingView.isHidden = true
            feedingView.isHidden = true
            vaccineView.isHidden = true
            sleepView.isHidden = true
            
            diaperView.getDiaperChart()
            diaperView.tableGrapsh.reloadData()
            diaperView.isHidden = false
            self.containerView.bringSubviewToFront(diaperView)
        }
        else if type == CAT_TYPE.Liquid.rawValue {
            
            growthView.isHidden = true
            milstoneView.isHidden = true
            teethView.isHidden = true
            diaperView.isHidden = true
            temperatureView.isHidden = true
            medicationView.isHidden = true
            pumpingView.isHidden = true
            feedingView.isHidden = true
            vaccineView.isHidden = true
            sleepView.isHidden = true
            
            liquidFoodView.tableGrapsh.reloadData()
            liquidFoodView.isHidden = false
            self.containerView.bringSubviewToFront(liquidFoodView)
        }
        else if type == CAT_TYPE.Temperature.rawValue {
            
            growthView.isHidden = true
            milstoneView.isHidden = true
            teethView.isHidden = true
            liquidFoodView.isHidden = true
            diaperView.isHidden = true
            medicationView.isHidden = true
            pumpingView.isHidden = true
            feedingView.isHidden = true
            vaccineView.isHidden = true
            sleepView.isHidden = true
             
            temperatureView.getTemperatureChart()
            temperatureView.tableGrapsh.reloadData()
            temperatureView.isHidden = false
            self.containerView.bringSubviewToFront(temperatureView)
        }
        else if type == CAT_TYPE.Medication.rawValue {
            
            growthView.isHidden = true
            milstoneView.isHidden = true
            teethView.isHidden = true
            liquidFoodView.isHidden = true
            diaperView.isHidden = true
            temperatureView.isHidden = false
            pumpingView.isHidden = true
            feedingView.isHidden = true
            vaccineView.isHidden = true
            sleepView.isHidden = true
            
            medicationView.getMedicalChart()
            medicationView.tableViews.reloadData()
            medicationView.isHidden = false
            self.containerView.bringSubviewToFront(medicationView)
        }
        else if type == CAT_TYPE.Pumping.rawValue {
            
            growthView.isHidden = true
            milstoneView.isHidden = true
            teethView.isHidden = true
            liquidFoodView.isHidden = true
            diaperView.isHidden = true
            temperatureView.isHidden = false
            medicationView.isHidden = true
            feedingView.isHidden = true
            vaccineView.isHidden = true
            sleepView.isHidden = true
            
            pumpingView.getPumpingChart()
            pumpingView.tableGrapsh.reloadData()
            pumpingView.isHidden = false
            self.containerView.bringSubviewToFront(pumpingView)
        }
        else if type == CAT_TYPE.Feeding.rawValue {
            
            growthView.isHidden = true
            milstoneView.isHidden = true
            teethView.isHidden = true
            liquidFoodView.isHidden = true
            diaperView.isHidden = true
            temperatureView.isHidden = false
            medicationView.isHidden = true
            pumpingView.isHidden = true
            vaccineView.isHidden = true
            sleepView.isHidden = true
            
            feedingView.getFeedChart()
            feedingView.tableViews.reloadData()
            feedingView.isHidden = false
            self.containerView.bringSubviewToFront(feedingView)
        }
        else if type == CAT_TYPE.Vaccination.rawValue {
            
            growthView.isHidden = true
            milstoneView.isHidden = true
            teethView.isHidden = true
            liquidFoodView.isHidden = true
            diaperView.isHidden = true
            temperatureView.isHidden = false
            medicationView.isHidden = true
            pumpingView.isHidden = true
            feedingView.isHidden = true
            sleepView.isHidden = true
            
            vaccineView.getVaccinationChart()
            vaccineView.tableViews.reloadData()
            vaccineView.isHidden = false
            vaccineView.showCircularProgress()
            self.containerView.bringSubviewToFront(vaccineView)
        }
        else if type == CAT_TYPE.Sleep.rawValue {
            
            growthView.isHidden = true
            milstoneView.isHidden = true
            teethView.isHidden = true
            liquidFoodView.isHidden = true
            diaperView.isHidden = true
            temperatureView.isHidden = false
            medicationView.isHidden = true
            pumpingView.isHidden = true
            feedingView.isHidden = true
            vaccineView.isHidden = true
            
            sleepView.getSleepChart()
            sleepView.tableViews.reloadData()
            sleepView.isHidden = false
            self.containerView.bringSubviewToFront(sleepView)
        }
    }
    
    //MARK:- Apis Functions
    func getCategories() {
        self.view.endEditing(true)
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken()]
        APIRequestUtil.getAnalysisCategory(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.arrCategory.removeAll()
                    
                    let valueArray: [JSON] = dict["data"]!.arrayValue
                    for dict in valueArray {
                        let dictValue: Dictionary<String, JSON> = dict.dictionaryValue
                        let obj = Categories.init(withDictionary: dictValue)
                        self.arrCategory.append(obj)
                    }
                    self.collectionAnalysis.reloadData()
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }

    //MARK:- Button Actions
    @IBAction func addNewTapped(_ sender: Any) {
        let viewController = AddGrowthVC(nibName:String(describing: AddGrowthVC.self), bundle:nil)
        viewController.delegate = self
        let navigationController = UINavigationController.init(rootViewController: viewController)
        navigationController.navigationBar.isHidden = true
        navigationController.modalPresentationStyle = .custom
        navigationController.modalTransitionStyle = .crossDissolve
        self.present(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func addNewTempreatureTapped(_ sender: Any) {
        let viewController = AddTempretaureVc(nibName:String(describing: AddTempretaureVc.self), bundle:nil)
        let navigationController = UINavigationController.init(rootViewController: viewController)
        navigationController.navigationBar.isHidden = true
        navigationController.modalPresentationStyle = .custom
        navigationController.modalTransitionStyle = .crossDissolve
        self.present(navigationController, animated: true, completion: nil)
    }
    
    @objc func categorySelectTapped(_ sender:UIButton) {
        currentIndex = sender.tag
        collectionAnalysis.reloadData()
        self.showHideCategories(type: self.arrCategory[sender.tag].titleEn)
    }
    
    @IBAction func sideMenuTapped(_ sender: Any) {
        self.sideMenuViewController.presentRightMenuViewController()
    }
    
    @IBAction func notifyTapped(_ sender: Any) {
        let viewController = NotificationVC(nibName:String(describing: NotificationVC.self), bundle:nil)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func dropDownBabyTapped(_ sender: UIButton) {
        if sender.tag == 0 {
            sender.tag = 1
            tableBaby.isHidden = false
        } else {
            sender.tag = 0
            tableBaby.isHidden = true
        }
    }
}

// MARK: - Tableview Delegate-
extension AnalysisVC : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AppManager.shared.arrBabies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        tableView.register(UINib(nibName: "BabyCell", bundle: nil), forCellReuseIdentifier: "BabyCell")
        var cell : BabyCell! = tableView.dequeueReusableCell(withIdentifier: "BabyCell") as? BabyCell
        
        if (cell == nil) {
            cell = BabyCell.init(style: UITableViewCell.CellStyle.default,
                                          reuseIdentifier:"BabyCell");
        }
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        let objG = AppManager.shared.arrBabies[indexPath.row]
        cell.lablBabyName.text = objG.babyName
        cell.lablYear.text = objG.age
        AppUtility.loadImage(imageView: cell.imgBaby, urlString: objG.babyImage, placeHolderImageString: "bb")
        
        return cell
            
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tableBaby {
            self.tableBaby.isHidden = true
            AppUtility.saveBabyId(babyID: AppManager.shared.arrBabies[indexPath.row].babyID)
            
            //self.getGrowthChart()
            
            AppUtility.loadImage(imageView: self.imgBaby, urlString: AppManager.shared.arrBabies[indexPath.row].babyImage, placeHolderImageString: "bb")
            self.lablBabyName.text = AppManager.shared.arrBabies[indexPath.row].babyName
            self.lablYear.text = AppManager.shared.arrBabies[indexPath.row].age
            
            self.showHideCategories(type: self.arrCategory[currentIndex].titleEn)
        }
    }
}

// MARK: - CollectionView Delegate-
extension AnalysisVC: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrCategory.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return  CGSize(width: 130, height: 45)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        collectionView.register(UINib(nibName: String(describing: CategoryCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: CategoryCell.self))
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: CategoryCell.self), for: indexPath as IndexPath) as! CategoryCell
        
        cell.btnCategory.addTarget(self, action: #selector(self.categorySelectTapped(_:)), for: .touchUpInside)
        cell.btnCategory.tag = indexPath.row
        
        cell.btnCategory.setTitle(self.arrCategory[indexPath.row].titleEn, for: .normal)
        
        if currentIndex == indexPath.row {
            cell.btnCategory.endColor = COLORS.TABBAR_COLOR
            cell.btnCategory.startColor = COLORS.APP_THEME_COLOR
            cell.btnCategory.setTitleColor(.white, for: .normal)
        } else {
            cell.btnCategory.endColor = COLORS.UNSELECT_CATEGORY_COLOR
            cell.btnCategory.startColor = COLORS.UNSELECT_CATEGORY_COLOR
            cell.btnCategory.setTitleColor(COLORS.TABBAR_COLOR, for: .normal)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        currentIndex = indexPath.row
        collectionView.reloadData()
    }
}

extension AnalysisVC:AddGrowthVCDelegate {
    func addGrowth() {
        self.growthView.getGrowthChart()
    }
}

//MARK:- Functions of container view
extension AnalysisVC:GrowthViewsDelegate {
    func addNewGrowthTapped() {
        self.addNewTapped(self)
    }
}

extension AnalysisVC:TemperatureViewDelegate {
    func addTempretaure() {
        self.addNewTempreatureTapped(self)
    }
}
