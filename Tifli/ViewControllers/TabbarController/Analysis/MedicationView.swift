//
//  MedicationView.swift
//  Tifli
//
//  Created by zubair on 18/11/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON
import ActionSheetPicker_3_0

class MedicationView: UIView {
    
    @IBOutlet weak var tableViews: UITableView!
    @IBOutlet weak var lablDate: UILabel!
    @IBOutlet weak var lablTitle: UILabel!
    
    var view: UIView!
    var medicalDate = ""
    var arrMedication = [MedicationAnalysis]()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        backgroundColor = .clear
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = true
        addSubview(view)
        medicalDate = AppUtility.convertTeethDateToString(date: NSDate())
        self.lablDate.text = AppUtility.get_Teeth_MMM(dateString: medicalDate)
        //setupCollectionView()
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        return nibView
    }
    
    func setupLanguage() {
        lablTitle.text = lablTitle.text?.localized()
    }
    
    //MARK:- Apis functions
    func getMedicalChart() {
        self.view.endEditing(true)
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "category":"Medication", "babyId":AppUtility.getBabyId(), "month":medicalDate]
        APIRequestUtil.getAnalysis(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)
                print(swiftyJsonVar)
                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.arrMedication.removeAll()
                    
                    let valueArray: [JSON] = dict["data"]!.arrayValue
                    for dict in valueArray {
                        let dictValue: Dictionary<String, JSON> = dict.dictionaryValue
                        let obj = MedicationAnalysis.init(withDictionary: dictValue)
                        self.arrMedication.append(obj)
                    }
                    self.tableViews.reloadData()
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    //MARK:- Button Actions
    @IBAction func montshTapped(_ sender: UIButton) {
        let datePicker = ActionSheetDatePicker(title: "Select date".localized(), datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let date:NSDate = value as! NSDate
            
            self.medicalDate = AppUtility.convertTeethDateToString(date: date)
            self.lablDate.text = AppUtility.get_Teeth_MMM(dateString: self.medicalDate)
            self.getMedicalChart()
            
            return
            
        }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview!.superview)
        datePicker?.show()
    }
}

// MARK: - Tableview Delegate-
extension MedicationView : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrMedication.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        tableView.register(UINib(nibName: "MedicationViewCell", bundle: nil), forCellReuseIdentifier: "MedicationViewCell")
        var cell : MedicationViewCell! = tableView.dequeueReusableCell(withIdentifier: "MedicationViewCell") as? MedicationViewCell
        
        if (cell == nil) {
            cell = MedicationViewCell.init(style: UITableViewCell.CellStyle.default,
                                          reuseIdentifier:"MedicationViewCell");
        }
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        let obj = self.arrMedication[indexPath.row]
        cell.lablDate.text = obj.medicationDate
        cell.lablDetail.text = "Medication name".localized() + " ("+obj.medicationName+"), "+"Medication Amount".localized()+" ("+obj.amount+" "+obj.amountType+")"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
