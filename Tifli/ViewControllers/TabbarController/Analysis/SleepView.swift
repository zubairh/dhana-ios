//
//  SleepView.swift
//  Tifli
//
//  Created by zubair on 18/11/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import SwiftyJSON

class SleepView: UIView {
    
    @IBOutlet weak var tableViews: UITableView!
    @IBOutlet weak var layoutHeightTable: NSLayoutConstraint!
    @IBOutlet weak var lablHours: UILabel!
    @IBOutlet weak var lablDay: UILabel!
    @IBOutlet weak var lablHourCondition: UILabel!
    @IBOutlet weak var lablDayCondition: UILabel!
    @IBOutlet weak var lablDateTime: UILabel!
    @IBOutlet weak var lablError: UILabel!
    @IBOutlet weak var lablDayTime: UILabel!
    @IBOutlet weak var lablNightTime: UILabel!
    @IBOutlet weak var lablDailySleep: UILabel!
    @IBOutlet weak var lablSleepDay: UILabel!
    @IBOutlet weak var lablDetail: UILabel!
    @IBOutlet weak var lablTitle: UILabel!
    
    var view: UIView!
    var sleepDate = ""
    var objSleepAnalysis = SleepAnalysis()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        backgroundColor = .clear
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = true
        addSubview(view)
        
        sleepDate = AppUtility.convertTeethDateToString(date: NSDate())
        self.lablDateTime.text = AppUtility.get_Teeth_MMM(dateString: sleepDate)
        self.tableViews.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
        //setupCollectionView()
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        return nibView
    }
    
    func setupLanguage() {
        lablDailySleep.text = lablDailySleep.text?.localized()
        lablSleepDay.text = lablSleepDay.text?.localized()
        lablDetail.text = lablDetail.text?.localized()
        lablTitle.text = lablTitle.text?.localized()
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        layoutHeightTable.constant = tableViews.contentSize.height
    }
    
    //MARK:- Apis functions
    func getSleepChart() {
        self.view.endEditing(true)
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "category":"Sleep", "babyId":AppUtility.getBabyId(), "month":sleepDate]
        APIRequestUtil.getAnalysis(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)
                print(swiftyJsonVar)
                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.objSleepAnalysis = SleepAnalysis()
                    if let dicts: Dictionary<String, JSON> = dict["data"]?.dictionaryValue {
                        self.objSleepAnalysis = SleepAnalysis.init(withDictionary: dicts)
                    }
                    self.lablHours.text = self.objSleepAnalysis.sleepHours
                    self.lablDay.text = self.objSleepAnalysis.sleepPerDay
                    self.lablDayCondition.text = self.objSleepAnalysis.sleepPerDayCondition
                    self.lablHourCondition.text = self.objSleepAnalysis.sleepCondition
                    self.lablDayTime.text = self.objSleepAnalysis.weekDayTimeSleep+" "+"this week".localized()
                    self.lablNightTime.text = self.objSleepAnalysis.weekNightTimeSleep+" "+"this week".localized()
                    
                    self.tableViews.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
                    self.tableViews.reloadData()
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    //MARK:- Button Actions
    @IBAction func montshTapped(_ sender: UIButton) {
        let datePicker = ActionSheetDatePicker(title: "Select date".localized(), datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let date:NSDate = value as! NSDate
            
            self.sleepDate = AppUtility.convertTeethDateToString(date: date)
            self.lablDateTime.text = AppUtility.get_Teeth_MMM(dateString: self.sleepDate)
            self.getSleepChart()
            
            return
            
        }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview!.superview)
        datePicker?.show()
    }

}

// MARK: - Tableview Delegate-
extension SleepView : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.objSleepAnalysis.arrRoutine.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        tableView.register(UINib(nibName: "SleepCell", bundle: nil), forCellReuseIdentifier: "SleepCell")
        var cell : SleepCell! = tableView.dequeueReusableCell(withIdentifier: "SleepCell") as? SleepCell
        
        if (cell == nil) {
            cell = SleepCell.init(style: UITableViewCell.CellStyle.default,
                                          reuseIdentifier:"SleepCell");
        }
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        let obj = self.objSleepAnalysis.arrRoutine[indexPath.row]
        cell.lablDayTime.text = String(obj.dayTimeSleepTime)
        cell.lablNightTime.text = String(obj.nightTimeSleepTime)
        cell.lablTotal.text = "Total".localized()+": "+String(obj.total)
        cell.lablDate.text = obj.sleepDate
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 79
    }
}
