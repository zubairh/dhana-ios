//
//  MilestoneView.swift
//  Tifli
//
//  Created by zubair on 18/11/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import CircleProgressView
import SwiftyJSON

class MilestoneView: UIView {
    
    @IBOutlet weak var tableViews: UITableView!
    @IBOutlet weak var progressWeakness: CircularProgressBar!
    @IBOutlet weak var progressSocial: CircularProgressBar!
    @IBOutlet weak var progressLanguage: CircularProgressBar!
    @IBOutlet weak var progressCognitive: CircularProgressBar!
    @IBOutlet weak var progressMilestone: CircularProgressBar!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var lablAchiveMilstone: UILabel!
    @IBOutlet weak var lablTotalMilstone: UILabel!
    @IBOutlet weak var lablSocialAchiev: UILabel!
    @IBOutlet weak var lablSocialTotal: UILabel!
    @IBOutlet weak var lablLanguageAchiev: UILabel!
    @IBOutlet weak var lablLanguageTotal: UILabel!
    @IBOutlet weak var lablCognitiveAchiev: UILabel!
    @IBOutlet weak var lablCognitiveTotal: UILabel!
    @IBOutlet weak var lablMoveAchiev: UILabel!
    @IBOutlet weak var lablMoveTotal: UILabel!
    @IBOutlet weak var layoutHeightTableView: NSLayoutConstraint!
    @IBOutlet weak var lablSocialTitle: UILabel!
    @IBOutlet weak var lablLanguageTitle: UILabel!
    @IBOutlet weak var lablCognitiveTitle: UILabel!
    @IBOutlet weak var llablMovementTitle: UILabel!
    @IBOutlet weak var lablTotalMovmnt: UILabel!
    
    var view: UIView!
    var objMilestone = Milestones()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        backgroundColor = .clear
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = true
        addSubview(view)
        showCircularProgress()
        setupLanguage()
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        return nibView
    }
    
    func setupLanguage() {
        lablSocialTitle.text = lablSocialTitle.text?.localized()
        lablLanguageTitle.text = lablLanguageTitle.text?.localized()
        lablCognitiveTitle.text = lablCognitiveTitle.text?.localized()
        llablMovementTitle.text = llablMovementTitle.text?.localized()
        lablTotalMovmnt.text = lablTotalMovmnt.text?.localized()
    }
    
    func showCircularProgress() {
        self.progressWeakness.lineWidth = 10
        self.progressWeakness.foregroundLayer.lineWidth = 9
        self.progressWeakness.backgroundLayer.lineWidth = 4
        self.progressWeakness.safePercent = 100
        self.progressWeakness.labelSize = 0
        self.progressWeakness.setProgress(to: 0 , withAnimation: true)
        
        self.progressSocial.lineWidth = 4
        self.progressSocial.foregroundLayer.lineWidth = 9
        self.progressSocial.backgroundLayer.lineWidth = 4
        self.progressSocial.safePercent = 100
        self.progressSocial.labelSize = 0
        self.progressSocial.setProgress(to: 0, withAnimation: true)
        
        self.progressLanguage.lineWidth = 4
        self.progressLanguage.foregroundLayer.lineWidth = 9
        self.progressLanguage.backgroundLayer.lineWidth = 4
        self.progressLanguage.safePercent = 100
        self.progressLanguage.labelSize = 0
        self.progressLanguage.setProgress(to: 0, withAnimation: true)
        
        self.progressCognitive.lineWidth = 4
        self.progressCognitive.foregroundLayer.lineWidth = 9
        self.progressCognitive.backgroundLayer.lineWidth = 4
        self.progressCognitive.safePercent = 100
        self.progressCognitive.labelSize = 0
        self.progressCognitive.setProgress(to: 0, withAnimation: true)
        
        self.progressMilestone.lineWidth = 4
        self.progressMilestone.foregroundLayer.lineWidth = 9
        self.progressMilestone.backgroundLayer.lineWidth = 4
        self.progressMilestone.safePercent = 100
        self.progressMilestone.labelSize = 0
        self.progressMilestone.setProgress(to: 0, withAnimation: true)
        
        tableViews.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        layoutHeightTableView.constant = tableViews.contentSize.height
    }
    
    func showCircularProgressWithValue() {
        var total = Double((objMilestone.totalAchieve/objMilestone.total))
        var totalSocial = Double((objMilestone.arrMilestone[0].totalAchieve/objMilestone.arrMilestone[0].total))
        var totalLanguage = Double((objMilestone.arrMilestone[1].totalAchieve/objMilestone.arrMilestone[1].total))
        var totalCognitive = Double((objMilestone.arrMilestone[2].totalAchieve/objMilestone.arrMilestone[2].total))
        var totalMovement = Double((objMilestone.arrMilestone[3].totalAchieve/objMilestone.arrMilestone[3].total))
        self.progressWeakness.lineWidth = 30
        self.progressWeakness.foregroundLayer.lineWidth = 30
        self.progressWeakness.backgroundLayer.lineWidth = 20
        self.progressWeakness.safePercent = 100
        self.progressWeakness.labelSize = 0
        self.progressWeakness.colorWeak = UIColor.init(hexString: objMilestone.color)
        if (total.isNaN || total.isInfinite) {
            total = 0
        }
        self.progressWeakness.setProgress(to:total , withAnimation: true)
        
        self.progressSocial.lineWidth = 4
        self.progressSocial.foregroundLayer.lineWidth = 9
        self.progressSocial.backgroundLayer.lineWidth = 4
        self.progressSocial.safePercent = 100
        self.progressSocial.labelSize = 0
        self.progressSocial.colorSocial = UIColor.init(hexString: objMilestone.arrMilestone[0].color)
        if (totalSocial.isNaN || totalSocial.isInfinite) {
            totalSocial = 0
        }
        self.progressSocial.setProgress(to: totalSocial, withAnimation: true)
        
        self.progressLanguage.lineWidth = 4
        self.progressLanguage.foregroundLayer.lineWidth = 9
        self.progressLanguage.backgroundLayer.lineWidth = 4
        self.progressLanguage.safePercent = 100
        self.progressLanguage.labelSize = 0
        self.progressLanguage.colorLanguage = UIColor.init(hexString: objMilestone.arrMilestone[1].color)
        if (totalLanguage.isNaN || totalLanguage.isInfinite) {
            totalLanguage = 0
        }
        self.progressLanguage.setProgress(to: totalLanguage, withAnimation: true)
        
        self.progressCognitive.lineWidth = 4
        self.progressCognitive.foregroundLayer.lineWidth = 9
        self.progressCognitive.backgroundLayer.lineWidth = 4
        self.progressCognitive.safePercent = 100
        self.progressCognitive.labelSize = 0
        self.progressCognitive.colorCognitive = UIColor.init(hexString: objMilestone.arrMilestone[2].color)
        if (totalCognitive.isNaN || totalCognitive.isInfinite) {
            totalCognitive = 0
        }
        self.progressCognitive.setProgress(to: totalCognitive, withAnimation: true)
        
        self.progressMilestone.lineWidth = 4
        self.progressMilestone.foregroundLayer.lineWidth = 9
        self.progressMilestone.backgroundLayer.lineWidth = 4
        self.progressMilestone.safePercent = 100
        self.progressMilestone.labelSize = 0
        self.progressMilestone.colorMovement = UIColor.init(hexString: objMilestone.arrMilestone[3].color)
        if (totalMovement.isNaN || totalMovement.isInfinite) {
            totalMovement = 0
        }
        self.progressMilestone.setProgress(to: totalMovement, withAnimation: true)
        
        self.lablAchiveMilstone.text = String(Int(objMilestone.totalAchieve))
        self.lablTotalMilstone.text = "/"+String(Int(objMilestone.total))
        
        self.lablSocialAchiev.text = String(Int(objMilestone.arrMilestone[0].totalAchieve))
        self.lablSocialTotal.text = "/"+String(Int(objMilestone.arrMilestone[0].total))
        self.lablLanguageAchiev.text = String(Int(objMilestone.arrMilestone[1].totalAchieve))
        self.lablLanguageTotal.text = "/"+String(Int(objMilestone.arrMilestone[1].total))
        self.lablCognitiveAchiev.text = String(Int(objMilestone.arrMilestone[2].totalAchieve))
        self.lablCognitiveTotal.text = "/"+String(Int(objMilestone.arrMilestone[2].total))
        self.lablMoveAchiev.text = String(Int(objMilestone.arrMilestone[3].totalAchieve))
        self.lablMoveTotal.text = "/"+String(Int(objMilestone.arrMilestone[3].total))
        
        self.lablAchiveMilstone.textColor = UIColor.init(hexString: objMilestone.color)
        self.lablTotalMilstone.textColor = UIColor.init(hexString: objMilestone.color)
        
        self.lablSocialAchiev.textColor = UIColor.init(hexString: objMilestone.arrMilestone[0].color)
        self.lablSocialTotal.textColor = UIColor.init(hexString: objMilestone.arrMilestone[0].color)
        self.lablLanguageAchiev.textColor = UIColor.init(hexString: objMilestone.arrMilestone[1].color)
        self.lablLanguageTotal.textColor = UIColor.init(hexString: objMilestone.arrMilestone[1].color)
        self.lablCognitiveAchiev.textColor = UIColor.init(hexString: objMilestone.arrMilestone[2].color)
        self.lablCognitiveTotal.textColor = UIColor.init(hexString: objMilestone.arrMilestone[2].color)
        self.lablMoveAchiev.textColor = UIColor.init(hexString: objMilestone.arrMilestone[3].color)
        self.lablMoveTotal.textColor = UIColor.init(hexString: objMilestone.arrMilestone[3].color)
        
    }
    
    //MARK:- Apis functions
    func getMilestoneChart(year:String) {
        self.view.endEditing(true)
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "category":"Milestones", "babyId":AppUtility.getBabyId(), "year":year]
        APIRequestUtil.getAnalysis(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)
                print(swiftyJsonVar)
                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.objMilestone = Milestones()
                    
                    if let dicts: Dictionary<String, JSON> = dict["data"]?.dictionaryValue {
                       
                        let valueArray: [JSON] = dicts["categories_milestones"]!.arrayValue
                        for dict in valueArray {
                            let dictValue: Dictionary<String, JSON> = dict.dictionaryValue
                            let obj = Milestones.init(withDictionary: dictValue)
                            self.objMilestone.arrMilestone.append(obj)
                        }
                        let catArray: [JSON] = dicts["achieved_milestones_list"]!.arrayValue
                        for dict in catArray {
                            let dictValue: Dictionary<String, JSON> = dict.dictionaryValue
                            let obj = Categories.init(withDictionary: dictValue)
                            self.objMilestone.arrCategory.append(obj)
                        }
                        self.objMilestone.totalAchieve = Float(dicts["achieved_milestones"]?.intValue ?? 0)
                        self.objMilestone.total = Float(dicts["total_milestones"]?.intValue ?? 0)
                    }
                    self.tableViews.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
                    self.showCircularProgressWithValue()
                    self.tableViews.reloadData()
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    //MARK:- Button Actions
    @IBAction func segmentTapped(_ sender: SegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            self.getMilestoneChart(year: "1")
        } else if sender.selectedSegmentIndex == 1 {
            self.getMilestoneChart(year: "2")
        } else if sender.selectedSegmentIndex == 2 {
            self.getMilestoneChart(year: "3")
        } else if sender.selectedSegmentIndex == 3 {
            self.getMilestoneChart(year: "4")
        } else if sender.selectedSegmentIndex == 4 {
            self.getMilestoneChart(year: "5")
        }
    }
    
}

// MARK: - Tableview Delegate-
extension MilestoneView : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.objMilestone.arrCategory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        tableView.register(UINib(nibName: "MilestoneViewCell", bundle: nil), forCellReuseIdentifier: "MilestoneViewCell")
        var cell : MilestoneViewCell! = tableView.dequeueReusableCell(withIdentifier: "MilestoneViewCell") as? MilestoneViewCell
        
        if (cell == nil) {
            cell = MilestoneViewCell.init(style: UITableViewCell.CellStyle.default,
                                          reuseIdentifier:"MilestoneViewCell");
        }
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        let obj = self.objMilestone.arrCategory[indexPath.row]
        cell.lablTitle.text = obj.titleEn
        cell.lablDate.text = AppUtility.get_EEE_DD_MMM_HH_mm(dateString: obj.babyMilestoneModifiedAt)
        cell.viewBg.layer.borderColor = UIColor.init(hexString: obj.color).cgColor
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        tableView.register(UINib(nibName: "NotificationHeaderCell", bundle: nil), forCellReuseIdentifier: "NotificationHeaderCell")
        let header = tableView.dequeueReusableCell(withIdentifier: "NotificationHeaderCell")! as! NotificationHeaderCell
        
        header.lablTitle.text = "2 months"
        
        return header.contentView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
