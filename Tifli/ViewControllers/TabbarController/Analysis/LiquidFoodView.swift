//
//  LiquidFoodView.swift
//  Tifli
//
//  Created by zubair on 20/11/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit

class LiquidFoodView: UIView {
    
    @IBOutlet weak var tableGrapsh: UITableView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    
    var view: UIView!
    var arrTitle = ["Right Breast", "Left Breast"]
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        backgroundColor = .clear
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = true
        addSubview(view)
        //setupCollectionView()
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        return nibView
    }

}

// MARK: - Tableview Delegate-
extension LiquidFoodView : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if AppManager.shared.arrDataChart.count > 0 {
            return self.arrTitle.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        tableView.register(UINib(nibName: "GraphCell", bundle: nil), forCellReuseIdentifier: "GraphCell")
        var cell : GraphCell! = tableView.dequeueReusableCell(withIdentifier: "GraphCell") as? GraphCell
        
        if (cell == nil) {
            cell = GraphCell.init(style: UITableViewCell.CellStyle.default,
                                          reuseIdentifier:"GraphCell");
        }
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.lablTitle.text = self.arrTitle[indexPath.row]
        if AppManager.shared.arrDataChart.count > 0 {
            cell.arrData = AppManager.shared.arrDataChart
            cell.updateGraph()
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
