//
//  TemperatureView.swift
//  Tifli
//
//  Created by zubair on 18/11/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol TemperatureViewDelegate {
    func addTempretaure()
}

class TemperatureView: UIView {
    
    @IBOutlet weak var tableGrapsh: UITableView!
    @IBOutlet weak var lablAddMeasurmnt: UILabel!
    
    var view: UIView!
    var delegate:TemperatureViewDelegate? = nil
    var arrTemprature = [Temperature]()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        backgroundColor = .clear
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = true
        addSubview(view)
        setupLanguage()
        //setupCollectionView()
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        return nibView
    }
    
    func setupLanguage() {
        lablAddMeasurmnt.text = lablAddMeasurmnt.text?.localized()
    }
    
    //MARK:- Apis functions
    func getTemperatureChart() {
        self.view.endEditing(true)
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "category":"Temperature", "babyId":AppUtility.getBabyId()]
        APIRequestUtil.getAnalysis(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)
                print(swiftyJsonVar)
                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.arrTemprature.removeAll()
                    
                    if let dicts: Dictionary<String, JSON> = dict["data"]?.dictionaryValue {
                        let valueArray: [JSON] = dicts["temperature_chart"]!.arrayValue
                        for dict in valueArray {
                            let dictValue: Dictionary<String, JSON> = dict.dictionaryValue
                            let obj = Temperature.init(withDictionary: dictValue)
                            self.arrTemprature.append(obj)
                        }
                    }
                    self.tableGrapsh.reloadData()
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    //MARK:- Button Actions
    @IBAction func addNewTapped(_ sender: Any) {
        self.delegate?.addTempretaure()
    }
}

// MARK: - Tableview Delegate-
extension TemperatureView : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrTemprature.count > 0 {
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        tableView.register(UINib(nibName: "TempratureCell", bundle: nil), forCellReuseIdentifier: "TempratureCell")
        var cell : TempratureCell! = tableView.dequeueReusableCell(withIdentifier: "TempratureCell") as? TempratureCell
        
        if (cell == nil) {
            cell = TempratureCell.init(style: UITableViewCell.CellStyle.default,
                                          reuseIdentifier:"TempratureCell");
        }
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.lablTitle.text = "Temperature Chart".localized()
        cell.arrData = arrTemprature
        cell.updateGraph()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
