//
//  PumpingView.swift
//  Tifli
//
//  Created by zubair on 18/11/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON
import ActionSheetPicker_3_0

class PumpingView: UIView {
    
    @IBOutlet weak var collectionViews: UICollectionView!
    @IBOutlet weak var tableGrapsh: UITableView!
    @IBOutlet weak var tableMedication: UITableView!
    @IBOutlet weak var layouTableGraph: NSLayoutConstraint!
    @IBOutlet weak var layoutTableMedical: NSLayoutConstraint!
    @IBOutlet weak var lablDate: UILabel!
    @IBOutlet weak var lablTitle: UILabel!
    
    var view: UIView!
    var arrTitle = ["Left Breast".localized(), "Right Breast".localized()]
    var arrPumping = [Pumping]()
    var totalPumping = 0
    var totalBreastAmount = 0
    var pummpingDate = ""
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        backgroundColor = .clear
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = true
        addSubview(view)
        setupTableView()
        setupLanguage()
        pummpingDate = AppUtility.convertTeethDateToString(date: NSDate())
        self.lablDate.text = AppUtility.get_Teeth_MMM(dateString: pummpingDate)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        return nibView
    }
    
    func setupTableView() {
        tableGrapsh.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
        tableMedication.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
    }
    
    func setupLanguage() {
        lablTitle.text = lablTitle.text?.localized()
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        layouTableGraph.constant = tableGrapsh.contentSize.height
        layoutTableMedical.constant = tableMedication.contentSize.height
    }
    
    //MARK:- Apis functions
    func getPumpingChart() {
        self.view.endEditing(true)
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "category":"Pumping", "babyId":AppUtility.getBabyId(), "month":pummpingDate]
        APIRequestUtil.getAnalysis(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)
                print(swiftyJsonVar)
                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.arrPumping.removeAll()
                    
                    if let dicts: Dictionary<String, JSON> = dict["data"]?.dictionaryValue {
                        self.totalPumping = dicts["total_number_of_pumping_sessions"]?.intValue ?? 0
                        self.totalBreastAmount = dicts["total_breast_milk_amount"]?.intValue ?? 0
                        let valueArray: [JSON] = dicts["pumping_routine"]!.arrayValue
                        for dict in valueArray {
                            let dictValue: Dictionary<String, JSON> = dict.dictionaryValue
                            let obj = Pumping.init(withDictionary: dictValue)
                            self.arrPumping.append(obj)
                        }
                    }
                    self.tableMedication.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
                    self.tableMedication.reloadData()
                    self.collectionViews.reloadData()
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    //MARK:- Button Actions
    @IBAction func montshTapped(_ sender: UIButton) {
        let datePicker = ActionSheetDatePicker(title: "Select date".localized(), datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let date:NSDate = value as! NSDate
            
            self.pummpingDate = AppUtility.convertTeethDateToString(date: date)
            self.lablDate.text = AppUtility.get_Teeth_MMM(dateString: self.pummpingDate)
            self.getPumpingChart()
            
            return
            
        }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview!.superview)
        datePicker?.show()
    }
}

// MARK: - Tableview Delegate-
extension PumpingView : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableGrapsh {
            return 0
        } else {
            return self.arrPumping.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tableGrapsh {
            
            tableView.register(UINib(nibName: "GraphCell", bundle: nil), forCellReuseIdentifier: "GraphCell")
            var cell : GraphCell! = tableView.dequeueReusableCell(withIdentifier: "GraphCell") as? GraphCell
            
            if (cell == nil) {
                cell = GraphCell.init(style: UITableViewCell.CellStyle.default,
                                              reuseIdentifier:"GraphCell");
            }
            
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            cell.lablTitle.text = self.arrTitle[indexPath.row]
            if AppManager.shared.arrDataChart.count > 0 {
                cell.arrData = AppManager.shared.arrDataChart
                cell.updateGraph()
            }
            
            return cell
            
        } else {
            
            tableView.register(UINib(nibName: "PumpingViewCell", bundle: nil), forCellReuseIdentifier: "PumpingViewCell")
            var cell : PumpingViewCell! = tableView.dequeueReusableCell(withIdentifier: "PumpingViewCell") as? PumpingViewCell
            
            if (cell == nil) {
                cell = PumpingViewCell.init(style: UITableViewCell.CellStyle.default,
                                              reuseIdentifier:"PumpingViewCell");
            }
            
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            
            let obj = self.arrPumping[indexPath.row]
            
            cell.lablDate.text = obj.pumpingDate
            cell.lablLeftBreast.text = "Left breast".localized()+" "+obj.leftBrest
            cell.lablLRightBreast.text = "Right breast".localized()+" "+obj.rightBreast
            cell.lablAmount.text = "Amount".localized()+" "+obj.totalAmount
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

// MARK: - CollectionView Delegate-
extension PumpingView: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return  CGSize(width: collectionView.bounds.width/2, height: 131)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        collectionView.register(UINib(nibName: String(describing: DiaperViewCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: DiaperViewCell.self))
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: DiaperViewCell.self), for: indexPath as IndexPath) as! DiaperViewCell
        
        if indexPath.row == 0 {
            cell.lablTitle.text = "Total no of\nPumping session".localized()
            cell.lablCount.text = String(totalPumping)
        } else {
            cell.lablTitle.text = "Total amount\nBreast milk".localized()
            cell.lablCount.text = String(totalBreastAmount)+" ml"
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}
