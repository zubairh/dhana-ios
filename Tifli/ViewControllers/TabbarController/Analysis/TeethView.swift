//
//  TeethView.swift
//  Tifli
//
//  Created by zubair on 18/11/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON
import ActionSheetPicker_3_0

class TeethView: UIView {
    
    @IBOutlet weak var collectionTeeth: UICollectionView!
    @IBOutlet weak var lablDate: UILabel!
    @IBOutlet weak var labllDetail: UILabel!
    
    let circularLayout = DSCircularLayout()
    var arrTeeth = [Teeth]()
    var selectedIndex = -1
    var view: UIView!
    var teethDate = ""
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        backgroundColor = .clear
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = true
        addSubview(view)
        setupLanguage()
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        return nibView
    }
    
    func setupLanguage() {
        labllDetail.text = labllDetail.text?.localized()
    }
    
    func setCircular() {
        if SCREEN_WIDTH > 375 {
            circularLayout.initWithCentre(CGPoint(x: collectionTeeth.bounds.width/2, y: collectionTeeth.bounds.height/2), radius: collectionTeeth.bounds.width / 2 - 70 / 2, itemSize: CGSize(width: 38, height: 38), andAngularSpacing: 6)
        } else {
            circularLayout.initWithCentre(CGPoint(x: collectionTeeth.bounds.width/2, y: collectionTeeth.bounds.height/2), radius: collectionTeeth.bounds.width / 2 - 38 / 2, itemSize: CGSize(width: 38, height: 38), andAngularSpacing: 0)
        }
        circularLayout.setStartAngle(.pi * 2, endAngle: 0)
        circularLayout.mirrorX = true
        circularLayout.mirrorY = true
        circularLayout.rotateItems = true
        circularLayout.scrollDirection = .horizontal
        collectionTeeth.collectionViewLayout = circularLayout
        
        teethDate = AppUtility.convertTeethDateToString(date: NSDate())
        self.lablDate.text = AppUtility.get_Teeth_MMM(dateString: teethDate)
    }
    
    //MARK:- Apis functions
    func getTeethChart() {
        self.view.endEditing(true)
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "category":"Teething", "babyId":AppUtility.getBabyId(), "month":teethDate]
        APIRequestUtil.getAnalysis(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)
                print(swiftyJsonVar)
                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.arrTeeth.removeAll()
                    
                    let valueArray: [JSON] = dict["data"]!.arrayValue
                    for dict in valueArray {
                        let dictValue: Dictionary<String, JSON> = dict.dictionaryValue
                        let obj = Teeth.init(withDictionary: dictValue)
                        self.arrTeeth.append(obj)
                    }
                    self.collectionTeeth.reloadData()
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    //MARK:- Button Actions
    @IBAction func montshTapped(_ sender: UIButton) {
        let datePicker = ActionSheetDatePicker(title: "Select date".localized(), datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let date:NSDate = value as! NSDate
            
            self.teethDate = AppUtility.convertTeethDateToString(date: date)
            self.lablDate.text = AppUtility.get_Teeth_MMM(dateString: self.teethDate)
            self.getTeethChart()
            
            return
            
        }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview!.superview)
        datePicker?.show()
    }
}

// MARK: - CollectionView Delegate-
extension TeethView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrTeeth.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        collectionView.register(UINib(nibName: String(describing: TeethCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: TeethCell.self))
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: TeethCell.self), for: indexPath as IndexPath) as! TeethCell
        
        AppUtility.loadImage(imageView: cell.imgTeeth, urlString: self.arrTeeth[indexPath.row].image, placeHolderImageString: "")
        
        if self.arrTeeth[indexPath.row].isHighlight {
            cell.imgTeeth.alpha = 1
        } else {
            cell.imgTeeth.alpha = 0.4
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        selectedIndex = indexPath.row
//        collectionTeeth.reloadData()
    }
}
