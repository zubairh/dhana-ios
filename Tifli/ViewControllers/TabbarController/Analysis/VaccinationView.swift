//
//  VaccinationView.swift
//  Tifli
//
//  Created by zubair on 18/11/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

class VaccinationView: UIView {
    
    @IBOutlet weak var tableViews: UITableView!
    @IBOutlet weak var progressVaccination: CircularProgressBar!
    @IBOutlet weak var lablAchived: UILabel!
    @IBOutlet weak var lablTotal: UILabel!
    @IBOutlet weak var segmentControl: SegmentedControl!
    @IBOutlet weak var lablTitle: UILabel!
    
    var view: UIView!
    var arrVaccine = [Vaccine]()
    var arrUpcomingVacc = [Vaccine]()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        backgroundColor = .clear
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = true
        addSubview(view)
        showCircularProgress()
        setupLanguage()
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        return nibView
    }
    
    func setupLanguage() {
        lablTitle.text = lablTitle.text?.localized()
        segmentControl.setTitle(segmentControl.titleForSegment(at: 0)?.localized(), forSegmentAt: 0)
        segmentControl.setTitle(segmentControl.titleForSegment(at: 1)?.localized(), forSegmentAt: 1)
    }
    
    func showCircularProgress() {
        self.progressVaccination.lineWidth = 30
        self.progressVaccination.foregroundLayer.lineWidth = 30
        self.progressVaccination.backgroundLayer.lineWidth = 20
        self.progressVaccination.safePercent = 100
        self.progressVaccination.labelSize = 0
        self.progressVaccination.setProgress(to: 0, withAnimation: true)
    }
    
    //MARK:- Apis functions
    func getVaccinationChart() {
        self.view.endEditing(true)
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "category":"Vaccination", "babyId":AppUtility.getBabyId()]
        APIRequestUtil.getAnalysis(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)
                print(swiftyJsonVar)
                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.arrVaccine.removeAll()
                    self.arrUpcomingVacc.removeAll()
                    
                    if let dicts: Dictionary<String, JSON> = dict["data"]?.dictionaryValue {
                       
                        let valueArray: [JSON] = dicts["immunization_schedule"]!.arrayValue
                        for dict in valueArray {
                            let dictValue: Dictionary<String, JSON> = dict.dictionaryValue
                            let obj = Vaccine.init(withDictionary: dictValue)
                            if !obj.isTaken {
                                self.arrUpcomingVacc.append(obj)
                            }
                            self.arrVaccine.append(obj)
                        }

                        let achiveVac = Float(dicts["baby_total_taken_vacciantions"]?.intValue ?? 0)
                        let totalVac = Float(dicts["total_vacciantions"]?.intValue ?? 0)
                        
                        self.lablTotal.text = "/"+String(Int(totalVac))
                        self.lablAchived.text = String(Int(achiveVac))
                        
                        var total = Double((achiveVac/totalVac))
                        
                        self.progressVaccination.lineWidth = 30
                        self.progressVaccination.foregroundLayer.lineWidth = 30
                        self.progressVaccination.backgroundLayer.lineWidth = 20
                        self.progressVaccination.safePercent = 100
                        self.progressVaccination.labelSize = 0
                        self.progressVaccination.colorCognitive = UIColor(red: 28.0/255.0, green: 199.0/255.0, blue: 189.0/255.0, alpha: 1.0)
                        if (total.isNaN || total.isInfinite) {
                            total = 0
                        }
                        self.progressVaccination.setProgress(to:total , withAnimation: true)
                    }
                    
                    self.tableViews.reloadData()
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    //MARK:- Button Actions
    @IBAction func segmentTapped(_ sender: SegmentedControl) {
        tableViews.reloadData()
    }
    
}

// MARK: - Tableview Delegate-
extension VaccinationView : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if segmentControl.selectedSegmentIndex == 0 {
            return self.arrUpcomingVacc.count
        }
        return self.arrVaccine.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        tableView.register(UINib(nibName: "MilestoneViewCell", bundle: nil), forCellReuseIdentifier: "MilestoneViewCell")
        var cell : MilestoneViewCell! = tableView.dequeueReusableCell(withIdentifier: "MilestoneViewCell") as? MilestoneViewCell
        
        if (cell == nil) {
            cell = MilestoneViewCell.init(style: UITableViewCell.CellStyle.default,
                                          reuseIdentifier:"MilestoneViewCell");
        }
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        if segmentControl.selectedSegmentIndex == 0 {
            let obj = self.arrUpcomingVacc[indexPath.row]
            cell.lablTitle.text = obj.titles
            cell.lablDate.isHidden = true
        } else {
            let obj = self.arrVaccine[indexPath.row]
            cell.lablTitle.text = obj.titles
            if obj.isTaken {
                cell.imgCheck.image = UIImage(named: "tick")
                cell.lablDate.isHidden = false
                cell.lablDate.text = "Taken".localized()+" "+obj.dates
            } else {
                cell.imgCheck.image = UIImage(named: "circle")
                cell.lablDate.isHidden = true
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        tableView.register(UINib(nibName: "NotificationHeaderCell", bundle: nil), forCellReuseIdentifier: "NotificationHeaderCell")
        let header = tableView.dequeueReusableCell(withIdentifier: "NotificationHeaderCell")! as! NotificationHeaderCell
        
        header.lablTitle.text = "2 months"
        
        return header.contentView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
