//
//  FeedView.swift
//  Tifli
//
//  Created by zubair on 18/11/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON
import ActionSheetPicker_3_0

class FeedView: UIView {
    
    @IBOutlet weak var tableViews: UITableView!
    @IBOutlet weak var collectionViews: UICollectionView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var layoutTableFeed: NSLayoutConstraint!
    @IBOutlet weak var tableFeed: UITableView!
    @IBOutlet weak var lablDate: UILabel!
    @IBOutlet weak var lablTitle: UILabel!
    
    var view: UIView!
    var arrImage = ["Breastfeeding", "pumping-1", "Bottle", "Solids"]
    var arrTitle = ["Breast Feeding".localized(), "Breast milk".localized(), "Formula".localized(), "Solids".localized()]
    var arrFeeding = [Feeding]()
    var feedDate = ""
    var breastFeed = "0"
    var breastMilk = "0"
    var breastMilkAmount = "0 ml"
    var formulaMilk = "0"
    var formulaMilkAmount = "0 ml"
    var solidFood = "0"
    var subCategoryTwo = "Daily Avg"
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        backgroundColor = .clear
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = true
        addSubview(view)
        setupTableView()
        
        feedDate = AppUtility.convertTeethDateToString(date: NSDate())
        self.lablDate.text = AppUtility.get_Teeth_MMM(dateString: feedDate)
        setupTableView()
        setupLanguage()
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        return nibView
    }
    
    func setupLanguage() {
        lablTitle.text = lablTitle.text?.localized()
        segmentControl.setTitle(segmentControl.titleForSegment(at: 0)?.localized(), forSegmentAt: 0)
        segmentControl.setTitle(segmentControl.titleForSegment(at: 1)?.localized(), forSegmentAt: 1)
    }
    
    func setupTableView() {
        tableFeed.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        layoutTableFeed.constant = tableFeed.contentSize.height
    }
    
    //MARK:- Apis functions
    func getFeedChart() {
        self.view.endEditing(true)
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "category":"Feeding", "babyId":AppUtility.getBabyId(), "month":feedDate, "subCategoryTwo":subCategoryTwo]
        APIRequestUtil.getAnalysis(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)
                print(swiftyJsonVar)
                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.arrFeeding.removeAll()
                    
                    if let dicts: Dictionary<String, JSON> = dict["data"]?.dictionaryValue {
                        self.breastFeed = String(dicts["breast_feeding"]?.intValue ?? 0)
                        self.breastMilk = String(dicts["breast_milk"]?.intValue ?? 0)
                        self.breastMilkAmount = dicts["breast_milk_amount"]?.stringValue ?? "0"
                        self.formulaMilk = String(dicts["formula_milk"]?.intValue ?? 0)
                        self.formulaMilkAmount = dicts["formula_milk_amount"]?.stringValue ?? "0"
                        self.solidFood = String(dicts["solid_food"]?.intValue ?? 0)
                        
                        let valueArray: [JSON] = dicts["feeding_routine"]!.arrayValue
                        for dict in valueArray {
                            let dictValue: Dictionary<String, JSON> = dict.dictionaryValue
                            let obj = Feeding.init(withDictionary: dictValue)
                            self.arrFeeding.append(obj)
                        }
                    }
                    self.tableFeed.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
                    self.tableFeed.reloadData()
                    self.collectionViews.reloadData()
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    //MARK:- Button Actions
    @IBAction func montshTapped(_ sender: UIButton) {
        let datePicker = ActionSheetDatePicker(title: "Select date".localized(), datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let date:NSDate = value as! NSDate
            
            self.feedDate = AppUtility.convertTeethDateToString(date: date)
            self.lablDate.text = AppUtility.get_Teeth_MMM(dateString: self.feedDate)
            self.getFeedChart()
            
            return
            
        }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview!.superview)
        datePicker?.show()
    }
    
    @IBAction func segmentTapped(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            subCategoryTwo = "Daily Avg"
        } else {
            subCategoryTwo = "All Time"
        }
        self.getFeedChart()
    }
}

// MARK: - Tableview Delegate-
extension FeedView : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrFeeding.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        tableView.register(UINib(nibName: "FoodViewCell", bundle: nil), forCellReuseIdentifier: "FoodViewCell")
        var cell : FoodViewCell! = tableView.dequeueReusableCell(withIdentifier: "FoodViewCell") as? FoodViewCell
        
        if (cell == nil) {
            cell = FoodViewCell.init(style: UITableViewCell.CellStyle.default,
                                          reuseIdentifier:"FoodViewCell");
        }
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        let obj = self.arrFeeding[indexPath.row]
        cell.lablDate.text = obj.logDate
        cell.lablBottleCount.text = obj.formulaMilk
        cell.lablBottleAmount.text = "("+obj.formulaMilkAmount+" ml)"
        cell.lablBowl.text = obj.solidFood
        cell.lablPump.text = obj.breastMilk
        cell.lablBreast.text = obj.breastFeeding
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 79
    }
}

// MARK: - CollectionView Delegate-
extension FeedView: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return  CGSize(width: collectionView.bounds.width/2, height: 150)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        collectionView.register(UINib(nibName: String(describing: DiaperViewCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: DiaperViewCell.self))
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: DiaperViewCell.self), for: indexPath as IndexPath) as! DiaperViewCell
        
        cell.lablTitle.text = self.arrTitle[indexPath.row]
        cell.imgIcon.image = UIImage(named: self.arrImage[indexPath.row])
        
        if indexPath.row == 0 {
            cell.lablCount.text = self.breastFeed
            cell.lablTotalAmount.isHidden = true
        } else if indexPath.row == 1 {
            cell.lablCount.text = self.breastMilk
            cell.lablTotalAmount.isHidden = true
        } else if indexPath.row == 2 {
            cell.lablCount.text = self.formulaMilk
            cell.lablTotalAmount.text = self.formulaMilkAmount
            cell.lablTotalAmount.isHidden = false
        } else if indexPath.row == 3 {
            cell.lablTotalAmount.isHidden = true
            cell.lablCount.text = self.solidFood
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}
