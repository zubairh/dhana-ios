//
//  DiaperView.swift
//  Tifli
//
//  Created by zubair on 18/11/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON
import ActionSheetPicker_3_0

class DiaperView: UIView {
    
    @IBOutlet weak var collectionItems: UICollectionView!
    @IBOutlet weak var tableGrapsh: UITableView!
    @IBOutlet weak var layoutTableHeightGraph: NSLayoutConstraint!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var lablDate: UILabel!
    @IBOutlet weak var lablTitle: UILabel!
    
    var view: UIView!
    var arrTitle = ["Clean".localized(), "Pee".localized(), "Liquid Poo".localized(), "Soft Poo".localized(), "Solid Poo".localized(), "Mixed".localized()]
    var clean: String = "0"
    var pee: String = "0"
    var liquidPoo: String = "0"
    var softPoo: String = "0"
    var solidPoo: String = "0"
    var mixed: String = "0"
    var subCategoryOne = "Day"
    var arrDiaper = [Diaper]()
    var pummpingDate = ""
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        backgroundColor = .clear
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = true
        addSubview(view)
        setupTableView()
        setupLanguage()
        pummpingDate = AppUtility.convertTeethDateToString(date: NSDate())
        self.lablDate.text = AppUtility.get_Teeth_MMM(dateString: pummpingDate)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        return nibView
    }
    
    func setupTableView() {
        tableGrapsh.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
    }
    
    func setupLanguage() {
        lablTitle.text = lablTitle.text?.localized()
        segmentControl.setTitle(segmentControl.titleForSegment(at: 0)?.localized(), forSegmentAt: 0)
        segmentControl.setTitle(segmentControl.titleForSegment(at: 1)?.localized(), forSegmentAt: 1)
        segmentControl.setTitle(segmentControl.titleForSegment(at: 2)?.localized(), forSegmentAt: 2)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        layoutTableHeightGraph.constant = tableGrapsh.contentSize.height
    }
    
    //MARK:- Apis functions
    func getDiaperChart() {
        self.view.endEditing(true)
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN, "userToken":AppUtility.getUserToken(), "category":"Diaper", "babyId":AppUtility.getBabyId(), "subCategoryOne":subCategoryOne, "month":pummpingDate]
        APIRequestUtil.getAnalysis(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)
                print(swiftyJsonVar)
                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.arrDiaper.removeAll()
                    
                    if let dicts: Dictionary<String, JSON> = dict["data"]?.dictionaryValue {
                        self.clean = String(dicts["clean"]?.intValue ?? 0)
                        self.pee = String(dicts["pee"]?.intValue ?? 0)
                        self.liquidPoo = String(dicts["liquid_poo"]?.intValue ?? 0)
                        self.softPoo = String(dicts["soft_poo"]?.intValue ?? 0)
                        self.solidPoo = String(dicts["solid_poo"]?.intValue ?? 0)
                        self.mixed = String(dicts["mixed"]?.intValue ?? 0)
                        
                        let valueArray: [JSON] = dicts["diaper_routine"]!.arrayValue
                        for dict in valueArray {
                            let dictValue: Dictionary<String, JSON> = dict.dictionaryValue
                            let obj = Diaper.init(withDictionary: dictValue)
                            self.arrDiaper.append(obj)
                        }
                    }
                    self.tableGrapsh.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
                    self.tableGrapsh.reloadData()
                    self.collectionItems.reloadData()
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    @IBAction func segmentTapped(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            subCategoryOne = "Day"
        } else if sender.selectedSegmentIndex == 1 {
            subCategoryOne = "Week"
        } else {
            subCategoryOne = "Month"
        }
        self.getDiaperChart()
    }
    
    //MARK:- Button Actions
    @IBAction func montshTapped(_ sender: UIButton) {
        let datePicker = ActionSheetDatePicker(title: "Select date".localized(), datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let date:NSDate = value as! NSDate
            
            self.pummpingDate = AppUtility.convertTeethDateToString(date: date)
            self.lablDate.text = AppUtility.get_Teeth_MMM(dateString: self.pummpingDate)
            self.getDiaperChart()
            
            return
            
        }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview!.superview)
        datePicker?.show()
    }
}

// MARK: - Tableview Delegate-
extension DiaperView : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrDiaper.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        tableView.register(UINib(nibName: "DiaperListingCell", bundle: nil), forCellReuseIdentifier: "DiaperListingCell")
        var cell : DiaperListingCell! = tableView.dequeueReusableCell(withIdentifier: "DiaperListingCell") as? DiaperListingCell
        
        if (cell == nil) {
            cell = DiaperListingCell.init(style: UITableViewCell.CellStyle.default,
                                          reuseIdentifier:"DiaperListingCell");
        }
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        let obj = self.arrDiaper[indexPath.row]
        cell.lablPee.text = obj.pee
        cell.lablClean.text = obj.clean
        cell.lablMixed.text = obj.mixed
        cell.lablSoftPo.text = obj.softPoo
        cell.lablSolidPo.text = obj.solidPoo
        cell.lablLiquidPoo.text = obj.liquidPoo
        cell.lablDate.text = obj.logDate
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 127
    }
}

// MARK: - CollectionView Delegate-
extension DiaperView: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrTitle.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return  CGSize(width: collectionView.bounds.width/2, height: 131)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        collectionView.register(UINib(nibName: String(describing: DiaperViewCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: DiaperViewCell.self))
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: DiaperViewCell.self), for: indexPath as IndexPath) as! DiaperViewCell
        
        cell.lablTitle.text = self.arrTitle[indexPath.row]
        if indexPath.row == 0 {
            cell.lablCount.text = self.clean
        } else if indexPath.row == 1 {
            cell.lablCount.text = self.pee
        } else if indexPath.row == 2 {
            cell.lablCount.text = self.liquidPoo
        } else if indexPath.row == 3 {
            cell.lablCount.text = self.softPoo
        } else if indexPath.row == 4 {
            cell.lablCount.text = self.solidPoo
        } else if indexPath.row == 5 {
            cell.lablCount.text = self.mixed
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}
