//
//  WalkthroughViewController.swift
//  Tifli
//
//  Created by zubair on 11/02/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftyJSON

class WalkthroughViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var lablFree: UILabel!
    //@IBOutlet weak var lablFeeDate: UILabel!
    @IBOutlet weak var btnTrial: GradientButton!
    @IBOutlet weak var btnNotify: UIButton!
    @IBOutlet weak var lablDateTitle: UILabel!
    @IBOutlet weak var collectionPackages: UICollectionView!
    @IBOutlet weak var btnLLoginAcount: UIButton!
    
    var arrImages = ["Walk through milestones ", "Walkthrough diary", "Walkthrough analysis ", "walkthrough memories", "walkthrough guide", "walkthrough subscribe"]
    var arrTitles = ["Milestones".localized(), "Diary".localized(), "Analysis".localized(), "Memories".localized(), "Guide".localized(), ""]
    var selectedIndex = -1
    var arrSubscription = [Subscription]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lablFree.text = self.lablFree.text?.localized()
        self.btnTrial.setTitle(self.btnTrial.titleLabel?.text?.localized(), for: .normal)
        self.btnNotify.setTitle(self.btnNotify.titleLabel?.text?.localized(), for: .normal)
        self.btnLLoginAcount.setTitle(self.btnLLoginAcount.titleLabel?.text?.localized(), for: .normal)
        
        let now = NSDate()
        let next14days = now.addingTimeInterval(14*60*60*24)
        
        //let finalDate = AppUtility.getEEE_DD_MMM_HH_mm_package(dateString: AppUtility.convertDateToTimeString(date: next14days))
        //lablDateTitle.text = "Then 30 KD per year starting from "+finalDate
        
        self.getSubscriptionList()
    }
    
    //MARK:- Apis Function
    func getSubscriptionList() {
        let paramDict: [String: Any]  = ["appToken":APP_TOKEN]
        APIRequestUtil.getSubscription(parameters: paramDict) { (result, error) in
            if(result != nil) {
                let swiftyJsonVar = JSON(result!)

                let dict = swiftyJsonVar.dictionaryValue
                let status = dict["status"]?.intValue
                let message = dict["message"]?.string
                
                if status == SUCESS_CODE {
                    
                    self.arrSubscription.removeAll()
                    
                    //let dictionary: Dictionary<String, JSON> = dict["data"]!.dictionaryValue
                    let valueArray: [JSON] = dict["data"]!.arrayValue
                    for dict in valueArray {
                        let dictValue: Dictionary<String, JSON> = dict.dictionaryValue
                        let obj = Subscription.init(withDictionary: dictValue)
                        self.arrSubscription.append(obj)
                    }
                    self.collectionPackages.reloadData()
                    
                } else {
                    
                    AppUtility.showErrorMessage(message: message ?? SERVER_ERROR)
                    return
                }
            }
        }
    }
    
    //MARK:- Functions
    func openLogin() {
        let viewController = LoginViewController(nibName:String(describing: LoginViewController.self), bundle:nil)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    //MARK:- ScrollView Delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let page = scrollView.contentOffset.x / scrollView.frame.size.width
        pageControl.currentPage = Int(page)
        if appSceneDelegate.isArabic {
            if page == 0 {
                self.btnNotify.isHidden = false
                self.btnTrial.isHidden = false
                self.lablFree.isHidden = false
                //self.lablFeeDate.isHidden = false
                self.collectionPackages.isHidden = false
            } else {
                self.btnNotify.isHidden = true
                self.btnTrial.isHidden = true
                self.lablFree.isHidden = true
                //self.lablFeeDate.isHidden = true
                self.collectionPackages.isHidden = true
            }
        } else {
            if page == 5 {
                self.btnNotify.isHidden = false
                self.btnTrial.isHidden = false
                self.lablFree.isHidden = false
                //self.lablFeeDate.isHidden = false
                self.collectionPackages.isHidden = false
            } else {
                self.btnNotify.isHidden = true
                self.btnTrial.isHidden = true
                self.lablFree.isHidden = true
                //self.lablFeeDate.isHidden = true
                self.collectionPackages.isHidden = true
            }
        }
    }

    //MARK:- Button Actions
    @IBAction func signInTapped(_ sender: Any) {
        self.openLogin()
    }
    
    @IBAction func getStartedTapped(_ sender: Any) {
        AppManager.shared.planId = "Trial"
        let viewController = AddBabyViewController(nibName:String(describing: AddBabyViewController.self), bundle:nil)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func notifyTapped(_ sender: Any) {
        
    }
}

extension WalkthroughViewController: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionPackages {
            return self.arrSubscription.count
        }
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collectionPackages {
            return  CGSize(width: collectionPackages.bounds.width/3, height: collectionPackages.bounds.size.height)
        }
        let screenRect = UIScreen.main.bounds
        return  CGSize(width: screenRect.size.width, height: screenRect.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionPackages {
            
            collectionView.register(UINib(nibName: String(describing: PackageCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: PackageCell.self))
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: PackageCell.self), for: indexPath as IndexPath) as! PackageCell
            
            let obj = self.arrSubscription[indexPath.row]
            
            cell.lablDuration.text = obj.planDuration
            cell.lablPrice.text = obj.planPrice
            cell.lablPerMonth.text = "per".localized()+" "+obj.planDuration
            
            if self.selectedIndex == indexPath.row {
                cell.viewOuter.isHidden = false
                cell.viewInner.backgroundColor = #colorLiteral(red: 0.9843137255, green: 0.2901960784, blue: 0.2941176471, alpha: 1)
                cell.lablDuration.textColor = .white
                cell.lablPrice.textColor = .white
                cell.lablPerMonth.textColor = .white
            } else {
                cell.viewOuter.isHidden = true
                cell.viewInner.backgroundColor = #colorLiteral(red: 0.831372549, green: 0.8156862745, blue: 0.8941176471, alpha: 1)
                cell.lablDuration.textColor = .black
                cell.lablPrice.textColor = .black
                cell.lablPerMonth.textColor = .black
            }
            
            return cell
            
        } else {
            
            collectionView.register(UINib(nibName: String(describing: WalkthroughCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: WalkthroughCell.self))
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: WalkthroughCell.self), for: indexPath as IndexPath) as! WalkthroughCell
            
            cell.lablTitle.text = self.arrTitles[indexPath.row]
            cell.imgBg.image = UIImage(named: self.arrImages[indexPath.row])
            
            if indexPath.row == 5 {
                cell.lablDetaill.text = ""
                
            } else {
                cell.lablDetaill.text = "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu consectetaur cillium adipisicing pecu"
            }
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectionPackages {
            selectedIndex = indexPath.row
            collectionPackages.reloadData()
            AppManager.shared.planId = self.arrSubscription[indexPath.row].planId
        }
    }
    
}
