//
//  APIRequestUtil.swift
//  Global Paint
//
//  Created by Apple on 01/09/2018.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

typealias RequestCompletion = (_ response: Any?, _ error: Error?) -> Void

class APIRequestUtil {
  
  public static var BASE_URL = APPURL.BASE_URL
    
    public static func Login(parameters: Parameters,completion: @escaping RequestCompletion) {
        POSTRequest(url: API.LOGIN, parameters: parameters, completion: completion)
    }
    
    public static func SignUp(parameters: Parameters,completion: @escaping RequestCompletion) {
        POSTRequest(url: API.SIGN_UP, parameters: parameters, completion: completion)
    }
    
    public static func forgotPassword(parameters: Parameters,completion: @escaping RequestCompletion) {
        POSTRequest(url: API.FORGOT, parameters: parameters, completion: completion)
    }
    
    public static func verifyOtp(parameters: Parameters,completion: @escaping RequestCompletion) {
        POSTRequest(url: API.VERIFY_OTP, parameters: parameters, completion: completion)
    }
    
    public static func addGrowth(parameters: Parameters, data : Data, extensionString: String, fileNameKey : String, isEdit:Bool,  completion: @escaping RequestCompletion) {
        if isEdit {
            POSTUploadMedia(url: API.EDIT_GROWTH, parameters: parameters, data: data, extensionString: extensionString, fileNameKey: fileNameKey, completion: completion)
        }
        else {
            POSTUploadMedia(url: API.ADD_GROWTH, parameters: parameters, data: data, extensionString: extensionString, fileNameKey: fileNameKey, completion: completion)
        }
    }
    
    public static func getHomeListing(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.GET_GROWTH, parameters: parameters, completion: completion)
    }
    
    public static func getGuideCategory(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.GET_GUIDE_CAT, parameters: parameters, completion: completion)
    }
    
    public static func getAnalysisCategory(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.GET_ANALYSING_CATEGORIES, parameters: parameters, completion: completion)
    }
    
    public static func getAnalysis(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.GET_ANALYSIS, parameters: parameters, completion: completion)
    }
    
    public static func getGuideList(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.GET_ARTICLES, parameters: parameters, completion: completion)
    }
    
    public static func getGuideDetail(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.GET_ARTICLES_DETAIL, parameters: parameters, completion: completion)
    }
    
    public static func getBabyList(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.GET_BABY_LIST, parameters: parameters, completion: completion)
    }
    
    public static func getAlergyList(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.GET_ALERGY, parameters: parameters, completion: completion)
    }
    
    public static func getRelationList(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.GET_Relations, parameters: parameters, completion: completion)
    }
    
    public static func getMedicalList(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.GET_Medical, parameters: parameters, completion: completion)
    }
    
    public static func getVaccineList(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.GET_VACCINE, parameters: parameters, completion: completion)
    }
    
    public static func getBabyDetail(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.GET_BABY_DETAIL, parameters: parameters, completion: completion)
    }
    
    public static func uploadPhoto(parameters: Parameters, data : Data, extensionString: String, fileNameKey : String, completion: @escaping RequestCompletion) {
        POSTUploadMedia(url: API.UPDATE_PROFILE, parameters: parameters, data: data, extensionString: extensionString, fileNameKey: fileNameKey, completion: completion)
    }
    
    public static func updateProfile(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.UPDATE_PROFILE_USER, parameters: parameters, completion: completion)
    }
    
    public static func resentOtp(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.RESEND_OTP, parameters: parameters, completion: completion)
    }
    
    public static func logout(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.LOGOUT, parameters: parameters, completion: completion)
    }
    
    public static func getGrowthChartData(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.GET_GROWTH_CHARTS, parameters: parameters, completion: completion)
    }
    
    public static func getMilestones(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.GET_MILESTONE, parameters: parameters, completion: completion)
    }
    
    public static func deleteAccount(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.DELETE_ACCOUNT, parameters: parameters, completion: completion)
    }
    
    public static func getUserSetting(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.GET_USER_SETTING, parameters: parameters, completion: completion)
    }
    
    public static func updateUserSettings(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.UPDATE_USER_SETTING, parameters: parameters, completion: completion)
    }
    
    public static func getContactDetail(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.GET_CONTACT_DETAIL, parameters: parameters, completion: completion)
    }
    
    public static func updatePassword(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.CHANGE_PASSWORD, parameters: parameters, completion: completion)
    }
    
    public static func addContactUs(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.CONTACT_ADD_DETAIL, parameters: parameters, completion: completion)
    }
    
    public static func addMecial(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.ADD_MEDICAL, parameters: parameters, completion: completion)
    }
    
    public static func addFood(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.ADD_FOOD, parameters: parameters, completion: completion)
    }
    
    public static func addAllergy(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.ADD_ALERGY, parameters: parameters, completion: completion)
    }
    
    public static func addVaccine(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.ADD_VACCINE, parameters: parameters, completion: completion)
    }
    
    public static func getNotifications(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.GET_NOTIFICATIONS, parameters: parameters, completion: completion)
    }
    
    public static func deleteBaby(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.DELETE_BABY, parameters: parameters, completion: completion)
    }
    
    public static func likeArticle(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.LIKE_ARTICLE, parameters: parameters, completion: completion)
    }
    
    public static func addArticleComment(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.ADD_COMMENT, parameters: parameters, completion: completion)
    }
    
    public static func clearAllNotifications(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.CLEAR_NOTIFICATION, parameters: parameters, completion: completion)
    }
    
    public static func addNotes(url:String, parameters: Parameters, data : Data, extensionString: String, fileNameKey : String, completion: @escaping RequestCompletion) {
        POSTUploadMedia(url: url, parameters: parameters, data: data, extensionString: extensionString, fileNameKey: fileNameKey, completion: completion)
    }
    
    public static func addSleep(url: String, parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: url, parameters: parameters, completion: completion)
    }
    
    public static func addAppointment(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.ADD_APPOINTMENT, parameters: parameters, completion: completion)
    }
    
    public static func addPumping(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.ADD_PUMPING, parameters: parameters, completion: completion)
    }
    
    public static func addTemprature(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.ADD_TEMPRATURE, parameters: parameters, completion: completion)
    }
    
    public static func getMedicationList(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.GET_MEDICATION_LIST, parameters: parameters, completion: completion)
    }
    
    public static func addMedical(url:String, parameters: Parameters, data : Data, extensionString: String, fileNameKey : String, completion: @escaping RequestCompletion) {
        POSTUploadMedia(url: url, parameters: parameters, data: data, extensionString: extensionString, fileNameKey: fileNameKey, completion: completion)
    }
    
    public static func addFeedering(url:String, parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: url, parameters: parameters, completion: completion)
    }
    
    public static func getActivity(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.GET_ACTIVITY, parameters: parameters, completion: completion)
    }
    
    public static func getVaccine(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.GET_BABY_VACCINE, parameters: parameters, completion: completion)
    }
    
    public static func AddBabyVaccine(parameters: Parameters, isEdit:Bool, completion: @escaping RequestCompletion) {
        if isEdit {
            POSTRequest(url: API.EDIT_VACCINATION, parameters: parameters, completion: completion)
        } else {
            POSTRequest(url: API.ADD_BABY_VACCINE, parameters: parameters, completion: completion)
        }
    }
    
    public static func getTeeth(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.GET_TEETH, parameters: parameters, completion: completion)
    }
    
    public static func getDiary(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.GET_DIARY, parameters: parameters, completion: completion)
    }
    
    public static func getMessages(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.GET_MESSAGES, parameters: parameters, completion: completion)
    }
    
    public static func getSubscriptionList(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.GET_SUBSCRIPTION_LIST, parameters: parameters, completion: completion)
    }
    
    public static func getSubscription(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.GET_SUBSCRIPTION, parameters: parameters, completion: completion)
    }
    
    public static func buySubscription(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.BUY_SUBSCRIPTION, parameters: parameters, completion: completion)
    }
    
    public static func deleteCategoryOnDiary(url:String ,parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: url, parameters: parameters, completion: completion)
    }
    
    public static func updateDiary(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.DELETE_DIARY, parameters: parameters, completion: completion)
    }
    
    public static func sendMessages(parameters: Parameters, data : Data, extensionString: String, fileNameKey : String, completion: @escaping RequestCompletion) {
        POSTUploadMedia(url: API.SEND_MESSAGES, parameters: parameters, data: data, extensionString: extensionString, fileNameKey: fileNameKey, completion: completion)
    }
    
    public static func addDaiper(url:String, parameters: Parameters, data : Data, extensionString: String, fileNameKey : String, completion: @escaping RequestCompletion) {
        POSTUploadMedia(url: url, parameters: parameters, data: data, extensionString: extensionString, fileNameKey: fileNameKey, completion: completion)
    }
    
    public static func addActivity(parameters: Parameters, data : Data, extensionString: String, fileNameKey : String, isEdit:Bool, completion: @escaping RequestCompletion) {
        if isEdit {
            POSTUploadMedia(url: API.EDIT_ACTIVITY, parameters: parameters, data: data, extensionString: extensionString, fileNameKey: fileNameKey, completion: completion)
        } else {
            POSTUploadMedia(url: API.ADD_ACTIVITY, parameters: parameters, data: data, extensionString: extensionString, fileNameKey: fileNameKey, completion: completion)
        }
    }
    
    public static func addTeeth(parameters: Parameters, data : Data, extensionString: String, fileNameKey : String, completion: @escaping RequestCompletion) {
        POSTUploadMedia(url: API.ADD_TEETH, parameters: parameters, data: data, extensionString: extensionString, fileNameKey: fileNameKey, completion: completion)
    }
    
    public static func addCaregiver(parameters: Parameters, data : Data, extensionString: String, fileNameKey : String, completion: @escaping RequestCompletion) {
        POSTUploadMedia(url: API.ADD_CARE_GIVER, parameters: parameters, data: data, extensionString: extensionString, fileNameKey: fileNameKey, completion: completion)
    }
    
    public static func addDocument(parameters: Parameters, data : Data, extensionString: String, fileNameKey : String, completion: @escaping RequestCompletion) {
        POSTUploadMedia(url: API.ADD_DOCUMENT, parameters: parameters, data: data, extensionString: extensionString, fileNameKey: fileNameKey, completion: completion)
    }
    
    public static func addToys(parameters: Parameters, data : Data, extensionString: String, fileNameKey : String, completion: @escaping RequestCompletion) {
        POSTUploadMedia(url: API.ADD_TOYS, parameters: parameters, data: data, extensionString: extensionString, fileNameKey: fileNameKey, completion: completion)
    }
    
    public static func addNewBaby(parameters: Parameters, data : Data, extensionString: String, fileNameKey : String, completion: @escaping RequestCompletion) {
        POSTUploadMedia(url: API.ADD_NEW_BABY, parameters: parameters, data: data, extensionString: extensionString, fileNameKey: fileNameKey, completion: completion)
    }
    
    public static func updateNewBaby(parameters: Parameters, data : Data, extensionString: String, fileNameKey : String, completion: @escaping RequestCompletion) {
        POSTUploadMedia(url: API.EDIT_BABY, parameters: parameters, data: data, extensionString: extensionString, fileNameKey: fileNameKey, completion: completion)
    }
    
    public static func addNewBabyProfile(parameters: Parameters, data : Data, extensionString: String, fileNameKey : String, dataTwins : Data , extensionStringTwins : String,fileNameKeyTwins : String, arrDocument:[UIImage], arrToys:[UIImage],  completion: @escaping RequestCompletion) {
        POSTUploadMediaWithBaby(url: API.ADD_NEW_BABY, parameters: parameters, data: data, extensionString: extensionString, fileNameKey: fileNameKey, dataTwins: dataTwins, extensionStringTwins: extensionStringTwins, fileNameKeyTwins: fileNameKeyTwins, arrDocument: arrDocument, arrToys: arrToys, completion: completion)
    }
    
    public static func addMilestoneArchive(parameters: Parameters, data : Data, extensionString: String, fileNameKey : String, completion: @escaping RequestCompletion) {
        POSTUploadMedia(url: API.SAVE_ACHIEVE_MILESTONE, parameters: parameters, data: data, extensionString: extensionString, fileNameKey: fileNameKey, completion: completion)
    }
    
    public static func getMemoriesList(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.GET_MEMORIES_LIST, parameters: parameters, completion: completion)
    }
    
    public static func getMemoriesAlbumList(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.GET_MEMORIES_ALBUM_LIST, parameters: parameters, completion: completion)
    }
    
    public static func addEditAlbum(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.ADD_EDIT_ALBUM, parameters: parameters, completion: completion)
    }
    
    public static func deleteMemory(parameters: Parameters, completion: @escaping RequestCompletion) {
        POSTRequest(url: API.DELETE_MEMORY, parameters: parameters, completion: completion)
    }
    
    public static func addEditMemory(parameters: Parameters, data : Data, extensionString: String, fileNameKey : String, completion: @escaping RequestCompletion) {
        POSTUploadMedia(url: API.ADD_EDIT_MEMORY, parameters: parameters, data: data, extensionString: extensionString, fileNameKey: fileNameKey, completion: completion)
    }
    
    //MARK:- Private functions
    fileprivate static func GETRequest(url: String, parameters: Parameters,
                                       completion: @escaping RequestCompletion) {
        
        AppUtility.showProgress()

        var headers:[String : String]? = nil
        headers = ["Content-Type": "application/json",
                   "Cache-Control": "no-cache",
                   "Accept": "\"[\"application/json\", \"text/html\"]"]

        var urlString = BASE_URL + url
        urlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        Alamofire.request(
        urlString,
        method: .get,
        parameters: parameters,
        headers: headers)
        
        .validate(statusCode: 200..<600)
        
        .responseJSON { (response) -> Void in

        if response.result.isSuccess {
            
            AppUtility.hideProgress()
            if response.result.value != nil {
                let swiftyJsonVar = JSON(response.result.value!)
                let messsage = swiftyJsonVar["message"].stringValue
                if  messsage == "Unauthenticated"{
                    UserDefaults.standard.set(nil, forKey: USER_LOGIN)
                    appSceneDelegate.loadLanguage()
                }
            }
            completion(response.result.value, nil)

          }else {
            AppUtility.hideProgress()

            completion(nil, response.result.error)

          }
        }
    }
    
    fileprivate static func POSTRequest(url: String, parameters: Parameters,
                                    completion: @escaping RequestCompletion) {
        
        let UrL =  BASE_URL + url
        print(UrL)
        if url != API.GET_MESSAGES && url != API.GET_GROWTH && url != API.GET_MILESTONE {
            AppUtility.showProgress()
        }
        
        Alamofire.upload(
                multipartFormData: { multipartFormData in
                    for (key, value) in parameters
                    {
                        multipartFormData.append("\(value)".data(using: .utf8)!, withName: key)
                    }
            },
            usingThreshold:UInt64.init(),
            to: UrL ,
            method:.post,
            headers:nil,
            encodingCompletion: { encodingResult in
                
                switch encodingResult
                {
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        print(response.result.value)
                        if url != API.GET_ANALYSING_CATEGORIES {
                            AppUtility.hideProgress()
                        }
                       completion(response.result.value, nil)
                }
                    
                case .failure(let encodingError):
                    AppUtility.hideProgress()
                    completion(nil, encodingError)
                }
        })
    }

    fileprivate static func POSTUploadMedia(url: String, parameters: Parameters, data : Data , extensionString : String,fileNameKey : String, completion: @escaping RequestCompletion) {
        
        let UrL =  BASE_URL + url
        if url != API.SEND_MESSAGES {
            AppUtility.showProgress()
        }
        
        Alamofire.upload(
                multipartFormData: { multipartFormData in
                    if extensionString == "audio/m4a" {
                        if data.count > 0 {
                            multipartFormData.append(data, withName: fileNameKey, fileName: "file_\(Int.random(in: 0..<1000)).mp3", mimeType: extensionString)
                        }
                    } else if extensionString == "video/quicktime" {
                        if data.count > 0 {
                            multipartFormData.append(data, withName: fileNameKey, fileName: "file_\(Int.random(in: 0..<1000)).mov", mimeType: extensionString)
                        }
                    } else {
                        if data.count > 0 {
                            let imgg = UIImage(data: data)
                            multipartFormData.append((imgg?.jpegData(compressionQuality: 0.3))!, withName: fileNameKey, fileName: "file_\(Int.random(in: 0..<1000)).png", mimeType: extensionString)
                        }
                    }
                    for (key, value) in parameters
                    {
                        multipartFormData.append("\(value)".data(using: .utf8)!, withName: key)
                    }
            },
            usingThreshold:UInt64.init(),
            to: UrL ,
            method:.post,
            headers:nil,
            encodingCompletion: { encodingResult in
                
                switch encodingResult
                {
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        
                        AppUtility.hideProgress()
                       completion(response.result.value, nil)
                }
                    
                case .failure(let encodingError):
                    AppUtility.hideProgress()
                    completion(nil, encodingError)
                }
        })
    }
    
    fileprivate static func POSTUploadMediaWithBaby(url: String, parameters: Parameters, data : Data , extensionString : String,fileNameKey : String , dataTwins : Data , extensionStringTwins : String,fileNameKeyTwins : String, arrDocument:[UIImage], arrToys:[UIImage], completion: @escaping RequestCompletion) {
        
        let UrL =  BASE_URL + url
        print(UrL)
       
        AppUtility.showProgress()
        
        Alamofire.upload(
                multipartFormData: { multipartFormData in

                    multipartFormData.append(data, withName: fileNameKey, fileName: "file_\(Int.random(in: 0..<1000)).png", mimeType: extensionString)
                    multipartFormData.append(dataTwins, withName: fileNameKeyTwins, fileName: "file_\(Int.random(in: 0..<1000)).png", mimeType: extensionStringTwins)
                    
                    for i in 0 ..< arrDocument.count {
                        let timeStamp = Date().timeIntervalSince1970 * 1000
                        let fileName = "\(timeStamp).png"
                        let img = AppUtility.resizeImage(image: arrDocument[i], newWidth: arrDocument[i].size.width/2)
                        multipartFormData.append(img.jpegData(compressionQuality: 0.3)!, withName: "babyDocuments[\(i)]", fileName: fileName, mimeType: "image/jpeg")
                    }
                    
                    for i in 0 ..< arrToys.count {
                        let timeStamp = Date().timeIntervalSince1970 * 1000
                        let fileName = "\(timeStamp).png"
                        let img = AppUtility.resizeImage(image: arrToys[i], newWidth: arrToys[i].size.width/2)
                        multipartFormData.append(img.jpegData(compressionQuality: 0.3)!, withName: "babyToys[\(i)]", fileName: fileName, mimeType: "image/jpeg")
                    }
                    
                    for (key, value) in parameters
                    {
                        multipartFormData.append("\(value)".data(using: .utf8)!, withName: key)
                    }
            },
            usingThreshold:UInt64.init(),
            to: UrL ,
            method:.post,
            headers:nil,
            encodingCompletion: { encodingResult in
                
                switch encodingResult
                {
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        print(response.result.value)
                        AppUtility.hideProgress()
                       completion(response.result.value, nil)
                }
                    
                case .failure(let encodingError):
                    print(encodingError.localizedDescription)
                    AppUtility.hideProgress()
                    completion(nil, encodingError)
                }
        })
    }
}
