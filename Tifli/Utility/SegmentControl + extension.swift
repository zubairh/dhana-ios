//
//  SegmentControl + extension.swift
//  Part-Time
//
//  Created by Codesk Studio Macmini on 29/08/2021.
//  Copyright © 2021 Zubair Habib. All rights reserved.
//

import Foundation
import UIKit


class SegmentedControl: UISegmentedControl {
    
    let unselectMilestoneColor = UIColor(red: 212.0/255.0, green: 208.0/255.0, blue: 228.0/255.0, alpha: 1.0)
    
    override func layoutSubviews() {
      super.layoutSubviews()
        if self.tag == 0 {
            layer.cornerRadius = self.bounds.size.height / 2.0
            layer.borderColor = UIColor(red: 59.0/255.0, green: 159.0/255.0, blue: 239.0/255.0, alpha: 1.0).cgColor
            layer.borderWidth = 0.0
            layer.masksToBounds = true
            clipsToBounds = true
            let defaultAttributes = [
                NSAttributedString.Key.font: UIFont(name: "ProximaNova-Semibold", size: 15.0)!,
                NSAttributedString.Key.foregroundColor: UIColor.black]
            let selectedAttributes = [
                        NSAttributedString.Key.font: UIFont(name: "ProximaNova-Semibold", size: 15.0)!,
                NSAttributedString.Key.foregroundColor: UIColor.white
                    ]
            
            self.setTitleTextAttributes(defaultAttributes, for: .normal)
            self.setTitleTextAttributes(selectedAttributes, for: .selected)
            
            for i in 0...subviews.count - 1 {
                if let subview = subviews[i] as? UIImageView {
                    if i == self.selectedSegmentIndex {
                        subview.backgroundColor = UIColor(red: 0.0/255.0, green: 168.0/255.0, blue: 45.0/255.0, alpha: 1.0)
                    } else {
                        subview.backgroundColor = i == 1 ? .white : .clear
                    }
                }
            }
        } else if tag == 1 {
            layer.cornerRadius = self.bounds.size.height / 2.0
            layer.borderColor = UIColor(red: 59.0/255.0, green: 159.0/255.0, blue: 239.0/255.0, alpha: 1.0).cgColor
            layer.borderWidth = 0.0
            layer.masksToBounds = true
            clipsToBounds = true
            let defaultAttributes = [
                NSAttributedString.Key.font: UIFont(name: "Open-Sans-Regular", size: 15.0) ?? UIFont.systemFont(ofSize: 15),
                NSAttributedString.Key.foregroundColor: UIColor.darkGray]
            let selectedAttributes = [
                        NSAttributedString.Key.font: UIFont(name: "Open-Sans-Regular", size: 15.0) ?? UIFont.systemFont(ofSize: 15),
                NSAttributedString.Key.foregroundColor: UIColor.white]
            
            self.setTitleTextAttributes(defaultAttributes, for: .normal)
            self.setTitleTextAttributes(selectedAttributes, for: .selected)
            
            for i in 0...subviews.count - 1 {
                if let subview = subviews[i] as? UIImageView {
                    if i == self.selectedSegmentIndex {
                        subview.backgroundColor = UIColor(red: 114.0/255.0, green: 73.0/255.0, blue: 187.0/255.0, alpha: 1.0)
                    } else {
                        if i == 0 {
                            subview.backgroundColor = i == 0 ? unselectMilestoneColor : .clear
                        } else if i == 1 {
                            subview.backgroundColor = i == 1 ? unselectMilestoneColor : .clear
                        } else if i == 2 {
                            subview.backgroundColor = i == 2 ? unselectMilestoneColor : .clear
                        } else if i == 3 {
                            subview.backgroundColor = i == 3 ? unselectMilestoneColor : .clear
                        } else if i == 4 {
                            subview.backgroundColor = i == 4 ? unselectMilestoneColor : .clear
                        }
                    }
                }
            }
        } else if tag == 2 {
            layer.cornerRadius = self.bounds.size.height / 2.0
            layer.borderColor = UIColor(red: 59.0/255.0, green: 159.0/255.0, blue: 239.0/255.0, alpha: 1.0).cgColor
            layer.borderWidth = 0.0
            layer.masksToBounds = true
            clipsToBounds = true
            let defaultAttributes = [
                NSAttributedString.Key.font: UIFont(name: "Open-Sans-Regular", size: 15.0) ?? UIFont.systemFont(ofSize: 15),
                NSAttributedString.Key.foregroundColor: UIColor.darkGray]
            let selectedAttributes = [
                        NSAttributedString.Key.font: UIFont(name: "Open-Sans-Regular", size: 15.0) ?? UIFont.systemFont(ofSize: 15),
                NSAttributedString.Key.foregroundColor: UIColor.white]
            
            self.setTitleTextAttributes(defaultAttributes, for: .normal)
            self.setTitleTextAttributes(selectedAttributes, for: .selected)
            
            for i in 0...subviews.count - 1 {
                if let subview = subviews[i] as? UIImageView {
                    if i == self.selectedSegmentIndex {
                        subview.backgroundColor = UIColor(red: 114.0/255.0, green: 73.0/255.0, blue: 187.0/255.0, alpha: 1.0)
                    } else {
                        if i == 0 {
                            subview.backgroundColor = i == 0 ? unselectMilestoneColor : .clear
                        } else if i == 1 {
                            subview.backgroundColor = i == 1 ? unselectMilestoneColor : .clear
                        } else if i == 2 {
                            subview.backgroundColor = i == 2 ? unselectMilestoneColor : .clear
                        }
                    }
                }
            }
        } else if tag == 3 {
            layer.cornerRadius = self.bounds.size.height / 2.0
            layer.borderColor = UIColor(red: 59.0/255.0, green: 159.0/255.0, blue: 239.0/255.0, alpha: 1.0).cgColor
            layer.borderWidth = 0.0
            layer.masksToBounds = true
            clipsToBounds = true
            let defaultAttributes = [
                NSAttributedString.Key.font: UIFont(name: "Open-Sans-Regular", size: 15.0) ?? UIFont.systemFont(ofSize: 15),
                NSAttributedString.Key.foregroundColor: UIColor.darkGray]
            let selectedAttributes = [
                        NSAttributedString.Key.font: UIFont(name: "Open-Sans-Regular", size: 15.0) ?? UIFont.systemFont(ofSize: 15),
                NSAttributedString.Key.foregroundColor: UIColor.white]
            
            self.setTitleTextAttributes(defaultAttributes, for: .normal)
            self.setTitleTextAttributes(selectedAttributes, for: .selected)
            
            for i in 0...subviews.count - 1 {
                if let subview = subviews[i] as? UIImageView {
                    if i == self.selectedSegmentIndex {
                        subview.backgroundColor = UIColor(red: 114.0/255.0, green: 73.0/255.0, blue: 187.0/255.0, alpha: 1.0)
                    } else {
                        if i == 0 {
                            subview.backgroundColor = i == 0 ? unselectMilestoneColor : .clear
                        } else if i == 1 {
                            subview.backgroundColor = i == 1 ? unselectMilestoneColor : .clear
                        } 
                    }
                }
            }
        } else if tag == 10 {
            layer.cornerRadius = self.bounds.size.height / 2.0
            layer.borderColor = UIColor.white.cgColor
            layer.borderWidth = 2.0
            layer.masksToBounds = true
            clipsToBounds = true
            let defaultAttributes = [
                NSAttributedString.Key.font: UIFont(name: "Open-Sans-Regular", size: 15.0) ?? UIFont.systemFont(ofSize: 15),
                NSAttributedString.Key.foregroundColor: UIColor.darkGray]
            let selectedAttributes = [
                        NSAttributedString.Key.font: UIFont(name: "Open-Sans-Regular", size: 15.0) ?? UIFont.systemFont(ofSize: 15),
                NSAttributedString.Key.foregroundColor: UIColor.white]
            
            self.setTitleTextAttributes(defaultAttributes, for: .normal)
            self.setTitleTextAttributes(selectedAttributes, for: .selected)
            
            for i in 0...subviews.count - 1 {
                if let subview = subviews[i] as? UIImageView {
                    if i == self.selectedSegmentIndex {
                        subview.backgroundColor = UIColor(red: 114.0/255.0, green: 73.0/255.0, blue: 187.0/255.0, alpha: 1.0)
                    } else {
                        if i == 0 {
                            subview.backgroundColor = i == 0 ? .white : .white
                        } else if i == 1 {
                            subview.backgroundColor = i == 1 ? .white : .white
                        }
                    }
                }
            }
        }
   }
}
