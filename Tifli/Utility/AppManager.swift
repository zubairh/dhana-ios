//
//  AppManager.swift
//  ServiceRight
//
//  Created by Rhenusone Rosalia on 01/01/21.
//  Copyright © 2021 ServiceRight B.V. All rights reserved.
//

import Foundation

class AppManager {
    
    static var shared = AppManager()
    
    public var babyName: String?
    public var babyGender: String?
    public var babyImage: UIImage?
    public var babyDOB:String?
    public var babyDeliveryType:String?
    public var babyWeekPregnancy:String?
    public var otherRelationValue:String?
    public var arrBabyRelation = NSMutableArray.init()
    public var arrBabyMedical = NSMutableArray.init()
    public var arrBabyAllergy = NSMutableArray.init()
    public var arrBabyVaccine = NSMutableArray.init()
    public var arrBabyFoods = NSMutableArray.init()
    public var arrBabyToysTitle = NSMutableArray.init()
    public var arrBabyDocumentTitle = NSMutableArray.init()
    
    public var arrBabyVacc = [BabyType]()
    public var arrBabyMed = [BabyType]()
    public var arrBabyAller = [BabyType]()
    public var arrBabyRel = [BabyType]()
    public var arrBabyFood = [BabyType]()
    public var arrBabyToys = [UIImage]()
    public var arrBabyDocument = [UIImage]()
    public var arrBabies = [Baby]()
    
    // for twins
    public var babyNameTwins: String?
    public var babyGenderTwins: String?
    public var babyImageTwins: UIImage?
    public var arrBabyRelationTwins = NSMutableArray.init()
    public var arrBabyMedicalTwins = NSMutableArray.init()
    public var arrBabyAllergyTwins = NSMutableArray.init()
    public var arrBabyVaccineTwins = NSMutableArray.init()
    public var arrBabyFoodsTwins = NSMutableArray.init()
    public var arrBabyToysTitleTwins = NSMutableArray.init()
    public var arrBabyDocumentTitleTwins = NSMutableArray.init()
    
    public var arrBabyVaccTwins = [BabyType]()
    public var arrBabyMedTwins = [BabyType]()
    public var arrBabyAllerTwins = [BabyType]()
    public var arrBabyRelTwins = [BabyType]()
    public var arrBabyFoodTwins = [BabyType]()
    public var arrBabyToysTwins = [UIImage]()
    public var arrBabyDocumentTwins = [UIImage]()
    public var arrBabiesTwins = [Baby]()
    
    // for ingrediants
    public var arrIngrediants = NSMutableArray.init()
    
    public var arrTeeth = [Teeth]()
    public var arrDataChart = [GrowthChart]()
    
    //for subscription
    public var arrSubscription = [Subscription]()
    public var planId = "0"
}



