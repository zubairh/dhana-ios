//
//  AppUtility.swift

import Foundation
import UIKit
import SDWebImage
import SwiftMessages
import SwiftyJSON
import KRProgressHUD

class AppUtility: NSObject {
    class func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    class func convertToArray(text: String) -> AnyObject {
           if let data = text.data(using: .utf8) {
               do {
                   return try JSONSerialization.jsonObject(with: data, options: []) as AnyObject
               } catch {
                   print(error.localizedDescription)
               }
           }
           return "" as AnyObject
    }
    
    class func showAnimation(view:UIView) {
        UIView.animate(withDuration: 1.0, animations: {() -> Void in
            view.alpha = 1.0
        })
    }
    
    
    class func getDateFromTimeStamp1(timeStamp : Int) -> String {

        let date = Date(timeIntervalSince1970: TimeInterval(timeStamp))

        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "dd MMM YY, hh:mm a"

        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        return dateString
    }
    
    class func convertDateToString(date :NSDate) ->String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let myString = formatter.string(from: date as Date)
        let yourDate: Date? = formatter.date(from: myString)
        formatter.dateFormat = "MM/dd/yyyy"
        let updatedString = formatter.string(from: yourDate!)
        
        return updatedString
    }
    
    class func convertTeethDateToString(date :NSDate) ->String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let myString = formatter.string(from: date as Date)
        let yourDate: Date? = formatter.date(from: myString)
        formatter.dateFormat = "yyyy-MM"
        let updatedString = formatter.string(from: yourDate!)
        
        return updatedString
    }
    
    class func convertDateToStringTime(date :NSDate) ->String {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let myString = formatter.string(from: date as Date)
        let yourDate: Date? = formatter.date(from: myString)
        formatter.dateFormat = "HH:mm"
        let updatedString = formatter.string(from: yourDate!)
        
        return updatedString
    }
    
    class func convertDateToStringTimeSleep(date :NSDate) ->String {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let myString = formatter.string(from: date as Date)
        let yourDate: Date? = formatter.date(from: myString)
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let updatedString = formatter.string(from: yourDate!)
        
        return updatedString
    }
    
    class func convertDateToStringDate(date :NSDate) ->String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let myString = formatter.string(from: date as Date)
        let yourDate: Date? = formatter.date(from: myString)
        formatter.dateFormat = "yyyy-MM-dd"
        let updatedString = formatter.string(from: yourDate!)
        
        return updatedString
    }
    
    class func convertDateToTimeString(date :NSDate) ->String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let myString = formatter.string(from: date as Date)
        let yourDate: Date? = formatter.date(from: myString)
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let updatedString = formatter.string(from: yourDate!)
        
        return updatedString
    }
    
    class func getEEE_DD_MMM_HH_mm(dateString:String) ->String {
        
        let dateFormatter = DateFormatter()
        var date:Date! = AppUtility.getLocalDateTime(fromString2: dateString)
        if(date == nil) {
            date = Date()
        }
        dateFormatter.dateFormat = "dd MMM, yyyy"
        dateFormatter.locale = Locale(identifier: "en_US")
        return dateFormatter.string(from: date)
    }
    
    class func getEEE_DD_MMM_HH_mm_package(dateString:String) ->String {
        
        let dateFormatter = DateFormatter()
        var date:Date! = AppUtility.getLocalDateTimePackage(fromString2: dateString)
        if(date == nil) {
            date = Date()
        }
        dateFormatter.dateFormat = "dd MMM, yyyy"
        dateFormatter.locale = Locale(identifier: "en_US")
        return dateFormatter.string(from: date)
    }
    
    class func get_EEE_DD_MMM_HH_mm(dateString:String) ->String {
        
        let dateFormatter = DateFormatter()
        var date:Date! = AppUtility.getLocalDateTime(fromString2: dateString)
        if(date == nil) {
            date = Date()
        }
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        dateFormatter.locale = Locale(identifier: "en_US")
        return dateFormatter.string(from: date)
    }
    
    class func get_memory_date_time(dateString:String) ->String {
        
        let dateFormatter = DateFormatter()
        var date:Date! = AppUtility.getLocalDateTime(fromString2: dateString)
        if(date == nil) {
            date = Date()
        }
        dateFormatter.dateFormat = "dd MMM yyyy hh a"
        dateFormatter.locale = Locale(identifier: "en_US")
        return dateFormatter.string(from: date)
    }
    
    class func get_MMM(dateString:String) ->String {
        
        let dateFormatter = DateFormatter()
        var date:Date! = AppUtility.getLocalDateTimeCalender(fromString2: dateString)
        if(date == nil) {
            date = Date()
        }
        dateFormatter.dateFormat = "MMM dd"
        dateFormatter.locale = Locale(identifier: "en_US")
        return dateFormatter.string(from: date)
    }
    
    class func get_Teeth_MMM(dateString:String) ->String {
        
        let dateFormatter = DateFormatter()
        var date:Date! = AppUtility.getLocalDateTimeAnalysis(fromString2: dateString)
        if(date == nil) {
            date = Date()
        }
        dateFormatter.dateFormat = "MMM yyyy"
        dateFormatter.locale = Locale(identifier: "en_US")
        return dateFormatter.string(from: date)
    }
    
    class func getEEE_DD_MMM_HH_Time(dateString:String) ->String {
        
        let dateFormatter = DateFormatter()
        var date:Date! = AppUtility.getLocalDateTimeChat(fromString2: dateString)
        if(date == nil) {
            date = Date()
        }
        dateFormatter.dateFormat = "hh:mm:a dd MMM"
        dateFormatter.locale = Locale(identifier: "en_US")
        return dateFormatter.string(from: date)
    }
    
    class func getDateStringFormat_monthname_date_year_hour_min(date :String) ->String
    {
        let startDateInString = date.components(separatedBy: ".")
        let startDateFormatedInString = startDateInString[0]
        let replaced = startDateFormatedInString.replacingOccurrences(of: "T", with: " ")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let yourDate = dateFormatter.date(from: replaced)
        dateFormatter.dateFormat = "hh:mm:ss"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let updatedString = dateFormatter.string(from: yourDate ?? Date())
        
        return updatedString
    }
    
    class func getDate(dateString:String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        return dateFormatter.date(from: dateString)
    }
    
    class func getDateWithFormat(dateString:String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
//        dateFormatter.locale = NSLocale.current
        return dateFormatter.date(from: dateString)
    }
    
    class func showCustomAlert () {
        
    }
    
    class func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    class func getLocalDateTime(fromString2 dayString: String?) -> Date? {
        //2014-09-18 14:13:04
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale
        let someDateInUTC: Date? = dateFormatter.date(from: dayString ?? "")
        let timeZoneSeconds = TimeInterval(NSTimeZone.local.secondsFromGMT())
        let text = someDateInUTC?.addingTimeInterval(timeZoneSeconds)
        return text
    }
    
    class func getLocalDateTimePackage(fromString2 dayString: String?) -> Date? {
        //2014-09-18 14:13:04
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd, hh:mm a"
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale
        let someDateInUTC: Date? = dateFormatter.date(from: dayString ?? "")
        let timeZoneSeconds = TimeInterval(NSTimeZone.local.secondsFromGMT())
        let text = someDateInUTC?.addingTimeInterval(timeZoneSeconds)
        return text
    }
    
    class func getLocalDateTimeCalender(fromString2 dayString: String?) -> Date? {
        //2014-09-18 14:13:04
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale
        let someDateInUTC: Date? = dateFormatter.date(from: dayString ?? "")
        let timeZoneSeconds = TimeInterval(NSTimeZone.local.secondsFromGMT())
        let text = someDateInUTC?.addingTimeInterval(timeZoneSeconds)
        return text
    }
    
    class func getLocalDateTimeAnalysis(fromString2 dayString: String?) -> Date? {
        //2014-09-18 14:13:04
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM"
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale
        let someDateInUTC: Date? = dateFormatter.date(from: dayString ?? "")
        let timeZoneSeconds = TimeInterval(NSTimeZone.local.secondsFromGMT())
        let text = someDateInUTC?.addingTimeInterval(timeZoneSeconds)
        return text
    }
    
    class func getLocalDateTimeChat(fromString2 dayString: String?) -> Date? {
        //2014-09-18 14:13:04
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US") as Locale
        let someDateInUTC: Date? = dateFormatter.date(from: dayString ?? "")
        let timeZoneSeconds = TimeInterval(NSTimeZone.local.secondsFromGMT())
        let text = someDateInUTC?.addingTimeInterval(timeZoneSeconds)
        return text
    }
    
    class func getDateFromTimeStamp(timeStamp : Int) -> String {

        let date = Date(timeIntervalSince1970: TimeInterval(timeStamp))

        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "dd MMM YY, hh:mm a"
     // UnComment below to get only time
    //  dayTimePeriodFormatter.dateFormat = "hh:mm a"
        
       // let dateString = dayTimePeriodFormatter.string(from: date as Date)
        let dateString = date.timeAgoDisplay()
        return dateString
    }
    
    class func convertDateFormater(_ date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "MMM dd, yyyy"
        return  dateFormatter.string(from: date!)

    }
    
    class func getMMM_DD_YYYY_FromTimeStamp(timeStamp : Int) -> String {

        let date = Date(timeIntervalSince1970: TimeInterval(timeStamp))

        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "MMM dd, yyyy"

       // let dateString = dayTimePeriodFormatter.string(from: date as Date)
        let dateString = dayTimePeriodFormatter.string(from: date)
        return dateString
    }
    
    //MARK:- USER SAVING
    class func getUserLoggedIn() ->User{
        
        var u = User()
        
        if let dataJSON = USER_DEFAUTS.object(forKey: USER_LOGIN) {
            
            let swiftyJsonVar = JSON(dataJSON)
            var userDict: Dictionary<String, JSON>
            userDict = swiftyJsonVar["user"].dictionaryValue
            u = User.init(withDictionary: userDict)
        }
        return u
    }
    
    class func isUserLoggedIn() ->Bool{
        
        var flag:Bool = false
        
        if let dataJSON = USER_DEFAUTS.object(forKey: USER_LOGIN) {
            
            let swiftyJsonVar = JSON(dataJSON)
            var userDict: Dictionary<String, JSON>
            userDict = swiftyJsonVar["user"].dictionaryValue
            
            let u:User! = User.init(withDictionary: userDict)
            if(u.email != "" || u.phoneNumber != "")
            {
                flag = true
            }
        }
        return flag
    }
    
    class func saveUserToken(token:String) {
        UserDefaults.standard.set(token, forKey: USER_TOKEN)
    }
    
    class func getUserToken() -> String {
        return UserDefaults.standard.value(forKey: USER_TOKEN) as? String ?? ""
    }
    
    class func saveBabyId(babyID:String) {
        UserDefaults.standard.set(babyID, forKey: babyId)
    }
    
    class func getBabyId() -> String {
        return UserDefaults.standard.value(forKey: babyId) as? String ?? "0"
    }
    
    class func isBabySelected() ->Bool {
        
        var flag:Bool = false
        if UserDefaults.standard.value(forKey: babyId) != nil {
            flag = true
        } else {
            flag = false
        }
        return flag
    }
    
    class UnderlinedLabel: UILabel {

    override var text: String? {
        didSet {
            guard let text = text else { return }
            let textRange = NSMakeRange(0, text.count)
            let attributedText = NSMutableAttributedString(string: text)
            attributedText.addAttribute(NSAttributedString.Key.underlineStyle , value: NSUnderlineStyle.single.rawValue, range: textRange)
            // Add other attributes if needed
            self.attributedText = attributedText
            }
        }
    }
    
    class func setImage(url:String, image:UIImageView) {
        image.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "bb"))
    }
    
    class func loadImage(imageView: UIImageView, urlString: String, placeHolderImageString: String?)->Swift.Void{
        let placeHolder = UIImage(named: placeHolderImageString ?? "")
        imageView.image = placeHolder
        let encodedString:String = urlString.replacingOccurrences(of: " ", with: "%20")
        imageView.sd_setImage(with: URL(string: encodedString), placeholderImage: placeHolder)
    }
    
    class func setChatImage(url:String, image:UIImageView) {
        image.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "summary_placeholder"))
    }
    
    class func showProgress() {
        KRProgressHUD.show()
    }
    
    class func hideProgress() {
        DispatchQueue.main.asyncAfter(deadline: .now()+1) {
           KRProgressHUD.dismiss()
        }
    }
    
    class func showErrorMessage(message:String) {
        let error = MessageView.viewFromNib(layout: .tabView)
        error.configureTheme(.error)
        error.configureContent(title: "Error", body: message)
        error.button?.isHidden = true
        SwiftMessages.show(view: error)
    }
    
    class func showSuccessMessage(message:String) {
        let success = MessageView.viewFromNib(layout: .tabView)
        success.configureTheme(.success)
        success.configureContent(title: "Success", body: message)
        success.button?.isHidden = true
        SwiftMessages.show(view: success)
    }
    
    class func showInfoMessage(message:String) {
        let info = MessageView.viewFromNib(layout: .tabView)
        info.configureTheme(.warning)
        info.configureContent(title: "Info", body: message)
        info.button?.isHidden = true
        SwiftMessages.show(view: info)
    }
    
    class func stringToDate(_ str: String)->Date {
        let datefrmter = DateFormatter()
        datefrmter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if let date = datefrmter.date(from: str) {
            return date
        } else {
            return Date()
        }
    }
    
    class func getElapsedInterval(fromDate:String) -> String {
        let dateFrom = self.stringToDate(fromDate)
        
        let interval = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from:dateFrom , to:Date())
        
        if let year = interval.year, year > 0 {
            return year == 1 ? "\(year)" + " " + String(format:"%@", "year ago"):
                "\(year)" + " " + String(format:"%@","years ago")
        } else if let month = interval.month, month > 0 {
            return month == 1 ? "\(month)" + " " + String(format:"%@","month ago") :
                "\(month)" + " " + String(format:"%@", "months ago")
        } else if let day = interval.day, day > 0 {
            return day == 1 ? "\(day)" + " " + String(format:"%@","day ago") :
                "\(day)" + " " + String(format:"%@", "days ago")
        } else if let hour = interval.hour, hour > 0 {
            return hour == 1 ? "\(hour)" + " " + String(format:"%@", "hour ago") :
                "\(hour)" + " " + String(format:"%@", "hours ago")
        } else if let minute = interval.minute, minute > 0 {
            return minute == 1 ? "\(minute)" + " " + String(format:"%@","minute ago") :
                "\(minute)" + " " + String(format:"%@","minutes ago")
        } else if let second = interval.second, second > 0 {
            return second == 1 ? "\(second)" + " " + String(format:"%@", "second ago") :
                "\(second)" + " " + String(format:"%@","seconds ago")
        } else {
            return String(format:"%@", "a moment ago")
        }
    }
}
extension Date {
    func timeAgoDisplay() -> String {

        let calendar = Calendar.current
        let minuteAgo = calendar.date(byAdding: .minute, value: -1, to: Date())!
        let hourAgo = calendar.date(byAdding: .hour, value: -1, to: Date())!
        let dayAgo = calendar.date(byAdding: .day, value: -1, to: Date())!
        let weekAgo = calendar.date(byAdding: .day, value: -7, to: Date())!

        if minuteAgo < self {
            let diff = Calendar.current.dateComponents([.second], from: self, to: Date()).second ?? 0
            return "\(diff) " + "sec ago"
        } else if hourAgo < self {
            let diff = Calendar.current.dateComponents([.minute], from: self, to: Date()).minute ?? 0
            return "\(diff) " +  "min ago"
        } else if dayAgo < self {
            let diff = Calendar.current.dateComponents([.hour], from: self, to: Date()).hour ?? 0
            return "\(diff) " + "hours ago"
        } else if weekAgo < self {
            let diff = Calendar.current.dateComponents([.day], from: self, to: Date()).day ?? 0
            return "\(diff) " + "days ago"
        }
        let diff = Calendar.current.dateComponents([.weekOfYear], from: self, to: Date()).weekOfYear ?? 0
        return "\(diff) " + "weeks ago"
    }
}
extension String {
    func convertToDictionary() -> [String: Any]? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}
extension UIImage {
    
    func imageWithSize(scaledToSize newSize: CGSize) -> UIImage {
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        self.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
}


class FileDownloader {

    static func loadFileSync(url: URL, completion: @escaping (String?, Error?) -> Void)
    {
        let documentsUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!

        let destinationUrl = documentsUrl.appendingPathComponent(url.lastPathComponent)

        if FileManager().fileExists(atPath: destinationUrl.path)
        {
            print("File already exists [\(destinationUrl.path)]")
            completion(destinationUrl.path, nil)
        }
        else if let dataFromURL = NSData(contentsOf: url)
        {
            if dataFromURL.write(to: destinationUrl, atomically: true)
            {
                print("file saved [\(destinationUrl.path)]")
                completion(destinationUrl.path, nil)
            }
            else
            {
                print("error saving file")
                let error = NSError(domain:"Error saving file", code:1001, userInfo:nil)
                completion(destinationUrl.path, error)
            }
        }
        else
        {
            let error = NSError(domain:"Error downloading file", code:1002, userInfo:nil)
            completion(destinationUrl.path, error)
        }
    }

    static func loadFileAsync(url: URL, completion: @escaping (String?, Error?) -> Void)
    {
        
        let fileName = url.lastPathComponent
        let path = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        let documentDirectoryPath:String = path[0]
        let fileManager = FileManager()
        var destinationURLForFile = URL(fileURLWithPath: documentDirectoryPath.appending("/Centrale")) //\(String(describing: fileName!))
        do {
            try fileManager.createDirectory(at: destinationURLForFile, withIntermediateDirectories: true, attributes: nil)
            destinationURLForFile.appendPathComponent(String(describing: fileName))
            
            }catch(let error){
            print(error)
        }
//        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
//
        let destinationUrl = destinationURLForFile
        if FileManager().fileExists(atPath: destinationUrl.path)
        {
            print("File already exists [\(destinationUrl.path)]")
            completion(destinationUrl.path, nil)
        }
        else
        {
            let session = URLSession(configuration: URLSessionConfiguration.default, delegate: nil, delegateQueue: nil)
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            let task = session.dataTask(with: request, completionHandler:
            {
                data, response, error in
                if error == nil
                {
                    if let response = response as? HTTPURLResponse
                    {
                        if response.statusCode == 200
                        {
                            if let data = data
                            {
                                
                                
                                
                                if let _ = try? data.write(to: destinationUrl, options: Data.WritingOptions.atomic)
                                {
                                    completion(destinationUrl.path, error)
                                }
                                else
                                {
                                    completion(destinationUrl.path, error)
                                }
                            }
                            else
                            {
                                completion(destinationUrl.path, error)
                            }
                        }
                    }
                }
                else
                {
                    completion(destinationUrl.path, error)
                }
            })
            task.resume()
        }
    }
    
    static func downloadFile (urlString : String, viewController : UIViewController){
        let url = URL(string: urlString)
        let fileName = String((url!.lastPathComponent)) as NSString
        // Create destination URL
        let documentsUrl:URL =  (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL?)!
        let destinationFileUrl = documentsUrl.appendingPathComponent("\(fileName)")
        //Create URL to the source file you want to download
        let fileURL = URL(string: urlString)
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig)
        let request = URLRequest(url:fileURL!)
        let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
            if let tempLocalUrl = tempLocalUrl, error == nil {
                // Success
                if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                    print("Successfully downloaded. Status code: \(statusCode)")
                }
                do {
                    
                    try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                    do {
                        //Show UIActivityViewController to save the downloaded file
                        let contents  = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
                        for indexx in 0..<contents.count {
                            DispatchQueue.main.async {
                                if contents[indexx].lastPathComponent == destinationFileUrl.lastPathComponent {
                                    let activityViewController = UIActivityViewController(activityItems: [contents[indexx]], applicationActivities: nil)
                                    viewController.present(activityViewController, animated: true, completion: nil)
                                }
                            }
                            
                        }
                    }
                    catch (let err) {
                        print("error: \(err)")
                    }
                } catch (let writeError) {
                    
                    print("Error creating a file \(destinationFileUrl) : \(writeError)")
                   DispatchQueue.main.async {
                       let activityViewController = UIActivityViewController(activityItems: [destinationFileUrl], applicationActivities: nil)
                       viewController.present(activityViewController, animated: true, completion: nil)
                   }
                        
                }
            } else {
                print("Error took place while downloading a file. Error description: \(error?.localizedDescription ?? "")")
            }
        }
        task.resume()
    }
}

extension UIViewController {
    
    func showAlertWithMesssage(message:String, title:String, VC: UIViewController) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action1 = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
            print("You've pressed default");
        }
        alertController.addAction(action1)
        VC.present(alertController, animated: true, completion: nil)
    }
    
    func toastMessage(_ message: String){
        guard let window = UIApplication.shared.keyWindow else {return}
        let messageLbl = UILabel()
        messageLbl.text = message
        messageLbl.textAlignment = .center
        messageLbl.font = UIFont.systemFont(ofSize: 12)
        messageLbl.textColor = .white
        messageLbl.backgroundColor = UIColor(white: 0, alpha: 1)
        
        let textSize:CGSize = messageLbl.intrinsicContentSize
        let labelWidth = min(textSize.width, window.frame.width - 40)
        
        messageLbl.frame = CGRect(x: 20, y: 40, width: labelWidth + 30, height: textSize.height + 20)
        messageLbl.center.x = window.center.x
        messageLbl.layer.cornerRadius = messageLbl.frame.height/2
        messageLbl.layer.masksToBounds = true
        window.addSubview(messageLbl)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            
            UIView.animate(withDuration: 3, animations: {
                messageLbl.alpha = 0
            }) { (_) in
                messageLbl.removeFromSuperview()
            }
        }
    }
    
}
