//
//  AppDelegate.swift
//  Tifli
//
//  Created by zubair on 11/02/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import Firebase
import UserNotifications
import UserNotificationsUI
import BRYXBanner

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate, UNUserNotificationCenterDelegate {

    var deviceToken = ""
    var babyId = "0"

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        ApplicationDelegate.shared.application(
            application,
            didFinishLaunchingWithOptions: launchOptions
        )
        
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        
        if #available(iOS 10.0, *) {
          // For iOS 10 display notification (sent via APNS)
          UNUserNotificationCenter.current().delegate = self

          let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
          UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {_, _ in })
        } else {
          let settings: UIUserNotificationSettings =
          UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
          application.registerUserNotificationSettings(settings)
        }

        application.registerForRemoteNotifications()
        
        return true
    }

    // MARK: UISceneSession Lifecycle
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return ApplicationDelegate.shared.application(
            app,
            open: url,
            options: options
        )
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    func delay(_ delay:Double, closure:@escaping ()->()) {
        let when = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
    }
    
    // MARK: - Push Notifications Delegate
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
      print("Firebase registration token: \(String(describing: fcmToken))")

      let dataDict:[String: String] = ["token": fcmToken ?? ""]
      self.deviceToken = fcmToken!
      NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print(deviceTokenString)
//        self.deviceToken = deviceTokenString
        Messaging.messaging().apnsToken = deviceToken
    }
    
    //Called if unable to register for APNS.
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("i am not available in simulator \(error)")
    }
    
    func application(_ application: UIApplication,didReceiveRemoteNotification userInfo: [AnyHashable : Any],fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        let remoteNotificationDict = userInfo as NSDictionary
        let infoDict:NSDictionary = remoteNotificationDict
        print(infoDict)
        let apsDict = infoDict["aps"] as! NSDictionary
        let apsAlert = apsDict["alert"] as! NSDictionary
        
        let state: UIApplication.State = UIApplication.shared.applicationState
        
        let type = infoDict["type"] as? String
        
        if state == .active {
            
            if type == "milestone" {
                let banner = Banner(title: apsAlert["title"] as? String, subtitle: apsAlert["body"] as? String , image: UIImage(named: "AppIcon"), backgroundColor: COLORS.APP_THEME_COLOR)
                banner.dismissesOnTap = true
                banner.didTapBlock = {
                    appDelegate.babyId = String(infoDict["babyId"] as! Int)
                    appSceneDelegate.tabsViewController.selectedIndex = 1
                }
                banner.show(duration: 3.0)
            } else {
                let banner = Banner(title: apsAlert["title"] as? String, subtitle: apsAlert["body"] as? String , image: UIImage(named: "AppIcon"), backgroundColor: COLORS.APP_THEME_COLOR)
                banner.dismissesOnTap = true
                banner.didTapBlock = {
                    appSceneDelegate.tabsViewController.selectedIndex = 0
                }
                banner.show(duration: 3.0)
            }
        }
        print(apsAlert)
    }

}

