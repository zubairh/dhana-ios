//
//  AlergyCell.swift
//  Tifli
//
//  Created by zubair on 12/07/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit

class AlergyCell: UITableViewCell {

    @IBOutlet weak var lablTitle: UILabel!
    @IBOutlet weak var btnAdd: GradientButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
