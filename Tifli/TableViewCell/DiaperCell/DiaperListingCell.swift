//
//  DiaperListingCell.swift
//  Tifli
//
//  Created by zubair on 14/12/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit

class DiaperListingCell: UITableViewCell {

    @IBOutlet weak var lablClean: UILabel!
    @IBOutlet weak var lablMixed: UILabel!
    @IBOutlet weak var lablLiquidPoo: UILabel!
    @IBOutlet weak var lablSolidPo: UILabel!
    @IBOutlet weak var lablSoftPo: UILabel!
    @IBOutlet weak var lablPee: UILabel!
    @IBOutlet weak var lablDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
