//
//  DiaperViewCell.swift
//  Tifli
//
//  Created by zubair on 20/11/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit

class DiaperViewCell: UICollectionViewCell {

    @IBOutlet weak var lablCount: UILabel!
    @IBOutlet weak var lablTitle: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lablTotalAmount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
