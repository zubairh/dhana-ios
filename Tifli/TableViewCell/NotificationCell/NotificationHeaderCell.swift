//
//  NotificationHeaderCell.swift
//  Tifli
//
//  Created by zubair on 10/08/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit

class NotificationHeaderCell: UITableViewCell {
    
    @IBOutlet weak var lablTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
