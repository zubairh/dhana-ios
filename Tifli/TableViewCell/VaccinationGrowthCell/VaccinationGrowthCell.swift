//
//  VaccinationGrowthCell.swift
//  Tifli
//
//  Created by zubair on 08/11/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit

class VaccinationGrowthCell: UITableViewCell {
    
    @IBOutlet weak var lablTime: UILabel!
    @IBOutlet weak var lablTimeAgo: UILabel!
    @IBOutlet weak var imgUSer: UIImageView!
    @IBOutlet weak var lablUser: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lablDetail: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnDone: GradientButton!
    @IBOutlet weak var lablCatName: UILabel!
    @IBOutlet weak var imgCat: UIImageView!
    @IBOutlet weak var switchOneHour: UISwitch!
    @IBOutlet weak var switchOneday: UISwitch!
    @IBOutlet weak var imgNonActive: UIImageView!
    
    var objVaccination = Vaccination()
    var objAppointment = Appointment()
    var isAppointment = false

    override func awakeFromNib() {
        super.awakeFromNib()
        
        collectionView.dataSource = self
        collectionView.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

// MARK: - CollectionView Delegate-
extension VaccinationGrowthCell: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
      return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let label = UILabel(frame: CGRect.zero)
        if isAppointment {
            if indexPath.row == 0 {
                label.text = objAppointment.clinic
            } else if indexPath.row == 1 {
                label.text = objAppointment.doctorName
            }
        } else {
            if indexPath.row == 0 {
                label.text = objVaccination.titles
            } else if indexPath.row == 1 {
                label.text = objVaccination.clinic
            }
        }
        label.sizeToFit()
        label.font = UIFont(name: "Open-Sans-Regular", size: 12)
        return CGSize(width: label.frame.width + 14, height: 22)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        collectionView.register(UINib(nibName: String(describing: AddBabyMedCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: AddBabyMedCell.self))
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: AddBabyMedCell.self), for: indexPath as IndexPath) as! AddBabyMedCell
        
        if isAppointment {
            if indexPath.row == 0 {
                cell.lablTitle.text = objAppointment.clinic
            } else if indexPath.row == 1 {
                cell.lablTitle.text = objAppointment.doctorName
            }
        } else {
            if indexPath.row == 0 {
                cell.lablTitle.text = objVaccination.titles
            } else if indexPath.row == 1 {
                cell.lablTitle.text = objVaccination.clinic
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}
