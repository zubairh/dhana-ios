//
//  BabyProfileCell.swift
//  Tifli
//
//  Created by zubair on 01/08/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit

class BabyProfileCell: UITableViewCell {

    @IBOutlet weak var imgBaby: UIImageView!
    @IBOutlet weak var lablBabyName: UILabel!
    @IBOutlet weak var lablBabyAge: UILabel!
    @IBOutlet weak var lablRelation: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
