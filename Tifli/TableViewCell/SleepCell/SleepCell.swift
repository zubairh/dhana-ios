//
//  SleepCell.swift
//  Tifli
//
//  Created by zubair on 07/12/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit

class SleepCell: UITableViewCell {

    @IBOutlet weak var lablDayTime: UILabel!
    @IBOutlet weak var lablNightTime: UILabel!
    @IBOutlet weak var lablTotal: UILabel!
    @IBOutlet weak var lablDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
