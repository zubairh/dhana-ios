//
//  TempratureCell.swift
//  Tifli
//
//  Created by zubair on 12/12/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit

class TempratureCell: UITableViewCell {
    
    @IBOutlet weak var chart: LineChart!
    @IBOutlet weak var lablTitle: UILabel!
    @IBOutlet weak var lablValues: UILabel!
    @IBOutlet weak var lablNotes: UILabel!
    
    var arrData = [Temperature]()
    
    let color = UIColor(red: 28/255, green: 188/255, blue: 199/255, alpha: 1)

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.updateGraph()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func updateGraph() {
        chart.clear()
        var xValue = [String]()
        var yValue = [String]()
        var currentValue = [CGFloat]()
        var highValue = [Double]()

        for obj in self.arrData {
            xValue.append(obj.grapshDate)
            yValue.append(obj.grapshValue)
            currentValue.append(CGFloat(Double(obj.grapshValue) ?? 0))
            highValue.append(Double(obj.grapshValue) ?? 0)
        }
        if arrData.count == 1 {
            
            xValue.append("")
            yValue.append("")
            currentValue.append(CGFloat(0))
            
            xValue.append("")
            yValue.append("")
            currentValue.append(CGFloat(0))
            
            xValue.append("")
            yValue.append("")
            currentValue.append(CGFloat(0))
            
            xValue.append("")
            yValue.append("")
            currentValue.append(CGFloat(0))
            
        } else if arrData.count == 2 {
            
            xValue.append("")
            yValue.append("")
            currentValue.append(CGFloat(0))

            xValue.append("")
            yValue.append("")
            currentValue.append(CGFloat(0))
        
            xValue.append("")
            yValue.append("")
            currentValue.append(CGFloat(0))
    
        } else if arrData.count == 3 {
           
            xValue.append("")
            yValue.append("")
            currentValue.append(CGFloat(0))

     
            xValue.append("")
            yValue.append("")
            currentValue.append(CGFloat(0))
    
        } else if arrData.count == 4 {
          
            xValue.append("")
            yValue.append("")
            currentValue.append(CGFloat(0))
        }
        
        let xLabels: [String] = xValue
        if arrData.count > 0 {
            chart.clearAll()
            chart.animation.enabled = true
            chart.dots.visible = true
            chart.area = false
            chart.x.labels.visible = true
            chart.x.grid.count = 5
            chart.y.grid.count = 5
            chart.x.labels.values = xLabels
            chart.y.labels.values = yValue
            chart.y.labels.visible = true
            chart.addLine(currentValue)
        }
        
        self.lablValues.text = String(highValue.max() ?? 0)+" fh"
    }
    
}
