//
//  GraphCell.swift
//  Tifli
//
//  Created by zubair on 27/07/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import SwiftChart
import Charts

class GraphCell: UITableViewCell {
    
    //@IBOutlet var chart: LineChart!

    @IBOutlet weak var chart: LineChartView!
    @IBOutlet weak var lablTitle: UILabel!
    @IBOutlet weak var lablValues: UILabel!
    @IBOutlet weak var lablNotes: UILabel!
    
    var arrData = [GrowthChart]()
    
    let color = UIColor(red: 28/255, green: 188/255, blue: 199/255, alpha: 1)

    override func awakeFromNib() {
        super.awakeFromNib()
//
//        chart.backgroundColor = .clear
//        chart.gridBackgroundColor = .white
//        chart.drawGridBackgroundEnabled = true
//
//        chart.drawBordersEnabled = true
//
//        chart.chartDescription?.enabled = false
//
//        chart.pinchZoomEnabled = false
//        chart.dragEnabled = true
//        chart.setScaleEnabled(true)
//
//        chart.legend.enabled = false
//
//        chart.xAxis.enabled = true
//
//        var xValue = [String]()
//        var highValue = [Double]()
//        var lowerValue = [Double]()
//        var currentValue = [Double]()
//
//        for obj in self.arrData {
//            xValue.append(obj.month+"m")
//            highValue.append(Double(obj.higher) ?? 0)
//            lowerValue.append(Double(obj.lower) ?? 0)
//            //if obj.actual != "0" {
//            currentValue.append(Double(obj.actual) ?? 0)
//            //}
//        }
//
//        let leftAxis = chart.leftAxis
//        leftAxis.axisMaximum = 45
//        leftAxis.axisMinimum = 95
//        leftAxis.drawAxisLineEnabled = true
//
//        let xAxis = chart.xAxis
//        xAxis.axisMaximum = 24
//        xAxis.axisMinimum = 0
//        xAxis.drawAxisLineEnabled = true
//
//        chart.rightAxis.enabled = true
//        chart.xAxis.enabled = true
        
        updateGraph()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func updateGraph() {
        chart.clear()
        chart.clearValues()
        chart.data = nil
//        var xValue = [String]()
//        var yValue = [String]()
//        var highValue = [CGFloat]()
//        var lowerValue = [CGFloat]()
//        var currentValue = [CGFloat]()
//
//        for obj in self.arrData {
//            xValue.append(obj.month+"m")
//            highValue.append(CGFloat(Double(obj.higher) ?? 0))
//            lowerValue.append(CGFloat(Double(obj.lower) ?? 0))
//            //if obj.actual != "0" {
//            currentValue.append(CGFloat(Double(obj.actual) ?? 0))
//            //}
//        }
//        for i in 0..<self.arrData.count {
//            let obj = self.arrData[i]
//            if i == 0 {
//                yValue.append(obj.lower)
//            } else if i == 1 {
//                yValue.append(obj.lower)
//            } else {
//                yValue.append(obj.higher)
//            }
//        }
//        let xLabels: [String] = xValue
//        if arrData.count > 0 {
//            chart.clearAll()
//            chart.animation.enabled = true
//            chart.dots.visible = true
//            chart.area = true
//            chart.x.labels.visible = true
//            chart.x.grid.count = 5
//            chart.y.grid.count = 5
//            chart.x.labels.values = xLabels
//            chart.y.labels.values = yValue
//            chart.y.labels.visible = true
//            chart.addLine(highValue)
//            chart.addLine(lowerValue)
//            chart.addLine(currentValue)
//        }
        
//        chart.xLabels = xValue
//        chart.xLabelsFormatter = { String(Int(round($1))) + "m" }
//
//        let seriesActual = ChartSeries(currentValue)
//        seriesActual.color = color
//        seriesActual.area = false
//
//        let seriesHigher = ChartSeries(highValue)
//        seriesHigher.color = #colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1)
//        seriesHigher.area = false
//
//        // A partially filled series
//        let series3 = ChartSeries(lowerValue)
//        series3.color = #colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1)
//        series3.area = false
//
//        chart.add([seriesHigher, seriesActual, series3])
        
//        let yVals1 = (0..<count).map { (i) -> ChartDataEntry in
//            let val = Double(arc4random_uniform(range) + 50)
//            return ChartDataEntry(x: Double(i), y: val)
//        }
//        let yVals2 = (0..<count).map { (i) -> ChartDataEntry in
//            let val = Double(arc4random_uniform(range) + 450)
//            return ChartDataEntry(x: Double(i), y: val)
//        }
        
        var xValue = [String]()
        var xMaxValue = [Double]()
        var highValue = [Double]()
        var lowerValue = [Double]()
        var currentValue = [Double]()

        for obj in self.arrData {
            xValue.append(obj.month+"m")
            xMaxValue.append(Double(obj.month) ?? 0)
            highValue.append(Double(obj.higher) ?? 0)
            lowerValue.append(Double(obj.lower) ?? 0)
//            if obj.actual != "0" {
//                currentValue.append(Double(obj.actual) ?? 0)
//            } else {
//                currentValue.append(Double(lowerValue.min()+5 ?? 0))
//            }
        }
        
        chart.backgroundColor = #colorLiteral(red: 0.8941176471, green: 0.8862745098, blue: 0.9294117647, alpha: 1)
        chart.gridBackgroundColor = #colorLiteral(red: 0.1098039216, green: 0.737254902, blue: 0.7803921569, alpha: 1).withAlphaComponent(0.0)
        chart.drawGridBackgroundEnabled = false
        
        chart.drawBordersEnabled = false
        
        chart.chartDescription?.enabled = false
        
        chart.pinchZoomEnabled = true
        chart.dragEnabled = true
        chart.setScaleEnabled(true)
        chart.legend.enabled = false
        chart.xAxis.enabled = true
        
        chart.animate(xAxisDuration: 3.0)
        //chart.animate(xAxisDuration: 4.0, easingOption: .easeInOutCubic)
        
        var yVals1 = [ChartDataEntry]()
        
        for obj in self.arrData {
            yVals1.append(ChartDataEntry(x: Double(obj.day)!, y: Double(obj.lower)!))
        }
        
        var yVals2 = [ChartDataEntry]()
        
        for obj in self.arrData {
            yVals2.append(ChartDataEntry(x: Double(obj.day)!, y: Double(obj.higher)!))
        }
        
        var yVals3 = [ChartDataEntry]()
        
        for obj in self.arrData {
            if obj.actual != "0" {
                yVals3.append(ChartDataEntry(x: Double(obj.day)!, y: Double(obj.actual)!))
            }
//            else {
//                let min:Double = lowerValue.min()!
//                yVals3.append(ChartDataEntry(x: Double(obj.month)!, y: min))
//            }
        }
        
        let leftAxis = chart.leftAxis
        leftAxis.axisMaximum = highValue.max() ?? 0
        leftAxis.axisMinimum = lowerValue.min() ?? 0
        leftAxis.drawAxisLineEnabled = false
        
        let xAxis = chart.xAxis
//        xAxis.axisMaximum = xMaxValue.max() ?? 24
//        xAxis.axisMinimum = 1
        xAxis.drawAxisLineEnabled = true
        xAxis.labelPosition = .bottom
        xAxis.labelCount = 9
        self.chart.xAxis.valueFormatter = DefaultAxisValueFormatter(block: {(index, _) in
            return xValue[Int(index)]
        })
        
        chart.rightAxis.enabled = false
        
        let set1 = LineChartDataSet(entries: yVals1, label: "")
        set1.axisDependency = .left
        set1.setColor(#colorLiteral(red: 0.1098039216, green: 0.737254902, blue: 0.7803921569, alpha: 1).withAlphaComponent(0.2))
        set1.drawCirclesEnabled = false
        set1.lineWidth = 1
        set1.circleRadius = 3
        set1.fillAlpha = 1
        set1.drawFilledEnabled = true
        set1.fillColor = #colorLiteral(red: 0.8941176471, green: 0.8862745098, blue: 0.9294117647, alpha: 1)
        set1.highlightColor = #colorLiteral(red: 0.1098039216, green: 0.737254902, blue: 0.7803921569, alpha: 1).withAlphaComponent(0.2)
        set1.drawCircleHoleEnabled = false
        set1.fillFormatter = DefaultFillFormatter { _,_  -> CGFloat in
            return CGFloat(self.chart.leftAxis.axisMinimum)
        }
        
        let set2 = LineChartDataSet(entries: yVals2, label: "")
        set2.axisDependency = .left
        set2.setColor(#colorLiteral(red: 0.1098039216, green: 0.737254902, blue: 0.7803921569, alpha: 1).withAlphaComponent(0.2))
        set2.drawCirclesEnabled = false
        set2.lineWidth = 1
        set2.circleRadius = 3
        set2.fillAlpha = 1
        set2.drawFilledEnabled = true
        set2.fillColor = #colorLiteral(red: 0.8941176471, green: 0.8862745098, blue: 0.9294117647, alpha: 1)
        set2.highlightColor = #colorLiteral(red: 0.1098039216, green: 0.737254902, blue: 0.7803921569, alpha: 1).withAlphaComponent(0.2)
        set2.drawCircleHoleEnabled = false
        set2.fillFormatter = DefaultFillFormatter { _,_  -> CGFloat in
            return CGFloat(self.chart.leftAxis.axisMaximum)
        }
    
        let set3 = LineChartDataSet(entries: yVals3, label: "")
        set3.axisDependency = .left
        set3.setColor(#colorLiteral(red: 0.1098039216, green: 0.737254902, blue: 0.7803921569, alpha: 1))
        set3.drawCirclesEnabled = true
        set3.lineWidth = 2
        set3.circleRadius = 3
        set3.fillAlpha = 1
        set3.drawFilledEnabled = false
        set3.fillColor = #colorLiteral(red: 0.8941176471, green: 0.8862745098, blue: 0.9294117647, alpha: 1)
        set3.highlightColor = #colorLiteral(red: 0.1098039216, green: 0.737254902, blue: 0.7803921569, alpha: 1)
        set3.circleColors = [#colorLiteral(red: 0.1098039216, green: 0.737254902, blue: 0.7803921569, alpha: 1)]
        set3.drawCircleHoleEnabled = true
        set3.fillFormatter = DefaultFillFormatter { _,_  -> CGFloat in
            return CGFloat(self.chart.leftAxis.axisMaximum)
        }
        
        appDelegate.delay(2.7, closure: {
            self.chart.drawGridBackgroundEnabled = true
            self.chart.gridBackgroundColor = #colorLiteral(red: 0.1098039216, green: 0.737254902, blue: 0.7803921569, alpha: 1).withAlphaComponent(0.2)
        })
        let data = LineChartData(dataSets: [set1, set2, set3])
        data.setDrawValues(false)
        
        chart.data = data
    }
}
