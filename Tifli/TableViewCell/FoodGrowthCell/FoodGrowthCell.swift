//
//  FoodGrowthCell.swift
//  Tifli
//
//  Created by zubair on 08/11/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit

class FoodGrowthCell: UITableViewCell {

    @IBOutlet weak var lablTime: UILabel!
    @IBOutlet weak var lablTimeAgo: UILabel!
    @IBOutlet weak var imgUSer: UIImageView!
    @IBOutlet weak var lablUser: UILabel!
    @IBOutlet weak var tfNotes: UITextField!
    @IBOutlet weak var lablLeftBreast: UILabel!
    @IBOutlet weak var lablRightBreast: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnDone: GradientButton!
    @IBOutlet weak var btnPlayLeftBreast: UIButton!
    @IBOutlet weak var btnPlayRightBreast: UIButton!
    @IBOutlet weak var imgNonActive: UIImageView!
    
    var objFood = Feeding()
    var timerLeftBreast = Timer()
    var timerRightBreast = Timer()
    var totalSecondLeft:Int = 0
    var totalSecondRight:Int = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @objc func leftBreastTimeUpdate() {
        var hours: Int
        var minutes: Int
        var seconds: Int

        totalSecondLeft = totalSecondLeft + 1
        hours = totalSecondLeft / 3600
        minutes = (totalSecondLeft % 3600) / 60
        seconds = (totalSecondLeft % 3600) % 60
        lablLeftBreast.text = String(format: "%02d:%02d:%02d", hours, minutes, seconds)
    }
    
    @objc func rightBreastTimeUpdate() {
        var hours: Int
        var minutes: Int
        var seconds: Int

        totalSecondRight = totalSecondRight + 1
        hours = totalSecondRight / 3600
        minutes = (totalSecondRight % 3600) / 60
        seconds = (totalSecondRight % 3600) % 60
        lablRightBreast.text = String(format: "%02d:%02d:%02d", hours, minutes, seconds)
    }
    
    @IBAction func leftBreastTapped(_ sender: Any) {
        if btnPlayLeftBreast.isSelected {
            btnPlayLeftBreast.isSelected = false
            timerLeftBreast.invalidate()
        } else {
            btnPlayLeftBreast.isSelected = true
            timerLeftBreast = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.leftBreastTimeUpdate), userInfo: nil, repeats: true)
        }
    }
    
    @IBAction func rightBreastTapped(_ sender: Any) {
        if btnPlayRightBreast.isSelected {
            btnPlayRightBreast.isSelected = false
            timerRightBreast.invalidate()
        } else {
            btnPlayRightBreast.isSelected = true
            timerRightBreast = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.rightBreastTimeUpdate), userInfo: nil, repeats: true)
        }
    }
}
