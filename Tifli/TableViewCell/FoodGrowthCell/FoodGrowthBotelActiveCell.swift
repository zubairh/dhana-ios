//
//  FoodGrowthBotelActiveCell.swift
//  Tifli
//
//  Created by zubair on 08/02/2022.
//  Copyright © 2022 Flattechs. All rights reserved.
//

import UIKit

class FoodGrowthBotelActiveCell: UITableViewCell {
    
    @IBOutlet weak var lablTime: UILabel!
    @IBOutlet weak var lablTimeAgo: UILabel!
    @IBOutlet weak var imgUSer: UIImageView!
    @IBOutlet weak var lablUser: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnDone: GradientButton!
    @IBOutlet weak var tfAmount: UITextField!
    @IBOutlet weak var collectionTypes: UICollectionView!
    @IBOutlet weak var collectionBottel: UICollectionView!
    @IBOutlet weak var lablDetail: UILabel!
    
    var arrTitle = ["Botel".localized()]
    var arrIcon = ["Milk"]
    var arrTitleBottle = ["Breast Milk".localized(), "Formula".localized()]
    var arrIconBottle = ["Breast Milk", "Formula"]
    var selectedBotelIndex = -1
    var objFood = Feeding()

    override func awakeFromNib() {
        super.awakeFromNib()
        
        collectionTypes.dataSource = self
        collectionTypes.delegate = self
        collectionBottel.dataSource = self
        collectionBottel.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setData() {
        self.tfAmount.text = objFood.amount
        if objFood.bottleType == "breast_milk" {
            selectedBotelIndex = 0
        } else {
            selectedBotelIndex = 1
        }
        collectionBottel.dataSource = self
        collectionBottel.delegate = self
    }
    
    @IBAction func addAmountTapped(_ sender: Any) {
        var amount = 0
        if tfAmount.text?.count ?? 0 > 0 {
            amount = Int(tfAmount.text ?? "0") ?? 0
            amount = amount+1
            tfAmount.text = String(amount)
        } else {
            amount = amount+1
            tfAmount.text = String(amount)
        }
    }
    
    @IBAction func minusAmountTapped(_ sender: Any) {
        var amount = 0
        if tfAmount.text?.count ?? 0 > 0 {
            amount = Int(tfAmount.text ?? "0") ?? 0
            if amount >= 1 {
                amount = amount-1
            }
            tfAmount.text = String(amount)
        }
    }
    
}

extension FoodGrowthBotelActiveCell: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionBottel {
            return self.arrIconBottle.count
        }
        return self.arrIcon.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == collectionBottel {
            return CGSize(width: collectionView.bounds.width/2, height: 50)
        } else {
            return CGSize(width: collectionView.bounds.width, height: 50)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collectionTypes {
            
            collectionView.register(UINib(nibName: String(describing: FoodAddCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: FoodAddCell.self))
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: FoodAddCell.self), for: indexPath as IndexPath) as! FoodAddCell
            
            cell.imgIcon.image = UIImage(named: self.arrIcon[indexPath.row])
            cell.lablTitle.text = self.arrTitle[indexPath.row]
            
            cell.viewBg.layer.borderColor = COLORS.TABBAR_COLOR.cgColor
            cell.lablTitle.textColor = COLORS.TABBAR_COLOR
        
            return cell
            
        } else {
            
            collectionView.register(UINib(nibName: String(describing: FoodAddCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: FoodAddCell.self))
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: FoodAddCell.self), for: indexPath as IndexPath) as! FoodAddCell
            
            cell.imgIcon.image = UIImage(named: self.arrIconBottle[indexPath.row])
            cell.lablTitle.text = self.arrTitleBottle[indexPath.row]
            
            if indexPath.row == selectedBotelIndex {
                cell.viewBg.layer.borderColor = COLORS.TABBAR_COLOR.cgColor
                cell.lablTitle.textColor = COLORS.TABBAR_COLOR
            } else {
                cell.viewBg.layer.borderColor = UIColor.lightGray.cgColor
                cell.lablTitle.textColor = .lightGray
            }
            
            return cell
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectionBottel {
            selectedBotelIndex = indexPath.row
            collectionView.reloadData()
        }
    }
    
}
