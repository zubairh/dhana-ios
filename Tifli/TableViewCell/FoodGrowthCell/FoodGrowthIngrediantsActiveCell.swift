//
//  FoodGrowthIngrediantsActiveCell.swift
//  Tifli
//
//  Created by zubair on 08/02/2022.
//  Copyright © 2022 Flattechs. All rights reserved.
//

import UIKit

class FoodGrowthIngrediantsActiveCell: UITableViewCell {
    
    @IBOutlet weak var lablTime: UILabel!
    @IBOutlet weak var lablTimeAgo: UILabel!
    @IBOutlet weak var imgUSer: UIImageView!
    @IBOutlet weak var lablUser: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnDone: GradientButton!
    @IBOutlet weak var collectionIngrediants: UICollectionView!
    @IBOutlet weak var btnAddIngrediants: UIButton!
    @IBOutlet weak var collectionTypes: UICollectionView!
    @IBOutlet weak var lablDetail: UILabel!
    
    var arrTitle = ["Solids".localized()]
    var arrIcon = ["Solid"]
    var objFood = Feeding()
    var arrIngrediant = NSMutableArray.init()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        collectionTypes.dataSource = self
        collectionTypes.delegate = self
        collectionIngrediants.dataSource = self
        collectionIngrediants.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setData() {
        let arr = objFood.ingredients.components(separatedBy: ",")
        for v in arr {
            var isFound = false
            for c in self.arrIngrediant {
                if c as! String == v {
                    isFound = true
                }
            }
            if !isFound {
                if v.count > 0 {
                    self.arrIngrediant.add(v)
                }
            }
        }
        for v in AppManager.shared.arrIngrediants {
            self.arrIngrediant.add(v)
        }
        
        AppManager.shared.arrIngrediants.removeAllObjects()
        collectionIngrediants.reloadData()
    }
    
    @objc func deleteIngrediatsTapped(_ sender: UIButton) {
        self.arrIngrediant.removeObject(at: sender.tag)
        collectionIngrediants.reloadData()
    }
    
}

extension FoodGrowthIngrediantsActiveCell: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionIngrediants {
            return self.arrIngrediant.count
        }
        return self.arrIcon.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == collectionIngrediants {
            let label = UILabel(frame: CGRect.zero)
            label.text = (self.arrIngrediant.object(at: indexPath.row) as! String)
            label.sizeToFit()
            label.font = UIFont(name: "Open-Sans-Regular", size: 12)
            return CGSize(width: label.frame.size.width+58, height: 42)
            
        } else {
            return CGSize(width: collectionView.bounds.width, height: 50)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collectionTypes {
            
            collectionView.register(UINib(nibName: String(describing: FoodAddCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: FoodAddCell.self))
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: FoodAddCell.self), for: indexPath as IndexPath) as! FoodAddCell
            
            cell.imgIcon.image = UIImage(named: self.arrIcon[indexPath.row])
            cell.lablTitle.text = self.arrTitle[indexPath.row]
        
            cell.viewBg.layer.borderColor = COLORS.TABBAR_COLOR.cgColor
            cell.lablTitle.textColor = COLORS.TABBAR_COLOR
            
            return cell
            
        } else {
            
            collectionView.register(UINib(nibName: String(describing: FoodSolidCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: FoodSolidCell.self))
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: FoodSolidCell.self), for: indexPath as IndexPath) as! FoodSolidCell
            
            cell.lablTitle.text = (self.arrIngrediant.object(at: indexPath.row) as! String)
            cell.btnClose.tag = indexPath.row
            cell.btnClose.addTarget(self, action: #selector(self.deleteIngrediatsTapped(_:)), for: .touchUpInside)
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
}
