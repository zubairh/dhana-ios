//
//  TeethGrowthAudioCell.swift
//  Tifli
//
//  Created by zubair on 11/11/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit

class TeethGrowthAudioCell: UITableViewCell {
    
    @IBOutlet weak var lablTime: UILabel!
    @IBOutlet weak var lablTimeAgo: UILabel!
    @IBOutlet weak var imgUSer: UIImageView!
    @IBOutlet weak var lablUser: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lablDetail: UILabel!
    @IBOutlet weak var lablAudioTime: UILabel!
    @IBOutlet weak var progressAudio: UIProgressView!
    @IBOutlet weak var btnPlayAudio: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    
    let circularLayout = DSCircularLayout()
    
    var objTeeth = Teething()

    override func awakeFromNib() {
        super.awakeFromNib()
        
        appDelegate.delay(0.1, closure: {
            self.setCircular()
        })
        
        collectionView.dataSource = self
        collectionView.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setCircular() {
        if SCREEN_WIDTH > 375 {
            circularLayout.initWithCentre(CGPoint(x: collectionView.bounds.width/2, y: collectionView.bounds.height/2), radius: collectionView.bounds.width / 2 - 70 / 2, itemSize: CGSize(width: 38, height: 38), andAngularSpacing: 11)
        } else {
            circularLayout.initWithCentre(CGPoint(x: collectionView.bounds.width/2, y: collectionView.bounds.height/2), radius: collectionView.bounds.width / 2 - 38 / 2, itemSize: CGSize(width: 38, height: 38), andAngularSpacing: 5.4)
        }
        circularLayout.setStartAngle(.pi * 2, endAngle: 0)
        circularLayout.mirrorX = true
        circularLayout.mirrorY = true
        circularLayout.rotateItems = true
        circularLayout.scrollDirection = .horizontal
        collectionView.collectionViewLayout = circularLayout
    }
    
}

extension TeethGrowthAudioCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return objTeeth.arrTooth.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        collectionView.register(UINib(nibName: String(describing: TeethCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: TeethCell.self))
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: TeethCell.self), for: indexPath as IndexPath) as! TeethCell
        
        AppUtility.loadImage(imageView: cell.imgTeeth, urlString: objTeeth.arrTooth[indexPath.row].image, placeHolderImageString: "")
        
        if objTeeth.ID == objTeeth.arrTooth[indexPath.row].ID {
            cell.imgTeeth.alpha = 1
        } else {
            cell.imgTeeth.alpha = 0.3
        }
        
        return cell
    }
}

