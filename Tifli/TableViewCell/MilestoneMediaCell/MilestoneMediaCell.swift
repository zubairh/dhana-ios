//
//  MilestoneMediaCell.swift
//  Tifli
//
//  Created by zubair on 04/08/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit

class MilestoneMediaCell: UITableViewCell {

    @IBOutlet weak var imgArchive: UIImageView!
    @IBOutlet weak var lablDate: UILabel!
    @IBOutlet weak var lablMonth: UILabel!
    @IBOutlet weak var lablTitle: UILabel!
    @IBOutlet weak var imgCareGiver: UIImageView!
    @IBOutlet weak var lablDetail: UILabel!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lablQuestion: UILabel!
    @IBOutlet weak var tfNotes: UITextField!
    @IBOutlet weak var btnMedia: UIButton!
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var btnAudio: UIButton!
    @IBOutlet weak var btnReadMore: UIButton!
    @IBOutlet weak var btnAchieve: GradientButton!
    @IBOutlet weak var layoutHeightQuestion: NSLayoutConstraint!
    @IBOutlet weak var btnYes: UIButton!
    @IBOutlet weak var btnNo: UIButton!
    @IBOutlet weak var btnNotSure: UIButton!
    @IBOutlet weak var lablUsername: UILabel!
    @IBOutlet weak var layoutHeightNotArchived: NSLayoutConstraint!
    @IBOutlet weak var viewQuestion: UIView!
    @IBOutlet weak var viewAchived: UIView!
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var layoutHeightImage: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
