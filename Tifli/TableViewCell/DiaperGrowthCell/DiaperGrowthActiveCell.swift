//
//  DiaperGrowthActiveCell.swift
//  Tifli
//
//  Created by zubair on 08/02/2022.
//  Copyright © 2022 Flattechs. All rights reserved.
//

import UIKit

class DiaperGrowthActiveCell: UITableViewCell {
    
    @IBOutlet weak var lablTime: UILabel!
    @IBOutlet weak var lablTimeAgo: UILabel!
    @IBOutlet weak var imgUSer: UIImageView!
    @IBOutlet weak var lablUser: UILabel!
    @IBOutlet weak var imgItem: UIImageView!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnDone: GradientButton!
    @IBOutlet weak var collectionTypes: UICollectionView!
    @IBOutlet weak var layoutCollectionHeightType: NSLayoutConstraint!
    @IBOutlet weak var collectionColor: UICollectionView!
    @IBOutlet weak var layoutHeightImage: NSLayoutConstraint!
    @IBOutlet weak var lablDetail: UILabel!
    
    var objDiaper = Diaper()
    var selectedIndex = -1
    var selectedDiaperLog = -1
    var selectedIndexDaiperForm = -1
    var selectedIndexDaiperColor = -1
    var arrTitle = ["Clean".localized(), "Poo".localized(), "Pee".localized(), "Mixed".localized(), "Liquid".localized(), "Soft".localized(), "Solid".localized()]
    var arrColor = [UIColor.brown, UIColor.yellow, UIColor.green, UIColor.black, UIColor.gray, UIColor.red]
    var arrColorTitle = ["brown", "yellow", "green", "black", "gray", "red"]

    override func awakeFromNib() {
        super.awakeFromNib()
        
        collectionTypes.delegate = self
        collectionTypes.dataSource = self
        collectionColor.delegate = self
        collectionColor.dataSource = self
        
        collectionTypes.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.old, context: nil)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setData() {
        for i in 0 ..< arrTitle.count {
            let cc = self.arrTitle[i]
            if cc == objDiaper.form {
                selectedIndexDaiperForm = i
            }
        }
        for i in 0 ..< arrTitle.count {
            let cc = self.arrTitle[i]
            if cc == objDiaper.type {
                selectedDiaperLog = i
            }
        }
        for i in 0 ..< arrColorTitle.count {
            let cc = self.arrColorTitle[i]
            if cc == objDiaper.color {
                selectedIndexDaiperColor = i
            }
        }
        self.collectionTypes.reloadData()
        self.collectionColor.reloadData()
        
//        "type":cell.arrTitle[cell.selectedDiaperLog], "form":cell.arrTitle[cell.selectedIndexDaiperForm], "color":cell.arrColorTitle[cell.selectedIndexDaiperColor]
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        layoutCollectionHeightType.constant = collectionTypes.contentSize.height
    }
}

extension DiaperGrowthActiveCell: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionTypes {
            return self.arrTitle.count
        }
        return self.arrColor.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == collectionTypes {
            if indexPath.row > 3 {
                return CGSize(width: collectionTypes.frame.width/4, height: 50)
            } else {
                return CGSize(width: collectionTypes.frame.width/4, height: 50)
            }
        }
        return CGSize(width: 64, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collectionTypes {
            
            collectionView.register(UINib(nibName: String(describing: DiaperSelectionCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: DiaperSelectionCell.self))
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: DiaperSelectionCell.self), for: indexPath as IndexPath) as! DiaperSelectionCell
            
            cell.lablTitle.text = self.arrTitle[indexPath.row]
            
            if indexPath.row > 3 {
                if indexPath.row == selectedIndexDaiperForm {
                    cell.viewBg.layer.borderColor = COLORS.TABBAR_COLOR.cgColor
                    cell.lablTitle.textColor = COLORS.TABBAR_COLOR
                } else {
                    cell.viewBg.layer.borderColor = UIColor.lightGray.cgColor
                    cell.lablTitle.textColor = .lightGray
                }
            } else {
                if indexPath.row == selectedDiaperLog {
                    cell.viewBg.layer.borderColor = COLORS.TABBAR_COLOR.cgColor
                    cell.lablTitle.textColor = COLORS.TABBAR_COLOR
                } else {
                    cell.viewBg.layer.borderColor = UIColor.lightGray.cgColor
                    cell.lablTitle.textColor = .lightGray
                }
            }
            
            return cell
            
        } else {
            
            collectionView.register(UINib(nibName: String(describing: DiaperColorCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: DiaperColorCell.self))
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: DiaperColorCell.self), for: indexPath as IndexPath) as! DiaperColorCell
            
            cell.imgBg.backgroundColor = self.arrColor[indexPath.row]
            
            if indexPath.row == selectedIndexDaiperColor {
                cell.viewBg.layer.borderColor = COLORS.TABBAR_COLOR.cgColor
            } else {
                cell.viewBg.layer.borderColor = UIColor.lightGray.cgColor
            }
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectionTypes {
            if indexPath.row > 3 {
                selectedIndexDaiperForm = indexPath.row
            } else {
                selectedDiaperLog = indexPath.row
            }
        } else {
            selectedIndexDaiperColor = indexPath.row
        }
        collectionView.reloadData()
    }
    
}
