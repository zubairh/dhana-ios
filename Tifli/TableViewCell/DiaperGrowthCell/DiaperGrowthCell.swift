//
//  DiaperGrowthCell.swift
//  Tifli
//
//  Created by zubair on 08/11/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit

class DiaperGrowthCell: UITableViewCell {
    
    @IBOutlet weak var lablTime: UILabel!
    @IBOutlet weak var lablTimeAgo: UILabel!
    @IBOutlet weak var imgUSer: UIImageView!
    @IBOutlet weak var lablUser: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var imgItem: UIImageView!
    @IBOutlet weak var lablDetail: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnDone: GradientButton!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
