//
//  TeethCell.swift
//  Tifli
//
//  Created by zubair on 28/10/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit

class TeethCell: UICollectionViewCell {

    @IBOutlet weak var imgTeeth: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
