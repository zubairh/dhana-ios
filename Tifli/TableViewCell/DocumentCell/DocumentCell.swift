//
//  DocumentCell.swift
//  Tifli
//
//  Created by zubair on 02/08/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit

class DocumentCell: UICollectionViewCell {

    @IBOutlet weak var imgDocument: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
