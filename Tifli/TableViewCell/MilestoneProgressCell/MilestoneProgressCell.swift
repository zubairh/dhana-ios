//
//  MilestoneProgressCell.swift
//  Tifli
//
//  Created by zubair on 04/08/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit

class MilestoneProgressCell: UITableViewCell {

    @IBOutlet weak var lablTitle: UILabel!
    @IBOutlet var viewStep: UIView!
    @IBOutlet weak var lablAchievement: UILabel!
    @IBOutlet weak var lablStatus: UILabel!
    
    var stepProgress = StepIndicatorView()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        self.stepProgress.numberOfSteps = 5
//        self.stepProgress.currentStep = 0
//        self.stepProgress.circleColor = UIColor(red: 179.0/255.0, green: 189.0/255.0, blue: 194.0/255.0, alpha: 1.0)
//        self.stepProgress.circleTintColor = UIColor(red: 0.0/255.0, green: 180.0/255.0, blue: 124.0/255.0, alpha: 1.0)
//        self.stepProgress.circleStrokeWidth = 3.0
//        self.stepProgress.circleRadius = 10.0
//        self.stepProgress.lineColor = self.stepProgress.circleColor
//        self.stepProgress.lineTintColor = self.stepProgress.circleTintColor
//        self.stepProgress.lineMargin = 4.0
//        self.stepProgress.lineStrokeWidth = 2.0
//        self.stepProgress.displayNumbers = false //indicates if it displays numbers at the center instead of the core circle
//        self.stepProgress.direction = .leftToRight
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
