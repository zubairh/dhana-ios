//
//  ActivityGrowthActiveCell.swift
//  Tifli
//
//  Created by zubair on 08/02/2022.
//  Copyright © 2022 Flattechs. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class ActivityGrowthActiveCell: UITableViewCell {

    @IBOutlet weak var lablTime: UILabel!
    @IBOutlet weak var lablTimeAgo: UILabel!
    @IBOutlet weak var imgUSer: UIImageView!
    @IBOutlet weak var lablUser: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnDone: GradientButton!
    @IBOutlet weak var tfActivity: UITextField!
    @IBOutlet weak var lablDetail: UILabel!
    
    @IBOutlet weak var imgItem: UIImageView!
    @IBOutlet weak var layoutHeightImage: NSLayoutConstraint!
    
    var arrActivityList = [BabyType]()
    var objActivity = Activity()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setData() {
        self.tfActivity.text = objActivity.titles
        self.tfActivity.tag = Int(objActivity.activityId) ?? 0
    }
    
    @IBAction func activityTapped(_ sender: Any) {
        var arr = [String]()
        for obj in self.objActivity.arrActivity {
            arr.append(obj.typeTitle)
        }
        ActionSheetStringPicker.show(withTitle: "Select Activity List".localized(), rows: arr, initialSelection: 0, doneBlock: {
            picker, indexes, values in
            
            self.tfActivity.text = values as? String
            self.tfActivity.tag = Int(self.objActivity.arrActivity[indexes].ID)!

            return
        }, cancel: { ActionSheetStringPicker in return},origin: sender)
    }
    
}
