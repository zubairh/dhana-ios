//
//  ActivityGrowthCell.swift
//  Tifli
//
//  Created by zubair on 08/11/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit

class ActivityGrowthCell: UITableViewCell {
    
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var lablDetail: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
