//
//  GrowthAudioCell.swift
//  Tifli
//
//  Created by zubair on 19/10/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit

class GrowthAudioCell: UITableViewCell {

    @IBOutlet weak var lablTime: UILabel!
    @IBOutlet weak var lablTimeAgo: UILabel!
    @IBOutlet weak var imgUSer: UIImageView!
    @IBOutlet weak var lablUser: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lablAudioTime: UILabel!
    @IBOutlet weak var progressAudio: UIProgressView!
    @IBOutlet weak var btnPlayAudio: UIButton!
    @IBOutlet weak var layoutHeightCollection: NSLayoutConstraint!
    @IBOutlet weak var lablCatName: UILabel!
    @IBOutlet weak var imgCat: UIImageView!
    @IBOutlet weak var lablDetail: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    
    
    var objGrowth = Growth()
    var isNotes = false
    var objDiaper = Diaper()
    var objActivity = Activity()
    var isDiaper = false
    var isActivity = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        collectionView.dataSource = self
        collectionView.delegate = self
        
        if isNotes {
            layoutHeightCollection.constant = 0
        } else {
            layoutHeightCollection.constant = 24
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

// MARK: - CollectionView Delegate-
extension GrowthAudioCell: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isNotes {
            return 0
        } else if isActivity {
            return 1
        }
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let label = UILabel(frame: CGRect.zero)
        if isDiaper {
            if indexPath.row == 0 {
                label.text = objDiaper.type
            } else if indexPath.row == 1 {
                label.text = "    "
            } else if indexPath.row == 2 {
                label.text = objDiaper.form
            }
            label.sizeToFit()
            label.font = UIFont(name: "Open-Sans-Regular", size: 12)
            return CGSize(width: label.frame.width + 14, height: 22)
        } else if isActivity {
            label.text = objActivity.titles
            label.sizeToFit()
            label.font = UIFont(name: "Open-Sans-Regular", size: 12)
            return CGSize(width: label.frame.width + 14, height: 22)
        } else {
            if indexPath.row == 0 {
                label.text = "Height "+objGrowth.height+"cm"
            } else if indexPath.row == 1 {
                label.text = "Weight "+objGrowth.weight+"kg"
            } else if indexPath.row == 2 {
                label.text = "Head circ. "+objGrowth.circle+"cm"
            }
            label.sizeToFit()
            label.font = UIFont(name: "Open-Sans-Regular", size: 12)
            return CGSize(width: label.frame.width + 14, height: 22)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        collectionView.register(UINib(nibName: String(describing: AddBabyMedCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: AddBabyMedCell.self))
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: AddBabyMedCell.self), for: indexPath as IndexPath) as! AddBabyMedCell
        
        if isDiaper {
            if indexPath.row == 0 {
                cell.lablTitle.text = objDiaper.type
                cell.imgColor.isHidden = true
            } else if indexPath.row == 1 {
                cell.lablTitle.text = "    "
                cell.imgColor.isHidden = false
                if objDiaper.color == "brown" {
                    cell.imgColor.backgroundColor = .brown
                } else if objDiaper.color == "yellow" {
                    cell.imgColor.backgroundColor = .yellow
                } else if objDiaper.color == "green" {
                    cell.imgColor.backgroundColor = .green
                } else if objDiaper.color == "black" {
                    cell.imgColor.backgroundColor = .black
                } else if objDiaper.color == "gray" {
                    cell.imgColor.backgroundColor = .gray
                } else if objDiaper.color == "red" {
                    cell.imgColor.backgroundColor = .red
                }
            } else if indexPath.row == 2 {
                cell.imgColor.isHidden = true
                cell.lablTitle.text = objDiaper.form
            }
        } else if isActivity {
            cell.lablTitle.text = objActivity.titles
        } else {
            if indexPath.row == 0 {
                cell.lablTitle.text = "Height "+objGrowth.height+"cm"
            } else if indexPath.row == 1 {
                cell.lablTitle.text = "Weight "+objGrowth.weight+"kg"
            } else if indexPath.row == 2 {
                cell.lablTitle.text = "Head circ. "+objGrowth.circle+"cm"
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}
