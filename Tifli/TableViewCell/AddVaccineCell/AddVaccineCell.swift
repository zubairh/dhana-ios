//
//  AddVaccineCell.swift
//  Tifli
//
//  Created by zubair on 26/10/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit

class AddVaccineCell: UITableViewCell {

    @IBOutlet weak var imgTick: UIImageView!
    @IBOutlet weak var lablStatus: UILabel!
    @IBOutlet weak var lablTitle: UILabel!
    @IBOutlet weak var lablDetail: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
