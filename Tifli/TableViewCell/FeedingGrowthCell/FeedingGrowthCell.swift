//
//  FeedingGrowthCell.swift
//  Tifli
//
//  Created by zubair on 08/11/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit


class FeedingGrowthCell: UITableViewCell {

    @IBOutlet weak var lablTime: UILabel!
    @IBOutlet weak var lablTimeAgo: UILabel!
    @IBOutlet weak var imgUSer: UIImageView!
    @IBOutlet weak var lablUser: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lablDetail: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    
    
    var objFeed = Feeding()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        collectionView.dataSource = self
        collectionView.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

// MARK: - CollectionView Delegate-
extension FeedingGrowthCell: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if objFeed.type == "bottle" {
            return 2
        } else if objFeed.type == "breast" {
            return 2
        }
        return self.objFeed.ingredients.components(separatedBy: ",").count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let label = UILabel(frame: CGRect.zero)
        if objFeed.type == "bottle" {
            if indexPath.row == 0 {
                label.text = "Bottle Type: "+objFeed.bottleType
            } else if indexPath.row == 1 {
                label.text = objFeed.amount+" ml"
            }
        } else if objFeed.type == "breast" {
            if indexPath.row == 0 {
                label.text = "left breast duration :"+objFeed.leftBreastDuration
            } else if indexPath.row == 1 {
                label.text = "right breast duration :"+objFeed.rightBreastDuration
            }
        } else {
            let arr = self.objFeed.ingredients.components(separatedBy: ",")
            label.text = arr[indexPath.row]
        }
        label.sizeToFit()
        label.font = UIFont(name: "Open-Sans-Regular", size: 12)
        return CGSize(width: label.frame.width + 14, height: 22)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        collectionView.register(UINib(nibName: String(describing: AddBabyMedCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: AddBabyMedCell.self))
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: AddBabyMedCell.self), for: indexPath as IndexPath) as! AddBabyMedCell
        
        if objFeed.type == "bottle" {
            if indexPath.row == 0 {
                cell.lablTitle.text = "Bottle Type: "+objFeed.bottleType
            } else if indexPath.row == 1 {
                cell.lablTitle.text = objFeed.amount+" ml"
            }
        } else if objFeed.type == "breast" {
            if indexPath.row == 0 {
                cell.lablTitle.text = "left breast duration :"+objFeed.leftBreastDuration
            } else if indexPath.row == 1 {
                cell.lablTitle.text = "right breast duration :"+objFeed.rightBreastDuration
            }
        } else {
            let arr = self.objFeed.ingredients.components(separatedBy: ",")
            cell.lablTitle.text = arr[indexPath.row]
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}
