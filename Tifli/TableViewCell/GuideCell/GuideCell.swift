//
//  GuideCell.swift
//  Tifli
//
//  Created by zubair on 25/07/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit

class GuideCell: UITableViewCell {

    @IBOutlet weak var imgGuide: UIImageView!
    @IBOutlet weak var lablTitle: UILabel!
    @IBOutlet weak var btnAutherName: UIButton!
    @IBOutlet weak var imgAuther: UIImageView!
    @IBOutlet weak var lablDetail: UILabel!
    @IBOutlet weak var lablTotalLike: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        DispatchQueue.main.async {
            self.imgGuide.roundCorners(corners: [.topRight, .topLeft], radius: 8)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
