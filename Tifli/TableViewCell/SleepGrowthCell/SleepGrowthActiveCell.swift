//
//  SleepGrowthActiveCell.swift
//  Tifli
//
//  Created by zubair on 07/02/2022.
//  Copyright © 2022 Flattechs. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class SleepGrowthActiveCell: UITableViewCell {
    
    @IBOutlet weak var lablTime: UILabel!
    @IBOutlet weak var lablTimeAgo: UILabel!
    @IBOutlet weak var imgUSer: UIImageView!
    @IBOutlet weak var lablUser: UILabel!
    @IBOutlet weak var lablCatName: UILabel!
    @IBOutlet weak var imgCat: UIImageView!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnDone: GradientButton!
    
    @IBOutlet weak var tfStartTime: UITextField!
    @IBOutlet weak var tfEndTime: UITextField!
    @IBOutlet weak var lablSetTimer: UILabel!
    @IBOutlet weak var lablSetManual: UILabel!
    @IBOutlet weak var btnPlayPause: UIButton!
    @IBOutlet weak var lablTimer: UILabel!
    @IBOutlet weak var viewTimer: UIView!
    @IBOutlet weak var btnManual: UIButton!
    @IBOutlet weak var lablDetail: UILabel!
    
    var timerSleep = Timer()
    var totalSecond:Int = 0
    var objSleep = Sleep()

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @objc func sleepTimeUpdate() {
        var hours: Int
        var minutes: Int
        var seconds: Int

        totalSecond = totalSecond + 1
        hours = totalSecond / 3600
        minutes = (totalSecond % 3600) / 60
        seconds = (totalSecond % 3600) % 60
        lablTimer.text = String(format: "%02d:%02d:%02d", hours, minutes, seconds)
    }
    
    func setData() {
        if objSleep.startTime == "00:00:00" && objSleep.endTime == "00:00:00" {
            self.setManualTapped(btnManual)
            if objSleep.duration.count == 0 {
                self.lablTimer.text = "00:00:00"
            } else {
                self.lablTimer.text = objSleep.duration
            }
        } else {
            self.tfStartTime.text = objSleep.startTime
            self.tfEndTime.text = objSleep.endTime
        }
    }
    
    //MARK:- Button Action
    @IBAction func setManualTapped(_ sender: UIButton) {
        timerSleep.invalidate()
        if sender.tag == 0 {
            sender.tag = 1
            lablSetTimer.text = "Set by manual".localized()
            viewTimer.isHidden = false
            self.tfStartTime.text = ""
            self.tfEndTime.text = ""
            lablTimer.text = "00:00:00"
        } else {
            sender.tag = 0
            lablSetTimer.text = "Set by timer".localized()
            self.tfStartTime.text = ""
            self.tfEndTime.text = ""
            lablTimer.text = "00:00:00"
            viewTimer.isHidden = true
        }
    }
    
    @IBAction func playTimeTapped(_ sender: Any) {
        if btnPlayPause.isSelected {
            btnPlayPause.isSelected = false
            timerSleep.invalidate()
        } else {
            let backgroundTaskIdentifier =
                          UIApplication.shared.beginBackgroundTask(expirationHandler: nil)

                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                            self.timerSleep = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.sleepTimeUpdate), userInfo: nil, repeats: true)
                           
                            UIApplication.shared.endBackgroundTask(backgroundTaskIdentifier)
                        }
            btnPlayPause.isSelected = true
            
        }
    }
    
    @IBAction func startTimeTapped(_ sender: UIButton) {
        let datePicker = ActionSheetDatePicker(title: "Select Start Time".localized(), datePickerMode: UIDatePicker.Mode.time, selectedDate: Date(), doneBlock: {
            picker, value, index in
            let date:NSDate = value as! NSDate
            
            self.tfStartTime.text = AppUtility.convertDateToStringTime(date: date)
            
            return
            
        }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview!.superview)
        datePicker?.show()
    }
    
    @IBAction func endTimeTapped(_ sender: UIButton) {
        if tfStartTime.text?.count ?? 0 > 0 {
            let datePicker = ActionSheetDatePicker(title: "Select End Time".localized(), datePickerMode: UIDatePicker.Mode.time, selectedDate: Date(), doneBlock: {
                picker, value, index in
                let date:NSDate = value as! NSDate
                
                let formatter = DateFormatter()
                formatter.dateFormat = "HH:mm"

                let date1 = formatter.date(from: self.tfStartTime.text!)
                let date2 = formatter.date(from: AppUtility.convertDateToStringTime(date: date))

                var result: ComparisonResult? = nil
                if let date2 = date2 {
                    result = date1?.compare(date2)
                }
                if result == .orderedDescending {
                    print("date1 is later than date2")
                    AppUtility.showInfoMessage(message: "end time should be next time of start time".localized())
                    return
                } else if result == .orderedAscending {
                    self.tfEndTime.text = AppUtility.convertDateToStringTime(date: date)
                    print("date2 is later than date1")
                } else {
                    print("date1 is equal to date2")
                    AppUtility.showInfoMessage(message: "end time should be next time of start time".localized())
                    return
                }
                
                return
                
            }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview!.superview)
            datePicker?.show()
        }
        else {
            AppUtility.showInfoMessage(message: "Please select first start time".localized())
        }
    }
}
