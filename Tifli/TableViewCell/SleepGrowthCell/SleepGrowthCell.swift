//
//  SleepGrowthCell.swift
//  Tifli
//
//  Created by zubair on 08/11/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit

class SleepGrowthCell: UITableViewCell {
    
    @IBOutlet weak var lablTime: UILabel!
    @IBOutlet weak var lablTimeAgo: UILabel!
    @IBOutlet weak var imgUSer: UIImageView!
    @IBOutlet weak var lablUser: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lablCatName: UILabel!
    @IBOutlet weak var imgCat: UIImageView!
    @IBOutlet weak var lablDetail: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    
    var isTempretaure = false
    var objTemp = Temperature()
    var objSleep = Sleep()

    override func awakeFromNib() {
        super.awakeFromNib()
        
        collectionView.dataSource = self
        collectionView.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

// MARK: - CollectionView Delegate-
extension SleepGrowthCell: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isTempretaure {
            return 1
        } else {
            if objSleep.duration.count == 0 {
                return 2
            }
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let label = UILabel(frame: CGRect.zero)
        if isTempretaure {
            if indexPath.row == 0 {
                label.text = objTemp.temperature+" "+"c"
            }
        } else {
            if objSleep.duration.count == 0 {
                if indexPath.row == 0 {
                    label.text = "start time "+objSleep.startTime
                } else if indexPath.row == 1 {
                    label.text = "end time "+objSleep.endTime
                }
            } else {
                let tt = TimeInterval(objSleep.sleepTimeDifference)
                label.text = "duration "+String(tt.stringFromTimeInterval())
            }
        }
        label.sizeToFit()
        label.font = UIFont(name: "Open-Sans-Regular", size: 12)
        return CGSize(width: label.frame.width + 14, height: 22)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        collectionView.register(UINib(nibName: String(describing: AddBabyMedCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: AddBabyMedCell.self))
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: AddBabyMedCell.self), for: indexPath as IndexPath) as! AddBabyMedCell
        
        if isTempretaure {
            if indexPath.row == 0 {
                cell.lablTitle.text = objTemp.temperature+" "+"c"
            }
        } else {
            if objSleep.duration.count == 0 {
                if indexPath.row == 0 {
                    cell.lablTitle.text = "start time "+objSleep.startTime
                } else if indexPath.row == 1 {
                    cell.lablTitle.text = "end time "+objSleep.endTime
                }
            } else {
                if indexPath.row == 0 {
                    let tt = TimeInterval(objSleep.sleepTimeDifference)
                    cell.lablTitle.text = "duration "+String(tt.stringFromTimeInterval())
                }
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}
