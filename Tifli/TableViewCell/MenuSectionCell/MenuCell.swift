//
//  MenuCell.swift
//  Tifli
//
//  Created by zubair on 29/07/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {

    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lablTitle: UILabel!
    @IBOutlet weak var imgSep: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
