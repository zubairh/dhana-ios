//
//  MemoryAlbumCell.swift
//  Tifli
//
//  Created by zubair on 24/11/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit

protocol MemoryAlbumCellDelegate {
    func openDetailAlbum(objAlbum:Memory)
}

class MemoryAlbumCell: UITableViewCell {

    @IBOutlet weak var collectionMemory: UICollectionView!
    @IBOutlet weak var lablTitle: UILabel!
    
    var arrMemory = [Memory]()
    var delegate:MemoryAlbumCellDelegate? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        collectionMemory.delegate = self
        collectionMemory.dataSource = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

// MARK: - CollectionView Delegate-
extension MemoryAlbumCell: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrMemory.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return  CGSize(width: collectionView.bounds.width/3, height: 171)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        collectionView.register(UINib(nibName: String(describing: MemoryCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: MemoryCell.self))
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: MemoryCell.self), for: indexPath as IndexPath) as! MemoryCell
        
        let obj = self.arrMemory[indexPath.row]
        cell.lablTitle.text = obj.memoryTitle
        cell.lablDate.text = AppUtility.get_memory_date_time(dateString: obj.memoryDate)
        
        if obj.fileType == "image" {
            AppUtility.setImage(url: obj.memoryFile, image: cell.imgMemory)
            cell.btnPlay.isHidden = true
        } else {
            AppUtility.setImage(url: obj.memoryFileThumbnail, image: cell.imgMemory)
            cell.btnPlay.isHidden = false
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.openDetailAlbum(objAlbum:self.arrMemory[indexPath.row])
    }
}
