//
//  MemoryCell.swift
//  Tifli
//
//  Created by zubair on 24/11/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit

class MemoryCell: UICollectionViewCell {
    
    @IBOutlet weak var lablTitle: UILabel!
    @IBOutlet weak var lablDate: UILabel!
    @IBOutlet weak var imgMemory: UIImageView!
    @IBOutlet weak var btnPlay: UIImageView!
    @IBOutlet weak var imgSelectUnselect: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
