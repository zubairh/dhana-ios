//
//  MedicationViewCell.swift
//  Tifli
//
//  Created by zubair on 21/11/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit

class MedicationViewCell: UITableViewCell {

    @IBOutlet weak var lablDate: UILabel!
    @IBOutlet weak var lablDetail: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
