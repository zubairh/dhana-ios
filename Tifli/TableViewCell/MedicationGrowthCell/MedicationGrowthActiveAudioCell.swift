//
//  MedicationGrowthActiveAudioCell.swift
//  Tifli
//
//  Created by zubair on 08/02/2022.
//  Copyright © 2022 Flattechs. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class MedicationGrowthActiveAudioCell: UITableViewCell {

    @IBOutlet weak var lablTime: UILabel!
    @IBOutlet weak var lablTimeAgo: UILabel!
    @IBOutlet weak var imgUSer: UIImageView!
    @IBOutlet weak var lablUser: UILabel!
    @IBOutlet weak var lablDetail: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnDone: GradientButton!
    @IBOutlet weak var lablAudioTime: UILabel!
    @IBOutlet weak var progressAudio: UIProgressView!
    @IBOutlet weak var btnPlayAudio: UIButton!
    
    @IBOutlet weak var tfMedical: UITextField!
    @IBOutlet weak var tfAmount: UITextField!
    @IBOutlet weak var btnMl: UIButton!
    @IBOutlet weak var btnDrops: UIButton!
    
    var medicationAmountType = "ml"
    var arrMedicationList = [BabyType]()
    var objMed = Medication()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setData() {
        self.tfMedical.text = objMed.titles
        self.tfMedical.tag = Int(objMed.medicationId) ?? 0
        self.tfAmount.text = objMed.amount
        if objMed.amountType == "drops" {
            self.dropTapped(btnDrops)
        } else {
            self.mlTapped(btnMl)
        }
    }
    
    @IBAction func addMedicationTapped(_ sender: Any) {
        var arr = [String]()
        for obj in self.objMed.arrMedication {
            arr.append(obj.typeTitle)
        }
        ActionSheetStringPicker.show(withTitle: "Select Medical List".localized(), rows: arr, initialSelection: 0, doneBlock: {
            picker, indexes, values in
            
            self.tfMedical.text = values as? String
            self.tfMedical.tag = Int(self.objMed.arrMedication[indexes].ID)!

            return
        }, cancel: { ActionSheetStringPicker in return},origin: sender)
    }
    
    @IBAction func addAmountTapped(_ sender: Any) {
        var amount = 0
        if tfAmount.text?.count ?? 0 > 0 {
            amount = Int(tfAmount.text ?? "0") ?? 0
            amount = amount+1
            tfAmount.text = String(amount)
        } else {
            amount = amount+1
            tfAmount.text = String(amount)
        }
    }
    
    @IBAction func minusAmountTapped(_ sender: Any) {
        var amount = 0
        if tfAmount.text?.count ?? 0 > 0 {
            amount = Int(tfAmount.text ?? "0") ?? 0
            if amount >= 1 {
                amount = amount-1
            }
            tfAmount.text = String(amount)
        }
    }
    
    @IBAction func mlTapped(_ sender: UIButton) {
        medicationAmountType = "ml"
        btnMl.backgroundColor = #colorLiteral(red: 0.4431372549, green: 0.2156862745, blue: 0.7568627451, alpha: 1)
        btnMl.setTitleColor(.white, for: .normal)
        btnDrops.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.9294117647, blue: 0.9294117647, alpha: 1)
        btnDrops.setTitleColor(.darkGray, for: .normal)
    }
    
    @IBAction func dropTapped(_ sender: UIButton) {
        medicationAmountType = "drops"
        btnDrops.backgroundColor = #colorLiteral(red: 0.4431372549, green: 0.2156862745, blue: 0.7568627451, alpha: 1)
        btnDrops.setTitleColor(.white, for: .normal)
        btnMl.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.9294117647, blue: 0.9294117647, alpha: 1)
        btnMl.setTitleColor(.darkGray, for: .normal)
    }
    
}
