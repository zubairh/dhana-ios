//
//  ChatTableViewCell.swift
//  Sportify
//
//  Created by Waseem Devicebee on 10/23/18.
//  Copyright © 2018 Waseem Devicebee. All rights reserved.
//

import UIKit

class ChatTableViewCell: UITableViewCell {

    @IBOutlet weak var btnDetail: UIButton!
    @IBOutlet weak var lblMessage: UITextView!
    @IBOutlet weak var lblDateTime: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var viewChatBg: UIView!
    @IBOutlet weak var imageViewChat: UIImageView!
    @IBOutlet weak var containerTopConst: NSLayoutConstraint!
    @IBOutlet weak var containerBottomConst: NSLayoutConstraint!
    @IBOutlet weak var containerHeightConst: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
