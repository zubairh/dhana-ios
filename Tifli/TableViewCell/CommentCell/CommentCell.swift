//
//  CommentCell.swift
//  Tifli
//
//  Created by zubair on 25/07/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit

class CommentCell: UITableViewCell {

    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lablComment: UILabel!
    @IBOutlet weak var lablDate: UILabel!
    @IBOutlet weak var lablUserName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
