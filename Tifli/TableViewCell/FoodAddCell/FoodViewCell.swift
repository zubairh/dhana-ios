//
//  FoodViewCell.swift
//  Tifli
//
//  Created by zubair on 22/11/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit

class FoodViewCell: UITableViewCell {

    @IBOutlet weak var lablDate: UILabel!
    @IBOutlet weak var lablBottleCount: UILabel!
    @IBOutlet weak var lablBottleAmount: UILabel!
    @IBOutlet weak var lablBowl: UILabel!
    @IBOutlet weak var lablPump: UILabel!
    @IBOutlet weak var lablBreast: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
