//
//  FoodAddCell.swift
//  Tifli
//
//  Created by zubair on 26/10/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit

class FoodAddCell: UICollectionViewCell {

    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lablTitle: UILabel!
    @IBOutlet weak var viewBg: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
