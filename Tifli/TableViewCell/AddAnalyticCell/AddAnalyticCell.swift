//
//  AddAnalyticCell.swift
//  Tifli
//
//  Created by zubair on 19/07/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit

class AddAnalyticCell: UICollectionViewCell {

    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lablTitle: UILabel!
    @IBOutlet weak var viewBg: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
