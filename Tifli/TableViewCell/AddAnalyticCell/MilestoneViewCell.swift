//
//  MilestoneViewCell.swift
//  Tifli
//
//  Created by zubair on 19/11/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit

class MilestoneViewCell: UITableViewCell {

    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var lablTitle: UILabel!
    @IBOutlet weak var imgCheck: UIImageView!
    @IBOutlet weak var lablDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
