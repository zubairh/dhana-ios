//
//  PackageCell.swift
//  Tifli
//
//  Created by zubair on 27/12/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit

class PackageCell: UICollectionViewCell {

    @IBOutlet weak var viewOuter: UIView!
    @IBOutlet weak var viewInner: UIView!
    @IBOutlet weak var lablDuration: UILabel!
    @IBOutlet weak var lablPrice: UILabel!
    @IBOutlet weak var lablPerMonth: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
