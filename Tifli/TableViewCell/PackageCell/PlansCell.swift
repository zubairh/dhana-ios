//
//  PlansCell.swift
//  Tifli
//
//  Created by zubair on 27/12/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit

class PlansCell: UITableViewCell {
    
    @IBOutlet weak var viewOuter: UIView!
    @IBOutlet weak var viewInner: UIView!
    @IBOutlet weak var lablDuration: UILabel!
    @IBOutlet weak var lablPrice: UILabel!
    @IBOutlet weak var lablPerMonth: UILabel!
    @IBOutlet weak var btnPlan: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
