//
//  SceneDelegate.swift
//  Tifli
//
//  Created by zubair on 11/02/2021.
//  Copyright © 2021 Flattechs. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import FBSDKCoreKit
import GoogleSignIn
import Localize_Swift

class SceneDelegate: UIResponder, UIWindowSceneDelegate, GIDSignInDelegate {

    var isArabic = false
    var language = "1"
    var window: UIWindow?
    var tabsViewController = UITabBarController()
    var myLat :Double = 0
    var myLng : Double = 0
    var myLocation = ""
    var menuVC:SideMenuVC!
    var sideMenu = RESideMenu()
    var navController = UINavigationController()
    var backgroundUpdateTask: UIBackgroundTaskIdentifier = UIBackgroundTaskIdentifier(rawValue: 0)

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        
        let frame = UIScreen.main.bounds
        window = UIWindow(frame: frame)
        
        // show toolbar on keyboard
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        
        GIDSignIn.sharedInstance().clientID = "319209250457-ab2jno01k3b4brv2b32rv8ojo4681ggd.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
        
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = "Done".localized()
        
        UIFont.familyNames.forEach({ familyName in
            let fontNames = UIFont.fontNames(forFamilyName: familyName)
            print(familyName, fontNames)
        })
        
        sleep(3)
        
        if UserDefaults.standard.bool(forKey: LANGUAGE) == true {
            self.isArabic = true
            ISENGLISH = "ar"
            language = "2"
            Localize.setCurrentLanguage("ar")
            UILabel.appearance().substituteFontName = "Tajawal-Medium"
            if #available(iOS 9.0, *) {
                UIView.appearance().semanticContentAttribute = UISemanticContentAttribute.forceRightToLeft
            }
        } else {
            self.isArabic = false
            ISENGLISH = "en"
            language = "1"
            Localize.setCurrentLanguage("en")
//            UILabel.appearance().substituteFontName = "Tajawal-Regular"
//            UILabel.appearance().substituteFontNameBold = "Tajawal-Medium"
            
            if #available(iOS 9.0, *) {
                UIView.appearance().semanticContentAttribute = UISemanticContentAttribute.forceLeftToRight
            }
        }
        
        if AppUtility.isUserLoggedIn() && AppUtility.getUserLoggedIn().isPhoneVerified == "1" || AppUtility.getUserLoggedIn().isEmailVerified == "1" {
            self.loadTab()
        } else {
            self.loadLanguage()
        }
        
        guard let _ = (scene as? UIWindowScene) else { return }
    }
    
    override init() {
        super.init()
        UIFont.overrideInitialize()
    }
    
    //MARK:- Functions
    func loadLanguage() {
        
        let viewController = LanguageViewController(nibName:"LanguageViewController", bundle:nil)
        let navigationController = UINavigationController.init(rootViewController: viewController)
        navigationController.navigationBar.isHidden = true

        guard let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene,
            let sceneDelegate = windowScene.delegate as? SceneDelegate
        else {
            return
        }
        let window = UIWindow(windowScene: windowScene)
        window.rootViewController = navigationController
        self.window = window
        window.makeKeyAndVisible()
        
    }
    
    func loadWalkthrough() {
        
        let viewController = WalkthroughViewController(nibName:"WalkthroughViewController", bundle:nil)
        let navigationController = UINavigationController.init(rootViewController: viewController)
        navigationController.navigationBar.isHidden = true

        guard let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene,
            let sceneDelegate = windowScene.delegate as? SceneDelegate
        else {
            return
        }
        let window = UIWindow(windowScene: windowScene)
        window.rootViewController = navigationController
        self.window = window
        window.makeKeyAndVisible()
        
    }
    
    func loadTab() {
        let diaryVC = DiaryVC(nibName:String(describing: DiaryVC.self), bundle:nil)
        let memoryVC = MemoryV(nibName: String(describing: MemoryV.self), bundle:nil)
        let milestoneVC = MilestoneVC(nibName: String(describing: MilestoneVC.self), bundle:nil)
        let analysisVC = AnalysisVC(nibName: String(describing: AnalysisVC.self), bundle: nil)
        let guideVC = GuideVC(nibName: String(describing: GuideVC.self), bundle: nil)
        let diaryNavigationController = UINavigationController.init(rootViewController: diaryVC)
        let memoryNavigationController = UINavigationController.init(rootViewController: memoryVC)
        let milestoneNavigationController = UINavigationController.init(rootViewController: milestoneVC)
        let analysisNavigationController = UINavigationController.init(rootViewController: analysisVC)
        let guideNavigationController = UINavigationController.init(rootViewController: guideVC)
        
        diaryNavigationController.tabBarItem = UITabBarItem.init(title: "Diary", image: UIImage(named: "diary"), selectedImage: UIImage(named: "diarySelected"))
        //diaryNavigationController.tabBarItem.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        
        memoryNavigationController.tabBarItem = UITabBarItem.init(title: "Memories", image: UIImage(named: "memories"), selectedImage: UIImage(named: "memoriesSelected"))
        //memoryNavigationController.tabBarItem.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        
        milestoneNavigationController.tabBarItem = UITabBarItem.init(title: "Milestones", image: UIImage(named: "milestones-1"), selectedImage: UIImage(named: "milestonesSelected"))
        //milestoneNavigationController.tabBarItem.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        
        analysisNavigationController.tabBarItem = UITabBarItem.init(title: "Analysis", image: UIImage(named: "analysis"), selectedImage: UIImage(named: "analysisSelected"))
        //analysisNavigationController.tabBarItem.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        
        guideNavigationController.tabBarItem = UITabBarItem.init(title: "Guide", image: UIImage(named: "guide"), selectedImage: UIImage(named: "guideSelected"))
        //guideNavigationController.tabBarItem.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        
        let array: NSArray? = NSArray(objects:diaryNavigationController, memoryNavigationController, milestoneNavigationController,analysisNavigationController, guideNavigationController)
        
        self.tabsViewController.viewControllers = array as! [UIViewController]?
        diaryNavigationController.navigationBar.isHidden = true
        memoryNavigationController.navigationBar.isHidden = true
        milestoneNavigationController.navigationBar.isHidden = true
        analysisNavigationController.navigationBar.isHidden = true
        guideNavigationController.navigationBar.isHidden = true
        
        UITabBar.appearance().backgroundImage = UIImage(named: "")
        self.tabsViewController.selectedIndex = 0
        
        self.tabsViewController.delegate = self as? UITabBarControllerDelegate
        UITabBar.appearance().isTranslucent = false
        UITabBar.appearance().tintColor = COLORS.TABBAR_COLOR
        UITabBar.appearance().unselectedItemTintColor = UIColor.darkGray
//        guard let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene,
//          let sceneDelegate = windowScene.delegate as? SceneDelegate
//        else {
//          return
//        }
//        let window = UIWindow(windowScene: windowScene)
//        self.window = window
//        window.makeKeyAndVisible()
//        window.rootViewController = self.tabsViewController
        
        self.loadSideMenu()
    }
    
    func loadSideMenu() {
        self.menuVC = SideMenuVC(nibName:"SideMenuVC", bundle:nil)
        let menuNavigationController = UINavigationController.init(rootViewController: self.menuVC)
        menuNavigationController.isNavigationBarHidden = true
        sideMenu = RESideMenu.init(contentViewController: self.tabsViewController, leftMenuViewController: nil, rightMenuViewController: menuNavigationController)
        sideMenu.parallaxEnabled = false
        sideMenu.menuPreferredStatusBarStyle = UIStatusBarStyle(rawValue: 1)!
        sideMenu.contentViewShadowColor = UIColor.black
        sideMenu.contentViewShadowOffset = CGSize(width: 0, height: 0)
        sideMenu.contentViewShadowOpacity = 0.25
        sideMenu.contentViewShadowRadius = 8
        sideMenu.contentViewShadowEnabled = true
        sideMenu.panGestureEnabled = false
    
        guard let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene,
          let sceneDelegate = windowScene.delegate as? SceneDelegate
        else {
          return
        }
        let window = UIWindow(windowScene: windowScene)
        self.window = window
        window.makeKeyAndVisible()
        window.rootViewController = self.sideMenu
    }
    
    func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
        guard let url = URLContexts.first?.url else {
            return
        }

        ApplicationDelegate.shared.application(
            UIApplication.shared,
            open: url,
            sourceApplication: nil,
            annotation: [UIApplication.OpenURLOptionsKey.annotation]
        )
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        print(url)
        
        return true
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
      if let error = error {
        if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
          print("The user has not signed in before or they have since signed out.")
        } else {
          print("\(error.localizedDescription)")
        }
        return
      }
      // Perform any operations on signed in user here.
//      let userId = user.userID                  // For client-side use only!
//      let idToken = user.authentication.idToken // Safe to send to the server
//      let fullName = user.profile.name
//      let givenName = user.profile.givenName
//      let familyName = user.profile.familyName
//      let email = user.profile.email
//      let picture = user.profile.imageURL(withDimension: 300*300)
        
        NotificationCenter.default.post(name: Notification.Name(NOTIFICATION_GETGOOGLE_INFO), object: user, userInfo: nil)

      // ...
    }

    func sceneDidDisconnect(_ scene: UIScene) {
    
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        
    }

    func sceneWillResignActive(_ scene: UIScene) {
        
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        
    }

    func sceneDidEnterBackground(_ scene: UIScene) {

    }
}

